package cz.mendelu.pur.xkosteln;

import java.util.ArrayList;

import cz.mendelu.pur.xkosteln.DatabaseFiller;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class PURAndroidActivity extends ListActivity implements OnScrollListener {
	private SQLiteDatabase db = null;
	private Cursor quoteCursor = null;
	private String whereConstraintValue= "";
	private String whereConstraint = "";
	private int currPage;
	private Button nextBtn;
	private Button prevBtn;

	
	

	protected void setWhereConstraintValue(String whereConstraintValue) {
		this.whereConstraintValue = whereConstraintValue;
	}

	protected void setWhereConstraint(String whereConstraint) {
		this.whereConstraint = whereConstraint;
	}

	/* async variant
	private AddAsyncCitation asyncCit;
	private ArrayAdapter<String> adapter;
	*/
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currPage = 0;
        this.fillDB();
        this.selectDataFromDB();
        setContentView(R.layout.main);
        /*
          Async variant
         
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        this.setListAdapter(adapter);
		asyncCit = new AddAsyncCitation(quoteCursor, adapter);
		*/
        prevBtn =(Button) findViewById(R.id.prevBtn);
        nextBtn = (Button) findViewById(R.id.nextBtn);
        
		this.registerForContextMenu(getListView());
		this.registerClickItem();
		this.registerScroll();
		this.bindClickButtons();
		/* async variant - without pagination
		asyncCit.execute();
		*/
    }

	@Override
	protected void onDestroy() {
		super.onDestroy();
		this.db.close();
		this.quoteCursor.close();

	}
	private void fillDB() {
		db = (new DatabaseFiller(this)).getWritableDatabase();
	}
    
    private void selectDataFromDB(){
    	//make sure all records are visible
    	int from = currPage -2; 
    	String query = "SELECT _ID, author, topic "
				+ "FROM citations ";
    	if (!whereConstraintValue.isEmpty()) {
    		query+= " WHERE "+whereConstraint + " ='" + whereConstraintValue + "' "; 
    	}
    	query += " ORDER BY author ";
    	query += " LIMIT "+ from +","+ 50;
    	quoteCursor = db.rawQuery(query, null);
        
    	if (quoteCursor.getCount() != 0){
    		ListAdapter adapter = new SimpleCursorAdapter(this, R.layout.row, quoteCursor, 
    			new String [] {"author", "topic"}, new int [] { R.id.author, R.id.topic});
    		
    		
    		this.setListAdapter(adapter);
    		this.pageLoaded();
    	}
    	else {
    		//this.noMoreRecords();
    	}
    	
		
		
    }
    
    //private selectCathegories() {
    //	Cursor cathegories = db.query("citations", columns, selection, selectionArgs, groupBy, having, orderBy)
    //}
    private void registerScroll(){
    	this.getListView().setOnScrollListener(this);
    }
    
    private void registerClickItem(){
    	this.getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View arg1, int position,
					long itemID) {
				Bundle valuesBundle = selectCitation(itemID);
				startCitationActivity(valuesBundle);
					
				
			}

			
		});    		
    	
    }
    
    public Bundle selectCitation(long itemID) {
    	Cursor citationCursor = this.db.query("citations", new String[] {"author", "topic", "citation"}, 
    			"_id" + "=" + new Long(itemID).toString(), null, null, null, null);
    	Bundle citationBundle = new Bundle();
    	citationCursor.moveToFirst();
    	
    	citationBundle.putString("author", citationCursor.getString(citationCursor.getColumnIndex("author")));
    	citationBundle.putString("topic", citationCursor.getString(citationCursor.getColumnIndex("topic")));
    	citationBundle.putString("citation", citationCursor.getString(citationCursor.getColumnIndex("citation")));
    	citationCursor.close();
    	return citationBundle;
    }
    
    public void startCitationActivity(Bundle particularCitationBundle) {
    	
    	Intent citationIntent = new Intent(this, CitationActivity.class);
		citationIntent.putExtras(particularCitationBundle);
		this.startActivity(citationIntent);
		
	}
    
    public String[] selectCathegories(){
    	Cursor catCursor = db.query("topics", new String[] {"name"}, null, null, null, null, "name");
    	ArrayList<String> cathegories = new ArrayList<String>();
    	int counter = 0;
    	while (catCursor.moveToNext()){
    		cathegories.add(catCursor.getString(catCursor.getColumnIndex("name")));		
    	}
    	return cathegories.toArray(new String[cathegories.size()]);
    	
    }

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
			if (++firstVisibleItem + visibleItemCount > totalItemCount) {
				currPage = currPage + 50;
				selectDataFromDB();
			}
			
		
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.cathegories:
				Log.d("PUR", "cathegories pushed");
				String [] cathegories = selectCathegories();
				Bundle cathegoriesBundle = new Bundle();
		    	cathegoriesBundle.putStringArray("cathegories", cathegories);
		    	cathegoriesBundle.putString("whereConstraintValue", whereConstraintValue);
		    	
		    	Intent catIntent = new Intent(this, CathegoriesActitvity.class);
		    	catIntent.putExtras(cathegoriesBundle);
		    	int foo=0;
		    	startActivityForResult(catIntent, foo);
				
		}
		return true;
		
	}
	
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && data.hasExtra("whereConstraintValue") && data.hasExtra("whereConstraint")){
			whereConstraint = data.getStringExtra("whereConstraint");
			whereConstraintValue = data.getStringExtra("whereConstraintValue");
			currPage = 0;
			if (whereConstraintValue.equals("All")) {
				whereConstraint = "";
				whereConstraintValue = "";
				Log.d("PUR", "Loading all samples");
			}
			this.selectDataFromDB();
		}
	}

	public void pageLoaded () {
		Toast.makeText(this, "Loaded !", Toast.LENGTH_LONG).show();		
	}
	
	public void noMoreRecords () {
		Toast.makeText(this, "No more pages !", Toast.LENGTH_LONG).show();		
	}
	
	public void bindClickButtons() {
		prevBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				currPage = currPage - 50;
				selectDataFromDB();				
			}
		});
		
		nextBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				currPage = currPage + 50;
				selectDataFromDB();
			}
		});
		
	}
}