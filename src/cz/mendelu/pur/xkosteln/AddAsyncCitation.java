package cz.mendelu.pur.xkosteln;

import android.database.Cursor;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.SimpleCursorAdapter;

public class AddAsyncCitation extends AsyncTask<Void, String, Void>{
	private final Cursor cursor;
	private ArrayAdapter<String> adapter;
	
	
	public AddAsyncCitation(Cursor cursor, ArrayAdapter<String> adapter) {
		this.cursor = cursor;
		this.adapter = adapter;
	}
	@Override
	protected Void doInBackground(Void... arg0) {
		while (cursor.moveToNext()) {
			String [] newValues = new String[2];
			newValues[0] = cursor.getString(cursor.getColumnIndex("author"));
			newValues[1] = cursor.getString(cursor.getColumnIndex("topic"));
			publishProgress(newValues);
		}
		return null;
	}
	@Override
	protected void onProgressUpdate(String... values) {
		String outputString = new String();
		outputString = values[0] + " On " + values[1];
		adapter.add(outputString);		
	}
	
}
