
	 
package cz.mendelu.pur.xkosteln;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.SensorManager;

public class DatabaseFiller extends SQLiteOpenHelper {
	private static final String DATABASE_NAME="PURdb";
	public static final String AUTHOR="author";
	public static final String TOPIC="topic";
	public static final String CITATION = "citation";
	public static final String NAME = "name";
	
	public DatabaseFiller(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// create two necessary tables
		db.execSQL("CREATE TABLE citations (_id INTEGER PRIMARY KEY AUTOINCREMENT, author TEXT, topic TEXT, citation TEXT);");
		
		
		//fill values in citations
		ContentValues cv=new ContentValues();
		cv.put(AUTHOR, "Groucho Marx");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "A child of five would understand this. Send someone to fetch a child of five.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "A friend doesn't go on a diet because you are fat.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "A government that robs Peter to pay Paul can always depend on the support of Paul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bob Hope");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "A James Cagney love scene is one where he lets the other guy live.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kevin Nealon");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "A lot of baby boomers are baby bongers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Yogi Berra");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "A nickel ain't worth a dime anymore.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Claude Pepper");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "A stockbroker urged me to buy a stock that would triple its value every year. I told him, \"At my age, I don't even buy green bananas.\"");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lana Turner");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "A successful man is one who makes more money than his wife can spend. A successful woman is one who can find such a man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jerry Seinfeld");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "A two-year-old is kind of like having a blender, but you don't have a top for it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Cosby");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "A word to the wise ain't necessary - it's the stupid ones that need the advice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert Hoover");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "All men are equal before fish.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Casey Stengel");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "All right everyone, line up alphabetically according to your height.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Cosby");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Always end the name of your child with a vowel, so that when you yell the name will carry.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hedy Lamarr");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Any girl can be glamorous. All you have to do is stand still and look stupid.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Groucho Marx");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Anyone who says he can see through women is missing a lot.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Buddy Hackett");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "As a child my family's menu consisted of two choices: take it or leave it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tracey Ullman");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "As I get older, I just prefer to knit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. B. White");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Be obscure clearly.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "P. J. O'Rourke");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Because of their size, parents may be difficult to discipline properly.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Carrey");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Behind every great man is a woman rolling her eyes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tillie Olsen");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "And when is there time to remember, to sift, to weigh, to estimate, to total?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jonathan Carroll");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Both young children and old people have a lot of time on their hands. That's probably why they get along so well.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aeschylus");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "But time growing old teaches all things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Malcolm Forbes");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "By the time we've made it, we've had it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Faulkner");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Clocks slay time... time is dead as long as it is being clicked off by little wheels; only when the clock stops does time come to life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Day, n. A period of twenty-four hours, mostly misspent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Everything happens to everybody sooner or later if there is time enough.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mariel Hemingway");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Finding some quiet time in your life, I think, is hugely important.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doug Larson");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "For disappearing acts, it's hard to beat what happens to the eight hours supposedly left after eight of sleep and eight of work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Golda Meir");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "I must govern the clock, not be governed by it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James L. Brooks");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "I took some time out for life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. G. Wells");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "I want to go ahead of Father Time with a scythe of my own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Wooden");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "If you don't have time to do it right, when will you have time to do it over?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francois Rabelais");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "It is my feeling that Time ripens all things; with Time all things are revealed; Time is the father of truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dario Fo");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Know how to live the time that is given you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roger Babson");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Let him who would enjoy a good future waste none of his present.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Schiller");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Lose not yourself in a far off time, seize the moment that is thine.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Lost time is never found again.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dion Boucicault");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Men talk of killing time, while time quietly kills them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Caleb Colton");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Much may be done in those little shreds and patches of time which every day produces, and which most men throw away.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stewart Alsop");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "A dying man needs to die, as a sleepy man needs to sleep, and there comes a time when it is wrong, as well as useless, to resist.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gustave Flaubert");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "A friend who dies, it's something of you who dies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miguel de Unamuno");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "A man does not die of love or his liver or even of old age; he dies of being a man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther King, Jr.");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "A man who won't die for something is not fit to live.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Philip Johnson");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "All architects want to live beyond their deaths.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maurice Maeterlinck");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "All our knowledge merely helps us to die a more painful death than animals that know nothing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Errol Flynn");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Any man who has $10,000 left when he dies is a failure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elie Wiesel");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Because of indifference, one dies before one actually dies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bryant H. McGill");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Birth and death; we all move between these two unknowns.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aeschylus");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Call no man happy till he is dead.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abu Bakr");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Cursed is the man who dies, but the evil done by him survives.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Hall");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Death borders upon our birth, and our cradle stands in the grave.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. Somerset Maugham");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Death is a very dull, dreary affair, and my advice to you is to have nothing whatsoever to do with it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carter Burwell");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Death is always around the corner, but often our society gives it inordinate help.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Keller");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Death is no more than passing from one room into another. But there's a difference for me, you know. Because in that other room I shall be able to see.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Thomas Ellis");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Death is the last enemy: once we've got past that I think everything will be alright.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barry Cornwall");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Death is the tyrant of the imagination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Death most resembles a prophet who is without honor in his own land or a poet who is a stranger among his people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Katharine Hepburn");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Death will be a great relief. No more interviews.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Sexton");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Death's in the good-bye.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don Herold");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "A humorist is a person who feels bad, but who feels good about it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Winston Churchill");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "A joke is a very serious thing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Chevy Chase");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "A laugh is a surprise. And all humor is physical. I was always athletic, so that came naturally to me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "A person without a sense of humor is like a wagon without springs. It's jolted by every pebble on the road.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doug Larson");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "A pun is the lowest form of humor, unless you thought of it yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mignon McLaughlin");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "A sense of humor is a major defense against minor troubles.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clifton Paul Fadiman");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "A sense of humor is the ability to understand a joke - and that the joke is oneself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hugh Sidey");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "A sense of humor... is needed armor. Joy in one's heart and some laughter on one's lips is a sign that the person down deep has a pretty good grasp of life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jessamyn West");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "A taste for irony has kept more hearts from breaking than a sense of humor, for it takes irony to appreciate the joke which is on oneself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Arthur Ward");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "A well-developed sense of humor is the pole that adds balance to your steps as you walk the tightrope of life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Groucho Marx");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Alimony is like buying hay for a dead horse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fred Allen");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "All I know about humor is that I don't know anything about it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. B. White");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Analyzing humor is like dissecting a frog. Few people are interested and the frog dies of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sid Caesar");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Comedy has to be based on truth. You take the truth and you put a little curlicue at the end.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Ustinov");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Comedy is simply a funny way of being serious.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. B. Priestley");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Comedy, we may say, is society protecting itself - with a smile.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William James");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Common sense and a sense of humor are the same thing, moving at different speeds. A sense of humor is just common sense, dancing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Everything human is pathetic. The secret source of humor itself is not joy but sorrow. There is no humor in heaven.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Everywhere is within walking distance if you have the time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack Benny");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Gags die, humor doesn't.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ayn Rand");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "A creative man is motivated by the desire to achieve, not by the desire to beat others.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William James");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Act as if what you do makes a difference. It does.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ella Wheeler Wilcox");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Always continue the climb. It is possible for you to do whatever you choose, if you first get to know who you are and are willing to work with a power that is greater than ourselves to do it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sophocles");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Always desire to learn something useful.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Og Mandino");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Always do your best. What you plant now, you will harvest later.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Saint Teresa of Avila");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Be gentle to all and stern with yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wayne Dyer");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Be miserable. Or motivate yourself. Whatever has to be done, it's always your choice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Donne");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Be thine own palace, or the world's thy jail.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William James");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Begin to be now what you will be hereafter.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Vincent Peale");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Believe in yourself! Have faith in your abilities! Without a humble but reasonable confidence in your own powers you cannot be successful or happy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Swami Sivananda");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Crave for a thing, you will get it. Renounce the craving, the object will follow you by itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Determine never to be idle. No person will have occasion to complain of the want of time who never loses any. It is wonderful how much may be done if we are always doing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baruch Spinoza");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Do not weep; do not wax indignant. Understand.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Do you want to know who you are? Don't ask. Act! Action will delineate and define you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Rohn");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Either you run the day or the day runs you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Kiam");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Even if you fall on your face, you're still moving forward.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred A. Montapert");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Expect problems and eat them for breakfast.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baruch Spinoza");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Fear cannot be without hope nor hope without fear.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sasha Cohen");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Follow your dreams, work hard, practice and persevere. Make sure you eat a variety of foods, get plenty of exercise and maintain a healthy lifestyle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eliza Dushku");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Go big or go home. Because it's true. What do you have to lose?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Josh Billings");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "A dog is the only thing on earth that loves you more than you love yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Benchley");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "A dog teaches a boy fidelity, perseverance, and to turn around three times before lying down.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mason Cooley");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "A happy arrangement: many people prefer cats to other people, and many cats prefer people to other cats.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Ralph Augustine");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "A hungry dog hunts best. A hungrier dog hunts even better.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Agnes Repplier");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "A kitten is chiefly remarkable for rushing about like mad at nothing whatever, and generally stopping before it gets there.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Southey");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "A kitten is in the animal world what a rosebud is in the garden.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Eliot");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Animals are such agreeable friends - they ask no questions; they pass no criticisms.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cleveland Amory");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "As anyone who has ever been around a cat for any length of time well knows, cats have enormous patience with the limitations of the human kind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Herford");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Cat: a pygmy lion who loves mice, hates dogs, and patronizes human beings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Herriot");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Cats are connoisseurs of comfort.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mason Cooley");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Cats are inquisitive, but hate to admit it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Garrison Keillor");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Cats are intended to teach us that not everything in nature has a purpose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Wood Krutch");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Cats are rather delicate creatures and they are subject to a good many different ailments, but I have never heard of one who suffered from insomnia.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roger Caras");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Cats don't like change without their consent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rod McKuen");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Cats have it all - admiration, an endless sleep, and company only when they want it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. L. George");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Cats know how to obtain food without labor, shelter without confinement, and love without penalties.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Wood Krutch");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Cats seem to go on the principle that it never does any harm to ask for what you want.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Benchley");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Dachshunds are ideal dogs for small children, as they are already stretched and pulled to such a length that the child cannot do much harm one way or the other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martha Scott");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Do not make the mistake of treating your dogs like humans or they will treat you like dogs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Dean Anderson");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Dogs are my favorite people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lewis Hallam");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "A journey by Sea and Land, Five Hundred Miles, is not undertaken without money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Edward Moore");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "A man travels the world in search of what he needs and returns home to find it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Hazlitt");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "A wise traveler never despises his own country.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Boliska");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Airline travel is hours of boredom interrupted by moments of stark terror.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Gore");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Airplane travel is nature's way of making you look like your passport photo.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miriam Beard");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Certainly, travel is more than the seeing of sights; it is a change that goes on, deep and permanent, in the ideas of living.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry B. Adams");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Everyone carries his own inch rule of taste, and amuses himself by applying it, triumphantly, wherever he travels.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rudyard Kipling");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "He travels the fastest who travels alone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Palin");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "I am not a great cook, I am not a great artist, but I love art, and I love food, so I am the perfect traveller.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "I dislike feeling at home when I am abroad.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Guy Clark");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "I get pretty much all the exercise I need walking down airport concourses carrying bags.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Cameron");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "I love short trips to New York; to me it is the finest three-day town on earth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wallis Simpson");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "I never make a trip to the United States without visiting a supermarket. To me they are more fascinating than any fashion salon.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Attenborough");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "I think a major element of jetlag is psychological. Nobody ever tells me what time it is at home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Louis Stevenson");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "I travel not to go anywhere, but to go. I travel for travel's sake. The great affair is to move.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Laurel Clark");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "I've always enjoyed traveling and having experience with different cultures and different people. But it's also a wonderful thing to be able to benefit and enable research, not only in our country but around the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ray Floyd");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "If you travel first class, you think first class and you are more likely to play first class.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Benchley");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "In America there are two classes of travel - first class, and with children.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ivanka Trump");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "In both business and personal life, I've always found that travel inspires me more than anything else I do. Evidence of the languages, cultures, scenery, food, and design sensibilities that I discover all over the world can be found in every piece of my jewelry.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Buddha");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "It is better to travel well than to arrive.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "A peace is of the nature of a conquest; for then both parties nobly are subdued, and neither party loser.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ronald Reagan");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "A people free to choose will always choose peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "An eye for an eye only ends up making the whole world blind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Swami Sivananda");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Complete peace equally reigns between two mental waves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Even peace may be purchased at too high a price.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sydney Madwed");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Every goal, every action, every thought, every feeling one experiences, whether it be consciously or unconsciously known, is an attempt to increase one's level of peace of mind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Irving Babbitt");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "For behind all imperialism is ultimately the imperialistic individual, just as behind all peace is ultimately the peaceful individual.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "He that would live in peace and at ease must not speak all he knows or all he sees.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Muhammad Ali");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "I believe in the religion of Islam. I believe in Allah and peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isabel Allende");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "I can promise you that women working together - linked, informed and educated - can bring peace and prosperity to this forsaken planet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Georges Clemenceau");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "I don't know whether war is an interlude during peace, or peace an interlude during war.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nelson Mandela");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "I dream of an Africa which is in peace with itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Garrett Morris");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "I happen to dig being able to use whatever mystique I have to further the idea of peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Godfrey Reggio");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "I think it's naive to pray for world peace if we're not going to change the form in which we live.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dwight D. Eisenhower");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "I think that people want peace so much that one of these days government had better get out of their way and let them have it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joan Jett");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "I'm concentrating on staying healthy, having peace, being happy, remembering what is important, taking in nature and animals, spending time reading, trying to understand the universe, where science and the spiritual meet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "If they want peace, nations should avoid the pin-pricks that precede cannon shots.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mother Teresa");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "If we have no peace, it is because we have forgotten that we belong to each other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nelson Mandela");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "If you want to make peace with your enemy, you have to work with your enemy. Then he becomes your partner.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Lennon");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Imagine all the people living life in peace. You may say I'm a dreamer, but I'm not the only one. I hope someday you'll join us, and the world will be as one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A poem begins as a lump in the throat, a sense of wrong, a homesickness, a lovesickness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas Dunn");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A poem can have an impact, but you can't expect an audience to understand all the nuances.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Harrison");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A poem conveys not a message so much as the provenance of a message, an advent of sense.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Valery");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A poem is never finished, only abandoned.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. M. Forster");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A poem is true if it hangs together. Information points to something else. A poem points to nothing but itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Stevenson");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A poem might be defined as thinking about feelings - about human feelings and frailties.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A poet can survive everything but a misprint.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. H. Auden");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A poet is, before anything else, a person who is passionately in love with language.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wallace Stevens");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A poet looks at the world the way a man looks at a woman.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Yevgeny Yevtushenko");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A poet's autobiography is his poetry. Anything else is just a footnote.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Salman Rushdie");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A poet's work is to name the unnameable, to point at frauds, to take sides, start arguments, shape the world, and stop it going to sleep.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Cocteau");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "A true poet does not bother to be poetical. Nor does a nursery gardener scent his roses.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "All bad poetry springs from genuine feeling.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Baudelaire");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Always be a poet, even in prose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Baudelaire");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Any healthy man can go without food for two days - but not without poetry.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Muriel Rukeyser");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Breathe-in experience, breathe-out poetry.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Cocteau");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Children and lunatics cut the Gordian knot which the poet spends his life patiently trying to untie.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred de Musset");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Each memorable verse of a true poet has two or three times the written content.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Stevenson");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Each word bears its weight, so you have to read my poems quite slowly.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "A. E. Housman");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Even when poetry has a meaning, as it usually has, it may be inadvisable to draw it out... Perfect understanding will sometimes almost extinguish pleasure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Lothrop Motley");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "A good lawyer is a bad Christian.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Dunbar");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "A lawyer who does not know men is handicapped.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Herbert");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "A lean compromise is better than a fat lawsuit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sol Wachtler");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "A married woman has the same right to control her own body as does an unmarried woman.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "A successful lawsuit is the one worn by a policeman.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. G. Wells");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Advertising is legalized lying.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Conrad");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "All ambitions are lawful except those which climb upward on the miseries or credulities of mankind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ian Hislop");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "All the libel lawyers will tell you there's no libel any more, that everyone's given up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jay Alan Sekulow");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "As a private lawyer, I could bill $750 an hour, but I don't.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Williams");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "As a rule lawyers tend to want to do whatever they can to win.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sam Waterston");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "As for lawyers, it's more fun to play one than to be one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean de la Bruyere");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Avoid lawsuits beyond all things; they pervert your conscience, impair your health, and dissipate your property.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dennis Hastert");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Cagey trial lawyers have figured out there's a pretty good likelihood their case - no matter what its merit - will literally get its day in court because of favorable judges.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Calvin Coolidge");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Collecting more taxes than is absolutely necessary is legalized robbery.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Louis Stevenson");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Compromise is the best and cheapest lawyer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Herbert");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Deceive not thy physician, confessor, nor lawyer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack Kingston");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Frivolous lawsuits are booming in this county. The U.S. has more costs of litigation per person than any other industrialized nation in the world, and it is crippling our economy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Harington");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "From your confessor, lawyer and physician, hide not your case on no condition.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Lamb");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "He is no lawyer who cannot take two sides.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I busted a mirror and got seven years bad luck, but my lawyer thinks he can get me five.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wayne Gretzky");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "A good hockey player plays where the puck is. A great hockey player plays where the puck is going to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jesse Owens");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "A lifetime of training for just ten seconds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Academe, n.: An ancient school where morality and philosophy were taught. Academy, n.: A modern school where football is taught.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Arthur Ward");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Adversity causes some men to break; others to break records.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gordie Howe");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "All hockey players are bilingual. They know English and profanity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pierre de Coubertin");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "All sports for all people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doug Collins");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Any time Detroit scores more than 100 points and holds the other team below 100 points they almost always win.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phil Jackson");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Approach the game with no preset agendas and you'll probably come away surprised at your overall efforts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Jordan");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "As athletes, we're used to reacting quickly. Here, it's 'come, stop, come, stop.' There's a lot of downtime. That's the toughest part of the day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Leonard");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Baseball happens to be a game of cumulative tension but football, basketball and hockey are played with hand grenades and machine guns.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Baseball has the great advantage over cricket of being sooner ended.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Patrick Murray");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Baseball is a game where a curve is an optical illusion, a screwball can be a pitch or a person, stealing is legal and you can spit anywhere you like except in the umpire's eye or on the ball.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Ueberroth");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Baseball is a public trust. Players turn over, owners turn over and certain commissioners turn over. But baseball goes on.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Veeck");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Baseball is almost the only orderly thing in a very unorderly world. If you get three strikes, even the best lawyer in the world can't get you off.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joe Garagiola");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Baseball is drama with an endless run and an ever-changing cast.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ted Williams");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Baseball is the only field of endeavor where a man can succeed three times out of ten and be considered a good performer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nolan Ryan");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Baseball life is a tough life on the family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Bouton");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Baseball players are smarter than football players. How often do you see a baseball team penalized for too many men on the field?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Red Auerbach");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Basketball is like war in that offensive weapons are developed first, and it always takes a while for the defense to catch up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dan Quayle");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Bobby Knight told me this: 'There is nothing that a good defense cannot beat a better offense.' In other words a good offense wins.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Ratzinger");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "A just laicism allows religious freedom. The state does not impose religion but rather gives space to religions with a responsibility toward civil society, and therefore it allows these religions to be factors in building up society.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "C. S. Lewis");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Aim at heaven and you will get earth thrown in. Aim at earth and you get neither.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dennis Miller");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Born again?! No, I'm not. Excuse me for getting it right the first time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isaac Asimov");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Creationists make it sound as though a 'theory' is something you dreamt up after being drunk all night.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isaac Bashevis Singer");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Doubt is part of all religion. All the religious thinkers were doubters.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Annie Dillard");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Eskimo: \"If I did not know about God and sin, would I go to hell?\" Priest: \"No, not if you did not know.\" Eskimo: \"Then why did you tell me?\"");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Valery");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "God made everything out of nothing, but the nothingness shows through.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas Adams");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "He hoped and prayed that there wasn't an afterlife. Then he realized there was a contradiction involved here and merely hoped that there wasn't an afterlife.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Heaven is under our feet as well as over our heads.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Blaise Pascal");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Human beings must be known to be loved; but Divine beings must be loved to be known.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "C. S. Lewis");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "I believe in Christianity as I believe that the sun has risen: not only because I see it, but because by it I see everything else.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Galileo Galilei");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "I do not feel obliged to believe that the same God who has endowed us with sense, reason, and intellect has intended us to forgo their use.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur C. Clarke");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "I don't believe in God but I'm very interested in her.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "I like your Christ, I do not like your Christians. Your Christians are so unlike your Christ.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "I love you when you bow in your mosque, kneel in your temple, pray in your church. For you and I are sons of one religion, and it is the spirit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "I never knew how to worship until I knew how to love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frederick Douglass");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "I prayed for twenty years but received no answer until I prayed with my legs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aaron Neville");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "I was raised Catholic, but my father's people were Methodist, so we went to both churches.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Camus");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "I would rather live my life as if there is a God and die to find out there isn't, than live my life as if there isn't and die to find out there is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Green Ingersoll");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "If a man would follow, today, the teachings of the Old Testament, he would be a criminal. If he would follow strictly the teachings of the New, he would be insane.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Stoppard");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "A healthy attitude is contagious but don't wait to catch it from others. Be a carrier.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Urich");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "A healthy outside starts from the inside.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Deacon");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "Arguments are healthy. They clear the air.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Addison");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "Cheerfulness is the best promoter of health and is as friendly to the mind as to the body.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Muir");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "Everybody needs beauty as well as bread, places to play in and pray in, where nature may heal and give strength to body and soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hippocrates");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "Healing is a matter of time, but it is sometimes also a matter of opportunity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Wilson Schaef");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "Healthy people live with their world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gloria Swanson");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I became a fanatic about healthy food in 1944.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Darren L. Johnson");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I believe it is important for people to create a healthy mental environment in which to accomplish daily tasks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victoria Principal");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I believe that how you feel is very important to how you look - that healthy equals beautiful.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pedro Martinez");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I believe that if you're healthy, you're capable of doing everything. There's no one else who can give you health but God, and by being healthy I believe that God is listening to me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Casper Van Dien");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I eat really healthy, and if I'm tired, I take a nap.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Piazza");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I feel pretty good. My body actually looks like an old banana, but it's fine.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fran Drescher");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I had my moments when I got very frightened that I would not recover.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lorna Luft");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I have a healthy body, free of the chemicals that once controlled it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Spike Milligan");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I have the body of an eighteen year old. I keep it in the fridge.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Denzel Washington");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I made a commitment to completely cut out drinking and anything that might hamper me from getting my mind and body together. And the floodgates of goodness have opened upon me - spiritually and financially.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Ditka");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I really believe the only way to stay healthy is to eat properly, get your rest and exercise. If you don't exercise and do the other two, I still don't think it's going to help you that much.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lexa Doig");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I sit on my duff, smoke cigarettes and watch TV. I'm not exactly a poster girl for healthy living.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Melissa Etheridge");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I stand before you a totally healthy person.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "A man's true state of power and riches is to be in himself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pierre Corneille");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "A true king is neither husband nor father; he considers his throne and nothing else.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "All the forces in the world are not so powerful as an idea whose time has come.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baruch Spinoza");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Ambition is the immoderate desire for power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Schiff");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "America has a critical role to play as the most powerful member of the world community.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phaedrus");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "An alliance with a powerful person is never safe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abigail Adams");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Arbitrary power is like most other things which are very hard, very liable to be broken.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Thatcher");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Being powerful is like being a lady. If you have to tell people you are, you aren't.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Watts");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "But to me nothing - the negative, the empty - is exceedingly powerful.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barney Frank");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Capitalism works better from every perspective when the economic decision makers are forced to share power with those who will be affected by those decisions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Booker T. Washington");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Character is power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Circumstances are beyond human control, but our conduct is in our own power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Plomer");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Creativity is the power to connect the seemingly unconnected.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phillips Brooks");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Do not pray for tasks equal to your powers. Pray for powers equal to your tasks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Deborah Tannen");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Each underestimates her own power and overestimates the other's.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Moulton Marston");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Every crisis offers you extra desired power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ayn Rand");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Every man builds his world in his own image. He has the power to choose, but no power to escape the necessity of choice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aeschylus");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Excessive fear is always powerless.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Wordsworth");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Getting and spending, we lay waste our powers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pat Buckley");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "I believe in the power of weakness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ingrid Bergman");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "A kiss is a lovely trick designed by nature to stop speech when words become superfluous.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cyrano de Bergerac");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "A kiss is a rosy dot over the 'i' of loving.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rupert Brooke");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "A kiss makes the heart young again and wipes out the years.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Carlyle");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "A loving heart is the beginning of all knowledge.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Jean Nathan");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "A man reserves his true and deepest love not for the species of woman in whose company he finds himself electrified and enkindled, but for that one in whose company he may feel tenderly drowsy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Nietzsche");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "A pair of powerful spectacles has sometimes sufficed to cure a person in love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stendhal");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "A very small degree of hope is sufficient to cause the birth of love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Honore de Balzac");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "A woman knows the face of the man she loves as a sailor knows the open sea.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francois de La Rochefoucauld");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Absence diminishes mediocre passions and increases great ones, as the wind extinguishes candles and fans fires.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "C. S. Lewis");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Affection is responsible for nine-tenths of whatever solid and durable happiness there is in our lives.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Julie Andrews");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "All love shifts and changes. I don't know if you can be wholeheartedly in love all the time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "All mankind love a lover.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andre Breton");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "All my life, my heart has yearned for a thing I cannot name.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "As soon go kindle fire with snow, as seek to quench the fire of love with words.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plato");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "At the touch of love everyone becomes a poet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rita Rudner");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Before I met my husband, I'd never fallen in love. I'd stepped in it a few times.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lao Tzu");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Being deeply loved by someone gives you strength, while loving someone deeply gives you courage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Bach");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Can miles truly separate you from friends... If you want to be with someone you love, aren't you already there?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Lover");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Come live in my heart, and pay no rent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Og Mandino");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Do all things with love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Greta Scacchi");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "A relationship requires a lot of work and commitment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Usher Raymond");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Appearance is something you should definitely consider when you're going out. Have your girlfriend clip your nails or something like that.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Winkler");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Assumptions are the termites of relationships.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anna Kournikova");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "At this year's Open, I'll have five boyfriends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vanessa Hudgens");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Do not just look at your boyfriend as just a boyfriend. Look at him as a friend, too.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lucy Liu");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Everything I buy is vintage and smells funny. Maybe that's why I don't have a boyfriend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Keanu Reeves");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Falling in love and having a relationship are two different things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francesca Annis");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "However successful you are, there is no substitute for a close relationship. We all need them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hugh Hefner");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I always say now that I'm in my blonde years. Because since the end of my marriage, all of my girlfriends have been blonde.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orlando Bloom");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I am a hopeless romantic and I love to spoil my girlfriends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert Gottfried");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I can't even find someone for a platonic relationship, much less the kind where someone wants to see me naked.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pink");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I change my mind so much I need two boyfriends and a girlfriend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Candice Bergen");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I didn't have a financial need, and I wasn't very gifted at relationships. I probably was more like what we think of boys as being: hard to pin down and wary of commitment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mitch Hedberg");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I don't have a girlfriend. But I do know a woman who'd be mad at me for saying that.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Gere");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I don't know any of us who are in relationships that are totally honest - it doesn't exist.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jason Schwartzman");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I don't know the first real thing about the dating game. I don't know how to talk to a specific person and connect. I just think you have to go to person by person and do the best you can with people in general.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Britney Spears");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I don't understand the whole dating thing. I know right off the bat if I'm interested in someone, and I don't want them to waste their money on me and take me out to eat if I know I'm not interested in that person.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Spalding Gray");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I fantasize about going back to high school with the knowledge I have now. I would shine. I would have a good time, I would have a girlfriend. I think that's where a lot of my pain comes from. I think I never had any teenage years to go back to.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anna Kournikova");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I have a lot of boyfriends, I want you to write that. Every country I visit, I have a different boyfriend. And I kiss them all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Penelope Cruz");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I have stepped off the relationship scene to come to terms with myself. I have spent most of my adult life being 'someone's girlfriend', and now I am happy being single.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Fripp");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "A mistake is always forgivable, rarely excusable and always unacceptable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Always forgive your enemies - nothing annoys them so much.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ausonius");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgive many things in others; nothing in yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgive your enemies, but never forget their names.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Euripides");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgive, son; men are men; they needs must err.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Arthur Ward");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness is a funny thing. It warms the heart and cools the sting.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Suzanne Somers");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness is a gift you give yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Indira Gandhi");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness is a virtue of the brave.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mason Cooley");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness is like faith. You have to keep reviving it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dag Hammarskjold");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness is the answer to the child's dream of a miracle by which what is broken is made whole again, what is soiled is made clean again.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hannah More");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness is the economy of the heart... forgiveness saves the expense of anger, the cost of hatred, the waste of spirits.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Reinhold Niebuhr");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness is the final form of love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness is the fragrance that the violet sheds on the heel that has crushed it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George MacDonald");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness is the giving, and so the receiving, of life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hannah Arendt");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness is the key to action and freedom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Saint Augustine");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness is the remission of sins. For it is by this that what has been lost, and was found, is saved from being lost again.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gerald Jampolsky");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgiveness means letting go of the past.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "F. Scott Fitzgerald");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Forgotten is forgiven.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Duer Miller");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Genuine forgiveness does not deny anger but faces it head-on.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Heinrich Heine");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "God will forgive me. It's his job.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "A soldier will fight long and hard for a bit of colored ribbon.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert V. Prochnow");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "A visitor from Mars could easily pick out the civilized nations. They have the best implements of war.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sun Tzu");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "All war is deception.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francois Fenelon");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "All wars are civil wars, because all men are brothers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Tullius Cicero");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "An unjust peace is better than a just war.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dwight D. Eisenhower");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Every gun that is made, every warship launched, every rocket fired, signifies in the final sense a theft from those who hunger and are not fed, those who are cold and are not clothed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "He who joyfully marches to music in rank and file has already earned my contempt. He has been given a large brain by mistake, since for him the spinal cord would suffice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Westmoreland");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "I do not believe that the men who served in uniform in Vietnam have been given the credit they deserve. It was a difficult war against an unorthodox enemy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ulysses S. Grant");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "I have never advocated war except as a means of peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Paul Jones");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "I have not yet begun to fight!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Adams");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "I must study politics and war that my sons may have liberty to study mathematics and philosophy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George McGovern");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "I'm fed up to the ears with old men dreaming up wars for young men to die in.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joan Baez");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "If it's natural to kill, how come men have to go into training to learn how?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. G. Wells");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "If we don't end war, war will end us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ernest Hemingway");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "In modern war... you will die like a dog for no good reason.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas MacArthur");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "In my dreams I hear again the crash of guns, the rattle of musketry, the strange, mournful mutter of the battlefield.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herodotus");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "In peace, sons bury their fathers. In war, fathers bury their sons.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lester B. Pearson");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "It has too often been too easy for rulers and governments to incite man to war.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Voltaire");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "It is forbidden to kill; therefore all murderers are punished unless they kill in large numbers and to the sound of trumpets.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert E. Lee");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "It is well that war is so terrible. We should grow too fond of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lenny Kravitz");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "A dramatic thing, the first time you stand up to your dad.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Howard Clark");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "A father is a man who expects his son to be as good a man as he meant to be, A father is someone who carries pictures where his money used to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Enid Bagnold");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "A father is always making his baby into a little woman. And when she is a woman he turns her back again.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Bergin");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "A father's disappointment can be a very powerful tool.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gabriel Garcia Marquez");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "A man knows when he is growing old because he begins to look like his father.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Angela Carter");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Aeneas carried his aged father on his back from the ruins of Troy and so do we all, whether we like it or not, perhaps even if we have never known them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Eliot");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "All the learnin' my father paid for was a bit o' birch at one end and an alphabet at the other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Publilius Syrus");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "An angry father is most cruel towards himself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christine Keeler");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "As a little girl I used to daydream about my real father coming on a white horse to rescue me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gloria Swanson");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "As Daddy said, life is 95 percent anticipation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Amy Heckerling");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Babies don't need fathers, but mothers do. Someone who is taking care of a baby needs to be taken care of.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Reagan");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Because of my father, we are that Shining City on a Hill.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kid Rock");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Being a father helps me be more responsible... you see more things than you've ever seen.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Bosley");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Being a father to my family and a husband is to me much more important than what I did in the business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Hurt");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Being a father, being a friend, those are the things that make me feel successful.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbie Hancock");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "But I have to be careful not to let the world dazzle me so much that I forget that I'm a husband and a father.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Buffalo Bill");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "But the love of adventure was in father's blood.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sean Penn");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Child-rearing is my main interest now. I'm a hands-on father.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Unser");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Dad taught me everything I know. Unfortunately, he didn't teach me everything he knows.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Reagan");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Dad was the only adult male I ever trusted.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Allen");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "A man sooner or later discovers that he is the master-gardener of his soul, the director of his life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Darwin");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "A man who dares to waste one hour of time has not discovered the value of life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "All life is an experiment. The more experiments you make the better.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Havelock Ellis");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "All the art of living lies in a fine mingling of letting go and holding on.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anton Chekhov");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Any idiot can face a crisis - it's day to day living that wears you out.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William James");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Believe that life is worth living and your belief will help create the fact.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Buddha");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Do not dwell in the past, do not dream of the future, concentrate the mind on the present moment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Don't go around saying the world owes you a living. The world owes you nothing. It was here first.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Every creature is better alive than dead, men and moose and pine trees, and he who understands it aright will rather preserve its life than destroy it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Wallace");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Every man dies. Not every man really lives.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean-Paul Sartre");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Everything has been figured out, except how to live.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Donald Trump");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Everything in life is luck.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Karen Horney");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Fortunately analysis is not the only way to resolve inner conflicts. Life itself still remains a very effective therapist.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Nietzsche");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "He who has a why to live can bear almost any how.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Bach");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Here is the test to find whether your mission on Earth is finished: if you're alive, it isn't.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. B. White");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "I arise in the morning torn between a desire to improve the world and a desire to enjoy the world. This makes it hard to plan the day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lillie Langtry");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "I do not regret one moment of my life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Roosevelt Longworth");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "I have a simple philosophy: Fill what's empty. Empty what's full. Scratch where it itches.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anthony Hopkins");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "I love life because what more is there.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Burroughs");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "I still find each day too short for all the thoughts I want to think, all the walks I want to take, all the books I want to read, and all the friends I want to see.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Minna Antrim");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "A homely face and no figure have aided many women heavenward.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marion Jones");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "A lot of women say they love being pregnant, but I wasn't such a big fan.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbra Streisand");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "A man who graduated high in his class at Yale Law School and made partnership in a top law firm would be celebrated. A man who invested wisely would be admired, but a woman who accomplishes this is treated with suspicion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Nietzsche");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Ah, women. They make the highs higher and the lows more frequent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eddie Bernice Johnson");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "All issues are women's issues - and there are several that are just women's business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Franklin P. Jones");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "All women should know how to take care of children. Most of them will have a husband some day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Warren Farrell");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "All women's issues are to some degree men's issues and all men's issues are to some degree women's issues because when either sex wins unilaterally both sexes lose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Mason Brown");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "America is a land where men govern, but women rule.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lara Flynn Boyle");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "And I think women have come a very, very long way, but they have a long way to go.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kathleen Turner");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Being a sex symbol has to do with an attitude, not looks. Most men think it's looks, most women know otherwise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. M. Forster");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Charm, in most men and nearly all women, is a decoration.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Clever and attractive women do not want to vote; they are willing to let men govern as long as they govern men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Grace Kelly");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Emancipation of women has made them lose their mystery.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Juvenal");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "For women's tears are but the sweat of eyes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Santayana");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Friends are generally of the same sex, for when men and women agree, it is only in the conclusions; their reasons are always different.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Angelina Jolie");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I always play women I would date.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christine Baranski");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I always say God should have given women one extra decade at least, especially if you want a family. You're trying to pack a lot in.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Cuomo");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I believe women still face a glass ceiling that must be shattered.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Evita Peron");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I demanded more rights for women because I know what women had to put up with.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Wollstonecraft Shelley");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I do not wish women to have power over men; but over themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James H. Boren");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "A dress that zips up the back will bring a husband and wife together.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michel de Montaigne");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "A good marriage would be between a blind wife and a deaf husband.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Rowland");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "A husband is what is left of a lover, after the nerve has been extracted.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Steinbeck");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "A journey is like marriage. The certain way to be wrong is to think you control it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zsa Zsa Gabor");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "A man in love is incomplete until he has married. Then he's finished.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. Somerset Maugham");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "A man marries to have a home, but also because he doesn't want to be bothered with sex and all that sort of thing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joey Adams");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "A psychiatrist asks a lot of expensive questions your wife asks for nothing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mignon McLaughlin");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "A successful marriage requires falling in love many times, always with the same person.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eddie Cantor");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "A wedding is a funeral where you smell your own flowers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Raymond Hull");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "All marriages are happy. It's the living together afterward that causes all the trouble.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Red Skelton");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "All men make mistakes, but married men find out about them sooner.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sydney J. Harris");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Almost no one is foolish enough to imagine that he automatically deserves great success in any field of activity; yet almost everyone believes that he automatically deserves success in marriage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sacha Guitry");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "An ideal wife is one who remains faithful to you but tries to be just as charming as if she weren't.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isadora Duncan");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Any intelligent woman who reads the marriage contract, and then goes into it, deserves all the consequences.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Johnson");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Bachelors have consciences, married men have wives.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. L. Mencken");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Bachelors know more about women than married men; if they didn't they'd be married too.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woody Allen");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Basically my wife was immature. I'd be at home in the bath and she'd come in and sink my boats.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marilyn Monroe");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Before marriage, a girl has to make love to a man to hold him. After marriage, she has to hold him to make love to him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Kerr");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Being divorced is like being hit by a Mack truck. If you live through it, you start looking very carefully to the right and to the left.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Astell");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "But, alas! what poor Woman is ever taught that she should have a higher Design than to get her a Husband?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Suze Orman");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "A big part of financial freedom is having your heart and mind free from worry about the what-ifs of life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clarence Day");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "A moderate addiction to money may not always be hurtful; but when taken in excess it is nearly always bad for the health.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Smith");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "All money is a matter of belief.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Greenspan");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Any informed borrower is simply less vulnerable to fraud and abuse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Cruz Smith");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "As a novelist, I tell stories and people give me money. Then financial planners tell me stories and I give them money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bo Bennett");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "As sure as the spring will follow the winter, prosperity and economic growth will follow recession.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Marshall");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Capital is that part of wealth which is devoted to obtaining further wealth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John D. Rockefeller");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Do you know the only thing that gives me pleasure? It's to see my dividends coming in.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Don't gamble; take all your savings and buy some good stock and hold it till it goes up, then sell it. If it don't go up, don't buy it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Baudelaire");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "For the merchant, even honesty is a financial speculation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Ricardo");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Gold and silver, like other commodities, have an intrinsic value, which is not arbitrary, but is dependent on their scarcity, the quantity of labour bestowed in procuring them, and the value of the capital employed in the mines which produce them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Milton Friedman");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "I am favor of cutting taxes under any circumstances and for any excuse, for any reason, whenever it's possible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Honore de Balzac");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "I do not regard a broker as a member of the human race.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christie Hefner");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "I don't think about financial success as the measurement of my success.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jules Renard");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "I finally know what distinguishes man from the other beasts: financial worries.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bernard Baruch");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "I made my money by selling too soon.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ruth Handler");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "I wasn't a financial pro, and I paid the price.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Regis Philbin");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "I'm involved in the stock market, which is fun and, sometimes, very painful.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Feldon");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "I've always supported myself. I like the sense of knowing exactly where I stand financially, but there is a side of me that longs for a knight in shining armor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Greenspan");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "In the absence of the gold standard, there is no way to protect savings from confiscation through inflation. There is no safe store of value.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Feather");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "A budget tells us what we can't afford, but it doesn't keep us from buying it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Covey");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "A cardinal principle of Total Quality escapes too many managers: you cannot continuously improve interdependent systems and processes until you progressively perfect interdependent, interpersonal relationships.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bobby Darin");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "A group or an artist shouldn't get his money until his boss gets his.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert Hoover");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "About the time we can make the ends meet, somebody moves the ends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred A. Montapert");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "All lasting business is built on friendship.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Peters");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Almost all quality improvement comes via simplification of design, manufacturing... layout, processes, and procedures.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fred Allen");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "An advertising agency is 85 percent confusion and 15 percent commission.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Laurence J. Peter");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "An economist is an expert who will know tomorrow why the things he predicted yesterday didn't happen today.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "An economist's guess is liable to be as good as anybody else's.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Carnegie");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "And while the law of competition may be sometimes hard for the individual, it is best for the race, because it ensures the survival of the fittest in every department.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tim Berners-Lee");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Anyone who has lost track of time when using a computer knows the propensity to dream, the urge to make dreams come true and the tendency to miss lunch.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Greenleaf Whittier");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "As a small businessperson, you have no greater leverage than the truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edgar R. Fiedler");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Ask five economists and you'll get five different answers - six if one went to Harvard.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Carlyle");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Blessed is he who has found his work; let him ask no other blessedness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andre Maurois");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Business is a combination of war and sport.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Garet Garrett");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Business is in itself a power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ford");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Business is never so healthy as when, like a chicken, it must do a certain amount of scratching around for what it gets.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry R. Luce");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Business, more than any other occupation, is a continual dealing with the future; it is a continual calculation, an instinctive exercise in foresight.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Drucker");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Business, that's easily defined - it's other people's money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stanislaw Lem");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Cannibals prefer those who have no spines.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Larry Ellison");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "A corporation's primary goal is to make money. Government's primary role is to take a big chunk of that money and give it to others.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Owens");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Americans no longer look to government for economic security; rather, they look to their portfolios.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Ancient Rome declined because it had a Senate, now what's going to happen to us with both a House and a Senate?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Owens");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "As a governor, I am naturally inclined to focus on the domestic side of protecting the United States.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dale Dauten");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Bureaucracy gives birth to itself and then expects maternity benefits.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jonathan Mayhew");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "But let us remember, at the same time, government is sacred, and not to be trifled with.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Russell Lowell");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Democracy gives every man the right to be his own oppressor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jorge Luis Borges");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Democracy is an abuse of statistics.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. L. Mencken");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Democracy is the art and science of running the circus from the monkey cage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sydney J. Harris");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Democracy is the only system that persists in asking the powers that be whether they are the powers that ought to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Kenneth Galbraith");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Economics is extremely useful as a form of employment for economists.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "P. J. O'Rourke");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Feeling good about government is like looking on the bright side of any catastrophe. When you quit looking on the bright side, the catastrophe is still there.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jonathan Swift");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "For in reason, all government without the consent of the governed is the very definition of slavery.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "P. J. O'Rourke");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Giving money and power to government is like giving whiskey and car keys to teenage boys.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ayn Rand");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Government \"help\" to business is just as disastrous as government persecution... the only way a government can be of service to national prosperity is by keeping its hands off.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dirk Kempthorne");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Government alone cannot solve the problems we deal with in our correctional facilities, treatment centers, homeless shelters and crisis centers - we need our faith-based and community partners.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Clay");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Government is a trust, and the officers of the government are trustees. And both the trust and the trustees are created for the benefit of the people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fred Woodworth");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Government is an unnecessary evil. Human beings, when accustomed to taking responsibility for their own behavior, can cooperate on a basis of mutual trust and helpfulness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Washington");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Government is not reason; it is not eloquent; it is force. Like fire, it is a dangerous servant and a fearful master.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Paine");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Government, even in its best state, is but a necessary evil; in its worst state, an intolerable one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aesop");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "A crust eaten in peace is better than a banquet partaken in anxiety.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Tolstoy");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "A man can live and be healthy without killing animals for food; therefore, if he eats meat, he participates in taking animal life merely for the sake of his appetite.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "A. J. Liebling");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "An Englishman teaching an American about food is like the blind leading the one-eyed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Animals are my friends... and I don't eat my friends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jo Brand");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Anything is good if it's made of chocolate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Beware the hobby that eats.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Neal Barnard");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Chicken fat, beef fat, fish fat, fried foods - these are the foods that fuel our fat genes by giving them raw materials for building body fat.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Borlaug");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Civilization as it is known today could not have evolved, nor can it survive, without an adequate food supply.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sally Schneider");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Confit is the ultimate comfort food, and trendy or not, it is dazzling stuff.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Tullius Cicero");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Cultivation to the mind is as necessary as food to the body.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dave Barry");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Eating rice cakes is like chewing on a foam coffee cup, only less filling.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcelene Cox");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Eating without conversation is only stoking.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Pollan");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Every major food company now has an organic division. There's more capital going into organic agriculture than ever before.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mitch Hedberg");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Fettucini alfredo is macaroni and cheese for adults.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vanessa Carlton");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Food can become such a point of anxiety - not because it's food, but just because you have anxiety. That's how eating disorders develop.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dorothy Day");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Food for the body is not enough. There must be food for the soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Beard");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Food is our common ground, a universal experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ken Hill");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Food is your body's fuel. Without fuel, your body wants to shut down.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Keller");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Food should be fun.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Paul");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Food simply isn't important to me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Morris Chestnut");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "A lot of times, women don't get the male perspective in regards to a relationship, what men go through when they're not really dealing well.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hilaire Belloc");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "All men have an instinct for conflict: at least, all healthy men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Chief Joseph");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "All men were made by the Great Spirit Chief. They are all brothers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walter Scott");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "All men who have turned out worth anything have had the chief hand in their own education.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herodotus");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "All men's gains are the fruit of venturing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Jean Nathan");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Beauty makes idiots sad and wise men merry.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kin Hubbard");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Boys will be boys, and so will a lot of middle-aged men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "By indignities men come to dignities.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Geoffrey Chaucer");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "By nature, men love newfangledness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Certainly the best works, and of greatest merit for the public, have proceeded from the unmarried, or childless men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herodotus");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Circumstances rule men; men do not rule circumstances.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Howard Thurman");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Commitment means that it is possible for a man to yield the nerve center of his consent to a purpose or cause, a movement or an ideal, which may be more important to him than whether he lives or dies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Menander");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Culture makes all men gentle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles de Gaulle");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Deliberation is the work of many men. Action, of one alone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Democritus");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Do not trust all men, but trust men of worth; the former course is silly, the latter a mark of prudence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Max Lerner");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Either men will learn to live like brothers, or they will die like beasts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lucius Annaeus Seneca");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "For many men, the acquisition of wealth does not end their troubles, it only changes them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clifton Paul Fadiman");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "For most men life is a search for the proper manila envelope in which to get themselves filed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elizabeth Barrett Browning");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "For tis not in mere death that men die most.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Dickens");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Great men are seldom over-scrupulous in the arrangement of their attire.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Camus");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "A man's work is nothing but this slow trek to rediscover, through the detours of art, those two or three great and simple images in whose presence his heart first opened.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Budge");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "After two weeks of working on a project, you know whether it will work or not.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther King, Jr.");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "All labor that uplifts humanity has dignity and importance and should be undertaken with painstaking excellence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "All things are difficult before they are easy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Evan Esar");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "America believes in education: the average professor earns more money in a year than a professional athlete earns in a whole week.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Donald Norman");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "And to get real work experience, you need a job, and most jobs will require you to have had either real work experience or a graduate degree.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Capp");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Anyone who can walk to the welfare office can walk to work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Irvin S. Cobb");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "As I understand it, sport is hard work for which you do not get paid.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leigh Steinberg");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Be open to the amazing changes which are occurring in the field that interest you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gustave Flaubert");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Be regular and orderly in your life, so that you may be violent and original in your work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gloria Steinem");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Because I have work to care about, it is possible that I may be less difficult to get along with than other women when the double chins start to form.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "By working faithfully eight hours a day you may eventually get to be boss and work twelve hours a day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Do not hire a man who does your work for money, but him who does it for love of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Gibson");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Dreaming in public is an important part of our job description, as science writers, but there are bad dreams as well as good dreams. We're dreamers, you see, but we're also realists, of a sort.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Carlyle");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Every noble work is at first impossible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodore Roosevelt");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Far and away the best prize that life has to offer is the chance to work hard at work worth doing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. Jackson Brown, Jr.");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Find a job you like and you add five days to every week.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. Paul Getty");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Going to work for a large company is like getting on a train. Are you going sixty miles an hour or is the train going sixty miles an hour and you're just sitting still?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sam Ewing");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Hard work spotlights the character of people: some turn up their sleeves, some turn up their noses, and some don't turn up at all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edgar Winter");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "I can't imagine anything more worthwhile than doing what I most love. And they pay me for it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gene Tunney");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "A boxer's diet should be low in fat and high in proteins and sugar. Therefore you should eat plenty of lean meat, milk, leafy vegetables, and fresh fruit and ice cream for sugar.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gro Harlem Brundtland");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "A safe and nutritionally adequate diet is a basic individual right and an essential condition for sustainable development, especially in developing countries.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lee Haney");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "A systemic cleansing and detox is definitely the way to go after each holiday. It is the key to fighting high blood pressure, heart disease, cancer, and other health-related illnesses.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Linford Christie");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Adopting a new healthier lifestyle can involve changing diet to include more fresh fruit and vegetables as well as increasing levels of exercise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Irv Kupcinet");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "An optimist is a person who starts a new diet on Thanksgiving Day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michelle Obama");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "And let's be clear: It's not enough just to limit ads for foods that aren't healthy. It's also going to be critical to increase marketing for foods that are healthy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hattie McDaniel");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "As for those grapefruit and buttermilk diets, I'll take roast chicken and dumplings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Toomey");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "As I mentioned previously, the tools that allow for optimum health are diet and exercise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bruce Barton");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Cereal eating is almost a marker for a healthy lifestyle. It sets you up for the day, so you don't overeat.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bruce Froemming");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Dieting is murder on the road. Show me a man who travels and I'll show you one who eats.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joan Rivers");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Diets, like clothes, should be tailored to you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack LaLanne");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Do you know how many calories are in butter and cheese and ice cream? Would you get your dog up in the morning for a cup of coffee and a donut?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Talent");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Each of us should take personal responsibility for our diet, and our children's diet, and the government's role should be to make certain it provides the best information possible to help people stay healthy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gina Gershon");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Feeding is a very important ritual for me. I don't trust people who don't like to eat.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cameron Diaz");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "French fries. I love them. Some people are chocolate and sweets people. I love French fries. That and caviar.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Calvin Trillin");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Health food makes me sick.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Pollan");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "High-quality food is better for your health.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carre Otis");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I am not naturally that thin, so I had to go through everything from using drugs to diet pills to laxatives to fasting. Those were my main ways of controlling my weight.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Banting");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I am now in that happy comfortable state that I do not hesitate to indulge in any fancy in regard to diet, but watch the consequences, and do not continue any course which adds to weight or bulk and consequent discomfort.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joely Fisher");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I don't believe in depriving myself of any food or being imprisoned by a diet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward G. Bulwer-Lytton");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "A fool flatters himself, a wise man flatters the fool.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nelson Mandela");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "A good head and a good heart are always a formidable combination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herb Caen");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "A man begins cutting his wisdom teeth the first time he bites off more than he can chew.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Katharine Graham");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "A mistake is simply another way of doing things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "A poem begins in delight and ends in wisdom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "A prudent question is one-half of wisdom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Moliere");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "A wise man is superior to any insults which can be put upon him, and the best reply to unseemly behavior is patience and moderation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Adopt the pace of nature: her secret is patience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "All this worldly wisdom was once the unamiable heresy of some wise man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Santayana");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Almost every wise saying has an opposite one, no less wise, to balance it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phil Jackson");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Always keep an open mind and a compassionate heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ben Hogan");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "As you walk down the fairway of life you must smell the roses, for you only get to play one round.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sidonie Gabrielle Colette");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Be happy. It's one way of being wise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baltasar Gracian");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Better mad with the rest of the world than wise alone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Confucius");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "By three methods we may learn wisdom: First, by reflection, which is noblest; Second, by imitation, which is easiest; and third by experience, which is the bitterest.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Euripides");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Cleverness is not wisdom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Muir");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Climb the mountains and get their good tidings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Taylor Coleridge");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Common sense in an uncommon degree is what the world calls wisdom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Rohn");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Discipline is the bridge between goals and accomplishment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Do not go where the path may lead, go instead where there is no path and leave a trail.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Donella Meadows");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "A knowledgeable and courageous U.S. president could help enormously in leading the world's nations toward saving the climate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maurice Strong");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "After all, sustainability means running the global environment - Earth Inc. - like a corporation: with depreciation, amortization and maintenance accounts. In other words, keeping the asset whole, rather than undermining your natural capital.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Hawken");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "All is connected... no one thing can change by itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roger Tory Peterson");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Birds are indicators of the environment. If they are in trouble, we know we'll soon be in trouble.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aeschylus");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "By polluting clear water with slime you will never find good drinking water.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aldo Leopold");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Conservation is a state of harmony between men and land.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barry Commoner");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Earth Day 1970 was irrefutable evidence that the American people understood the environmental threat and wanted action to resolve it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barry Commoner");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Environmental concern is now firmly embedded in public life: in education, medicine and law; in journalism, literature and art.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Warren Christopher");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Environmental degradation, overpopulation, refugees, narcotics, terrorism, world crime movements, and organized crime are worldwide problems that don't stop at a nation's borders.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Bradley");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Every time I have some moment on a seashore, or in the mountains, or sometimes in a quiet forest, I think this is why the environment has to be preserved.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexandra Paul");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "For me, going vegan was an ethical and environmental decision. I'm doing the right thing by the animals.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Muir");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "God has cared for these trees, saved them from drought, disease, avalanches, and a thousand tempests and floods. But he cannot save them from fools.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aldo Leopold");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Harmony with land is like harmony with a friend; you cannot cherish his right hand and chop off his left.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pat Buckley");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "I can find God in nature, in animals, in birds and the environment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roger Tory Peterson");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "I consider myself to have been the bridge between the shotgun and the binoculars in bird watching. Before I came along, the primary way to observe birds was to shoot them and stuff them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isaac Bashevis Singer");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "I did not become a vegetarian for my health, I did it for the health of the chickens.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Fowler");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "I don't think we're going to save anything if we go around talking about saving plants and animals only; we've got to translate that into what's in it for us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Murkowski");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "I firmly believe that we can have a healthy environment and a sustainable timber industry.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Luther Burbank");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "I see humanity now as one vast plant, needing for its highest fulfillment only love, the natural blessings of the great outdoors, and intelligent crossing and selection.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ted Turner");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "I think Captain Cousteau might be the father of the environmental movement.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "A friend never defends a husband who gets his wife an electric skillet for her birthday.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lucius Annaeus Seneca");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "A gift consists not in what is done or given, but in the intention of the giver or doer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "A gift, with a kind countenance, is a double present.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kurt Vonnegut");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "About astrology and palmistry: they are good because they make people vivid and full of possibilities. They are communism at its best. Everybody has a birthday and almost everybody has a palm.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Harrison");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "All the world is birthday cake, so take a piece, but not too much.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Golda Meir");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Being seventy is not a sin.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sammy Hagar");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Every year on your birthday, you get a chance to start new.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "For my birthday I got a humidifier and a de-humidifier... I put them in the same room and let them fight it out.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Voltaire");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "God gave us the gift of life; it is up to us to give ourselves the gift of living well.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doug Coupland");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Handmade presents are scary because they reveal that you have too much free time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Satchel Paige");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "How old would you be if you didn't know how old you are?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kirstie Alley");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I binge when I'm happy. When everything is going really well, every day is like I'm at a birthday party.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isla Fisher");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I crashed my boyfriend's birthday when I was 12 years old. He didn't invite me and so I showed up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Liv Tyler");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I cried on my 18th birthday. I thought 17 was such a nice age. You're young enough to get away with things, but you're old enough, too.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ruth Warrick");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I do like to shock and surprise people. When it's all in good fun, of course.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Hopkins");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I had a birthday one night on a farm we were shooting on. I walked into the tent, and there were 150 people waiting for me, all wearing masks of my face.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zane Grey");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I hate birthdays.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Celine Dion");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I often buy myself presents. Sometimes I will spend $100,000 in one day in a posh boutique.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barry White");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I quit high school on my birthday. It was my senior year and I didn't see the point. This was 1962, and I was ready to make music.");
		db.insert("citations", AUTHOR, cv);

		

		cv.put(AUTHOR, "Arthur Erickson");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Space has always been the spiritual dimension of architecture. It is not the physical statement of the structure so much as what it contains that moves us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Russell Lynes");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "The bungalow had more to do with how Americans live today than any other building that has gone remotely by the name of architecture in our history.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert A. M. Stern");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "The dialogue between client and architect is about as intimate as any conversation you can have, because when you're talking about building a house, you're talking about dreams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Saul Steinberg");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "The frightening thought that what you draw may become a building makes for reasoned lines.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Schiller");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Mankind is made great or little by its own will.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Camus");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Men must live and create. Live to the point of tears.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Blaise Pascal");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Noble deeds that are concealed are most esteemed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Audrey Hepburn");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Nothing is impossible, the word itself says 'I'm possible'!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pope Paul VI");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Nothing makes one feel so strong as a call for help.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pope Paul VI");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Of all human activities, man's listening to God is the supreme act of his reasoning and will.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Bartram");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "On the recollection of so many and great favours and blessings, I now, with a high sense of gratitude, presume to offer up my sincere thanks to the Almighty, the Creator and Preserver.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Barber Lightfoot");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Plant thy foot firmly in the prints which His foot has made before thee.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Swami Sivananda");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Put your heart, mind, and soul into even your smallest acts. This is the secret of success.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fulton J. Sheen");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Show me your hands. Do they have scars from giving? Show me your feet. Are they wounded in service? Show me your heart. Have you left a place for divine love?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Tears are often the telescope by which men see far into heaven.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hosea Ballou");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Tears of joy are like the summer rain drops pierced by sunbeams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "The best way out is always through.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Hopkins");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "The essential elements of giving are power and love - activity and affection - and the consciousness of the race testifies that in the high and appropriate exercise of these is a blessedness greater than any other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Buscaglia");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "The fact that I can plant a seed and it becomes a flower, share a bit of knowledge and it becomes another's, smile at someone and receive a smile in return, are to me continual spiritual exercises.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "The glow of one warm thought is to me worth more than money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ray Manzarek");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "The only thing that ultimately matters is to eat an ice-cream cone, play a slide trombone, plant a small tree, good God, now you're free.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Muir");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "The power of imagination makes us infinite.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emile Zola");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "The truth is on the march and nothing will stop it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edith Wharton");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "There are two ways of spreading light: to be the candle or the mirror that reflects it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Knute Nelson");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "In the midst of these hard times it is our good health and good sleep that are enjoyable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gro Harlem Brundtland");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Investing in health will produce enormous benefits.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "It is health that is real wealth and not pieces of gold and silver.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jonah Lomu");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "It's been a long road back to health and fitness for me. I am just glad to have been given the opportunity to do what I love most.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joely Fisher");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "It's challenging, but you have to at least try to eat right and exercise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Deborah Bull");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "It's not about weight, it's about fitness, and one component of being fit is to have relatively low body fat, because fat is not very efficient, whereas muscle is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cheryl Tiegs");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "It's very important to have the right clothing to exercise in. If you throw on an old T-shirt or sweats, it's not inspiring for your workout.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles M. Schulz");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Jogging is very beneficial. It's good for your legs and your feet. It's also very good for the ground. If makes it feel needed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Leave all the afternoon for exercise and recreation, which are as necessary as reading. I will rather say more necessary because health is worth more than learning.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Viggo Mortensen");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Like most people I can be lazy, so it's nice to have a goal or deadline or reason to work out. I feel better when I get to exercise, or when I'm outdoors. I like to hike, swim and run, and I love to play soccer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Wilson Schaef");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Looking after my health today gives me a better hope for tomorrow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Vaughan");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Muscles come and go; flab lasts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carol Vorderman");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "My bottom is so big it's got its own gravitational field.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Freeman Clarke");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Never hurry. Take plenty of exercise. Always be cheerful. Take all the sleep you need. You may expect to be well.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Soul");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Once in a while I'll get moved to do some exercise. It's something I long for but the biggest problem is bending down and putting my tennis shoes on. Once I go out I'm OK.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Our growing softness, our increasing lack of physical fitness, is a menace to our security.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jurgen Klinsmann");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Personally, I need a high level of physical fitness in order to feel at ease.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Physical fitness is not only one of the most important keys to a healthy body, it is the basis of dynamic and creative intellectual activity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Orben");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Quit worrying about your health. It will go away.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lord Chesterfield");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Regularity in the hours of rising and retiring, perseverance in exercise, adaptation of dress to the variations of climate, simple and nutritious aliment, and temperance in all things are necessary branches of the regimen of health.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Anouilh");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Some men like to make a little garden out of life and walk down a path.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Les Brown");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Someone's sitting in the shade today because someone planted a tree a long time ago.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "The best place to find God is in a garden. You can dig for him there.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Pollan");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "The garden suggests there might be a place where we can meet nature halfway.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Gardiner");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "The garden, by design, is concerned with both the interior and the land beyond the garden.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Austin");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "The glory of gardening: hands in the dirt, head in the sun, heart with nature. To nurture a garden is to feed not just on the body, but the soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gertrude Jekyll");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "The love of gardening is a seed once sown that never dies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Luther Burbank");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "The secret of improved plant breeding, apart from scientific knowledge, is love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Heidegger");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "To dwell is to garden.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zora Neale Hurston");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Trees and plants always look like the people they live with, somehow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas Wilson");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Use plants to bring life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Voltaire");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "We must cultivate our own garden. When man was put in the garden of Eden he was put there so that he should work, which proves that man was not born to rest.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcelene Cox");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Weather means more when you have a garden. There's nothing like listening to a shower and thinking how it is soaking in around your green beans.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dixie Lee Ray");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Well tended garden is better than a neglected wood lot.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Dudley Warner");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "What a man needs in gardening is a cast-iron back, with a hinge in it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "What is a weed? A plant whose virtues have never been discovered.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roy Rogers");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "What's a butterfly garden without butterflies?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Smithson");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "When a finished work of 20th century sculpture is placed in an 18th century garden, it is absorbed by the ideal representation of the past, thus reinforcing political and social values that are no longer with us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Cowper");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Who loves a garden loves a greenhouse too.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dogen Zenji");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Working with plants, trees, fences and walls, if they practice sincerely they will attain enlightenment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Morrow Lindbergh");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Men kick friendship around like a football, but it doesn't seem to crack. Women treat it like glass and it goes to pieces.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Pepys");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Mighty proud I am that I am able to have a spare bed for my friends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elbert Hubbard");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Never explain - your friends do not need it and your enemies will not believe you anyway.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baltasar Gracian");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Never have a companion that casts you in the shade.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plautus");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Nothing but heaven itself is better than a friend who is really a friend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Honore de Balzac");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Nothing so fortifies a friendship as a belief on the part of one friend that he is superior to the other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Santayana");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "One's friends are that part of the human race with which one can be human.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Toni Morrison");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "She is a friend of mind. She gather me, man. The pieces I am, she gather them and give them back to me in all the right order. It's good, you know, when you got a woman who is a friend of your mind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Lee Runbeck");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Silences make the real conversations between friends. Not the saying but the never needing to say is what counts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francesco Guicciardini");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Since there is nothing so well worth having as friends, never lose a chance to make them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Blake");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "The bird a nest, the spider a web, man friendship.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elbert Hubbard");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "The friend is the man who knows all about you, and still likes you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henri Nouwen");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "The friend who can be silent with us in a moment of despair or confusion, who can stay with us in an hour of grief and bereavement, who can tolerate not knowing... not healing, not curing... that is a friend who cares.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "The language of friendship is not words but meanings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "The most I can do for my friend is simply be his friend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "The only way to have a friend is to be one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eugene Kennedy");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "The real test of friendship is: can you literally do nothing with the other person? Can you enjoy those moments of life that are utterly simple?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Giotto di Bondone");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "The sincere friends of this world are as ship lights in the stormiest of nights.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Aquinas");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "There is nothing on this earth more to be prized than true friendship.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Butler Yeats");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Think where mans glory most begins and ends, and say my glory was I had such friends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "He who knows how to flatter also knows how to slander.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Winston Churchill");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Healthy citizens are the greatest asset any country can have.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Milton Friedman");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Hell hath no fury like a bureaucrat scorned.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. C. Fields");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Hell, I never vote for anybody, I always vote against.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Gilbert");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I always voted at my party's call, and I never thought of thinking for myself at all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Thatcher");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I am extraordinarily patient, provided I get my own way in the end.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gerhard Schroder");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I can't let important policy decisions hinge on the fact that an election is coming up every 90 days.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dan Quayle");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I do have a political agenda. It's to have as few regulations as possible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I don't make jokes. I just watch the government and report the facts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur C. Clarke");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I have a fantasy where Ted Turner is elected President but refuses because he doesn't want to give up power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I have no ambition to govern men; it is a painful and thankless office.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Koch");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I know many writers who first dictate passages, then polish what they have dictated. I speak, then I polish - occasionally I do windows.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I love power. But it is as an artist that I love it. I love it as a musician loves his violin, to draw out its sounds and chords and harmonies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "A. N. Wilson");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I should prefer to have a politician who regularly went to a massage parlour than one who promised a laptop computer for every teacher.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sydney Pollack");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I think it's a terrible shame that politics has become show business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gary Hart");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I think there is one higher office than president and I would call that patriot.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jeane Kirkpatrick");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I was a woman in a man's world. I was a Democrat in a Republican administration. I was an intellectual in a world of bureaucrats. I talked differently. This may have made me a bit like an ink blot.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barry Goldwater");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I would remind you that extremism in the defense of liberty is no vice! And let me remind you also that moderation in the pursuit of justice is no virtue.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Stone");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "I've been to war, and it's not easy to kill. It's bloody and messy and totally horrifying, and the consequences are serious.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Meg Greenfield");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "If a politician murders his mother, the first response of the press or of his opponents will likely be not that it was a terrible thing to do, but rather that in a statement made six years before he had gone on record as being opposed to matricide.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is the version of past events that people have decided to agree upon.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John W. Gardner");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History never looks like history when you are living through it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Winston Churchill");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History will be kind to me for I intend to write it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert Hoover");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Honor is not the exclusive property of any political party.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barry Goldwater");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Hubert Humphrey talks so fast that listening to him is like trying to read Playboy magazine with your wife turning the pages.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. G. Wells");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Human history becomes more and more a race between education and catastrophe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Julius Caesar");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "I came, I saw, I conquered.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard M. Nixon");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "I can see clearly now... that I was wrong in not acting more decisively and more forthrightly in dealing with Watergate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elizabeth II");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "I cannot lead you into battle. I do not give you laws or administer justice but I can do something else - I can give my heart and my devotion to these old islands and to all the peoples of our brotherhood of nations.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wendell Berry");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "I come into the peace of wild things who do not tax their lives with forethought of grief... For a time I rest in the grace of the world, and am free.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Calvin Coolidge");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "I have noticed that nothing I never said ever did me any harm.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles de Gaulle");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "I have tried to lift France out of the mud. But she will return to her errors and vomitings. I cannot prevent the French from being French.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver North");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "I haven't, in the 23 years that I have been in the uniformed services of the United States of America, ever violated an order - not one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "I like the dreams of the future better than the history of the past.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Foster Dulles");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "I wouldn't attach too much importance to these student riots. I remember when I was a student at the Sorbonne in Paris, I used to go out and riot occasionally.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert Hoover");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "I'm the only person of distinction who has ever had a depression named for him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lyndon B. Johnson");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "If one morning I walked on top of the water across the Potomac River, the headline that afternoon would read: \"President Can't Swim.\"");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "If we cannot now end our differences, at least we can help make the world safe for diversity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Horowitz");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "If we celebrate Martin Luther King Jr.'s birthday at a time of presidential inaugurals, this is thanks to Ronald Reagan who created the holiday, and not to the Democratic Congress of the Carter years, which rejected it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Desmond Tutu");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "If you are neutral in situations of injustice, you have chosen the side of the oppressor. If an elephant has its foot on the tail of a mouse and you say that you are neutral, the mouse will not appreciate your neutrality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bernard Baruch");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Millions saw the apple fall, but Newton was the one who asked why.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Duke Ellington");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "My attitude is never to be satisfied, never enough, never.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. Clement Stone");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "No matter how carefully you plan your goals they will never be more than pipe dreams unless you pursue them with gusto.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Carnegie");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "No person will make a great business who wants to do it all himself or get all the credit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. Joseph Cossman");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Obstacles are things a person sees when he takes his eyes off his goal.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. Joseph Cossman");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Our business in life is not to get ahead of others, but to get ahead of ourselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Drucker");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "People who don't take risks generally make about two big mistakes a year. People who do take risks generally make about two big mistakes a year.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Sowell");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "People who enjoy meetings should not be in charge of anything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry J. Kaiser");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Problems are only opportunities in work clothes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Freeman Clarke");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Strong convictions precede great actions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Sher");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "The amount of good luck coming your way depends on your willingness to act.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Confucius");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "The cautious seldom err.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. Paul Getty");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "The employer generally gets the employees he deserves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vaclav Havel");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "The exercise of power is determined by thousands of interactions between the world of the powerful and that of the powerless, all the more so because these worlds are never divided by a sharp line: everyone has a small part of himself in both.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Carnegie");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "The first man gets the oyster, the second man gets the shell.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles de Gaulle");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "The great leaders have always stage-managed their effects.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ken Olsen");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "The nicest thing about standards is that there are so many of them to choose from.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harvey S. Firestone");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "The secret of my success is a two word answer: Know people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Kay Ash");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "The speed of the leader is the speed of the gang.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry A. Kissinger");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "The task of the leader is to get his people from where they are to where they have not been.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Itzhak Perlman");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust your ability!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joyce Brothers");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust your hunches. They're usually based on facts filed away just below the conscious level.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Billy Wilder");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust your own instinct. Your mistakes might as well be your own, instead of someone else's.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johann Wolfgang von Goethe");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust yourself, then you will know how to live.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Spock");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust yourself, you know more than you think you do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Golda Meir");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust yourself. Create the kind of self that you will be happy to live with all your life. Make the most of yourself by fanning the tiny, inner sparks of possibility into flames of achievement.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Broughton");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trusting your individual uniqueness challenges you to lay yourself open.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wilma Mankiller");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "We must trust our own thinking. Trust where we're going. And get the job done.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Angelina Jolie");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "When I get logical, and I don't trust my instincts - that's when I get in trouble.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eliza Cook");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Who would not rather trust and be deceived?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hesiod");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Whoever has trusted a woman has trusted deceivers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sydney Madwed");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Would you want to do business with a person who was 99% honest?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Newt Gingrich");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "You can't trust anybody with power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. C. Fields");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "You can't trust water: Even a straight stick turns crooked in it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbra Streisand");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "You have got to discover you, what you do, and trust it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kary Mullis");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is subject to arbitrary fashion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Amy Lowell");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is the desire of a man to express himself, to record the reactions of his personality to the world he lives in.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is the most intense mode of individualism that the world has known.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Twyla Tharp");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is the only way to run away without leaving home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Nietzsche");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is the proper task of life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Schiller");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is the right hand of Nature. The latter has only given us being, the former has made us men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodore Dreiser");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is the stored honey of the human soul, gathered on wings of misery and travail.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gian Carlo Menotti");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is the unceasing effort to compete with the beauty of flowers - and never succeeding.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred de Vigny");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art ought never to be considered except in its relations with its ideal beauty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Cocteau");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art produces ugly things which frequently become more beautiful with time. Fashion, on the other hand, produces beautiful things which always become ugly with time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Sondheim");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art, in itself, is an attempt to bring order out of chaos.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert K. Chesterton");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art, like morality, consists in drawing the line somewhere.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anish Kapoor");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Artists don't make objects. Artists make mythologies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Smithson");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Artists themselves are not confined, but their output is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gustave Flaubert");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Artists who seek perfection in everything are those who cannot attain it in anything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Rostand");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Beauty in art is often nothing but ugliness subdued.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean de La Fontaine");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "By the work one knows the workman.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Scott Adams");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Creativity is allowing yourself to make mistakes. Art is knowing which ones to keep.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Wolfe");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Culture is the arts elevated to a set of beliefs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henri Matisse");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Drawing is like making an expressive gesture with the advantage of permanence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Claude Van Damme");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "God gave me a great body and it's my duty to take care of my physical temple.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Marlowe");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Goodness is beauty in the best estate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Chamberlain");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "'Handsome' means many things to many people. If people consider me handsome, I feel flattered - and have my parents to thank for it. Realistically, it doesn't hurt to be good-looking, especially in this business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cindy Margolis");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Having inner beauty is something you develop on your own, and I like to think I have that.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dante Alighieri");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Heat cannot be separated from fire, or beauty from The Eternal.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Milan Kundera");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "How goodness heightens beauty!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Traci Bingham");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "I can't live without my beauty products. I love to be in my bathroom with my candles lit, morning, noon and night. I like taking hot baths and hot showers, using my body scrubs and lotions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bob Dylan");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "I define nothing. Not beauty, not patriotism. I take each thing as it is, without prior rules about what it should be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Courtney Love");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "I don't need plastic in my body to validate me as a woman.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Frank");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "I don't think of all the misery but of the beauty that still remains.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brigitte Bardot");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "I gave my beauty and my youth to men. I am going to give my wisdom and experience to animals.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christy Turlington");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "I sincerely feel that beauty largely comes from within.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kirsten Dunst");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "I'd like to grow up and be beautiful. I know it doesn't matter, but it doesn't hurt.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Blaise Pascal");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Imagination disposes of everything; it creates beauty, justice, and happiness, which are everything in this world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Morley");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "In every man's heart there is a secret nerve that answers to the vibrations of beauty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Homer");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "In youth and beauty, wisdom is but rare!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Priscilla Presley");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Inner beauty should be the most important part of improving one's self.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Leonard");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Integrity reveals beauty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Lamb");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Let us live for the beauty of our own reality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ashley Smith");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Life is full of beauty. Notice it. Notice the bumble bee, the small child, and the smiling faces. Smell the rain, and feel the wind. Live your life to the fullest potential, and fight for your dreams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marie Curie");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I have frequently been questioned, especially by women, of how I could reconcile family life with a scientific career. Well, it has not been easy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nastassja Kinski");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I have these visions of myself being thirty, thirty-five, forty having a family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Antonio Tabucchi");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I live quietly at home among my family and friends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Clooney");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I love children and I get along with them great. It's just that I believe if you're going to be a parent, there has to be something inside you that says, 'I want a family.' I don't feel that sense of urgency.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Roker");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I love cooking for myself and cooking for my family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Manute Bol");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I love my family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Sandler");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I mean, I look at my dad. He was twenty when he started having a family, and he was always the coolest dad. He did everything for his kids, and he never made us feel like he was pressured. I know that it must be a great feeling to be a guy like that.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Patty Duke");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I never did quite fit the glamour mode. It is life with my husband and family that is my high now.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anthony Anderson");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I realized my family was funny, because nobody ever wanted to leave our house.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Regina King");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I stay in tune with my family and God.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Bush");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I think togetherness is a very important ingredient to family life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maureen O'Hara");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I was born into the most remarkable and eccentric family I could possibly have hoped for.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cameron Diaz");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I would love a family. I'm at the age where the wish for a child gets stronger. But who knows.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don Marquis");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I would rather start a family than finish one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "LaToya Jackson");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I'll never stop dreaming that one day we can be a real family, together, all of us laughing and talking, loving and understanding, not looking at the past but only to the future.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sting");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I'm not much of a family man. I'm just not that into it. I love kids, I adore them, but I don't want to live my life for them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jamie Lee Curtis");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I've always put my family first and that's just the way it is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard M. Daley");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I've given it my all. I've done my best. Now, I'm ready with my family to begin the next phase of our lives.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abdul Kalam");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "If a country is to be corruption free and become a nation of beautiful minds, I strongly feel there are three key societal members who can make a difference. They are the father, the mother and the teacher.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David McCallum");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "If I had no family, my wife and I would lead a much more romantic and nomadic existence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Blaise Pascal");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Small minds are concerned with the extraordinary, great minds with the ordinary.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gracie Allen");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Smartness runs in my family. When I went to school I was so smart my teacher was in my class for five years.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Pope");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "The bookful blockhead, ignorantly read With loads of learned lumber in his head.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ernest Newman");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "The higher the voice the smaller the intellect.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ed Parker");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "The intelligent man is one who has successfully fulfilled many accomplishments, and is yet willing to learn more.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plutarch");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "The mind is not a vessel to be filled but a fire to be kindled.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Watterson");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "The surest sign that intelligent life exists elsewhere in the universe is that it has never tried to contact us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "F. Scott Fitzgerald");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "The test of a first-rate intelligence is the ability to hold two opposed ideas in mind at the same time and still retain the ability to function.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "The true sign of intelligence is not knowledge but imagination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ronald Reagan");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "There are no great limits to growth because there are no limits of human intelligence, imagination, and wonder.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Fowles");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "There are only two races on this planet - the intelligent and the stupid.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Josh Billings");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "There is no greater evidence of superior intelligence than to be surprised at nothing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "T. S. Eliot");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "There is no method but to be very intelligent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don Herold");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "There is nobody so irritating as somebody with less intelligence and more sense than we have.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold J. Toynbee");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "To be able to fill leisure intelligently is the last product of civilization.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Orwell");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "We have now sunk to a depth at which restatement of the obvious is the first duty of intelligent men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "We should take care not to make the intellect our god; it has, of course, powerful muscles, but no personality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sigmund Freud");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "What a distressing contrast there is between the radiant intelligence of the child and the feeble mentality of the average adult.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Wit is educated insolence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roger Ebert");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Your intellect may be confused, but your emotions will never lie to you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Forget not that the earth delights to feel your bare feet and the winds long to play with your hair.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walt Whitman");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Give me odorous at sunrise a garden of beautiful flowers where I can walk undisturbed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pedro Calderon de la Barca");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Green is the prime color of the world, and that from which its loveliness arises.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Bartram");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Having contemplated this admirable grove, I proceeded towards the shrubberies on the banks of the river, and though it was now late in December, the aromatic groves appeared in full bloom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sandra Day O'Connor");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Having family responsibilities and concerns just has to make you a more understanding person.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Socrates");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "He is richest who is content with the least, for content is the wealth of nature.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Green Ingersoll");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Hope is the only bee that makes honey without flowers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emily Dickinson");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "How strange that nature does not knock, and yet does not intrude!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wendell Berry");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I am not bound for any public place, but for ground of my own where I have planted vines and orchard trees, and in the heat of the day climbed up into the healing shadow of the woods.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woody Allen");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I am two with nature.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walt Whitman");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I believe a leaf of grass is no less than the journey-work of the stars.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Lloyd Wright");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I believe in God, only I spell it Nature.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gustave Flaubert");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I believe that if one always looked at the skies, one would end up with wings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Georgia O'Keeffe");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I decided that if I could paint that flower in a huge scale, you could not ignore its beauty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Burroughs");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I go to nature to be soothed and healed, and to have my senses put in order.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Steichen");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I knew, of course, that trees and plants had roots, stems, bark, branches and foliage that reached up toward the light. But I was coming to realize that the real magician was light itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Willa Cather");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I like trees because they seem more resigned to the way they have to live than other things do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Washington Carver");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I love to think of nature as an unlimited broadcasting station, through which God speaks to us every hour, if we will only tune in.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Muir");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I never saw a discontented tree. They grip the ground as though they liked it, and though fast rooted they travel about as far as we do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Claude Monet");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "I perhaps owe having become a painter to flowers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gloria Steinem");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Most American children suffer too much mother and too little father.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phyllis Diller");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Most children threaten at times to run away from home. This is the only thing that keeps some parents going.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Evan Bayh");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Mothers - especially single mothers - are heroic in their efforts to raise our nation's children, but men must also take responsibility for their children and recognize the impact they have on their families' well-being.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Mothers all want their sons to grow up to be president, but they don't want them to become politicians in the process.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roberto Benigni");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "My father was a farmer and my mother was a farmer, but, my childhood was very good. I am very grateful for my childhood, because it was full of gladness and good humanity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marion Jones");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "My father wasn't really involved and my mom is the light in my life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isabelle Huppert");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "My mom's a Catholic, and my dad's a Jew, and they didn't want anything to do with anything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Quentin Crisp");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "My mother protected me from the world and my father threatened me with it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John H. Johnson");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "My mother was the influence in my life. She was strong; she had great faith in the ultimate triumph of justice and hard work. She believed passionately in education.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Never lend your car to anyone to whom you have given birth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miguel de Cervantes");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "No fathers or mothers think their own children ugly.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcelene Cox");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "No one knows his true character until he has run out of gas, purchased something on the installment plan and raised an adolescent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Parentage is a very important profession, but no test of fitness for it is ever imposed in the interest of the children.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clive Owen");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Parenthood and family come first for me, and when I'm not working I'm cool with the Teletubbies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christy Turlington");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Parents should not smoke in order to discourage their kids from smoking. A child is more likely to smoke when they have been raised in the environment of a smoker.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Boehner");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Studies show that children best flourish when one mom and one dad are there to raise them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kelly LeBrock");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Success for me its to raise happy, healthy human beings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold H. Glasow");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Telling a teenager the facts of life is like giving a fish a bath.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kenny G");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "That's my ideal day, time with my boys.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Spock");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "The child supplies the power but the parents have to do the steering.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Chase");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "The most imaginative people are the most credulous, for them everything is possible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charlie Chaplin");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "The saddest thing I can imagine is to get used to luxury.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert K. Chesterton");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "There are no rules of architecture for a castle in the clouds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Leahy");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "There are no shortcuts in life - only those we imagine.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edmund Burke");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "There is a boundary to men's passions when they act from feelings; but none when they are under the influence of imagination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "They are ill discoverers that think there is no land, when they can see nothing but sea.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dr. Seuss");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Think left and think right and think low and think high. Oh, the thinks you can think up if only you try!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edgar Allan Poe");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Those who dream by day are cognizant of many things that escape those who dream only at night.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anatole France");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "To imagine is everything, to know is nothing at all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Duane Michals");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Trust that little voice in your head that says 'Wouldn't it be interesting if...'; And then do it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Sullivan Macy");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "We imagine that we want to escape our selfish and commonplace existence, but we cling desperately to our chains.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kerry Thornley");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "What we imagine is order is merely the prevailing form of chaos.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "You can't depend on your eyes when your imagination is out of focus.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rita Dove");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "You have to imagine it possible before you can see something. You can have the evidence right in front of you, but if you can't imagine something that has never existed before, it's impossible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Azar Nafisi");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "You need imagination in order to imagine a future that doesn't exist.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Justin Timberlake");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "My teenage years were exactly what they were supposed to be. Everybody has their own path. It's laid out for you. It's just up to you to walk it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Keri Russell");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "People - not just in their teenage years - hold on to this fantasy of love when they're not ready to have a real relationship.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vanessa Hudgens");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "So many people try to grow up too fast, and it's not fun! You should stay a kid as long as possible!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Knowles");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Teenagers today are more free to be themselves and to accept themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fred G. Gosman");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Teenagers who are never required to vacuum are living in one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John C. Maxwell");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "The greatest day in your life and mine is when we take total responsibility for our attitudes. That's the day we truly grow up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Shue");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "The most important role models should and could be parents and teachers. But that said, once you're a teenager you've probably gotten as much of an example from your parents as you're going to.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louise J. Kaplan");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "The toddler must say no in order to find out who she is. The adolescent says no to assert who she is not.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Penelope Spheeris");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "This generation has given up on growth. They're just hoping for survival.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Ladd");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Time scoots along pretty fast when you grow up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dave Barry");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "To an adolescent, there is nothing in the world more embarrassing than a parent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ben Lindsey");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Trouble is, kids feel they have to shock their elders and each generation grows up into something harder to shock.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bryan White");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "We never really grow up, we only learn how to act in public.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jamie Lee Curtis");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Well, I could do it for a day, but I wouldn't want to be a teenager again. I really wouldn't.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Heather Locklear");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "When I look in the mirror I see the girl I was when I was growing up, with braces, crooked teeth, a baby face and a skinny body.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dario Argento");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "When I was a teenager, I read a lot of Poe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dean Kamen");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "You have teenagers thinking they're going to make millions as NBA stars when that's not realistic for even 1 percent of them. Becoming a scientist or engineer is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Winona Ryder");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "You've got to grow up sometime.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dave Barry");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Your modern teenager is not about to listen to advice from an old person, defined as a person who remembers when there was no Velcro.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Youth is the trustee of prosperity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Eames");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Recognizing the need is the primary condition for design.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christy Turlington");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Sacred spaces can be created in any environment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Genevieve Gorder");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "So, to really execute design in its highest form and making people feel joy, that's a great reward.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herodotus");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Some men give up their designs when they have almost reached the goal; While others, on the contrary, obtain a victory by exerting, at the last moment, more vigorous efforts than ever before.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Sometimes I can't figure designers out. It's as if they flunked human anatomy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "The artist in me cries out for design.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Fender");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "The design of each element should be thought out in order to be easy to make and easy to repair.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur Erickson");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "The details are the very source of expression in architecture. But we are caught in a vice between art and the bottom line.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arne Jacobsen");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "The primary factor is proportions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert Simon");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "The proper study of mankind is the science of design.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry Bertoia");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "The urge for good design is the same as the urge to go on living.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isaac Mizrahi");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "This is what I like about being a designer: You can't really get it until you see it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Milton Glaser");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "To design is to communicate clearly by whatever means you can control or master.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles J. Givens");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "To design the future effectively, you must first let go of your past.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Eames");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "To whom does design address itself: to the greatest number, to the specialist of an enlightened matter, to a privileged social class? Design addresses itself to the need.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mitchell Kapor");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "What is design? It's where you stand with a foot in two worlds - the world of technology and the world of people and human purposes - and you try to bring the two together.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tadao Ando");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "When I design buildings, I think of the overall composition, much as the parts of a body would fit together. On top of that, I think about how people will approach the building and experience that space.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walt Disney");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "You can design and create, and build the most wonderful place in the world. But it takes people to make the dream a reality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Colin Wilson");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "The mind has exactly the same power as the hands; not merely to grasp the world, but to change it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Watts");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "The only way to make sense out of change is to plunge into it, move with it, and join the dance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Kettering");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "The world hates change, yet it is the only thing that has brought progress.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rupert Murdoch");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "The world is changing very fast. Big will not beat small anymore. It will be the fast beating the slow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Agnetha Faltskog");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "There is a danger of changing too much in the search for perfection.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Confucius");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "They must often change, who would be constant in happiness or wisdom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Things alter for the worse spontaneously, if they be not altered for the better designedly.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Things do not change; we change.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harrison Ford");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "We all have big changes in our lives that are more or less a second chance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl T. Rowan");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "We emphasize that we believe in change because we were born of it, we have lived by it, we prospered and grew great by it. So the status quo has never been our god, and we ask no one else to bow down before it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "R. D. Laing");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "We live in a moment of history where change is so speeded up that we begin to see the present only when it is already disappearing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Havelock Ellis");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "What we call progress is the exchange of one nuisance for another nuisance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bruce Barton");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "When you are through changing, you are through.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stanislaw Lec");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "When you jump for joy, beware that no one moves the ground from beneath your feet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Shunryu Suzuki");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Without accepting the fact that everything changes, we cannot find perfect composure. But unfortunately, although it is true, it is difficult for us to accept it. Because we cannot accept the truth of transience, we suffer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Herbert");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Without change, something sleeps inside us, and seldom awakens. The sleeper must awaken.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Wright");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "You could always go on changing things but there comes a time when you have to decide to stop.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Denis Waitley");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "You must welcome change as the rule but not as your ruler.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Bean");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I think everything depends on money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sergei Bubka");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I think that focusing on the money, on the business, is not enough.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pablo Picasso");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I'd like to live as a poor man with lots of money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dean Kamen");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I'd rather lose my own money than someone else's.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henny Youngman");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I've got all the money I'll ever need, if I die by four o'clock.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paris Hilton");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I've made all my money on my own without my family and I work very hard.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marya Mannes");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "If American men are obsessed with money, American women are obsessed with weight. The men talk of gain, the women talk of loss, and I do not know which talk is the more boring.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edith Piaf");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "If God has allowed me to earn so much money, it is because He knows I give it all away.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Alda");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "If I can't get the girl, at least give me more money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Shia LaBeouf");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "If I have enough money to eat I'm good.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Placido Domingo");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "If money was my only motivation, I would organize myself differently.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shatner");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "If saving money is wrong, I don't want to be right!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Karcher");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "If the money we donate helps one child or can ease the pain of one parent, those funds are well spent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle Onassis");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "If women didn't exist, all the money in the world would have no meaning.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. Paul Getty");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "If you can count your money, you don't have a billion dollars.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodore White");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "If you make a living, if you earn your own money, you're free - however free one can be on this planet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Randolph Hearst");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "In suggesting gifts: Money is appropriate, and one size fits all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Earl Wilson");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Isn't it a shame that future generations can't be here to see all the wonderful things we're doing with their money?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Billy Idol");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "It doesn't matter about money; having it, not having it. Or having clothes, or not having them. You're still left alone with yourself in the end.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Taylor Caldwell");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "It is a waste of money to help those who show no desire to help themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phyllis Diller");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Never go to bed mad. Stay up and fight.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Jean Nathan");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "No man can think clearly when his fists are clenched.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Butler Yeats");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "One should not lose one's temper unless one is certain of getting more and more angry to the end.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Austin O'Malley");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Revenge is often like biting a dog because the dog bit you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Barkley");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Somebody hits me, I'm going to hit him back. Even if it does look like he hasn't eaten in a while.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Laurence J. Peter");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Speak when you are angry - and you'll make the best speech you'll ever regret.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Speak when you are angry and you will make the best speech you will ever regret.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bede Jarrett");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "The world needs anger. The world often continues to allow evil because it isn't angry enough.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plato");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "There are two things a person should never be angry at, what they can help, and what they cannot.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "There is nothing that so much gratifies an ill tongue as when it finds an angry heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Russell Lowell");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Usually when people are sad, they don't do anything. They just cry over their condition. But when they get angry, they bring about a change.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Whatever is begun in anger ends in shame.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Confucius");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "When anger rises, think of the consequences.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "When angry count to ten before you speak. If very angry, count to one hundred.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "When angry, count to four; when very angry, swear.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tad Williams");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Never make your home in a place. Make a home for yourself inside your own head. You'll find what you need to furnish it - memory, friends you can trust, love of learning, and other such things. That way it will go with you wherever you journey.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zig Ziglar");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "People who have good relationships at home are more effective in the marketplace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anthea Turner");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Run a home like you would a small business and treat it with the same seriousness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Tusser");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Seek home for rest, for home is best.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Amy Grant");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Since I travel so much, it's always great to be home. There's nothing like getting to raid my own refrigerator at two in the morning.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kin Hubbard");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "The fellow that owns his own home is always just coming out of a hardware store.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Ellery Channing");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "The home is the chief school of human virtues.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Le Corbusier");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "The home should be the treasure chest of living.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Coke");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "The home to everyone is to him his castle and fortress, as well for his defence against injury and violence, as for his repose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adolf Loos");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "The house has to please everyone, contrary to the work of art which does not. The work is a private matter for the artist. The house is not.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Ginola");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "The prospect of going home is very appealing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edgar Watson Howe");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "The worst feeling in the world is the homesickness that comes over a man occasionally when he is at home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Smedley Butler");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "There are only two things we should fight for. One is the defense of our homes and the other is the Bill of Rights.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jane Austen");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "There is nothing like staying at home for real comfort.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rosalynn Carter");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "There is nothing more important than a good, safe, secure home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kenny Guinn");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "There is something permanent, and something extremely profound, in owning a home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jill Scott");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "To be a queen of a household is a powerful thing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Pataki");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "We know that when people are safe in their homes, they are free to pursue their dream for a brighter economic future for themselves and their families.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tony Stewart");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "When I go home, its an easy way to be grounded. You learn to realize what truly matters.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sam Ewing");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "When you finally go back to your old home, you find it wasn't the old home you missed but your childhood.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "In the first place, God made idiots. That was for practice. Then he made school boards.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walter Annenberg");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "In the world today, a young lady who does not have a college education just is not educated.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Allen Klein");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "It has been said that 80% of what people learn is visual.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Green Ingersoll");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "It is a thousand times better to have common sense without education than to have education without common sense.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aeschylus");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "It is always in season for old men to learn.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "It is the mark of an educated mind to be able to entertain a thought without accepting it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Hamilton");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Learn to think continentally.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Miller");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Learning is a result of listening, which in turn leads to even better listening and attentiveness to the other person. In other words, to learn from the child, we must have empathy, and empathy grows as we learn.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abigail Adams");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Learning is not attained by chance, it must be sought for with ardor and diligence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Learning, n. The kind of ignorance distinguishing the studious.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John W. Gardner");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Much education today is monumentally ineffective. All too often we are giving young people cut flowers when we should be teaching them to grow their own plants.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert K. Chesterton");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "No man who worships education has got the best out of education... Without a gentle contempt for education no man's education is complete.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emma Goldman");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "No one has yet realized the wealth of sympathy, the kindness and generosity hidden in the soul of a child. The effort of every true education should be to unlock that treasure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brad Henry");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Reading builds the educated and informed electorate so vital to our democracy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ezra Pound");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Real education must ultimately be limited to men who insist on knowing, the rest is mere sheep-herding.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wendell Phillips");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Responsibility educates.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Judy Chicago");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "So women are at the beginning of building a language, and not all women are conscious of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Anthony");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Some people drink from the fountain of knowledge, others just gargle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Pope");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Some people will never learn anything, for this reason, because they understand everything too soon.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Santayana");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "The great difficulty in education is to get experience out of ideas.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Linus Torvalds");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Microsoft isn't evil, they just make really crappy operating systems.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elbert Hubbard");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "One machine can do the work of fifty ordinary men. No machine can do the work of one extraordinary man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alvin Toffler");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Our technological powers increase, but the side effects and potential hazards also escalate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Nader");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "People are stunned to hear that one company has data files on 185 million Americans.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Kay");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "People who are really serious about software should make their own hardware.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Larry Wall");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Real programmers can write assembly code in any language.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Perry Barlow");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Relying on the government to protect your privacy is like asking a peeping tom to install your window blinds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tim Berners-Lee");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Sites need to be able to interact in one single, universal space.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Arp");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Soon silence will have passed into legend. Man has turned his back on silence. Day after day he invents machines and devices that increase noise and distract humanity from the essence of life, contemplation, meditation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jaron Lanier");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Style used to be an interaction between the human soul and tools that were limiting. In the digital era, it will have to come from the soul alone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aldous Huxley");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Technological progress has merely provided us with more efficient means for going backwards.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jared Diamond");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Technology has to be invented or adopted.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Freeman Dyson");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Technology is a gift of God. After the gift of life it is perhaps the greatest of God's gifts. It is the mother of civilizations, of arts and of sciences.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodor Adorno");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Technology is making gestures precise and brutal, and with them men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel J. Boorstin");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Technology is so much fun but we can drown in our technology. The fog of information can drive out knowledge.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Max Frisch");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Technology is the knack of so arranging the world that we don't have to experience it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carrie P. Snow");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Technology... is a queer thing. It brings you great gifts with one hand, and it stabs you in the back with the other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Telephone, n. An invention of the devil which abrogates some of the advantages of making a disagreeable person keep his distance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fred Allen");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Television is a medium because anything well done is rare.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Gibson");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "The 'Net is a waste of time, and that's exactly what's right about it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Blaise Pascal");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is neither without us nor within us. It is in God, both without us and within us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eleanor Roosevelt");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is not a goal; it is a by-product.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dalai Lama");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is not something ready made. It comes from your own actions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Schweitzer");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is nothing more than good health and a bad memory.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ayn Rand");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is that state of consciousness which proceeds from the achievement of one's values.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Austin O'Malley");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is the harvest of a quiet eye.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don Marquis");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is the interval between periods of unhappiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Green Ingersoll");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is the only good. The time to be happy is now. The place to be happy is here. The way to be happy is to make others so.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is when what you think, what you say, and what you do are in harmony.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Levant");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness isn't something you experience; it's something you remember.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness makes up in height for what it lacks in length.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Smith");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness never lays its finger on its pulse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Barrymore");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness often sneaks in through a door you didn't know you left open.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pierre Corneille");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness seems made to be shared.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrei Platonov");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness will come from materialism, not from meaning.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness: an agreeable sensation arising from contemplating the misery of another.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leon Kass");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "If you have easy self-contentment, you might have a very, very cheap source of happiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Brainerd");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "If you hope for happiness in the world, hope for it from God, and not from the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maurice Chevalier");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "If you wait for the perfect moment when all is safe and assured, it may never arrive. Mountains will not be climbed, races won, or lasting happiness achieved.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Bach");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "If your happiness depends on what somebody else does, I guess you do have a problem.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Blaise Pascal");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is neither without us nor within us. It is in God, both without us and within us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eleanor Roosevelt");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is not a goal; it is a by-product.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dalai Lama");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is not something ready made. It comes from your own actions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Schweitzer");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is nothing more than good health and a bad memory.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ayn Rand");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is that state of consciousness which proceeds from the achievement of one's values.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Austin O'Malley");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is the harvest of a quiet eye.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don Marquis");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is the interval between periods of unhappiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Green Ingersoll");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is the only good. The time to be happy is now. The place to be happy is here. The way to be happy is to make others so.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is when what you think, what you say, and what you do are in harmony.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Levant");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness isn't something you experience; it's something you remember.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness makes up in height for what it lacks in length.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Smith");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness never lays its finger on its pulse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Barrymore");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness often sneaks in through a door you didn't know you left open.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pierre Corneille");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness seems made to be shared.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrei Platonov");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness will come from materialism, not from meaning.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness: an agreeable sensation arising from contemplating the misery of another.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leon Kass");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "If you have easy self-contentment, you might have a very, very cheap source of happiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Brainerd");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "If you hope for happiness in the world, hope for it from God, and not from the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maurice Chevalier");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "If you wait for the perfect moment when all is safe and assured, it may never arrive. Mountains will not be climbed, races won, or lasting happiness achieved.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Bach");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "If your happiness depends on what somebody else does, I guess you do have a problem.");
		db.insert("citations", AUTHOR, cv);

		//call the rest of citation db filling
		onCreate2(db);
		onCreate3(db);
		onCreate4(db);
		onCreate5(db);
		
		
		db.execSQL("CREATE TABLE topics (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT);");
		// fill values to topics
		cv = new ContentValues();
		
		cv.put(NAME, "All");
		db.insert("topics", NAME, cv);
		
		cv.put(NAME, "Age");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Anger");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Architecture");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Art");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Beauty");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Birthday");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Business");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Car");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Change");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Computers");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Dad");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Dating");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Death");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Design");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Diet");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Dreams");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Education");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Environmental");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Equality");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Experience");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Faith");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Family");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Finance");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Fitness");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Food");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Forgiveness");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Friendship");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Funny");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Gardening");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Government");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Graduation");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Happiness");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Health");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "History");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Home");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Humor");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Imagination");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Inspirational");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Intelligence");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Leadership");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Legal");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Life");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Love");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Marriage");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Medical");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Men");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Mom");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Money");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Motivational");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Movies");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Music");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Nature");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Parenting");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Patriotism");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Peace");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Pet");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Poetry");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Politics");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Power");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Religion");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Science");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Society");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Sports");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Success");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Technology");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Teen");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Time");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Travel");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Trust");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "War");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Wedding");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Wisdom");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Women");
		db.insert("topics", NAME, cv);

		cv.put(NAME, "Work");
		db.insert("topics", NAME, cv);
		
		

	}
	public void onCreate4(SQLiteDatabase db) {
		ContentValues cv = new ContentValues();
		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I remember when the candle shop burned down. Everyone stood around singing 'Happy Birthday.'");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doc Hastings");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "137 years later, Memorial Day remains one of America's most cherished patriotic observances. The spirit of this day has not changed - it remains a day to honor those who died defending our freedom and democracy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George William Curtis");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "A man's country is not a certain area of land, of mountains, rivers, and woods, but it is a principle and patriotism is loyalty to that principle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Ralph Inge");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "A nation is a society united by a delusion about its ancestry and by common hatred of its neighbours.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Vaughan");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "A real patriot is the fellow who gets a parking ticket and rejoices that the system works.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Blaise Pascal");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Can anything be stupider than that a man has the right to kill me because he lives on the other side of a river and his ruler has a quarrel with mine, though I have not quarrelled with him?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Howard Zinn");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Dissent is the highest form of patriotism.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dale Carnegie");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Each nation feels superior to other nations. That breeds patriotism - and wars.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Walpole");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Gentlemen have talked a great deal of patriotism. A venerable word, when duly practiced.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Washington");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Guard against the impostures of pretended patriotism.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Heroism on command, senseless violence, and all the loathsome nonsense that goes by the name of patriotism - how passionately I hate them!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Diogenes");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "I am not an Athenian or a Greek, but a citizen of the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Ignatieff");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "I distinguish, between nationalism and patriotism.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bob Riley");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "I have long believed that sacrifice is the pinnacle of patriotism.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eugene V. Debs");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "I have no country to fight for; my country is the earth, and I am a citizen of the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James A. Baldwin");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "I love America more than any other country in this world, and, exactly for this reason, I insist on the right to criticize her perpetually.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nathan Hale");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "I only regret that I have but one life to lose for my country.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edith Cavell");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "I realize that patriotism is not enough. I must have no hatred or bitterness towards anyone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Galloway");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "I'm an advocate of the great Dr. Johnson, the English man of letters who said that patriotism was the last refuge of the scoundrel.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Voltaire");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "It is lamentable, that to be a good patriot one must become the enemy of the rest of mankind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur C. Clarke");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "It is not easy to see how the more extreme forms of nationalism can long survive when men have seen the Earth in its true perspective as a single small globe against the stars.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "A mother's arms are made of tenderness and children sleep soundly in them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Honore de Balzac");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "A mother's happiness is like a beacon, lighting up the future but reflected also on the past in the guise of fond memories.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emma Bonino");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "A woman must combine the role of mother, wife and politician.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harriet Ann Jacobs");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Always it gave me a pang that my children had no lawful claim to a name.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sophocles");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Children are the anchors of a mother's life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anita Baker");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Completeness? Happiness? These words don't come close to describing my emotions. There truly is nothing I can say to capture what motherhood means to me, particularly given my medical history.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Camille Paglia");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Every man must define his identity against his mother. If he does not, he just falls back into her and is swallowed up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ashley Scott");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Everyone checks out my mom. My mom's hot.");
		db.insert("citations", AUTHOR, cv);

		

		cv.put(AUTHOR, "Victor Cousin");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "All truly historical peoples have an idea they must realize, and when they have sufficiently exploited it at home, they export it, in a certain way, by war; they make it tour the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James K. Polk");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Although... the Chief Magistrate must almost of necessity be chosen by a party and stand pledged to its principles and measures, yet in his official action he should not be the President of a party only, but of the whole people of the United States.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George S. Patton");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Americans love to fight. All real Americans love the sting of battle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Manchester");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "An Edwardian lady in full dress was a wonder to behold, and her preparations for viewing were awesome.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dante Alighieri");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Be as a tower firmly set; Shakes not its top for any blast that blows.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tacitus");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Be assured those will be thy worst enemies, not to whom thou hast done evil, but who have done evil to thee. And those will be thy best friends, not to whom thou hast done good, but who have done good to thee.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lyndon B. Johnson");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Being president is like being a jackass in a hailstorm. There's nothing to do but to stand there and take it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Beware of endeavoring to become a great man in a hurry. One such attempt in ten thousand may succeed. These are fearful odds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry B. Adams");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Chaos often breeds life, when order breeds habit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold J. Toynbee");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Civilization is a movement and not a condition, a voyage and not a harbor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas MacArthur");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Could I have but a line a century hence crediting a contribution to the advance of peace, I would yield every honor which has been accorded by war.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Kennedy");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Dad, I'm in some trouble. There's been an accident and you're going to hear all sorts of things about me from now on. Terrible things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Thatcher");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Democratic nations must try to find ways to starve the terrorist and the hijacker of the oxygen of publicity on which they depend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mao Tse-Tung");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Despise the enemy strategically, but take him seriously tactically.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward R. Murrow");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Difficulty is the excuse history never accepts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. P. Morgan");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "A man always has two reasons for doing anything: a good reason and the real reason.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Max Lucado");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "A man who wants to lead the orchestra must turn his back on the crowd.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Rohn");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Affirmation without discipline is the beginning of delusion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steve Jobs");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Be a yardstick of quality. Some people aren't used to an environment where excellence is expected.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. Clement Stone");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Be careful the environment you choose for it will shape you; be careful the friends you choose for you will become like them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Leonard");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Clarity affords focus.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Sher");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Doing is a quantum leap from imagining.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John D. Rockefeller");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Don't be afraid to give up the good to go for the great.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ford");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Don't find fault, find a remedy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Covey");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Effective leadership is putting first things first. Effective management is discipline, carrying it out.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Kay Ash");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Every silver lining has a cloud.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nolan Bushnell");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Everyone who's ever taken a shower has an idea. It's the person who gets out of the shower, dries off and does something about it who makes a difference.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Scott McNealy");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Get the best people and train them well.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Hopkins");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Getting in touch with your true self must be your first priority.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Rohn");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Give whatever you are doing and whoever you are with the gift of your attention.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. P. Morgan");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Go as far as you can see; when you get there, you'll be able to see farther.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Debbi Fields");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Good enough never is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Hawken");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Good management is the art of making problems so interesting and their solutions so constructive that everyone wants to get to work and deal with them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sun Tzu");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "He who is prudent and lies in wait for an enemy who is not, will be victorious.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sam Walton");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "High expectations are the key to everything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Leacock");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "A half truth, like half a brick, is always more forcible as an argument than a whole one. It carries better.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Spurgeon");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "A lie can travel half way around the world while the truth is putting on its shoes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harold MacMillan");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "A man who trusts nobody is apt to be the kind of man nobody trusts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Grantland Rice");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Depend upon yourself. Make your judgement trustworthy by trusting it. You can develop good judgement as you do the muscles of your body - by judicious, daily exercise. To be known as a man of sound judgement will be much in your favor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pat Boone");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Don't trust anyone over 30.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George MacDonald");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Few delights can equal the presence of one whom we trust utterly.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bo Bennett");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "For every good reason there is to lie, there is a better reason to tell the truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Renault");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "How can people trust the harvest, unless they see it sown?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kiana Tom");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "I always trust my gut reaction; it's always right.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harvey S. Firestone");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "I believe fundamental honesty is the keystone of business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cher");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "I can trust my friends These people force me to examine myself, encourage me to grow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paris Hilton");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "I get along with guys; most of my friends are guys. It's easier to trust men sometimes. I only have a few close girlfriends that I trust.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ednita Nazario");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "I have to trust what I do and then do it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frances McDormand");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "I never trusted good-looking boys.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "I repeat... that all power is a trust; that we are accountable for its exercise; that from the people and for the people all springs, and all must exist.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stanley Baldwin");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "I would rather trust a woman's instinct than a man's reason.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rita Dove");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "If we really want to be full and generous in spirit, we have no choice but to trust at some level.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Wilkerson");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "In these times, God's people must trust him for rest of body and soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jeff Goldblum");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "It's a delight to trust somebody so completely.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isaac Watts");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Learning to trust is one of life's most difficult tasks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hedy Lamarr");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "A good painting to me has always been like a friend. It keeps me company, comforts and inspires.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Edward Moore");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "A great artist is always before his time or behind it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Camus");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "A guilty conscience needs to confess. A work of art is a confession.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michelangelo");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "A man paints with his brains and not with his hands.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pierre Bonnard");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "A painting that is well composed is half finished.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Horace");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "A picture is a poem without words.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "A picture is worth a thousand words.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Moore");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "A sculptor is a person who is interested in the shape of things, a poet in words, a musician by sounds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "A work of art is the unique result of a unique temperament.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gertrude Stein");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "A writer should write with his eyes and a painter paint with his ears.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Capp");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Abstract art: a product of the untalented sold by the unprincipled to the utterly bewildered.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marshall McLuhan");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Ads are the cave art of the twentieth century.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marshall McLuhan");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Advertising is the greatest art form of the 20th century.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Federico Fellini");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "All art is autobiographical. The pearl is the oyster's autobiography.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lucius Annaeus Seneca");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "All art is but imitation of nature.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Horton Cooley");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "An artist cannot fail; it is a success to be one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Santayana");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "An artist is a dreamer consenting to dream of the actual world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Miller");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "An artist is always alone - if he is an artist. No, what the artist needs is loneliness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edgard Varese");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "An artist is never ahead of his time but most people are far behind theirs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Whistler");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "An artist is not paid for his labor but for his vision.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adlai E. Stevenson");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "A beauty is a woman you notice; a charmer is one who notices you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Keats");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "A thing of beauty is a joy forever: its loveliness increases; it will never pass into nothingness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Meredith");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "A witty woman is a treasure; a witty beauty is a power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Roiphe");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "A woman whose smile is open and whose expression is glad has a kind of beauty no matter what she wears.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alex Comfort");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "A women's greatest asset is her beauty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty and folly are old companions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dante Alighieri");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty awakens the soul to act.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Munshi Premchand");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty doesn't need ornaments. Softness can't bear the weight of ornaments.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kevyn Aucoin");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty has a lot to do with character.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Hume");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty in things exists in the mind which contemplates them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ovid");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is a fragile gift.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johann Wolfgang von Goethe");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is a manifestation of secret natural laws, which otherwise would have been hidden from us forever.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is all very well at first sight; but who ever looks at it when it has been in the house three days?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is eternity gazing at itself in a mirror.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johann Wolfgang von Goethe");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is everywhere a welcome guest.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. G. Wells");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is in the heart of the beholder.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eugene Ormandy");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is less important than quality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emily Dickinson");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is not caused. It is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Overbury");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is only skin deep.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Amanda Peet");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is only skin deep. If you go after someone just because she's beautiful but don't have anything to talk about, it's going to get boring fast. You want to look beyond the surface and see if you can have fun or if you have anything in common with this person.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Karl Wilhelm Friedrich Schlegel");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "A family can develop only with a loving woman as its center.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles R. Swindoll");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "A family is a place where principles are hammered and honed on the anvil of everyday living.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "A happy family is but an earlier heaven.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walt Disney");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "A man should never neglect his family for business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kathleen Turner");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "A woman can plan when to have her family and how to support a family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom DeLay");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "A woman can take care of the family. It takes a man to provide structure, to provide stability.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mel Gibson");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "A woman should be home with the children, building that home and making sure there's a secure family atmosphere.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Chaim Potok");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "All of us grow up in particular realities - a home, family, a clan, a small town, a neighborhood. Depending upon how we're brought up, we are either deeply aware of the particular reading of reality into which we are born, or we are peripherally aware of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christina Milian");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "Also, my mom and family are very important to me and I know that this is not expected.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mitt Romney");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "America cannot continue to lead the family of nations around the world if we suffer the collapse of the family here at home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Josh Billings");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "As a general thing, when a woman wears the pants in a family, she has a good right to them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gwen Stefani");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "At a certain point I'm going to want to have a family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elizabeth Fishel");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "Both within the family and without, our sisters hold up our mirrors: our images of who we are and of who we can dare to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Bush");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "Cherish your human connections - your relationships with friends and family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charlie Sheen");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "Dad kept us out of school, but school comes and goes. Family is forever.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vartan Gregorian");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "Dignity is not negotiable. Dignity is the honor of the family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael J. Fox");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "Family is not an important thing, it's everything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Princess Diana");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "Family is the most important thing in the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Drabble");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "Family life itself, that safest, most traditional, most approved of female choices, is not a sanctuary: It is, perpetually, a dangerous place.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Ogden Stiers");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "Family means no one gets left behind or forgotten.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Catherine Deneuve");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "A woman has to be intelligent, have charm, a sense of humor, and be kind. It's the same qualities I require from a man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert K. Chesterton");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "A woman uses her intelligence to find reasons to support her intuition.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Hill");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Action is the real measure of intelligence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johann Wolfgang von Goethe");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "All intelligent thoughts have already been thought; what is necessary is only to try to think them again.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lena Horne");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Always be smarter than the people who hire you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Camus");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "An intellectual is someone whose mind watches itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Alda");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Be as smart as you can, but remember that it is always better to be wise than to be smart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Julius Charles Hare");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Be what you are. This is the first step toward becoming better than you are.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Janis Joplin");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Being an intellectual creates a lot of questions and no answers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Anton Wilson");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Belief is the death of intelligence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Character is higher than intellect. A great soul will be strong to live as well as think.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Voltaire");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Common sense is not so common.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Schiller");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Every true genius is bound to be naive.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Georg C. Lichtenberg");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Everyone is a genius at least once a year. The real geniuses simply have their bright ideas closer together.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ford");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Failure is simply the opportunity to begin again, this time more intelligently.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Josh Billings");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Genius ain't anything more than elegant common sense.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Genius always finds itself a century too early.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. B. White");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Genius is more often found in a cracked pot than in a whole one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Whistler");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "I can't tell you if genius is hereditary, because heaven has granted me no offspring.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "I choose my friends for their good looks, my acquaintances for their good characters, and my enemies for their intellects. A man cannot be too careful in the choice of his enemies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lou Holtz");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "A bird doesn't sing because it has an answer, it sings because it has a song.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Butler");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "A hen is only an egg's way of making another egg.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Bronte");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "A light wind swept over the corn, and all nature laughed in the sunshine.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Reiner");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "A lot of people like snow. I find it to be an unnecessary freezing of water.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walt Whitman");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "A morning-glory at my window satisfies me more than the metaphysics of books.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hal Borland");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "A woodland in full color is awesome as a forest fire, in magnitude at least, but a single tree is like a dancing tongue of flame to warm the heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. G. Wells");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Adapt or perish, now as ever, is nature's inexorable imperative.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Russell Baker");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Ah, summer, what power you have to make us suffer and like it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Lincoln");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "All my life I have tried to pluck a thistle and plant a flower wherever the flower would grow in thought and mind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Browne");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "All things are artificial, for nature is the art of God.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Toni Morrison");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "All water has a perfect memory and is forever trying to get back to where it was.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anais Nin");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "And the day came when the risk to remain tight in a bud was more painful than the risk it took to blossom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Moore");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "And the heart that is soonest awake to the flowers is always the first to be touch'd by the thorns.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "And this, our life, exempt from public haunt, finds tongues in trees, books in the running brooks, sermons in stones, and good in everything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woody Allen");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "As the poet said, 'Only God can make a tree,' probably because it's so hard to figure out how to get the bark on.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Camus");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Autumn is a second spring when every leaf is a flower.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert White");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Bats drink on the wing, like swallows, by sipping the surface, as they play over pools and streams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Langston Hughes");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Beauty for some provides escape, who gain a happiness in eyeing the gorgeous buttocks of the ape or Autumn sunsets exquisitely dying.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roger Tory Peterson");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Birds have wings; they're free; they can fly where they want when they want. They have the kind of mobility many people envy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rose Kennedy");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Birds sing after a storm; why shouldn't people feel as free to delight in whatever remains to them?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Howard Clark");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "A baby is born with a need to be loved - and never outgrows it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Honore de Balzac");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "A mother who is really a mother is never free.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robbie Coltrane");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Believe me, my children have more stamina than a power station.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bob Filner");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Childhood obesity is best tackled at home through improved parental involvement, increased physical exercise, better diet and restraint from eating.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Jung");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Children are educated by what the grown-up is and not by his talk.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Todd Tiahrt");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Children that are raised in a home with a married mother and father consistently do better in every measure of well-being than their peers who come from divorced or step-parent, single-parent, cohabiting homes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Russell Baker");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Don't try to make children grow up to be like you, or they may do it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles R. Swindoll");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Each day of our lives we make deposits in the memory banks of our children.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Milton Sapirstein");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Education, like neurosis, begins at home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ron Taffel");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Even as kids reach adolescence, they need more than ever for us to watch over them. Adolescence is not about letting go. It's about hanging on during a very bumpy ride.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Liam Neeson");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Every cliche about kids is true; they grow up so quickly, you blink and they're gone, and you have to spend the time with them now. But that's a joy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Thicke");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Family involvement is a valuable thing and playing together actively can be the '90s version of it. Instead of just watching, you can do it together... something we don't spend enough time on. We can motivate and excite each other about fitness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Cormier");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Family life was wonderful. The streets were bleak. The playgrounds were bleak. But home was always warm. My mother and father had a great relationship. I always felt 'safe' there.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Allan Bloom");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Fathers and mothers have lost the idea that the highest aspiration they might have for their children is for them to be wise... specialized competence and success are all that they can imagine.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doug Larson");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Few things are more satisfying than seeing your children have teenagers of their own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miriam Makeba");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Girls are the future mothers of our society, and it is important that we focus on their well-being.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Matthew Broderick");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Having a baby changes the way you view your in-laws. I love it when they come to visit now. They can hold the baby and I can go out.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ryan Phillippe");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Home life's great, man. The kids are great, happy and healthy. I've reached this sort of wonderful precipice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Voltaire");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "How pleasant it is for a father to sit at his child's board. It is like an aged man reclining under the shadow of an oak which he has planted.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Beverly Cleary");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I don't think children's inner feelings have changed. They still want a mother and father in the very same house; they want places to play.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Antoine de Saint-Exupery");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "A rock pile ceases to be a rock pile the moment a single man contemplates it, bearing within him the image of a cathedral.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orison Swett Marden");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "All men who have achieved great things have been great dreamers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brian Tracy");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "All successful people men and women are big dreamers. They imagine what their future could be, ideal in every respect, and then they work every day toward their distant vision, that goal or purpose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sun Tzu");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Can you imagine what I would do if I could do all I can?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Addison");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Everything that is new or uncommon raises a pleasure in the imagination, because it fills the soul with an agreeable surprise, gratifies its curiosity, and gives it an idea of which it was not before possessed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jessamyn West");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Fiction reveals truths that reality obscures.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Chris Brown");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I always imagined I could be what I wanted to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elinor Wylie");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I am better able to imagine hell than heaven; it is my inheritance, I suppose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Nivio Zarlenga");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I am imagination. I can see what the eyes cannot see. I can hear what the ears cannot hear. I can feel what the heart cannot feel.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Duane Michals");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I believe in the imagination. What I cannot see is infinitely more important than what I can see.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Imran Khan");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I believed in myself. I never imagined myself as just an ordinary player.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I cannot imagine a God who rewards and punishes the objects of his creation and is but a reflection of human frailty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ursula K. Le Guin");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I doubt that the imagination can be suppressed. If you truly eradicated it in a child, he would grow up to be an eggplant.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jorge Luis Borges");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I have always imagined that Paradise will be a kind of library.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "e. e. cummings");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I imagine that yes is the only living thing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dr. Seuss");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I like nonsense, it wakes up the brain cells. Fantasy is a necessary ingredient in living, it's a way of looking at life through the wrong end of a telescope. Which is what I do, and that enables you to laugh at life's realities.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Calvin Trillin");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I never did very well in math - I could never seem to persuade the teacher that I hadn't meant my answers literally.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pablo Picasso");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I paint objects as I think them, not as I see them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michelangelo");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I saw the angel in the marble and carved until I set him free.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Simon Pegg");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "I used to lie in bed in my flat and imagine what would happen if there was a zombie attack.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John B. S. Haldane");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "A fairly bright boy is far more intelligent and far better company than the average adult.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "G. Stanley Hall");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Adolescence is a new birth, for the higher and more completely human traits are now born.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carol Burnett");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Adolescence is just one big walking pimple.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louise J. Kaplan");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Adolescence is the conjugator of childhood and adulthood.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Pipher");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Adolescence is when girls experience social pressure to put aside their authentic selves and to display only a small portion of their gifts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jena Malone");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Adolescence isn't just about prom or wearing sparkly dresses.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Virginia Satir");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Adolescents are not monsters. They are just people trying to learn how to make it among the adults in the world, who are probably not so sure themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joan Chen");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "All teenagers have this desire to somehow run away.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johnny Depp");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "As a teenager I was so insecure. I was the type of guy that never fitted in because he never dared to choose. I was convinced I had absolutely no talent at all. For nothing. And that thought took away all my ambition too.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fran Lebowitz");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "As a teenager you are at the last stage in your life when you will be happy to hear that the phone is for you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joe Bob Briggs");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "As I've said many times, the single most oppressed class in America right now is the teenager.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Milla Jovovich");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "At the age of 16 I was already dreaming of having a baby because I felt myself to be an adult, but my mum forbid it. Right now, I feel like a teenager and I want to have fun for one or two more years before starting a family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sophia Bush");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Being a teenager is an amazing time and a hard time. It's when you make your best friends - I have girls who will never leave my heart and I still talk to. You get the best and the worst as a teen. You have the best friendships and the worst heartbreaks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wayne Kramer");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "But when I was a teenager, the idea of spending the rest of my life in a factory was real depressing. So the idea that I could become a musician opened up some possibilities I didn't see otherwise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clyde Tombaugh");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Can you imagine young people nowadays making a study of trigonometry for the fun of it? Well I did.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louise J. Kaplan");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Children, even infants, are capable of sympathy. But only after adolescence are we capable of compassion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barry Gibb");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Everybody is a teenage idol.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Elkind");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Friendships in childhood are usually a matter of chance, whereas in adolescence they are most often a matter of choice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Terry Brooks");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Growing up, I didn't have a lot of toys, and personal entertainment depended on individual ingenuity and imagination - think up a story and go live it for an afternoon.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Claire Danes");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Growing up, I wanted desperately to please, to be a good girl.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edith Head");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "A designer is only as good as the star who wears her clothes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Antoine de Saint-Exupery");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "A designer knows he has achieved perfection not when there is nothing left to add, but when there is nothing left to take away.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Niklaus Wirth");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "A good designer must rely on experience, on precise, logic thinking; and on pedantic exactness. No magic will do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Manolo Blahnik");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "About half my designs are controlled fantasy, 15 percent are total madness and the rest are bread-and-butter designs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steve Ballmer");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Accessible design is good design.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pierre Bonnard");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Color does not add a pleasant quality to design - it reinforces it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miguel de Cervantes");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Delay always breeds danger; and to protract a great design is often to ruin it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Geoffrey Beene");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Design is an unknown.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ivan Chermayeff");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Design is directed toward human beings. To design is to solve human problems by identifying them and executing the best solution.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Rand");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Design is everything. Everything!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Issey Miyake");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Design is not for philosophy it's for life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steve Jobs");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Design is not just what it looks like and feels like. Design is how it works.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louis Kahn");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Design is not making beauty, beauty emerges from selection, affinities, integration, love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jane Jacobs");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Design is people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Rand");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Design is the method of putting form and content together. Design, just as art, has multiple definitions; there is no single definition. Design can be art. Design can be aesthetics. Design is so simple, that's why it is so complicated.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas J. Watson");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Design must reflect the practical and aesthetic in business but above all... good design must primarily serve people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ron Johnson");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Design works if it's authentic, inspired, and has a clear point of view. It can't be a collection of input.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tyra Banks");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Designers are very fickle. I never wanted to be a victim of that. You're in one minute, out the next.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kenzo Tange");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Designs of purely arbitrary nature cannot be expected to last long.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Collier");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Every contrivance of man, every tool, every instrument, every utensil, every article designed for use, of each and every kind, evolved from a very simple beginnings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Levine");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "A lot of people get impatient with the pace of change.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bruce Barton");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Action and reaction, ebb and flow, trial and error, change - this is the rhythm of living. Out of our over-confidence, fear; out of our fear, clearer vision, fresh hope. And out of hope, progress.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ellen Glasgow");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "All change is not growth, as all movement is not forward.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anatole France");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "All changes, even the most longed for, have their melancholy; for what we leave behind us is a part of ourselves; we must die to one life before we can enter another.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dean Acheson");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Always remember that the future comes one day at a time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold Bennett");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Any change, even a change for the better, is always accompanied by drawbacks and discomforts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Irene Peter");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Anyone who thinks there's safety in numbers hasn't looked at the stock market pages.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bertolt Brecht");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Because things are the way they are, things will not stay the way they are.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur Schopenhauer");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Change alone is eternal, perpetual, immortal.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack Welch");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Change before you have to.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nido Qubein");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Change brings opportunity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Change in all things is sweet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Billy Crystal");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Change is such hard work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Denise McCluggage");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Change is the only constant. Hanging on is the only sin.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Esther Dyson");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Change means that what was before wasn't perfect. People want things to be better.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry Emerson Fosdick");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Christians are supposed not merely to endure change, nor even to profit by it, but to cause it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Tolstoy");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Everyone thinks of changing the world, but no one thinks of changing himself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Reinhold Niebuhr");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "God grant me the serenity to accept the things I cannot change, the courage to change the things I can, and the wisdom to know the difference.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "He that will not apply new remedies must expect new evils; for time is the greatest innovator.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harold Wilson");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "He who rejects change is the architect of decay. The only human institution which rejects progress is the cemetery.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bob Hope");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A bank is a place that will lend you money if you can prove that you don't need it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Everett Dirksen");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A billion here, a billion there, and pretty soon you're talking about real money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ford");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A business that makes nothing but money is a poor business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stanley Weiser");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A fool and his money are lucky enough to get together in the first place.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Tusser");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A fool and his money are soon parted.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Bernstein");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A fool and his money get a lot of publicity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Publilius Syrus");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A good reputation is more valuable than money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Ruskin");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A little thought and a little kindness are often worth more than a great deal of money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Wendell Holmes, Jr.");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A man is usually more careful of his money than of his principles.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doyle Brunson");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A man with money is no match against a man on a mission.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joe Moore");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A simple fact that is hard to learn is that the time to save money is when you have some.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jonathan Swift");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A wise man should have money in his head, but not in his heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clare Boothe Luce");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "A woman's best protection is a little money of her own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle Onassis");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "After a certain point, money is meaningless. It ceases to be the goal. The game is what counts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Spike Milligan");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "All I ask is the chance to prove that money can't make me happy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ray Kroc");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "All money means to me is a pride in accomplishment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Curtis Carlson");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "All my life I knew that there was all the money you could want out there. All you have to do is go after it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Collier");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "All riches have their origin in mind. Wealth is in ideas - not money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gro Harlem Brundtland");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "An important lever for sustained action in tackling poverty and reducing hunger is money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Geffen");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Anybody who thinks money will make you happy, hasn't got money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "A man that studieth revenge keeps his own wounds green.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Morley");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "A man who has never made a woman angry is a failure in life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Fallows");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Always write angry letters to your enemies. Never mail them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cato");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "An angry man opens his mouth and shuts his eyes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Anger and intolerance are the enemies of correct understanding.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Eliot");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Anger and jealousy can no more bear to lose sight of their objects than love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Anger dwells only in the bosom of fools.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louis L'Amour");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Anger is a killing thing: it kills the man who angers, for each rage leaves him less than he had been before - it takes something from him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Horace");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Anger is a short madness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Green Ingersoll");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Anger is a wind which blows out the lamp of the mind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Anger is an acid that can do more harm to the vessel in which it is stored than to anything on which it is poured.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Anger is one of the sinews of the soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Anger makes dull men witty, but it keeps them poor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lucius Annaeus Seneca");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Anger, if not restrained, is frequently more hurtful to us than the injury that provokes it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Anybody can become angry - that is easy, but to be angry with the right person and to the right degree and at the right time and for the right purpose, and in the right way - that is not within everybody's power and is not easy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Kempis");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Be not angry that you cannot make others as you wish them to be, since you cannot make yourself as you wish to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maya Angelou");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Bitterness is like cancer. It eats upon the host. But anger is like fire. It burns it all clean.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard M. Nixon");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Don't get the impression that you arouse my anger. You see, one can only be angry with those he respects.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred A. Montapert");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Every time you get angry, you poison your own system.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roseanne Barr");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Experts say you should never hit your children in anger. When is a good time? When you're feeling festive?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Yunus Emre");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "A heart makes a good home for the friend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "A house is not a home unless it contains food and fire for the mind as well as the body.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Chase");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "A man's home is his wife's castle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Otis");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "A man's house is his castle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Jerome");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Any old place I can hang my hat is home sweet home to me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Thatcher");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Any woman who understands the problems of running a home will be nearer to understanding the problems of running a country.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sarah Ban Breathnach");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Be grateful for the home you have, knowing that at this moment, all you have is all you need.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Charity begins at home, but should not end there.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phillips Brooks");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Charity should begin at home, but should not stay there.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles M. Schulz");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Decorate your home. It gives the illusion that your life is more interesting than it really is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Meister Eckhart");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "God is at home, it's we who have gone out for a walk.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Angie Harmon");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Here's kind of my motto - if you're not happy at home, you're not happy anywhere else.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Henry Parkhurst");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Home interprets heaven. Home is heaven for beginners.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Rowland");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Home is any four walls that enclose the right person.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Anatole Grunwald");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Home is one's birthplace, ratified by memory.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Channing Pollock");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Home is the most popular, and will be the most enduring of all earthly establishments.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Billy Sunday");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Home is the place we love best and grumble the most.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Home is the place where, when you have to go there, they have to take you in.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pliny the Elder");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Home is where the heart is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Home life is no more natural to us than a cage is natural to a cockatoo.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Horace Mann");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "A human being is not attaining his full heights until he is educated.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "A. Bartlett Giamatti");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "A liberal education is at the heart of a civil society, and at the heart of a liberal education is the act of teaching.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "America is becoming so educated that ignorance will be a novelty. I will belong to the select few.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nicholas M. Butler");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "America is the best half-educated country in the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan K. Simpson");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "An educated man is thoroughly inoculated against humbug, thinks for himself and tries to give his thoughts, in speech or on paper, some style.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frederick The Great");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "An educated people can be easily governed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Russell Baker");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "An educated person is one who has learned that information almost always turns out to be at best incomplete and very often false, misleading, fictitious, mendacious - just dead wrong.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anatole France");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "An education isn't how much you have committed to memory, or even how much you know. It's being able to differentiate between what you know and what you don't.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ernest Dimnet");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Children have to be educated, but they have also to be left to educate themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clifford Stoll");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Data is not information, information is not knowledge, knowledge is not understanding, understanding is not wisdom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anthony J. D'Angelo");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Develop a passion for learning. If you do, you will never cease to grow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Everett");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is a better safeguard of liberty than a standing army.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Durant");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is a progressive discovery of our own ignorance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Ellison");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is all a matter of building bridges.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is an admirable thing, but it is well to remember from time to time that nothing that is worth knowing can be taught.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel J. Boorstin");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is learning what you didn't even know you didn't know.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Dewey");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is not preparation for life; education is life itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Butler Yeats");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is not the filling of a pail, but the lighting of a fire.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert K. Chesterton");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is simply the soul of a society as it passes from one generation to another.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is the ability to listen to almost anything without losing your temper or your self-confidence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Sagan");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "All of the books in the world contain no more information than is broadcast as video in a single large American city in a single year. Not all bits have equal value.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur C. Clarke");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Any sufficiently advanced technology is indistinguishable from magic.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dave Barry");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Bill Gates is a very rich man today... and do you want to know why? The answer is one word: versions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Larry Niven");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Building one space station for everyone was and is insane: we should have built a dozen.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jesse James Garrett");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Building technical systems involves a lot of hard work and specialized knowledge: languages and protocols, coding and debugging, testing and refactoring.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Graham Greene");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Champagne, if you are seeking the truth, is better than a lie detector. It encourages a man to be expansive, even reckless, while lie detectors are only a challenge to tell lies successfully.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred North Whitehead");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Civilization advances by extending the number of important operations which we can perform without thinking of them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Grove");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Congress will pass a law restricting public comment on the Internet to individuals who have spent a minimum of one hour actually accomplishing a specific task while on line.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Gibson");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Cyberspace. A consensual hallucination experienced daily by billions of legitimate operators, in every nation, by children being taught mathematical concepts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wietse Venema");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Defect-free software does not exist.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Boliska");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Do you realize if it weren't for Edison we'd be watching TV by candlelight?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Larry Wall");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Doing linear scans over an associative array is like trying to club someone to death with a loaded Uzi.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gertrude Stein");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Everybody gets so much information all day long that they lose their common sense.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard P. Feynman");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "For a successful technology, reality must take precedence over public relations, for Nature cannot be fooled.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wernher von Braun");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "For my confirmation, I didn't get a watch and my first pair of long pants, like most Lutheran boys. I got a telescope. My mother thought it would make the best gift.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stewart Alsop");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Gates is the ultimate programming machine. He believes everything can be defined, examined, reduced to essentials, and rearranged into a logical sequence that will achieve a particular goal.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mitchell Kapor");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Getting information off the Internet is like taking a drink from a fire hydrant.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jimmy Carter");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Globalization, as defined by rich people like us, is a very nice thing... you are talking about the Internet, you are talking about cell phones, you are talking about computers. This doesn't affect two-thirds of the people of the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "R. Buckminster Fuller");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Humanity is acquiring all the right technology for all the wrong reasons.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "I am sorry to say that there is too much point to the wisecrack that life is extinct on other planets because their scientists were more advanced than ours.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jane Austen");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "A large income is the best recipe for happiness I ever heard of.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bette Davis");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "A sure way to lose happiness, I found, is to want it at the expense of everything else.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Action may not always bring happiness; but there is no happiness without action.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baruch Spinoza");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "All happiness or unhappiness solely depends upon the quality of the object to which we are attached by love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lord Byron");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "All who joy would win must share it. Happiness was born a Twin.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baltasar Gracian");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Always leave something to wish for; otherwise you will be miserable from your very happiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lydia M. Child");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "An effort made for the happiness of others lifts above ourselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bertrand Russell");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Anything you're good at contributes to happiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Camus");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "But what is happiness except the simple harmony between a man and the life he leads?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Julian Casablancas");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Desire is individual. Happiness is common.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plutarch");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Do not speak of your happiness to one less fortunate than yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Walker");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Don't wait around for other people to be happy for you. Any happiness you get you've got to make yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Bach");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Every gift from a friend is a wish for your happiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bertolt Brecht");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Everyone chases after happiness, not noticing that happiness is right at their heels.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Future. That period of time in which our affairs prosper, our friends are true and our happiness is assured.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pearl S. Buck");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Growth itself contains the germ of happiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Rosten");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness comes only when we push our brains and hearts to the farthest reaches of which we are capable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Rooney");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness depends more on how life strikes you than on what happens.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness depends upon ourselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Butler");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness does not consist in self-love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mae West");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Between two evils, I always pick the one I never tried before.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bette Davis");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Brought up to respect the conventions, love had to end in marriage. I'm afraid it did.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "By trying we can easily endure adversity. Another man's, I mean.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fred Allen");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "California is a fine place to live - if you happen to be an orange.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "P. J. O'Rourke");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Cleanliness becomes more important when godliness is unlikely.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Cross country skiing is great if you live in a small country.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hesiod");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Do not let a flattering woman coax and wheedle you and deceive you; she is after your barn.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jay Leno");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Don't forget Mother's Day. Or as they call it in Beverly Hills, Dad's Third Wife Day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Benchley");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Drawing on my fine command of the English language, I said nothing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Carlin");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Electricity is really just organized lightning.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jerry Lewis");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Every man's dream is to be able to sink into the arms of a woman without also falling into her hands.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "P. J. O'Rourke");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Everybody knows how to raise children, except the people who have them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Milton Berle");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Experience is what you have after you've forgotten her name.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Fashions have done more harm than revolutions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Cosby");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Fatherhood is pretending the present you love most is soap-on-a-rope.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fran Lebowitz");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Food is an important part of a balanced diet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cathy Guisewite");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Food, love, career, and mothers, the four major guilt groups.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Get your facts first, then you can distort them as you please.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Go to Heaven for the climate, Hell for the company.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Naguib Mahfouz");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "God did not intend religion to be an exercise club.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Turner");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "My time is now.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Wanamaker");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "People who cannot find time for recreation are obliged sooner or later to find time for illness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Menachem Mendel Schneerson");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "So during those first moments of the day, which are yours and yours alone, you can circumvent these boundaries and concentrate fully on spiritual matters. And this gives you the opportunity to plan the time management of the entire day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Lincoln");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "The best thing about the future is that it comes one day at a time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tillie Olsen");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "The clock talked loud. I threw it away, it scared me what it talked.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "C. S. Lewis");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "The future is something which everyone reaches at the rate of 60 minutes an hour, whatever he does, whoever he is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Russell");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "The present is a point just passed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mason Cooley");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "The time I kill is killing me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Valery");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "The trouble with our times is that the future is not what it used to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles W. Chesnutt");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "There's time enough, but none to spare.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Lakein");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time = Life, Therefore, waste your time and waste of your life, or master your time and master your life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aeschylus");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time brings all things to pass.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Hardy");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time changes everything except something within us which is always surprised by change.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Max Frisch");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time does not change us. It just unfolds us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Austin Dobson");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time goes, you say? Ah, no! alas, time stays, we go.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Yevgeny Yevtushenko");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time has a way of demonstrating that the most stubborn are the most intelligent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time has been transformed, and we have changed; it has advanced and set us in motion; it has unveiled its face, inspiring us with bewilderment and exhilaration.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Faith Baldwin");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time is a dressmaker specializing in alterations.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time is but the stream I go a-fishing in.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time is money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elbert Hubbard");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Die, v.: To stop sinning suddenly.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bertolt Brecht");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Do not fear death so much but rather the inadequate life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Annie Lennox");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Dying is easy, it's living that scares me to death.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Bolt");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Even at our birth, death does but stand aside a little. And every day he looks towards us and muses somewhat to himself whether that day or the next he will draw nigh.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Morrie Schwartz");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Everything that gets born dies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Matthew Green");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Fling but a stone, the giant dies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Penn");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "For death is no more than a turning of us over from time to eternity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Juliette Binoche");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "For me, habit is just a synonym for death.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edvard Munch");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "From my rotting body, flowers shall grow and I am in them and that is eternity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Giovanni Falcone");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "He who doesn't fear death dies only once.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Clarke");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "He who is completely sanctified, or cleansed from all sin, and dies in this state, is fit for glory.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erik H. Erikson");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Healthy children will not fear life if their elders have integrity enough not to fear death.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. Robert Oppenheimer");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "I am become death, the destroyer of worlds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elie Wiesel");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "I decided to devote my life to telling the story because I felt that having survived I owe something to the dead. and anyone who does not remember betrays them again.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "I didn't attend the funeral, but I sent a nice letter saying I approved of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "T. S. Eliot");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "I had seen birth and death but had thought they were different.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clarence Darrow");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "I have never killed a man, but I have read many obituaries with great pleasure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "I saw few die of hunger; of eating, a hundred thousand.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Willa Cather");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "I shall not die of a cold. I shall die of having lived.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elisabeth Kubler-Ross");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "I've told my children that when I die, to release balloons in the sky to celebrate that I graduated. For me, death is a graduation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Flip Wilson");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Get well cards have become so humorous that if you don't get sick you're missing half the fun.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas W. Higginson");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Great men are rarely isolated mountain peaks; they are the summits of ranges.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Agnes Repplier");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor brings insight and tolerance. Irony brings a deeper and less friendly understanding.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Allen Klein");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor can alter any situation and help us cope at the very instant we are laughing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Agnes Repplier");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor distorts nothing, and only false gods are laughed off their earthly pedestals.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Allen Klein");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor does not diminish the pain - it makes the space around it get bigger.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Thurber");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is a serious thing. I like to think of it as one of our greatest earliest natural resources, which must be preserved at all cost.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward de Bono");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is by far the most significant activity of the human brain.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Thurber");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is emotional chaos remembered in tranquility.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Nye");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is everywhere, in that there's irony in just about anything a human does.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mel Brooks");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is just another defense against the universe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Langston Hughes");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is laughing at what you haven't got when you ought to have it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is mankind's greatest blessing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Irvin S. Cobb");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is merely tragedy standing on its head with its pants torn.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Morley");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is perhaps a sense of intellectual perspective: an awareness that some things are really important, others not; and that the two kinds are most oddly jumbled in everyday affairs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Groucho Marx");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is reason gone mad.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Kenneth Galbraith");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is richly rewarding to the person who employs it. It has some value in gaining and holding attention, but it has no persuasive value at all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Borge");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is something that thrives between man's aspirations and his limitations. There is more logic in humor than in anything else. Because, you see, humor is truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Rosten");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is the affectionate communication of insight.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Max Eastman");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Humor is the instinct for taking pain playfully.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Simone Weil");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "I can, therefore I am.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ken Venturi");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "I don't believe you have to be better than everybody else. I believe you have to be better than you ever thought you could be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Donna Brazile");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "I was motivated to be different in part because I was different.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mia Hamm");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "I've worked too hard and too long to let anything stand in the way of my goals. I will not let my teammates down and I will not let myself down.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emile Zola");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "If you ask me what I came into this life to do, I will tell you: I came to live out loud.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walt Disney");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "If you can dream it, you can do it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Rohn");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "If you don't design your own life plan, chances are you'll fall into someone else's plan. And guess what they have planned for you? Not much.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dale Carnegie");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "If you want to conquer fear, don't sit home and think about it. Go out and get busy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John D. Rockefeller");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "If you want to succeed you should strike out on new paths, rather than travel the worn paths of accepted success.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Carrey");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "If you've got a talent, protect it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rupert Murdoch");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "In motivating people, you've got to engage their minds and their hearts. I motivate people, I hope, by example - and perhaps by excitement, by having productive ideas to make others feel involved.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Vincent Peale");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "It's always too early to quit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baltasar Gracian");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Know or listen to those who know.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Burroughs");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Leap, and the net will appear.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Denis Waitley");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Learn from the past, set vivid, detailed goals for the future, and live in the only moment of time over which you have any control: now.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Rohn");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Learning is the beginning of wealth. Learning is the beginning of health. Learning is the beginning of spirituality. Searching and learning is where the miracle process all begins.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jessica Savitch");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "No matter how many goals you have achieved, you must set your sights on a higher one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "One may miss the mark by aiming too high as too low.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Marshall");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "One person with a belief is equal to a force of ninety-nine who have only interests.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Korda");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "One way to keep momentum going is to have constantly greater goals.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roger Caras");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Dogs are not our whole life, but they make our lives whole.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Max Eastman");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Dogs laugh, but they laugh with their tails.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ann Landers");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Don't accept your dog's admiration as conclusive evidence that you are wonderful.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mason Cooley");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Even cats grow lonely and anxious.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Winston Churchill");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I am fond of pigs. Dogs look up to us. Cats look down on us. Pigs treat us as equals.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jules Verne");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I believe cats to be spirits come to earth. A cat, I am sure, could walk on a cloud without coming through.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Dana");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I had been told that the training procedure with cats was difficult. It's not. Mine had me trained in two days.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Herriot");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I have felt cats rubbing their faces against mine and touching my cheek with claws carefully sheathed. These things, to me, are expressions of love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hippolyte Taine");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I have studied many philosophers and many cats. The wisdom of cats is infinitely superior.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Juliette Lewis");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I kind of imagine myself at eighty, a cat lady.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jeanette Winterson");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I live alone, with cats, books, pictures, fresh vegetables to cook, the garden, the hens to feed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rodney Dangerfield");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I looked up my family tree and found three dogs using it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Cocteau");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I love cats because I enjoy my home; and little by little, they become its visible soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dick Van Patten");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I love cats.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I poured spot remover on my dog. Now he's gone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nafisa Joseph");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I used to love dogs until I discovered cats.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William H. Macy");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I was a dog in a past life. Really. I'll be walking down the street and dogs will do a sort of double take. Like, Hey, I know him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edgar Allan Poe");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I wish I could write as mysterious as a cat.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rita Rudner");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I wonder if other dogs think poodles are members of a weird religious cult.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christine McVie");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I'm looking more like my dogs every day - it must be the shaggy fringe and the ears.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Hazlitt");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "It is not fit that every man should travel; it makes a wise man better, and a fool worse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henny Youngman");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Just got back from a pleasure trip: I took my mother-in-law to the airport.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Like all great travellers, I have seen more than I remember, and remember more than I have seen.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brandi Chastain");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "My parents and my grandfather on my mom's side would travel the earth. They went to Australia and China, and they went to probably every soccer game I ever played.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eric Roberts");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "My wife and I have so much fun when we travel and find anything... like stray cats and squirrels.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ernest Hemingway");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Never go on trips with anyone you do not love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carmen Electra");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "No matter where I've been overseas, the food stinks, except in Italy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lin Yutang");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "No one realizes how beautiful it is to travel until he comes home and rests his head on his old, familiar pillow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lisa Snowdon");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "On long haul flights I always drink loads and loads of water and eat light and healthy food.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "One travels more usefully when alone, because he reflects more.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Eliot");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Our deeds still travel with us from afar, and what we have been makes us what we are.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. C. Fields");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Reminds me of my safari in Africa. Somebody forgot the corkscrew and for several days we had to live on nothing but food and water.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Bartram");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "The attention of a traveller, should be particularly turned, in the first place, to the various works of Nature, to mark the distinctions of the climates he may explore, and to offer such useful observations on the different productions as may occur.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Glenn Tipton");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "The hardest part is to travel, and to be away from your family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Ade");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "The time to enjoy a European trip is about three weeks after unpacking.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert K. Chesterton");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "The traveler sees what he sees, the tourist sees what he has come to see.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel J. Boorstin");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "The traveler was active; he went strenuously in search of people, of adventure, of experience. The tourist is passive; he expects interesting things to happen to him. He goes \"sight-seeing.\"");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Palin");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "The trouble with travelling back later on is that you can never repeat the same experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Saint Augustine");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "The World is a book, and those who do not travel read only a page.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lord Chesterfield");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "The world is a country which nobody ever yet knew by description; one must travel through it one's self to be acquainted with it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur Henderson");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "It has become impossible to give up the enterprise of disarmament without abandoning the whole great adventure of building up a collective peace system.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "It is an unfortunate fact that we can secure peace only by preparing for war.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andre Gide");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "It is easier to lead men to combat, stirring up their passion, than to restrain them and direct them toward the patient labors of peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "It is madness for sheep to talk peace with a wolf.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eleanor Roosevelt");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "It isn't enough to talk about peace. One must believe in it. And it isn't enough to believe in it. One must work at it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nelson Mandela");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Let there be work, bread, water and salt for all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Nobody can bring you peace but yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Nonviolence is the first article of my faith. It is also the last article of my creed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phyllis McGinley");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Of one thing I am certain, the body is not the measure of healing, peace is the measure.");
		db.insert("citations", AUTHOR, cv);

		

		cv.put(AUTHOR, "Bill Gates");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Security is, I would say, our top priority because for all the exciting things you will be able to do with computers - organizing your lives, staying in touch with people, being creative - if we don't solve these security problems, then people will hold back.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Heinrich Heine");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "The Wedding March always reminds me of the music played when soldiers go into battle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Claire Forlani");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "There's a higher form of happiness in commitment. I'm counting on it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Rowland");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "Wedding: the point at which a man stops toasting a woman and begins roasting her.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eydie Gorme");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "When he came back from downtown, he had forgotten to bring his license, his identification, the $2 for the wedding license. So we got married two days later.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vera Wang");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "When I decided to get married at 40, I couldn't find a dress with the modernity or sophistication I wanted. That's when I saw the opportunity for a wedding gown business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vera Wang");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "When I design a wedding dress with a bustle, it has to be one the bride can dance in. I love the idea that something is practical and still looks great.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Oliver");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "When it's over, I want to say: all my life I was a bride married to amazement. I was the bridegroom, taking the world into my arms.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Morrow Lindbergh");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "When the wedding march sounds the resolute approach, the clock no longer ticks, it tolls the hour. The figures in the aisle are no longer individuals, they symbolize the human race.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anna Quindlen");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "Women who marry early are often overly enamored of the kind of man who looks great in wedding pictures and passes the maid of honor his telephone number.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bess Truman");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "You may invite the entire 35th Division to your wedding if you want to. I guess it's going to be yours as well as mine. We might as well have the church full while we are at it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John W. Gardner");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Life is the art of drawing without an eraser.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sai Baba");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Man learns through experience, and the spiritual path is full of different kinds of experiences. He will encounter many difficulties and obstacles, and they are the very experiences he needs to encourage and complete the cleansing process.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Bandura");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Most of the images of reality on which we base our actions are really based on vicarious experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ally Sheedy");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "My experiences have taught me a lot and I'm happy with my learnings, if not with what I went through to learn.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Agnetha Faltskog");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "My path has not been determined. I shall have more experiences and pass many more milestones.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walter Pater");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Not the fruit of experience, but experience itself, is the end.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Keats");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Nothing ever becomes real till it is experienced.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Auguste Rodin");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Nothing is a waste of time if you use the experience wisely.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "M. Scott Peck");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Real love is a permanently self-enlarging experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Ruskin");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Skill is the unified force of experience, intellect and passion in their operation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Walker");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "The experience of God, or in any case the possibility of experiencing God, is innate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Frusciante");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "The main thing experience has taught me is that one has to sort of hone their relationship to time, you know.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "The only source of knowledge is experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank McCourt");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "The sky is the limit. You never have the same experience twice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Osler");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "The value of experience is not in seeing much, but in seeing wisely.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "The years teach much which the days never know.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Stuart Mill");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "There are many truths of which the full meaning cannot be realized until personal experience has brought it home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Josh Billings");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "There is nothing so easy to learn as experience and nothing so hard to apply.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Amy Grant");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "There's a beauty to wisdom and experience that cannot be faked. It's impossible to be mature without having lived.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louise L. Hay");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "We are each responsible for all of our experiences.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nicolas Roeg");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Movies are not scripts - movies are films; they're not books, they're not the theatre.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wim Wenders");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Movies are something people see all over the world because there is a certain need for it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walt Disney");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Movies can and do have tremendous influence in shaping young lives in the realm of entertainment towards the ideals and objectives of normal adulthood.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roland Emmerich");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Nobody makes movies bad on purpose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Billy Wilder");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Shoot a few scenes out of focus. I want to win the foreign film award.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christina Aguilera");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "So, where's the Cannes Film Festival being held this year?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dennis Quaid");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Sometimes in movies, I still have to be the hero, but it's not all that important to me anymore.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Hitchcock");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "The length of a film should be directly related to the endurance of the human bladder.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "The movies are the only business where you can go out front and applaud yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Schmich");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "The movies we love and admire are to some extent a function of who we are when we see them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Lucas");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "The secret to film is that it's an illusion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Stone");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "There's an electrical thing about movies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "There's only one thing that can kill the movies, and that's education.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clint Eastwood");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "This film cost $31 million. With that kind of money I could have invaded some country.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Ansen");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "We are the movies and the movies are us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sydney Pollack");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "When you make a film you usually make a film about an idea.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Goldwyn");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Why should people go out and pay money to see bad films when they can stay at home and see bad television for nothing?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Schmich");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "You can map your life through your favorite movies, and no two people's maps will be the same.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steve Martin");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "You know what your problem is, it's that you haven't seen enough movies - all of life's riddles are answered in the movies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tim Roth");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "You read a script and its based on 'Reservoir Dogs' and 'Pulp Fiction', and it goes right in the bin.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sara Paretsky");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I went to college at the University of Kansas, where I got a degree in political science.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marc Garneau");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I went to military college in Canada and graduated as an officer in the Navy but also as an engineer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Eddings");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I wrote a novel for my degree, and I'm very happy I didn't submit that to a publisher. I sympathize with my professors who had to read it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard King");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I'm not impressed by someone's degree... I'm impressed by them making movies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Dundes");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "If a student takes the whole series of my folklore courses including the graduate seminars, he or she should learn something about fieldwork, something about bibliography, something about how to carry out library research, and something about how to publish that research.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Simon Newcomb");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "In 1858 I received the degree of D. S. from the Lawrence Scientific School, and thereafter remained on the rolls of the university as a resident graduate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ted Nelson");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "In my second year in graduate school, I took a computer course and that was like lightening striking.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ed O'Neill");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "In the summer we graduated we flipped out completely, drinking beer, cruising in our cars and beating up each other. It was a crazy summer. That's when I started to be interested in girls.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jimmy Carter");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "In this outward and physical ceremony we attest once again to the inner and spiritual strength of our Nation. As my high school teacher, Miss Julia Coleman, used to say: 'We must adjust to changing times and still hold to unchanging principles.'");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leigh Steinberg");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "It is soooooo necessary to get the basic skills, because by the time you graduate, undergraduate or graduate, that field would have totally changed from your first day of school.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bobby Scott");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "It is virtually impossible to compete in today's global economy without a college degree.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Cousins");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "It makes little difference how many university courses or degrees a person may own. If he cannot use words to move an idea from one point to another, his education is incomplete.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "It might be said now that I have the best of both worlds. A Harvard education and a Yale degree.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "T. S. Eliot");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "Knowledge is invariably a matter of degree: you cannot put your finger upon even the simplest datum and say this we know.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Shue");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "Life is the most exciting opportunity we have. But we have one shot. You graduate from college once, and that's it. You're going out of that nest. And you have to find that courage that's deep, deep, deep in there. Every step of the way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kenneth G. Wilson");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "My father was on the faculty in the Chemistry Department of Harvard University; my mother had one year of graduate work in physics before her marriage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Weinberg");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "My father, who was from a wealthy family and highly educated, a lawyer, Yale and Columbia, walked out with the benefit of a healthy push from my mother, a seventh grade graduate, who took a typing course and got a secretarial job as fast as she could.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kenneth G. Wilson");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "My graduate studies were carried out at the California Institute of Technology.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Evans");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "My main objective is to prepare candidates for professional baseball; however, the majority of our graduates will go home as much better qualified amateurs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jillian Bach");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "My personal advice is to go to school first and get a liberal arts education, and then if you want to pursue acting, go to graduate school.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Graves");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "If I have a style, I am not aware of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tadao Ando");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "If you give people nothingness, they can ponder what can be achieved from that nothingness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Graves");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "In any architecture, there is an equity between the pragmatic function and the symbolic function.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Delia Ephron");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "In Los Angeles, by the time you're 35, you're older than most of the buildings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Allan Coe");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "It is not the beauty of a building you should look at; its the construction of the foundation that will stand the test of time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ludwig Mies van der Rohe");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Less is more.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Light, God's eldest daughter, is a principal beauty in a building.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel Burnham");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Make big plans; aim high in hope and work, remembering that a noble, logical diagram once recorded will not die.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thom Mayne");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "My buildings don't speak in words but by means of their own spaciousness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Julia Morgan");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "My buildings will be my legacy... they will speak for me long after I'm gone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Rogers");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "My passion and great enjoyment for architecture, and the reason the older I get the more I enjoy it, is because I believe we - architects - can effect the quality of life of the people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Ende");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "No architect troubled to design houses that suited people who were to live in them, because that would have meant building a whole range of different houses. It was far cheaper and, above all, timesaving to make them identical.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rem Koolhaas");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Not many architects have the luxury to reject significant things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus V. Pollio");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Nothing requires the architect's care more than the due proportions of buildings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arne Jacobsen");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Proportions are what makes the old Greek temples classic in their beauty. They are like huge blocks, from which the air has been literally hewn out between the columns.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Meier");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Rome has not seen a modern building in more than half a century. It is a city frozen in time.");
		db.insert("citations", AUTHOR, cv);
	}
	
	public void onCreate3(SQLiteDatabase db) {
		ContentValues cv = new ContentValues();
		cv.put(AUTHOR, "Nicole Appleton");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Giving birth was easier than having a tattoo.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Walker");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "How simple a thing it seems to me that to know ourselves as we are, we must know our mothers names.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brigitte Bardot");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I am no mother, and I won't be one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. M. Forster");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I am sure that if the mothers of various nations could meet, there would be no more wars.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Geffen");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I am truly my mother's son.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ellen DeGeneres");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I ask people why they have deer heads on their walls. They always say because it's such a beautiful animal. There you go. I think my mother is attractive, but I have photographs of her.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kate Hudson");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I auditioned on my own. I tried to make a mark for myself without anybody's help, not even Mom's.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sandra Bullock");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I basically became a cheerleader because I had a very strict mom. That was my way of being a bad girl.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Antonio Villaraigosa");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I got to grow up with a mother who taught me to believe in me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Candice Bergen");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I guess I was a mom so late in life, my daughter was the greatest thing since sliced bread.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Bush");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I may be the only mother in America who knows exactly what their child is up to all the time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Rockefeller");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I owe much to mother. She had an expert's understanding, but also approached art emotionally.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Teller");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "A fact is a simple statement that everyone believes. It is innocent, unless found guilty. A hypothesis is a novel suggestion that no one wants to believe. It is guilty, until found effective.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward R. Murrow");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "A satellite has no conscience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Max Planck");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "A scientific truth does not triumph by convincing its opponents and making them see the light, but rather because its opponents eventually die and a new generation grows up that is familiar with it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Perlis");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "A year spent in artificial intelligence is enough to make one believe in God.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Smith");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Adventure upon all the tickets in the lottery, and you lose for certain; and the greater the number of your tickets the nearer your approach to this certainty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Kay Ash");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Aerodynamically, the bumble bee shouldn't be able to fly, but the bumble bee doesn't know it so it goes on flying anyway.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Mead");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Anthropology demands the open-mindedness with which one must look and listen, record in astonishment and wonder that which one would not have been able to guess.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jane Howard");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Anthropology was the science that gave her the platform from which she surveyed, scolded and beamed at the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Max Planck");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Anybody who has been seriously engaged is scientific work of any kind realizes that over the entrance to the gates of the temple of science are written the words: 'Ye must have faith.'");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John von Neumann");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Anyone who attempts to generate random numbers by deterministic means is, of course, living in a state of sin.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Bad times have a scientific value. These are occasions a good learner would not miss.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jay Leno");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Bush reiterated his stand to conservatives opposing his decision on stem cell research. He said today he believes life begins at conception and ends at execution.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rene Descartes");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Each problem that I solved became a rule, which served afterwards to solve other problems.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Dewey");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Every great advance in science has issued from a new audacity of imagination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Linus Pauling");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Facts are the air of scientists. Without them you can never fly.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Jay Gould");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Few tragedies can be more extensive than the stunting of life, few injustices deeper than the denial of an opportunity to strive or even to hope, by a limit imposed from without, but falsely identified as lying within.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dan Quayle");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "For NASA, space is still a high priority.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Hanks");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "From now on we live in a world where man has walked on the Moon. It's not a miracle; we just decided to go.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Henry Fischer");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Half of the modern drugs could well be thrown out of the window, except that the birds might eat them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Dana");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "He is so old that his blood type was discontinued.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Groucho Marx");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "A hospital bed is a parked taxi with the meter running.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Butler");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "A physician's physiology has much the same relation to his power of healing as a cleric's divinity has to his power of influencing conduct.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Mikulski");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "A pregnant woman facing the most dire circumstances must be able to count on her doctor to do what is medically necessary to protect her from serious physical harm.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isaac Asimov");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "All sorts of computer errors are now turning up. You'd be surprised to know the number of doctors who claim they are treating pregnant men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Frist");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "America has the best doctors, the best nurses, the best hospitals, the best medical technology, the best medical breakthrough medicines in the world. There is absolutely no reason we should not have in this country the best health care in the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack Kevorkian");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "As a medical doctor, it is my duty to evaluate the situation with as much data as I can gather and as much expertise as I have and as much experience as I have to determine whether or not the wish of the patient is medically justified.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nathan Deal");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "As a physician, I know many doctors want to utilize new technology, but they find the cost prohibitive.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anton Chekhov");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Doctors are just the same as lawyers; the only difference is that lawyers merely rob you, whereas doctors rob you and kill you too.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Jones");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Doctors coin money when they do procedures but family medicine doesn't have any procedures.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Doctors will have more lives to answer for in the next world than even we generals.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anthony Edwards");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Flying back from New York, the flight attendant said 'God, I wished you were here yesterday, we had a stroke on the plane. I said, if I have a stroke on a plane, I hope the pretend doctor isn't the one on the plane. I want a real doctor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Getting out of the hospital is a lot like resigning from a book club. You're not out of it until the computer says you're out of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zoe Saldana");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Growing up, my dolls were doctors and on secret missions. I had Barbie Goes Rambo.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alex Chiu");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Honestly, being a doctor could make you more close minded than regular people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James H. Boren");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "I got the bill for my surgery. Now I know what those doctors were wearing masks for.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ty Cobb");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "I regret to this day that I never went to college. I feel I should have been a doctor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nicolas Cage");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "I was always shocked when I went to the doctor's office and they did my X-ray and didn't find that I had eight more ribs than I should have or that my blood was the color green.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rita Rudner");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "I was going to have cosmetic surgery until I noticed that the doctor's office was full of portraits by Picasso.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jay London");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "I went to the doctor and he said I had acute appendicitis, and I said compared to who?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cesar Romero");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "I'm 86 and my doctor used to tell me to slow down - at least he did until he dropped dead.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "A dreamer is one who can only find his way by moonlight, and his punishment is that he sees the dawn before the rest of the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Barrymore");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "A man is not old until regrets take the place of dreams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack Kerouac");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "All human beings are also dream beings. Dreaming ties all mankind together.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "T. E. Lawrence");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "All men dream, but not equally. Those who dream by night in the dusty recesses of their minds, wake in the day to find that it was vanity: but the dreamers of the day are dangerous men, for they may act on their dreams with open eyes, to make them possible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Huneker");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "All men of action are dreamers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Faulkner");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "All of us failed to match our dreams of perfection. So I rate us on the basis of our splendid failure to do the impossible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walt Disney");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "All our dreams can come true, if we have the courage to pursue them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elias Canetti");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "All the things one has forgotten scream for help in dreams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edgar Allan Poe");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Deep into that darkness peering, long I stood there, wondering, fearing, doubting, dreaming dreams no mortal ever dared to dream before.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joy Page");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dream and give yourself permission to envision a You that you choose to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aldous Huxley");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dream in a pragmatic way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward G. Bulwer-Lytton");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dream manfully and nobly, and thy dreams shall be prophets.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johann Wolfgang von Goethe");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dream no small dreams for they have no power to move the hearts of men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Vincent Benet");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dreaming men are haunted men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lope de Vega");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dreaming of a tomorrow, which tomorrow, will be as distant then as 'tis today.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jane Roberts");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dreaming or awake, we perceive only events that have meaning to us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anais Nin");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dreams are necessary to life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dreams are the touchstones of our character.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edgar Cayce");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dreams are today's answers to tomorrow's questions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Lord Tennyson");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dreams are true while they last, and do we not live in dreams?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adlai E. Stevenson");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "A free society is one where it is safe to be unpopular.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dwight D. Eisenhower");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "A people that values its privileges above its principles soon loses both.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Madison");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "A pure democracy is a society consisting of a small number of citizens, who assemble and administer the government in person.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert A. Heinlein");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "An armed society is a polite society. Manners are good when one may have to back up his acts with his life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Timothy Leary");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Civilization is unbearable, but it is less unbearable at the top.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herb Caen");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Cockroaches and socialites are the only things that can stay up all night and eat anything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Do not waste your time on Social Questions. What is the matter with the poor is Poverty; what is the matter with the rich is Uselessness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Austin");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Exclusiveness in a garden is a mistake as great as it is in society.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Szasz");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Formerly, when religion was strong and science weak, men mistook magic for medicine; now, when science is strong and religion weak, men mistake medicine for magic.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martha Beck");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Good-looking individuals are treated better than homely ones in virtually every social situation, from dating to trial by jury.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Sweeney");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "How can a society that exists on instant mashed potatoes, packaged cake mixes, frozen dinners, and instant cameras teach patience to its young?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "A. Whitney Brown");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "I am as frustrated with society as a pyromaniac in a petrified forest.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Boyd Rice");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "I think this society suffers so much from too much freedom, too many rights that allow people to be irresponsible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "If a man walks in the woods for love of them half of each day, he is in danger of being regarded as a loafer. But if he spends his days as a speculator, shearing off those woods and making the earth bald before her time, he is deemed an industrious and enterprising citizen.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Roker");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "In our society leaving baby with Daddy is just one step above leaving the kids to be raised by wolves or apes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Sandburg");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "In these times you have to be an optimist to open your eyes when you awake in the morning.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jacques Verges");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "It is good for society to have this introspection.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jiddu Krishnamurti");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "It is no measure of health to be well adjusted to a profoundly sick society.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Raymond Chandler");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "It is not a fragrant world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jacques Barzun");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "It seems a long time since the morning mail could be called correspondence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "A faith is a necessity to a man. Woe to him who believes in nothing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Tullius Cicero");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "A man of courage is also full of faith.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "All who call on God in true faith, earnestly from the heart, will certainly be heard, and will receive what they have asked and desired.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emmanuel Teney");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "As your faith is strengthened you will find that there is no longer the need to have a sense of control, that things will flow as they will, and that you will flow with them, to your great delight and benefit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Be a sinner and sin strongly, but more strongly have faith and rejoice in Christ.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mother Teresa");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Be faithful in small things because it is in them that your strength lies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Doubt is a pain too lonely to know that faith is his twin brother.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Every tomorrow has two handles. We can take hold of it with the handle of anxiety or the handle of faith.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lillian Smith");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith and doubt both are needed - not as antagonists, but working side by side to take us around the unknown curve.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahalia Jackson");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith and prayer are the vitamins of the soul; man cannot live in health without them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Voltaire");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith consists in believing when it is beyond the power of reason to believe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Aquinas");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith has to do with things that are not seen and hope with things that are not at hand.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michelangelo");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith in oneself is the best and safest course.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Blaise Pascal");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith indeed tells what the senses do not tell, but not the contrary of what they see. It is above them and not contrary to them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith is a knowledge within the heart, beyond the reach of proof.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Wordsworth");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith is a passionate intuition.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "D. Elton Trueblood");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith is not belief without proof, but trust without reservation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sherwood Eddy");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith is not contrary to reason.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith is not something to grasp, it is a state to grow into.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sherwood Eddy");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith is reason grown courageous.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Brinkley");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "A successful man is one who can lay a firm foundation with the bricks others have thrown at him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pablo Picasso");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Action is the foundational key to all success.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Shirley Jones");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "After I won the Oscar, my salary doubled, my friends tripled, my children became more popular at school, my butcher made a pass at me, and my maid hit me up for a raise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Lincoln");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Always bear in mind that your own resolution to succeed is more important than any other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lydia M. Child");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Belief in oneself is one of the most important bricks in building any successful venture.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Edward Woodberry");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Defeat is not the worst of failures. Not to have tried is the true failure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dale Carnegie");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Develop success from failures. Discouragement and failure are two of the surest stepping stones to success.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Diligence is the mother of good fortune.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Frost");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Don't aim for success if you want it; just do what you love and believe in, and it will come naturally.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Don't confuse fame with success. Madonna is one; Helen Keller is the other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Malcolm Forbes");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Failure is success if we learn from it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dale Carnegie");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Flaming enthusiasm, backed up by horse sense and persistence, is the quality that most frequently makes for success.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Rohn");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Formal education will make you a living; self-education will make you a fortune.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. Paul Getty");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Formula for success: rise early, work hard, strike oil.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Logan P. Smith");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "How can they say my life is not a success? Have I not for more than sixty years got enough to eat and escaped being eaten?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jonathan Winters");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "I couldn't wait for success, so I went ahead without it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Cosby");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "I don't know the key to success, but the key to failure is trying to please everybody.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Burns");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "I honestly think it is better to be a failure at something you love than to be a success at something you hate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Jordan");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "I've failed over and over and over again in my life and that is why I succeed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. C. Fields");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "If at first you don't succeed, try, try again. Then quit. There's no point in being a damn fool about it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sidonie Gabrielle Colette");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "A woman who thinks she is intelligent demands the same rights as man. An intelligent woman gives up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kevin Kelly");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "All imaginable futures are not equally possible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Salmon P. Chase");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "All men are born equally free.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Allan Coe");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "All men are created equal, it is only men themselves who place themselves above equality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rudyard Kipling");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "All the people like us are we, and everyone else is They.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bob Dylan");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "All this talk about equality. The only thing people really have in common is that they are all going to die.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mason Cooley");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "As equality increases, so does the number of people struggling for predominance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Before God we are all equally wise - and equally foolish.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Coming generations will learn equality from poverty, and love from woes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Irving Kristol");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Democracy does not guarantee equality of conditions - it only guarantees equality of opportunity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mercy Otis Warren");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Democratic principles are the result of equality of condition.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Jordan");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Do not call for black power or green power. Call for brain power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Honda");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Equal pay isn't just a women's issue; when women get equal pay, their family incomes rise and the whole family benefits.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jenny Shipley");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Equality and development will not be achieved however if peace is not understood from women's' point of view.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Robbins");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Equality is not in regarding different things similarly, equality is in regarding different things differently.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frances Wright");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Equality is the soul of liberty; there is, in fact, no liberty without it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Honore de Balzac");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Equality may perhaps be a right, but no power on earth can ever turn it into a fact.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barry Goldwater");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Equality, rightly understood as our founding fathers understood it, leads to liberty and to the emancipation of creative differences; wrongly understood, as it has been so tragically in our time, it leads first to conformity and then to despotism.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Lincoln");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Fourscore and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Schurz");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "From the equality of rights springs identity of our highest interests; you cannot subvert your neighbor's rights without striking a dangerous blow at your own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aldous Huxley");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "After silence, that which comes nearest to expressing the inexpressible is music.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Cocteau");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "All good music resembles something. Good music stirs by its mysterious resemblance to the objects and feelings which motivated it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Billy Strayhorn");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "All music is beautiful.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Les Baxter");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Any good music must be an innovation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bryan Ferry");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "But when you get music and words together, that can be a very powerful thing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kin Hubbard");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Classical music is the kind we keep thinking will turn into a tune.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Howard Dietz");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Composers shouldn't think too much - it interferes with their plagiarism.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isaac Stern");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Everywhere in the world, music enhances a hall, with one exception: Carnegie Hall enhances the music.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Reba McEntire");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "For me, singing sad songs often has a way of healing a situation. It gets the hurt out in the open into the light, out of the darkness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Billy Wilder");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "He has Van Gogh's ear for music.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Hell is full of musical amateurs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ace Frehley");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "I asked my daughter when she was 16, What's the buzz on the street with the kids? She's going, to be honest, Dad, most of my friends aren't into Kiss. But they've all been told that it's the greatest show on Earth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dizzy Gillespie");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "I don't care much about music. What I like is sounds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elvis Presley");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "I don't know anything about music. In my line you don't have to.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adele");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "I don't make music for eyes. I make music for ears.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Billy Joel");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "I think music in itself is healing. It's an explosive expression of humanity. It's something we are all touched by. No matter what culture we're from, everyone loves music.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herb Alpert");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "I wake up in the morning, I do a little stretching exercises, pick up the horn and play.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ray Charles");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "I was born with music inside me. Music was one of my parts. Like my ribs, my kidneys, my liver, my heart. Like my blood. It was a force already within me when I arrived on the scene. It was a necessity for me-like food or water.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gary Cherone");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "I was growing up listening to Queen. Freddie Mercury threw those incredible melodies into his songs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "B. B. King");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "I've said that playing the blues is like having to be black twice. Stevie Ray Vaughan missed on both counts, but I never noticed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred P. Sloan");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "A car for every purse and purpose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Fussell");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Americans are the only people in the world known to me whose status anxiety prompts them to advertise their college and university affiliations in the rear window of their automobiles.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dave Barry");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Auto racing is boring except when a car is going at least 172 miles per hour upside down.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jay Inslee");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Back in the mid-1970s, we adopted some fairly ambitious goals to improve efficiency of our cars. What did we get? We got a tremendous boost in efficiency.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark-Paul Gosselaar");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "But my passion is racing cars. It's what I like to do in my off time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Picabo Street");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "But to personally satisfy my own adrenalin needs, I've been racing cars a little bit, which has been fun.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Car designers are just going to have to come up with an automobile that outlasts the payments.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. Joseph Cossman");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Drive-in banks were established so most of the cars today could see their real owners.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Arnold");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Environmentalists have a very conflicted relationship with their cars.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul D. Boyer");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Family trips to Yellowstone and to what are now national parks in Southern Utah, driving the primitive roads and cars of that day, were real adventures.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Bay");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Fast cars are my only vice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brock Yates");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I admit to wasting my life messing around with fast cars and motorcycles.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sammy Davis, Jr.");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I had more clothes than I had closets, more cars than garage space, but no money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexandra Paul");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I had never been able to get a car that said how much I cared about the environment until I drove electric.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I had to stop driving my car for a while... the tires got dizzy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Matthew Barney");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I have a need to make these sorts of connections literal sometimes, and a vehicle often helps to do that. I have a relationship to car culture. It isn't really about loving cars. It's sort of about needing them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I hooked up my accelerator pedal in my car to my brake lights. I hit the gas, people behind me stop, and I'm gone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Osborne");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I liken myself to Henry Ford and the auto industry, I give you 90 percent of what most people need.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lara Flynn Boyle");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I love fast cars... and to go too fast in them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Loretta Lynn");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I never rode in an automobile until I was 12.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maurice Chevalier");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "A comfortable old age is the reward of a well-spent youth. Instead of its bringing sad and melancholy prospects of decay, it would give us hopes of eternal youth in a better world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "A diplomat is a man who always remembers a woman's birthday but never remembers her age.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sophocles");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "A man growing old becomes a child again.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Tullius Cicero");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Advice in old age is foolish; for what can be more absurd than to increase our provisions for the road the nearer we approach to our journey's end.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bette Midler");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "After thirty, a body has a mind of its own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Gurley Brown");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "After you're older, two things are possibly more important than any others: health and money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rabindranath Tagore");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Age considers; youth ventures.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Fiebig");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Age does not diminish the extreme disappointment of having a scoop of ice cream fall from the cone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Stoppard");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Age is a very high price to pay for maturity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Age is an issue of mind over matter. If you don't mind, it doesn't matter.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jane Elliot");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Age is how we determine how valuable you are.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don Marquis");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Age is not a particularly interesting subject. Anyone can get old. All you have to do is live long enough.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Betty Friedan");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Aging is not lost youth but a new stage of opportunity and strength.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kitty O'Neill Collins");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Aging seems to be the only available way to live a long life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Camus");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Alas, after a certain age every man is responsible for his face.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "All diseases run into one, old age.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Agatha Christie");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "An archaeologist is the best husband a woman can have. The older she gets the more interested he is in her.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ford");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Anyone who stops learning is old, whether at twenty or eighty. Anyone who keeps learning stays young. The greatest thing in life is to keep your mind young.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Tullius Cicero");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "As I approve of a youth that has something of the old man in him, so I am no less pleased with an old man that has something of the youth. He that follows this rule may be old in body, but can never be so in mind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Carnegie");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "As I grow older, I pay less attention to what men say. I just watch what they do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emo Philips");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "A computer once beat me at chess, but it was no match for me at kick boxing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel J. Boorstin");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "A wonderful thing about a book, in contrast to a computer screen, is that you can take it to bed with you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kent Conrad");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Access to computers and the Internet has become a basic need for education in our society.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dave Parnas");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "As a rule, software systems do not work well until they have been used, and have failed repeatedly, in real applications.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Philip Emeagwali");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Because I believe that humans are computers, I conjectured that computers, like people, can have left- and right-handed versions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Larry Ellison");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Bill Gates is the pope of the personal computer industry. He decides who's going to build.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edsger Dijkstra");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Computer science is no more about computers than astronomy is about telescopes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Seth Lloyd");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Computers are famous for being able to do complicated things starting from simple programs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Campbell");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Computers are like Old Testament gods; lots of rules and no mercy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louis Gerstner");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Computers are magnificent tools for the realization of our dreams, but no machine can replace the human spark of spirit, compassion, love, and understanding.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pablo Picasso");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Computers are useless. They can only give you answers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tony Visconti");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Computers have virtually replaced tape recorders.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clifford Stoll");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Computers in classrooms are the filmstrips of the 1990s.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Rooney");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Computers make it easier to do a lot of things, but most of the things they make it easier to do don't need to be done.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Rooney");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Computers may save time but they sure waste a lot of paper. About 98 percent of everything printed out by a computer is garbage that no one ever reads.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nicholas Negroponte");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Computing is not about computers any more. It is about living.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nicholas Negroponte");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Even in the developing parts of the world, kids take to computers like fish to water.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Issey Miyake");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Even when I work with computers, with high technology, I always try to put in the touch of the hand.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miguel de Icaza");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Every piece of software written today is likely going to infringe on someone else's patent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jonathan Shapiro");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Gee, I am a complete Luddite when it comes to computers, I can barely log on!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Rowland");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "A bride at her second marriage does not wear a veil. She wants to see what she is getting.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Schiller");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "A gloomy guest fits not a wedding feast.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Simmons");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "A person's character is but half formed till after wedlock.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Sweeney");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "A wedding anniversary is the celebration of love, trust, partnership, tolerance and tenacity. The order varies for any given year.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Grace Hansen");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "A wedding is just like a funeral except that you get to smell your own flowers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Honore de Balzac");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "A young bride is like a plucked flower; but a guilty wife is like a flower that had been walked over.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Feather");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "An invitation to a wedding invokes more trouble than a summons to a police court.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "Bride: A woman with a fine prospect of happiness behind her.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ethel Merman");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "Christmas carols always brought tears to my eyes. I also cry at weddings. I should have cried at a couple of my own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nick Cave");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "Getting married, for me, was the best thing I ever did. I was suddenly beset with an immense sense of release, that we have something more important than our separate selves, and that is the marriage. There's immense happiness that can come from working towards that.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lord Byron");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I am about to be married, and am of course in all the misery of a man in pursuit of happiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Goldsmith");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I chose my wife, as she did her wedding gown, for qualities that would wear well.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Epps");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I don't know nothing about no marriages or nothing. I ain't even never been to a wedding.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lord Byron");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I have great hopes that we shall love each other all our lives as much as if we had never married at all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cindy Margolis");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I really did put up all my wedding pictures on my website. And I swear to you, my wedding pictures got downloaded just as much as my bikini pictures.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gwen Stefani");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I remember when I was in school, they would ask, 'What are you going to be when you grow up?' and then you'd have to draw a picture of it. I drew a picture of myself as a bride.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Sandler");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I sang a song at my sister's wedding. My mother forced me into that, too. But that one felt all right.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Walken");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I think that weddings have probably been crashed since the beginning of time. Cavemen crashed them. You go to meet girls. It makes sense.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Star Jones");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I want the big drama. I always said I don't want a wedding I want a parade.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Groucho Marx");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I was married by a judge. I should have asked for a jury.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "A man who carries a cat by the tail learns something he can learn in no other way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Wendell Holmes, Jr.");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "A mind that is stretched by a new experience can never go back to its old dimensions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wallis Simpson");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "A woman's life can really be a succession of lives, each revolving around some emotionally compelling situation or challenge, and each marked off by some intense experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Lord Tennyson");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "All experience is an arch wherethrough gleams that untravelled world whose margin fades for ever and for ever when I move.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry B. Adams");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "All experience is an arch, to build upon.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Georges Clemenceau");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "All that I know I learned after I was thirty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don Marquis");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "An optimist is a guy that has never had much experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jan Peter Balkenende");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Bitter experience has taught us how fundamental our values are and how great the mission they represent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Hodges");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Color is an intense experience on its own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Flannery O'Connor");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Conviction without experience makes for harshness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry James");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Deep experience is never peaceful.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pete Seeger");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Do you know the difference between education and experience? Education is when you read the fine print; experience is what you get when you don't.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jake Roberts");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Every moment is an experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andre Gide");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Everything has been said before, but since nobody listens we have to keep going back and beginning all over again.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "A. R. Ammons");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Everything is discursive opinion instead of direct experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Marston");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Excellence is not a skill. It is an attitude.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience - the wisdom that enables us to recognise in an undesirable old acquaintance the folly that we have already embraced.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Roux");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience comprises illusions lost, rather than wisdom gained.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Franklin P. Jones");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience enables you to recognize a mistake when you make it again.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "A. E. Housman");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience has taught me, when I am shaving of a morning, to keep watch over my thoughts, because, if a line of poetry strays into my memory, my skin bristles so that the razor ceases to act.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stanley Kubrick");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "A film is - or should be - more like music than like fiction. It should be a progression of moods and feelings. The theme, what's behind the emotion, the meaning, all that comes later.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Cocteau");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "A film is a petrified fountain of thought.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orson Welles");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "A film is never really good unless the camera is an eye in the head of a poet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Hitchcock");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "A good film is when the price of the dinner, the theatre admission and the babysitter were worth it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Hitchcock");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "A lot of movies are about life, mine are like a slice of cake.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean-Luc Godard");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "A story should have a beginning, a middle, and an end... but not necessarily in that order.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Goldwyn");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "A wide screen just makes a bad film twice as bad.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Pickford");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Adding sound to movies would be like putting lipstick on the Venus de Milo.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johnny Depp");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Am I a romantic? I've seen 'Wuthering Heights' ten times. I'm a romantic.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas Sirk");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "And in movies you must be a gambler. To produce films is to gamble.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Scorsese");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Cinema is a matter of what's in the frame and what's out.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean-Luc Godard");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Cinema is the most beautiful fraud in the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pauline Kael");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Citizen Kane is perhaps the one American talking picture that seems as fresh now as the day it opened. It may seem even fresher.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Federico Fellini");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Even if I set out to make a film about a fillet of sole, it would be about me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roger Ebert");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Every great film should seem new every time you see it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sydney Pollack");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Every single art form is involved in film, in a way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Milius");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Everybody's a filmmaker today.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sandra Bullock");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Everyone told me to pass on Speed because it was a 'bus movie.'");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elizabeth Taylor");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Everything makes me nervous - except making films.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francois Truffaut");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Film lovers are sick people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Orben");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "A graduation ceremony is an event where the commencement speaker tells thousands of students dressed in identical caps and gowns that 'individuality' is the key to success.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Sternberg");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "ACT and SAT each have their own parts of the country. The GRE has its lock on graduate admissions. And so, one could blame the companies, but really, economically, they have no incentive to change things very much because they're getting the business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Laurie Anderson");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "At the School of Visual Arts in New York, you can get your degree in Net art, which is really a fantastic way of thinking of theater in new ways.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marian Wright Edelman");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "Being considerate of others will take your children further in life than any college degree.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joe Baca");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "Catholic school graduates exhibit a wide variety of qualities that will not only help them in their careers but also in their family and community lives.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ruby Wax");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "College atheletes used to get a degree in bringing your pencil.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gordon Brown");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "Each year India and China produce four million graduates compared with just over 250,000 in Britain.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Conan O'Brien");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "Earlier today, Arnold Schwarzenegger criticized the California school system, calling it disastrous. Arnold says California's schools are so bad that its graduates are willing to vote for me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herb Ritts");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "Even though I didn't get a business degree, I enjoyed learning about economics.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "R. Lee Ermey");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "Even though I disagree with many of the changes, when I see the privates graduate at the end of the day, when they walk off that drill field at the end of the ceremony, they are still fine privates; outstanding, well motivated privates.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Faith Hill");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "Everybody wants you to do good things, but in a small town you pretty much graduate and get married. Mostly you marry, have children and go to their football games.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clive James");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "Everyone has a right to a university degree in America, even if it's in Hamburger Technology.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miguel de Cervantes");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "For a man to attain to an eminent degree in learning costs him time, watching, hunger, nakedness, dizziness in the head, weakness in the stomach, and other inconveniences.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Patrick J. Kennedy");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "For students today, only 10 percent of children from working-class families graduate from college by the age of 24 as compared to 58 percent of upper-middle-class and wealthy families.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elbert Hubbard");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "God will not look you over for medals degrees or diplomas, but for scars.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jon Secada");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "Having a college degree gave me the opportunity to be... well-rounded. Also, the people I met at the university, most of them are still my colleagues now. People I've known for years are all in the industry together.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christina Aguilera");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I always wanted to have my own album released before I graduated from high school.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Parker Stevenson");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I could be happy doing something like architecture. It would involve another couple of years of graduate school, but that's what I studied in college. That's what I always wanted to do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clay Aiken");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I did get a degree in special education.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel J. Evans");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I did graduate with a bachelor's degree in civil engineering in 1948.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ayn Rand");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "A building has integrity just like a man. And just as seldom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helmut Jahn");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "A city building, you experience when you walk; a suburban building, you experience when you drive.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louis Kahn");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "A great building must begin with the unmeasurable, must go through measurable means when it is being designed and in the end must be unmeasurable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Le Corbusier");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "A house is a machine for living in.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Lloyd Wright");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "An architect's most useful tools are an eraser at the drafting board, and a wrecking bar at the site.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Portman");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Architects in the past have tended to concentrate their attention on the building as a static object. I believe dynamics are more important: the dynamics of people, their interaction with spaces and environmental condition.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adolf Loos");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Architecture arouses sentiments in man. The architect's task therefore, is to make those sentiments more precise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Julia Morgan");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Architecture is a visual art, and the buildings speak for themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Yoshio Taniguchi");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Architecture is basically a container of something. I hope they will enjoy not so much the teacup, but the tea.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry Seidler");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Architecture is not an inspirational business, it's a rational procedure to do sensible and hopefully beautiful things; that's all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Philip Johnson");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Architecture is the art of how to waste space.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Le Corbusier");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Architecture is the learned game, correct and magnificent, of forms assembled in the light.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louis Kahn");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Architecture is the reaching out for the truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Gehry");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Architecture should speak of its time and place, but yearn for timelessness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ludwig Mies van der Rohe");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Architecture starts when you carefully put two bricks together. There it begins.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arne Jacobsen");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Architecture tends to consume everything else, it has become one's entire life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry von Zell");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "As a designer, the mission with which we have been charged is simple: providing space at the right cost.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Puryear");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "At a certain point, I just put the building and the art impulse together. I decided that building was a legitimate way to make sculpture.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alvar Aalto");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Building art is a synthesis of life in materialised form. We should try to bring in under the same hat not a splintered way of thinking, but all in harmony together.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Portman");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Buildings should serve people, not the other way around.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joyce A. Myers");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "A #2 pencil and a dream can take you anywhere.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "A compliment is something like a kiss through a veil.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Donald Cargill");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "And now, this is the sweetest and most glorious day that ever my eyes did see.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William James");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Belief creates the actual fact.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodore Roosevelt");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Believe you can and you're halfway there.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Vincent Peale");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Change your thoughts and you change your world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rabindranath Tagore");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Clouds come floating into my life, no longer to carry rain or usher storm, but to add color to my sunset sky.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Louis Stevenson");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Don't judge each day by the harvest you reap but by the seeds that you plant.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Every charitable act is a stepping stone toward heaven.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Merton");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Every moment and every event of every man's life on earth plants something in his soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Ellery Channing");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Faith is love taking the form of aspiration.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wallis Simpson");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "For a gallant spirit there can never be defeat.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ella Baker");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Give light and people will find the way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Elliot");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "God always gives His best to those who leave the choice with him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur Young");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "God sleeps in the minerals, awakens in plants, walks in animals, and thinks in man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Schiller");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Grace is the beauty of form under the influence of freedom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Gratitude is the fairest blossom which springs from the soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Great hopes make great men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Rohn");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Happiness is not something you postpone for the future; it is something you design for the present.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phillips Brooks");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Happiness is the natural flower of duty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "A lot of people are afraid of heights. Not me, I'm afraid of widths.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emil Zatopek");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "A runner must run with dreams in his heart, not money in his pocket.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Dudley White");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "A vigorous five-mile walk will do more good for an unhappy but otherwise healthy adult than all the medicine and psychology in the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sasha Cohen");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "A year ago I had a back injury and followed a good nutrition program to help speed up my recovery. I focused on exercise and staying healthy in order to get back out on the ice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Julie Bishop");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "And I believe that the best buy in public health today must be a combination of regular physical exercise and a healthy diet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Amy Irving");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "And you know, the baby boomers are getting older, and those off the rack clothes are just not fitting right any longer, and so, tailor-made suits are coming back into fashion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plato");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Attention to health is life's greatest hindrance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold Schwarzenegger");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Bodybuilding is much like any other sport. To be successful, you must dedicate yourself 100% to your training, diet and mental approach.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ann Oakley");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Clearly, society has a tremendous stake in insisting on a woman's natural fitness for the career of mother: the alternatives are all too expensive.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brock Yates");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Don't get me wrong, I think bikes are terrific. I own several of my own, including a trendy mountain style, and ride them for pleasure and light exercise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George A. Sheehan");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Exercise is done against one's wishes and maintained only because the alternative is worse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Johnson");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Exercise is labor without weariness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hugh Blair");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Exercise is the chief source of improvement in our faculties.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gene Tunney");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Exercise should be regarded as tribute to the heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lee Haney");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Exercise to stimulate, not to annihilate. The world wasn't formed in a day, and neither were we. Set small goals and build upon them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paula Abdul");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Find fitness with fun dancing. It is fun and makes you forget about the dreaded exercise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Thicke");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Fitness needs to be perceived as fun and games or we subconsciously avoid it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniela Pestova");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "For exercise, I now run with my chocolate Lab puppy, Oscar.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George William Curtis");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Happiness lies first of all in health.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Carlyle");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "He who has health, has hope; and he who has hope, has everything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roberto Burle Marx");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "A garden is a complex of aesthetic and plastic intentions; and the plant is, to a landscape artist, not only a plant - rare, unusual, ordinary or doomed to disappearance - but it is also a color, a shape, a volume or an arabesque in itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Luis Barragan");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "A garden must combine the poetic and he mysterious with a feeling of serenity and joy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "A good garden may have some weeds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "D. Elton Trueblood");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "A man has made at least a start on discovering the meaning of human life when he plants shade trees under which he knows full well he will never sit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Liberty Hyde Bailey");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "A person cannot love a plant after he has pruned it, then he has either done a poor job or is devoid of emotion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doug Larson");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "A weed is a plant that has mastered every survival skill except for learning how to grow in rows.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Kent");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "All gardening is landscape painting.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Cadbury");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "But if each man could have his own house, a large garden to cultivate and healthy surroundings - then, I thought, there will be for them a better opportunity of a happy family life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "May Sarton");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Everything that slows us down and forces patience, everything that sets us back into the slow circles of nature, is a help. Gardening is an instrument of grace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Luther Burbank");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Flowers always make people better, happier, and more helpful; they are sunshine, food and medicine for the soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Kent");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Garden as though you will live forever.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar de la Renta");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Gardening is how I relax. It's another form of creating and playing with colors.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Atwood");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Gardening is not a rational act.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rudyard Kipling");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Gardens are not made by singing 'Oh, how beautiful,' and sitting in the shade.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "God Almighty first planted a garden. And indeed, it is the purest of human pleasures.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Caecilius Statius");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "He plants trees to benefit another generation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "May Sarton");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Help us to be ever faithful gardeners of the spirit, who know that without darkness nothing comes to birth, and without light nothing flowers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Smith");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "How deeply seated in the human heart is the liking for gardens and gardening.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Fish");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "I am sure that if you plant the trees back again, it will do nothing but good.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martha Smith");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "I do some of my best thinking while pulling weeds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elbert Hubbard");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "A friend is one who knows you and loves you just the same.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Hall");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "A friend should be one in whose understanding and virtue we can equally confide, and whose opinion we can value at once for its justness and its sincerity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "A friend to all is a friend to none.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pam Brown");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "A friendship can weather most things and thrive in thin soil; but it needs a little mulch of letters and phone calls and small, silly presents every so often - just to save it from drying out completely.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John D. Rockefeller");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "A friendship founded on business is better than a business founded on friendship.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bil Keane");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "A hug is like a boomerang - you get it back right away.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "A man's growth is seen in the successive choirs of his friends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Buscaglia");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "A single rose can be my garden... a single friend, my world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold H. Glasow");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "A true friend never gets in your way unless you happen to be going down.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Buddha");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "An insincere and evil friend is more to be feared than a wild beast; a wild beast may wound your body, but an evil friend will wound your mind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Washington");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Be courteous to all, but intimate with few, and let those few be well tried before you give them your confidence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "But friendship is precious, not only in the shade, but in the sunshine of life, and thanks to a benevolent arrangement the greater part of life is sunshine.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Wendell Holmes");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "But friendship is the breathing rose, with sweets in every fold.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Camus");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Don't walk behind me; I may not lead. Don't walk in front of me; I may not follow. Just walk beside me and be my friend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Shirley MacLaine");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Fear makes strangers of people who would be friends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Walker");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Friends and good manners will carry you where money won't go.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry B. Adams");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Friends are born, not made.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mario Puzo");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Friendship and money: oil and water.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Friendship is a single soul dwelling in two bodies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Alexander Eastman");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Friendship is held to be the severest test of character. It is easy, we think, to be loyal to a family and clan, whose blood is in your own veins.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woodrow Wilson");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A conservative is a man who just sits and thinks, mostly sits.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Franklin D. Roosevelt");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A conservative is a man with two perfectly good legs who, however, has never learned how to walk forward.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Rosten");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A conservative is one who admires radicals centuries after they're dead.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Caskie Stinnett");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A diplomat is a person who can tell you to go to hell in such a way that you actually look forward to the trip.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A fool and his money are soon elected.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Lloyd Wright");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A free America... means just this: individual freedom for all, rich or poor, or else this system of government we call democracy is only an expedient to enslave man to the machine and make him like it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles W. Pickering");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A healthy democracy requires a decent society; it requires that we are honorable, generous, tolerant and respectful.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry S. Truman");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A leader in the Democratic Party is a boss, in the Republican Party he is a leader.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leonard Bernstein");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A liberal is a man or a woman or a child who looks forward to a better day, a more tranquil night, and a bright, infinite future.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carter Glass");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A liberal is a man who is willing to spend somebody else's money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Randolph");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A politician will do anything to keep his job - even become a patriot.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Pike");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "A war for a great principle ennobles a nation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George C. Wallace");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "After much prayerful consideration, I feel that I must say I have climbed my last political mountain.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry S. Truman");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "All the president is, is a glorified public relations man who spends his time flattering, kissing, and kicking people to get them to do what they are supposed to do anyway.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Quincy Adams");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Always vote for principle, though you may vote alone, and you may cherish the sweetest reflection that your vote is never lost.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Warren G. Harding");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "America's present need is not heroics but healing; not nostrums but normalcy; not revolution but restoration.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold H. Glasow");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "An idea not coupled with action will never get any bigger than the brain cell it occupied.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christy Romano");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "And after I make a lot of money, I'll be able to afford running for office.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gore Vidal");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Any American who is prepared to run for president should automatically, by definition, be disqualified from ever doing so.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gore Vidal");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Apparently, a democracy is a place where numerous elections are held at great cost without issues and with interchangeable candidates.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Colin Powell");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "90 percent of my time is spent on 10 percent of the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George S. Patton");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "A pint of sweat, saves a gallon of blood.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "A small body of determined spirits fired by an unquenchable faith in their mission can alter the course of history.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Grover Cleveland");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "A truly American sentiment recognizes the dignity of labor and the fact that honor lies in honest toil.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Thatcher");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "A world without nuclear weapons would be less stable and more dangerous for all of us.");
		db.insert("citations", AUTHOR, cv);
	}
	
	public void onCreate2(SQLiteDatabase db){
		ContentValues cv = new ContentValues();
		cv.put(AUTHOR, "David Borenstein");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "One cannot subdue a man by holding back his hands. Lasting peace comes not from force.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Morihei Ueshiba");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "One does not need buildings, money, power, or status to practice the Art of Peace. Heaven is right where you are standing, and that is the place to train.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace and friendship with all mankind is our wisest policy, and I wish we may be permitted to pursue it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dwight D. Eisenhower");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace and justice are two sides of the same coin.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mother Teresa");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace begins with a smile.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace cannot be achieved through violence, it can only be attained through understanding.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Greenleaf Whittier");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace hath higher tests of manhood, than battle ever knew.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lyndon B. Johnson");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace is a journey of a thousand miles and it must be taken one step at a time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace is its own reward.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Tullius Cicero");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace is liberty in tranquillity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baruch Spinoza");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace is not an absence of war, it is a virtue, a state of mind, a disposition for benevolence, confidence, justice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Franti");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Every single soul is a poem.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marilyn Hacker");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Everyone thinks they're going to write one book of poems or one novel.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gustave Flaubert");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Everything one invents is true, you may be perfectly sure of that. Poetry is as precise as geometry.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "T. S. Eliot");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Genuine poetry can communicate before it is understood.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Browning");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "God is the perfect poet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Sand");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "He who draws noble delights from sentiments of poetry is a true poet, though he has never written a line in all his life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Penn Warren");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "How do poems grow? They grow out of your life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Schuyler");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "However, if a poem can be reduced to a prose sentence, there can't be much to it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Davison");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "I like poems that are little games.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Howard Nemerov");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "I sometimes talk about the making of a poem within the poem.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carol Ann Duffy");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "I still read Donne, particularly his love poems.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "I was reading the dictionary. I thought it was a poem about everything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Hardy");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "If Galileo had said in verse that the world moved, the inquisition might have let him alone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Carradine");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "If you cannot be a poet, be the poem.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "M. H. Abrams");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "If you read quickly to get through a poem to what it means, you have missed the body of the poem.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Barton");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "No poem is easily grasped; so why should any reader expect fast results?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Horace");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "No poems can please for long or live that are written by water drinkers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Voltaire");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "One merit of poetry few persons will deny: it says more and in fewer words than prose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Muldoon");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "One will never again look at a birch tree, after the Robert Frost poem, in exactly the same way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Strand");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Pain is filtered in a poem so that it becomes finally, in the end, pleasure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James E. Rogers");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I come from a profession which has suffered greatly because of the lack of civility. Lawyers treat each other poorly and it has come home to haunt them. The public will not tolerate a lack of civility.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johnnie Cochran");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I decided I wanted to be a lawyer when I was 11 years of age.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nicole Kidman");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I have a different approach. I don't file lawsuits because I really don't care.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Alito");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I have been committed to carrying out my duties... in accordance with both the letter and spirit of all applicable rules of ethics and canons of conduct.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Janet Reno");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I have been surrounded by some of the smartest, brightest, most caring lawyers, by agents who are willing to risk their lives for others, by support staff that are willing to work as hard as they can.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Roberts Rinehart");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I never saw a lawyer yet who would admit he was making money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Max Walker");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I used to want to be a lawyer, but I didn't want to have half my brain sucked out.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel Goleman");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I would say that IQ is the strongest predictor of which field you can get into and hold a job in, whether you can be an accountant, lawyer or nurse, for example.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jay Alan Sekulow");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I wouldn't pretend to tell you we don't pay our lawyers well.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Glazer");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I'm afraid I talk a lot, too much, perhaps. I should have been a lawyer or a college professor or a windy politician, though I'm glad I am not any of these.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anita Hill");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I'm not sure I can say there is a clean line between me as an individual and me as a lawyer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver North");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "I'm trusting in the Lord and a good lawyer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Loni Anderson");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "If somebody invented cigarettes today, the government would not legalize them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lord Halifax");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "If the laws could speak for themselves, they would complain of the lawyers in the first place.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Addison Mizner");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Ignorance of the law excuses no man from practicing it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Butler");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "In law, nothing is certain but the expense.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carol Burnett");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "It costs a lot to sue a magazine, and it's too bad that we don't have a system where the losing team has to pay the winning team's lawyers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bugs Baer");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "It is impossible to tell where the law stops and justice begins.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louis XIV");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "It is legal because I wish it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Dershowitz");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Judges are the weakest link in our system of justice, and they are also the most protected.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joyce Carol Oates");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Boxing has become America's tragic theater.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Ralph Augustine");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Bulls do not win bull fights. People do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Billie Jean King");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Champions keep playing until they get it right.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Singletary");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Do you know what my favorite part of the game is? The opportunity to play.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Satchel Paige");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Don't look back. Something might be gaining on you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry Vardon");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Don't play too much golf. Two rounds a day are plenty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Grantland Rice");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Eighteen holes of match play will teach you more about your foe than 18 years of dealing with him across a desk.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Reggie Jackson");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Fans don't boo nobodies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert Hoover");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Fishing is much more than fish. It is the great occasion when we may return to the fine simplicity of our forefathers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Landry");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Football is an incredible game. Sometimes it's so incredible, it's unbelievable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roger Kahn");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Football is violence and cold weather and sex and college rye.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack Benny");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Give me golf clubs, fresh air and a beautiful partner, and you can keep the clubs and the fresh air.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eric Liddell");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "God made me fast. And when I run, I feel His pleasure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dan Gable");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Gold medals aren't really made of gold. They're made of sweat, determination, and a hard-to-find alloy called guts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Updike");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Golf appeals to the idiot in us and the child. Just how childlike golf players become is proven by their frequent inability to count past five.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Wordsworth");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Golf is a day spent in a round of strenuous idleness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woodrow Wilson");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Golf is a game in which one endeavors to control a ball with implements ill adapted for the purpose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Harvey");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Golf is a game in which you yell \"fore,\" shoot six, and write down five.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Golf is a good walk spoiled.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Bishop");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Golf is played by twenty million mature American men whose wives think they are out having fun.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Smith");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "If I had been the Virgin Mary, I would have said \"No.\"");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "If I had to choose a religion, the sun as the universal giver of life would be my god.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lenny Bruce");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "If Jesus had been killed twenty years ago, Catholic school children would be wearing little electric chairs around their necks instead of crosses.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "If the grandfather of the grandfather of Jesus had known what was hidden within him, he would have stood humble and awe-struck before his soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas Adams");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Isn't it enough to see that a garden is beautiful without having to believe that there are fairies at the bottom of it too?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. L. Mencken");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "It is now quite lawful for a Catholic woman to avoid pregnancy by a resort to mathematics, though she is still forbidden to resort to physics or chemistry.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Keller");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "It is wonderful how much time good people spend fighting the devil. If they would only expend the same amount of energy loving their fellow men, the devil would die in his own tracks of ennui.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Keller");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "It's wonderful to climb the liquid mountains of the sky. Behind me and before me is God and I have no fears.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Maher");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Jim Bakker spells his name with two k's because three would be too obvious.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Buddha");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Just as a candle cannot burn without fire, men cannot live without a spiritual life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Tullius Cicero");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Just as the soul fills the body, so God fills the world. Just as the soul bears the body, so God endures the world. Just as the soul sees but is not seen, so God sees but is not seen. Just as the soul feeds the body, so God gives food to the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Gates");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Just in terms of allocation of time resources, religion is not very efficient. There's a lot more I could be doing on a Sunday morning.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Garden");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Krishna children were taught that in the spiritual world there were no parents, only souls and hence this justified their being kept out of view from others, cloistered in separate buildings and sheltered from the evil material world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert K. Chesterton");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Let your religion be less of a theory and more of a love affair.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Morality is of the highest importance - but for us, not for God.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Chase");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "More and more people care about religious tolerance as fewer and fewer care about religion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abu Bakar Bashir");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Muslims must believe that all power, success and victory comes from God alone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "No man ever believes that the Bible means what it says: He is always convinced that it says what he means.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert A. Heinlein");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "One man's theology is another man's belly laugh.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Pennington");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Our Father and Our God, unto thee, O Lord we lift our souls.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steve Yzerman");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I'm exhausted trying to stay healthy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Darrell Royal");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I'm still healthy as can be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Picabo Street");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "I've made a promise to myself to be a 100% healthy person if nothing else.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robin Wright Penn");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "If you're happy, if you're feeling good, then nothing else matters.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Epictetus");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "It takes more than just a good looking body. You've got to have the heart and soul to go with it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry St. John");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "Liberty is to the collective body, what health is to every individual body. Without health no pleasure can be tasted by man; without liberty, no happiness can be enjoyed by society.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert K. Chesterton");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "Man does not live by soap alone; and hygiene, or even health, is not much good unless you can take a healthy view of it or, better still, feel a healthy indifference to it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Jung");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "Man needs difficulties; they are necessary for health.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold Schwarzenegger");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "My body is like breakfast, lunch, and dinner. I don't think about it, I just have it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clara Schumann");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "My health may be better preserved if I exert myself less, but in the end doesn't each person give his life for his calling?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Lafferty");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "My New Year's resolution is to stick to a good workout plan that will keep me healthy and happy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kiana Tom");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "My personal goals are to be happy, healthy and to be surrounded by loved ones.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henrik Ibsen");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "People who don't know how to keep themselves healthy ought to have the decency to get themselves buried, and not waste time about it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Marston");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "Rest when you're weary. Refresh and renew yourself, your body, your mind, your spirit. Then get back to work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victoria Principal");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "Success doesn't mean that you are healthy, success doesn't mean that you're happy, success doesn't mean that you're rested. Success really doesn't mean that you look good, or feel good, or are good.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Kerr");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "The average, healthy, well-adjusted adult gets up at seven-thirty in the morning feeling just plain terrible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martha Graham");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "The body is a sacred garment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francesca Annis");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "The minute anyone's getting anxious I say, You must eat and you must sleep. They're the two vital elements for a healthy life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert K. Chesterton");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "The trouble with always trying to preserve the health of the body is that it is so difficult to do without destroying the health of the mind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Deepak Chopra");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "The way you think, the way you behave, the way you eat, can influence your life by 30 to 50 years.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Black Elk");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "I cured with the power that came through me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oskar Schindler");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "I was now resolved to do everything in my power to defeat the system.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peace Pilgrim");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "If you realized how powerful your thoughts are, you would never think a negative thought.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Carnegie");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Immense power is acquired by assuring yourself in your secret reveries that you were born to control affairs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "A. J. P. Taylor");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "In my opinion, most of the great men of the past were only there for the beer - the wealth, prestige and grandeur that went with the power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carolyn Heilbrun");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Ironically, women who acquire power are more likely to be criticized for it than are the men who have always had it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Epicurus");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "It is folly for a man to pray to the gods for that which he has the power to obtain by himself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Blaise Pascal");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Justice and power must be brought together, so that whatever is just may be powerful, and whatever is powerful may be just.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Knowledge is power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alvin Toffler");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Knowledge is the most democratic source of power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bruce Lee");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Knowledge will give you power, but character respect.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Donald Trump");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Love him or hate him, Trump is a man who is certain about what he wants and sets out to get it, no holds barred. Women find his power almost as much of a turn-on as his money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Epictetus");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Make the best use of what is in your power, and take the rest as it happens.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Blaise Pascal");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Man's greatness lies in his power of thought.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Jackson");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Money is power, and in that government which pays all the public officers of the states will all political power be substantially concentrated.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Young");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Moral power is probably best when it is not used. The less you use it the more you have.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold Schwarzenegger");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "My relationship to power and authority is that I'm all for it. People need somebody to watch over them. Ninety-five percent of the people in the world need to be told what to do and how to behave.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Lincoln");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Nearly all men can stand adversity, but if you want to test a man's character, give him power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eleanor Roosevelt");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Never allow a person to tell you no who doesn't have the power to say yes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nelson Rockefeller");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Never forget that the most powerful force on earth is love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "First love is only a little foolishness and a lot of curiosity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Judy Garland");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "For it was not into my ear you whispered, but into my heart. It was not my lips you kissed, but my soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ovid");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Fortune and love favor the brave.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lord Byron");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Friendship is Love without his wings!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Caleb Colton");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Friendship often ends in love; but love in friendship - never.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Gravitation is not responsible for people falling in love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Pym");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "How absurd and delicious it is to be in love with somebody younger than yourself. Everybody should try it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodore Dreiser");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "I believe in the compelling power of love. I do not understand it. I believe it to be the most fragrant blossom of all this thorny existence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Judy Garland");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "I can live without money, but I cannot live without love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mother Teresa");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "I have found the paradox, that if you love until it hurts, there can be no more hurt, only more love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Eliot");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "I like not only to be loved, but also to be told I am loved.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. D. Salinger");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "I was about half in love with her by the time we sat down. That's the thing about girls. Every time they do something pretty... you fall half in love with them, and then you never know where the hell you are.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emmet Fox");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "If you could only love enough, you could be the most powerful person in the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "A. A. Milne");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "If you live to be a hundred, I want to live to be a hundred minus one day so I never have to live without you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michel de Montaigne");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "If you press me to say why I loved him, I can say no more than because he was he, and I was I.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ovid");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "If you want to be loved, be lovable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erich Fromm");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Immature love says: 'I love you because I need you.' Mature love says 'I need you because I love you.'");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erich Fromm");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "In love the paradox occurs that two beings become one and yet remain two.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Wadsworth Longfellow");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "It is difficult to know at what moment love begins; it is less difficult to know that it has begun.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miguel de Unamuno");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "It is sad not to love, but it is much sadder not to be able to love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Megan Fox");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I like the bad-boy types. Generally the guy I'm attracted to is the guy in the club with all the tattoos and nail polish. He's usually the lead singer in a punk band and plays guitar. But my serious boyfriends are relatively clean-cut, nice guys. So it's strange.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daphne Zuniga");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I started dating older men, and I would fall in love with them. I thought they could teach me about life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daphne Zuniga");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I stopped dating for six months a year ago. Dating requires a lot of energy and focus.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Kuralt");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I suppose I was a little bit of what would be called today a nerd. I didn't have girlfriends, and really I wasn't a very social boy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Patricia Velasquez");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I think if I could have a boyfriend like my brothers I'd be really happy. But without the brother thing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jennifer Love Hewitt");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I think more dating stuff is scheduling. It's needing people who understand your work schedule.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Matthew Perry");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I used to be a real prince charming if I went on a date with a girl. But then I'd get to where I was likely to have a stroke from the stress of keeping up my act. I've since learned the key to a good date is to pay attention on her.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kelly Osbourne");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I was dating this guy and we would spend all day text messaging each other. And he thought that he could tell that he liked me more because he actually spelt the word 'YOU' and I just put the letter 'U'.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Sandler");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I'm 31 now. I think I'm beginning to understand what life is, what romance is, and what a relationship means.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Garry Shandling");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I'm dating a woman now who, evidently, is unaware of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Shaun Cassidy");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I'm not cynical about marriage or romance. I enjoyed being married. And although being single was fun for a while, there was always the risk of dating someone who'd owned a lunch box with my picture on it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Taylor Swift");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I'm not the girl who always has a boyfriend. I'm the girl who rarely has a boyfriend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orlando Bloom");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I'm quite sensitive to women. I saw how my sister got treated by boyfriends. I read this thing that said when you are in a relationship with a woman, imagine how you would feel if you were her father. That's been my approach, for the most part.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kristin Davis");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I've been dating since I was fifteen. I'm exhausted. Where is he?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Taylor Dayne");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I've had enough boyfriends and enough issues. I'd seen enough train wrecks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leonardo DiCaprio");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "I've never been Romeo who meets a girl and falls for her immediately. It's been a much slower process for me each time I've gone into a relationship.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orson Welles");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "If there hadn't been women we'd still be squatting in a cave eating raw meat, because we made civilization in order to impress our girlfriends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joe Rogan");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "If you can lie, you can act, and if you can lie to crazy girlfriends, you can act under pressure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carmen Electra");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "If you don't have a valentine, hang out with your girlfriends, don't go looking for someone. When it's right, they'll come to you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jimmy Fallon");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "If you're a sports fan you realize that when you meet somebody, like a girlfriend, they kind of have to root for your team. They don't have a choice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "He that cannot forgive others breaks the bridge over which he must pass himself; for every man has need to be forgiven.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clarissa Pinkola Estes");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "How does one know if she has forgiven? You tend to feel sorrow over the circumstance instead of rage, you tend to feel sorry for the person rather than angry with him. You tend to have nothing left to say about it all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Publilius Syrus");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "How unhappy is he who cannot forgive himself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Paul");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Humanity is never so beautiful as when praying for forgiveness, or else forgiving another.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "I can forgive, but I cannot forget, is only another way of saying, I will not forgive. Forgiveness ought to be like a cancelled note - torn in two, and burned up, so that it never can be shown against one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Chaim Herzog");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "I do not bring forgiveness with me, nor forgetfulness. The only ones who can forgive are dead; the living have no right to forget.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Catherine the Great");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "I shall be an autocrat, that's my trade; and the good Lord will forgive me, that's his.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Nietzsche");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "If there is something to pardon in everything, there is also something to condemn.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Blake");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "It is easier to forgive an enemy than to forgive a friend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Grace Hopper");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "It is often easier to ask for forgiveness than to ask for permission.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jessamyn West");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "It is very east to forgive others their mistakes; it takes more grit and gumption to forgive them for having witnessed your own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lewis B. Smedes");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "It takes one person to forgive, it takes two people to be reunited.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Olin Miller");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "It's far easier to forgive an enemy after you've got even with him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lana Turner");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "It's said in Hollywood that you should always forgive your enemies - because you never know when you'll have to work with them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Cousins");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Life is an adventure in forgiveness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bruce Lee");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Mistakes are always forgivable, if one has the courage to admit them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Janine Turner");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "My desire is to be a forgiving, non-judgmental person.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. Jackson Brown, Jr.");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Never forget the three powerful resources you always have available to you: love, prayer, and forgiveness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marlene Dietrich");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Once a woman has forgiven her man, she must not reheat his sins for breakfast.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francois de La Rochefoucauld");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "One forgives to the degree that one loves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fred Woodworth");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "It seems like such a terrible shame that innocent civilians have to get hurt in wars, otherwise combat would be such a wonderfully healthy way to rid the human race of unneeded trash.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vanessa Kerry");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "It was definitely a part of our life. I mean, my mom had both her brothers and her fiancee in Vietnam at the same time, so it wasn't just my dad's story, it was my mom's story too. And we definitely grew up listening to the stories.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isaac Asimov");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "John Dalton's records, carefully preserved for a century, were destroyed during the World War II bombing of Manchester. It is not only the living who are killed in war.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Percy Bysshe Shelley");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Man has no right to kill his brother. It is no excuse that he does so in uniform: he only adds the infamy of servitude to the crime of murder.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Mankind must put an end to war before war puts an end to mankind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Winston Churchill");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Never in the field of human conflict was so much owed by so many to so few.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ernest Hemingway");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Never think that war, no matter how necessary, nor how justified, is not a crime.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry A. Kissinger");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "No country can act wisely simultaneously in every part of the globe at every moment of time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert Hoover");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Older men declare war. But it is the youth that must fight and die.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pedro Calderon de la Barca");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "One may know how to gain a victory, and know not how to use it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther King, Jr.");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "One of the greatest casualties of the war in Vietnam is the Great Society... shot down on the battlefield of Vietnam.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Santayana");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Only the dead have seen the end of the war.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Omar N. Bradley");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Ours is a world of nuclear giants and ethical infants. We know more about war that we know about peace, more about killing that we know about living.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bertrand Russell");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Patriots always talk of dying for their country and never of killing for their country.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Sandburg");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Sometime they'll give a war and nobody will come.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erwin Rommel");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Sweat saves blood.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Euripides");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "Ten soldiers wisely led will beat a hundred without a head.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "The basic problems facing the world today are not susceptible to a military solution.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Nietzsche");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "The best weapon against an enemy is another enemy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Friedman");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "The direct use of force is such a poor solution to any problem, it is generally employed only by small children and large nations.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Conrad Hall");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Dad, wherever you are, you are gone but you will never be forgotten.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dionne Warwick");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Do you know that other than my father, I've never had a man take care of me?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Levine");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Employee fathers need to step up to the plate and put their family needs on the table.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Ciardi");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Every parent is at some time the father of the unreturned prodigal, with nothing to do but keep his house open to hope.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Hudson");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Father or stepfather - those are just titles to me. They don't mean anything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Prince Charles");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Father told me that if I ever met a lady in a dress like yours, I must look her straight in the eyes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Mead");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Fathers are biological necessities, but social accidents.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Hudson");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Fathers in today's modern families can be so many things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Sandler");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Feels good to try, but playing a father, I'm getting a little older. I see now that I'm taking it more serious and I do want that lifestyle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dick Clark");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "Humor is always based on a modicum of truth. Have you ever heard a joke about a father-in-law?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hedy Lamarr");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I am not ashamed to say that no man I ever met was my father's equal, and I never loved any other man as much.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sidney Poitier");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I decided in my life that I would do nothing that did not reflect positively on my father's life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charley Pride");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I grew up not liking my father very much. I never saw him cry. But he must have. Everybody cries.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Annette Funicello");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I have always thought of Walt Disney as my second father.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Imelda Marcos");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I have never been a material girl. My father always told me never to love anything that cannot love you back.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orson Scott Card");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I hope I am remembered by my children as a good father.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miguel Indurain");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I inherited that calm from my father, who was a farmer. You sow, you wait for good or bad weather, you harvest, but working is something you always need to do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Jackson");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I just wish I could understand my father.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barry White");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I kept my babies fed. I could have dumped them, but I didn't. I decided that whatever trip I was on, they were going with me. You're looking at a real daddy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Halle Berry");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I know that I will never find my father in any other man who comes into my life, because it is a void in my life that can only be filled by him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ursula Andress");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "I take care of my flowers and my cats. And enjoy food. And that's living.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles M. Schulz");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "I think I've discovered the secret of life - you just hang around until you get used to it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "In three words I can sum up everything I've learned about life: it goes on.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "It is not length of life, but depth of life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Philip Green");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "It's all about quality of life and finding a happy balance between work and friends and family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Josh Billings");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life consists not in holding good cards but in playing those you hold well.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life does not cease to be funny when people die any more than it ceases to be serious when people laugh.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Antoine de Saint-Exupery");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life has meaning only if one barters it day by day for something other than itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sholom Aleichem");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is a dream for the wise, a game for the fool, a comedy for the rich, a tragedy for the poor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James M. Barrie");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is a long lesson in humility.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Truman Capote");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is a moderately good play with a badly written third act.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jose Ortega y Gasset");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is a series of collisions with the future; it is not the sum of what we have been, but what we yearn to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dave Barry");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is anything that dies when you stomp on it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sara Teasdale");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is but thought.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Herbert");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is half spent before we know what it is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miguel Angel Ruiz");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is like dancing. If we have a big floor, many people will dance. Some will get angry when the rhythm changes. But life is changing all the time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Cahan");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is much shorter than I imagined it to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert James Waller");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is never easy for those who dream.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fran Lebowitz");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is something to do when you can't get to sleep.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas Adams");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life is wasted on the living.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marilyn Monroe");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I don't know who invented high heels, but all women owe him a lot.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Coco Chanel");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I don't know why women want any of the things men have when one the things that women have is men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Galliano");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I don't love dolls. I love women. I love their bodies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jennifer Lopez");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I grew up in the Bronx where you would stay up late with your girlfriends, just being silly in our bedrooms, whatever. And I was always the clown.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Sinatra");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I like intelligent women. When you go out, it shouldn't be a staring contest.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "B. R. Ambedkar");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I measure the progress of a community by the degree of progress which women have achieved.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Katharine Hepburn");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I never realized until lately that women were supposed to be the inferior sex.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Thatcher");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I owe nothing to Women's Lib.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Thurber");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I think that maybe if women and children were in charge we would get somewhere.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dylan Moran");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I think that women just have a primeval instinct to make soup, which they will try to foist on anybody who looks like a likely candidate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charlize Theron");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I think today women are very scared to celebrate themselves, because then they just get labeled.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Laurie Anderson");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I think women are excellent social critics.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Heinrich Heine");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I will not say that women have no character; rather, they have a new one every day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Courtney Love");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I'll always prefer to play with women and hang out with women, and I'll always be a feminist.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Eliot");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I'm not denyin' the women are foolish. God Almighty made 'em to match the men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christie Hefner");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I'm surrounded by very powerful women and very progressive men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gloria Steinem");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "I've yet to be on a campus where most women weren't worrying about some aspect of combining marriage, children, and a career. I've yet to find one where many men were worrying about the same thing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alphonse Karr");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "If men knew all that women think, they would be twenty times more audacious.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "If women were particular about men's characters, they would never get married at all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elizabeth Wurtzel");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "In life, single women are the most vulnerable adults. In movies, they are given imaginary power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Mandrell");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "By our Heavenly Father and only because of God, only because of God. We're like other couples. We do not get along perfectly; we do not go without arguments and, as I call them, fights, and heartache and pain and hurting each other. But a marriage is three of us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Cuppy");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Caesar might have married Cleopatra, but he had a wife at home. There's always something.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Loretta Lynn");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Daddy was real gentle with kids. That's why I expected so much out of marriage, figuring that all men should be steady and pleasant.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abigail Adams");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Do not put such unlimited power into the hands of husbands. Remember all men would be tyrants if they could.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henny Youngman");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Do you know what it means to come home at night to a woman who'll give you a little love, a little affection, a little tenderness? It means you're in the wrong house, that's what it means.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James C. Dobson");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Don't marry the person you think you can live with; marry only the individual you think you can't live without.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Giraudoux");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Faithful women are all alike, they think only of their fidelity, never of their husbands.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "For years my wedding ring has done its job. It has led me not into temptation. It has reminded my husband numerous times at parties that it's time to go home. It has been a source of relief to a dinner companion. It has been a status symbol in the maternity ward.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zsa Zsa Gabor");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Getting divorced just because you don't love a man is almost as silly as getting married just because you do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jeremy Taylor");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "He that loves not his wife and children feeds a lioness at home, and broods a nest of sorrows.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mae West");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "He's the kind of man a woman would have to marry to get rid of.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "How can a woman be expected to be happy with a man who insists on treating her as if she were a perfectly normal human being.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Carson");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "I don't think my wife likes me very much, when I had a heart attack she wrote for an ambulance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lyndon B. Johnson");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "I have learned that only two things are necessary to keep one's wife happy. First, let her think she's having her own way. And second, let her have it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rita Rudner");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "I love being married. It's so great to find that one special person you want to annoy for the rest of your life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nancy Astor");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "I married beneath me, all women do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Bush");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "I married the first man I ever kissed. When I tell this to my children, they just about throw up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Reese Witherspoon");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "I think women are natural caretakers. They take care of everybody. They take care of their husbands and their kids and their dogs, and don't spend a lot of time just getting back and taking time out.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Bergin");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "I wanted to marry a girl just like my mom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bette Davis");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "I'd marry again if I found a man who had fifteen million dollars, would sign over half to me, and guarantee that he'd be dead within a year.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. F. Schumacher");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Infinite growth of material consumption in a finite world is an impossibility.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Milton Friedman");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Inflation is taxation without legislation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Camus");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "It is a kind of spiritual snobbery that makes people think they can be happy without money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich List");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "It is bad policy to regulate everything... where things may better regulate themselves and can be better promoted by private exertions; but it is no less bad policy to let those things alone which can only be promoted by interfering social power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ron Lewis");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "It is incumbent upon each of us to improve spending and savings practices to ensure our own individual financial security and preserve the collective economic well-being of our great society.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Spurgeon");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "It's not the having, it's the getting.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Suze Orman");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Many people are in the dark when it comes to money, and I'm going to turn on the lights.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. Paul Getty");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Money is like manure. You have to spread it around or it smells.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Petty");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Money is the best rule of commerce.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mason Cooley");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Money is to my social existence what health is to my body.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Donald Trump");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Money was never a big motivation for me, except as a way to keep score. The real excitement is playing the game.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Feather");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "One of the funny things about the stock market is that every time one person buys, another sells, and both think they are astute.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Locke");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Our incomes are like our shoes; if too small, they gall and pinch us; but if too large, they cause us to stumble and to trip.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Suze Orman");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Owning a home is a keystone of wealth... both financial affluence and emotional security.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Rohn");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Part of your heritage in this society is the opportunity to become financially independent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Muhammad Yunus");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Poverty is unnecessary.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander John Ellis");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Rounding to the nearest cent is sufficiently accurate for practical purposes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Wood Krutch");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Security depends not so much upon how much you have, as upon how much you can do without.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Greer Garson");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Starting out to make money is the greatest mistake in life. Do what you feel you have a flair for doing, and if you are good enough at it, the money will come.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Carnegie");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Surplus wealth is a sacred trust which its possessor is bound to administer in his lifetime for the good of the community.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robin Williams");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Carpe per diem - seize the check.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Corporation: An ingenious device for obtaining profit without individual responsibility.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Evan Esar");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Definition of a Statistician: A man who believes figures don't lie, but admits than under analysis some of them won't stand up either.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Evan Esar");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Definition of Statistics: The science of producing unreliable facts from reliable figures.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walt Disney");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Disneyland is a work of love. We didn't go into Disneyland just with the idea of making money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gary Ryan Blair");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Do more than is required. What is the distance between someone who achieves their goals consistently and those who spend their lives and careers merely following? The extra mile.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stanislaw Lem");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Do not trust people. They are capable of greatness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Colin Powell");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Don't let your ego get too close to your position, so that if your position gets shot down, your ego doesn't go with it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry Emerson Fosdick");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Don't simply retire from something; have something to retire to.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Howard Aiken");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Don't worry about people stealing your ideas. If your ideas are any good, you'll have to ram them down people's throats.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert Hoover");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Economic depression cannot be cured by legislative action or executive pronouncement. Economic wounds must be healed by the action of the cells of the economic body - the producers and consumers themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Hill");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Effort only fully releases its reward after a person refuses to quit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Warhol");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Employees make the best dates. You don't have to pick them up and they're always tax-deductible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Orben");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Every day I get up and look through the Forbes list of the richest people in America. If I'm not there, I go to work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ted Turner");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Every few seconds it changes - up an eighth, down an eighth -it's like playing a slot machine. I lose $20 million, I gain $20 million.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Every young man would do well to remember that all successful business stands on the foundation of morality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert H. Schuller");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Failure doesn't mean you are a failure it just means you haven't succeeded yet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Allen");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Family farms and small businesses are the backbone of our communities.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Cousins");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Government in the U.S. today is a senior partner in every business in the country.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodore Isaac Rubin");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Happiness does not come from doing easy work but from the afterglow of satisfaction that comes after the achievement of a difficult task that demanded our best.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William E. Gladstone");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Here is my first principle of foreign policy: good government at home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kay Bailey Hutchison");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "I have long believed taxpayers make better use of their money than the government ever could.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Woollcott");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "I'm tired of hearing it said that democracy doesn't work. Of course it doesn't work. We are supposed to work it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Harvey");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "If 'pro' is the opposite of 'con' what is the opposite of 'progress'?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fred Woodworth");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "If human beings are fundamentally good, no government is necessary; if they are fundamentally bad, any government, being composed of human beings, would be bad also.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Madison");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "If men were angels, no government would be necessary.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Saint Augustine");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "In the absence of justice, what is sovereignty but organized robbery?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "It is error alone which needs the support of government. Truth can stand by itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John W. Gardner");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "It is hard to feel individually responsible with respect to the invisible processes of a huge and distant government.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Jackson");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "It is to be regretted that the rich and powerful too often bend the acts of government to their own selfish purposes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Stoppard");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "It's not the voting that's democracy; it's the counting.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Madison");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Knowledge will forever govern ignorance; and a people who mean to be their own governors must arm themselves with the power which knowledge gives.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Otto von Bismarck");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Laws are like sausages, it is better not to see them being made.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Laws too gentle are seldom obeyed; too severe, seldom executed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Penn");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Let the people think they govern and they will be governed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Larry Flynt");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Majority rule only works if you're also considering individual rights. Because you can't have five wolves and one sheep voting on what to have for supper.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Denis Diderot");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Man will never be free until the last king is strangled with the entrails of the last priest.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lily Tomlin");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Ninety eight percent of the adults in this country are decent, hardworking, honest Americans. It's the other lousy two percent that get all the publicity. But then, we elected them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Lincoln");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "No man is good enough to govern another man without that other's consent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Ohio claims they are due a president as they haven't had one since Taft. Look at the United States, they have not had one since Lincoln.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Weil");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Get people back into the kitchen and combat the trend toward processed food and fast food.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter De Vries");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Gluttony is an emotional escape, a sign something is eating us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jonathan Swift");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "He was a bold man that first eat an oyster.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Redford");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Health food may be good for the conscience but Oreos taste a hell of a lot better.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Catherine Bateson");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Human beings do not eat nutrients, they eat food.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Mondavi");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I always knew that food and wine were vital, with my mother being Italian and a good cook.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marilu Henner");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I always say centered food equals centered behavior.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edie Brickell");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I can make dressing - or stuffing. Y'all call it stuffing up here, we call it dressing down there. It's really good dressing. That family recipe was passed on, and I love to make that.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I come from a family where gravy is considered a beverage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Brady");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I could talk food all day. I love good food.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jennifer Love Hewitt");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I do love Italian food. Any kind of pasta or pizza. My new pig out food is Indian food. I eat Indian food like three times a week. It's so good.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Rooney");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I don't like food that's too carefully arranged; it makes me think that the chef is spending too much time arranging and not enough time cooking. If I wanted a picture I'd buy a painting.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sasha Cohen");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I eat a variety of foods like vegetables, fruit and beef for protein and iron.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William J. Clinton");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I haven't eaten at a McDonald's since I became President.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sarah Michelle Gellar");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I like food. I like eating. And I don't want to deprive myself of good food.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Soul");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I like to eat and I love the diversity of foods.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tyra Banks");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I love all kinds of bread. Whenever I crave junk food, I want salty things like peanuts or potato chips.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tyra Banks");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I love food and feel that it is something that should be enjoyed. I eat whatever I want. I just don't overeat.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alma Guillermoprieto");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I love food and I love everything involved with food. I love the fun of it. I love restaurants. I love cooking, although I don't cook very much. I love kitchens.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Susan Lucci");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I love spaghetti and meatballs... I eat a lot.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Kingsley");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "He was one of those men who possess almost every gift, except the gift of the power to use them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. L. Mencken");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Honor is simply the morality of superior men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Heinrich Heine");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Human misery is too great for men to do without faith.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clarence Darrow");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "I am an agnostic; I do not pretend to know what many ignorant men are sure of.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Justin Timberlake");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "I have 20,000 girlfriends, all around the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Davy Crockett");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "I have always supported measures and principles and not men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Locke");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "I have always thought the actions of men the best interpreters of their thoughts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "I look only to the good qualities of men. Not being faultless myself, I won't presume to probe into the faults of others.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mae West");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "I only have 'yes' men around me. Who needs 'no' men?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mae West");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "I only like two kinds of men, domestic and imported.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Tolstoy");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "If so many men, so many minds, certainly so many hearts, so many kinds of love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rita Mae Brown");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "If the world were a logical place, men would ride side-saddle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis of Assisi");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "If you have men who will exclude any of God's creatures from the shelter of compassion and pity, you will have men who will deal likewise with their fellow men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Caleb Colton");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "In life we shall find many men that are great, and some that are good, but very few men that are both great and good.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frederick Douglass");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "It is easier to build strong children than to repair broken men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Diogenes");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "It is the privilege of the gods to want nothing, and of godlike men to want little.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emil Zatopek");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "It's at the borders of pain and suffering that the men are separated from the boys.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eva Herzigova");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Italian men do appreciate beautiful women. They're not afraid of the beauty, which is nice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Dawson");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Law describes the way things would work if men were angels.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Liberty means responsibility. That is why most men dread it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Nelson Bolles");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "I have always argued that change becomes stressful and overwhelming only when you've lost any sense of the constancy of your life. You need firm ground to stand on. From there, you can deal with that change.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walter Wager");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "I met an American woman and got married so I had to get a job.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Campbell");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "I think the person who takes a job in order to live - that is to say, for the money - has turned himself into a slave.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Ladd");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "I'm working myself to death.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Ustinov");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "If Botticelli were alive today he'd be working for Vogue.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ogden Nash");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "If you don't want to work you have to work to earn enough money so that you won't have to work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lawrence Welk");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "If you put all your strength and faith and vigor into a job and try to do the best you can, the money will come.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ron Livingston");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "It can be liberating to get fired because you realize the world doesn't end. There's other ways to make money, better jobs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pablo Picasso");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "It is your work in life that is the ultimate seduction.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Faulkner");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "It's a shame that the only thing a man can do for eight hours a day is work. He can't eat for eight hours; he can't drink for eight hours; he can't make love for eight hours. The only thing a man can do for eight hours is work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Frank");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Laziness may appear attractive, but work gives satisfaction.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Horace");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Life grants nothing to us mortals without hard work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lee Iacocca");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Management is nothing more than motivating other people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harold S. Geneen");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Management must manage!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harold S. Geneen");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Managers in all too many American companies do not achieve the desired results because nobody makes them do it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Fuller");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Men for the sake of getting a living forget to live.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Levine");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Most people treat the office manual the way they treat a software manual. They never look at it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Booker T. Washington");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Nothing ever comes to one, that is worth having, except as a result of hard work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Halas");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Nothing is work unless you'd rather be doing something else.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maya Angelou");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Nothing will work unless you do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joan Collins");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I don't believe in dieting.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jenny Craig");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I don't believe in fad diets.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peace Pilgrim");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I don't eat junk foods and I don't think junk thoughts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vanessa Hudgens");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I don't go long without eating. I never starve myself: I grab a healthy snack.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gene Tierney");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I followed the same diet for 20 years, eliminating starches, living on salads, lean meat, and small portions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fran Drescher");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I have been dairy free for several years, and I started because I felt it was going to reduce my allergies, which it did, and help me lose weight, which it did.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Powell");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I highly recommend worrying. It is much more effective than dieting.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Naomi Campbell");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I never diet. I smoke. I drink now and then. I never work out.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mae West");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I never worry about diets. The only carrots that interest me are the number you get in a diamond.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Mortimer");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I refuse to spend my life worrying about what I eat. There is no pleasure worth forgoing just for an extra three years in the geriatric ward.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Suzanne Somers");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I started dieting. I dieted, dieted, dieted and tried all the diets and I would lose and then I would go back to normal eating and would put it on and then some.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Warren Cuccurullo");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I thought, you know the food and the diet thing is one way to start yourself onto a healthy lifestyle, but if you don't move, if you don't start exercising you're gonna deteriorate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joe E. Lewis");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I told my doctor I get very tired when I go on a diet, so he gave me pep pills. Know what happened? I ate faster.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sophie Ellis Bextor");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I used to be hung up on my figure, but it's a waste of time. I don't believe in diets. Have four pints one night, be healthy the next.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eva Herzigova");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I'm lucky, I don't like sweets, not even chocolate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rachel Hunter");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I'm very girly. I love to talk about diets, exercise, kids, make-up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Totie Fields");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "I've been on a diet for two weeks and all I've lost is two weeks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Courteney Cox");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "If I like myself at this weight, then this is what I'm going to be. I don't have an eating disorder.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack LaLanne");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "If you've got a big gut and you start doing sit-ups, you are going to get bigger because you build up the muscle. You've got to get rid of that fat! How do you get rid of fat? By changing your diet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jerry Brown");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "It looks to me to be obvious that the whole world cannot eat an American diet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Nietzsche");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Does wisdom perhaps appear on the earth as a raven which is inspired by the smell of carrion?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joan Rivers");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Don't follow any advice, no matter how good, until you feel as deeply in your spirit as you think in your mind that the counsel is wise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dan Rather");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Don't taunt the alligator until after you've crossed the creek.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elbert Hubbard");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Every man is a damn fool for at least five minutes every day; wisdom consists in not exceeding the limit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas B. Macaulay");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Few of the many wise apothegms which have been uttered have prevented a single foolish action.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Publilius Syrus");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "From the errors of others, a wise man corrects his own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Archimedes");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Give me a lever long enough and a fulcrum on which to place it, and I shall move the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Good nature is worth more than knowledge, more than money, more than honor, to the persons who possess it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Huneker");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "He dares to be a fool, and that is the first step in the direction of wisdom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Wilson Little");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "He who devotes sixteen hours a day to hard study may become at sixty as wise as he thought himself at twenty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edgar R. Fiedler");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "He who lives by the crystal ball soon learns to eat ground glass.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Honesty is the first chapter in the book of wisdom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Josh Billings");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Honesty is the rarest wealth anyone can possess, and yet all the honesty in the world ain't lawful tender for a loaf of bread.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lucille Ball");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "I'd rather regret the things I've done than regret the things I haven't done.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lord Salisbury");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "If you believe the doctors, nothing is wholesome; if you believe the theologians, nothing is innocent; if you believe the military, nothing is safe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nelson Mandela");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "If you talk to a man in a language he understands, that goes to his head. If you talk to him in his language, that goes to his heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sidney Lanier");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "If you want to be found stand where the seeker seeks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Jordan");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "If you're trying to achieve, there will be roadblocks. I've had them; everybody has had them. But obstacles don't have to stop you. If you run into a wall, don't turn around and give up. Figure out how to climb it, go through it, or work around it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johann Wolfgang von Goethe");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Ignorant men raise questions that wise men answered a thousand years ago.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Muir");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "In every walk with nature one receives far more than he seeks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Gore");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "I think the cost of energy will come down when we make this transition to renewable energy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Redford");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "I think the environment should be put in the category of our national security. Defense of our resources is just as important as defense abroad. Otherwise what is there to defend?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brian Mulroney");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "I think the government has to reposition environment on top of their national and international priorities.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Wood Krutch");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "If people destroy something replaceable made by mankind, they are called vandals; if they destroy something irreplaceable made by God, they are called developers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Bartram");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "If we bestow but a very little attention to the economy of the animal creation, we shall find manifest examples of premeditation, perseverance, resolution, and consumate artifice, in order to effect their purpose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Wood Krutch");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "If we do not permit the earth to produce beauty and joy, it will in the end not produce food, either.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Lovelock");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "If we gave up eating beef we would have roughly 20 to 30 times more land for food than we have now.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Luther Burbank");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "If you violate Nature's laws you are your own prosecuting attorney, judge, jury, and hangman.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wangari Maathai");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "In a few decades, the relationship between the environment, resources and conflict may seem almost as obvious as the connection we see today between human rights, democracy and peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Garrett Hardin");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "In a finite world this means that the per capita share of the world's goods must steadily decrease.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Olympia Snowe");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "In today's world, it is no longer unimaginable to think that business can operate - and even thrive - in an environmentally-friendly manner.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Ray");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Industry is fortune's right hand, and frugality its left.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David R. Brower");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "It is absolutely imperative that we protect, preserve and pass on this genetic heritage for man and every other living thing in as good a condition as we received it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ansel Adams");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "It is horrifying that we have to fight our own government to save the environment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David R. Brower");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "It seems that every time mankind is given a lot of energy, we go out and wreck something with it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Tsongas");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Journey with me to a true commitment to our environment. Journey with me to the serenity of leaving to our children a planet in equilibrium.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Muir");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Keep close to Nature's heart... and break clear away, once in awhile, and climb a mountain or spend a week in the woods. Wash your spirit clean.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marsha Blackburn");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Liberals in Congress have spent the past three decades pandering to environmental extremists. The policies they have put in place are in large part responsible for the energy crunch we are seeing today. We have not built a refinery in this country for 30 years.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gale Norton");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Local innovation and initiative can help us better understand how to protect our environment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cathy McMorris");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Maintaining healthy forests is essential to those who make a living from the land and for those of us who use them for recreational purposes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eleanor Roosevelt");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I think, at a child's birth, if a mother could ask a fairy godmother to endow it with the most useful gift, that gift should be curiosity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johnny Vegas");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I used to be good with kids, but as I get older, I'm grumpy and terrible with them. As for doing a gig at a 6-year old's birthday party, you couldn't pay me enough.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mitch Hedberg");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I wanted to buy a candle holder, but the store didn't have one. So I got a cake.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gloria Stuart");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I'm amazed. When I was 40, I thought I'd never make 50. And at 50 I thought the frosting on the cake would be 60. At 60, I was still going strong and enjoying everything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victoria Beckham");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "I'm not materialistic. I believe in presents from the heart, like a drawing that a child does.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bruce Barton");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "If you can give your child only one gift, let it be enthusiasm.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Oates");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "If you look over the years, the styles have changed - the clothes, the hair, the production, the approach to the songs. The icing to the cake has changed flavors. But if you really look at the cake itself, it's really the same.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Annette Funicello");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "In 1993 my birthday present was a star on Hollywood's Walk of Fame.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ellen Glasgow");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "It is lovely, when I forget all birthdays, including my own, to find that somebody remembers me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sam Levenson");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "It was on my fifth birthday that Papa put his hand on my shoulder and said, 'Remember, my son, if you ever need a helping hand, you'll find one at the end of your arm.'");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marie Antoinette");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Let them eat cake.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plautus");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Let us celebrate the occasion with wine and sweet words.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Schmich");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Like many women my age, I am 28 years old.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brigham Young");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Love the giver more than the gift.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Staughton Lynd");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Most of us can remember a time when a birthday - especially if it was one's own - brightened the world as if a second sun has risen.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Loretta Lynn");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "My biggest hero, Gregory Peck, was my birthday present on April 14, 1973. I just sat and stared at him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Valvano");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "My father gave me the greatest gift anyone could give another person, he believed in me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Guy Johnson");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "My mother asked me what I wanted for my birthday, so I said I wanted to read poetry with her.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Lamb");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "New Year's Day is every man's birthday.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Karl Jaspers");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Only then, approaching my fortieth birthday, I made philosophy my life's work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Adler");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "It is the patriotic duty of every man to lie for his country.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ho Chi Minh");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "It was patriotism, not communism, that inspired me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "My fellow Americans, ask not what your country can do for you, ask what you can do for your country.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Aldington");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Nationalism is a silly cock crowing on his own dunghill.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Nationalism is an infantile disease. It is the measles of mankind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Ehrenreich");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "No matter that patriotism is too often the refuge of scoundrels. Dissent, rebellion, and all-around hell-raising remain the true duty of patriots.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Henry");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "On what rests the hope of the republic? One country, one language, one flag!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriot: the person who can holler the loudest without knowing what he is hollering about.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Bryce");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriotism consists not in waving the flag, but in striving that our country shall be righteous as well as strong.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Guy de Maupassant");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriotism is a kind of religion; it is the egg from which wars are hatched.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mick Jagger");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriotism is an instant reaction that fades away when the war starts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Calvin Coolidge");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriotism is easy to understand in America. It means looking out for yourself by looking out for your country.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adlai E. Stevenson");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriotism is not short, frenzied outbursts of emotion, but the tranquil and steady dedication of a lifetime.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Jean Nathan");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriotism is often an arbitrary veneration of real estate above principles.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriotism is supporting your country all the time, and your government when it deserves it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriotism is the virtue of the vicious.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bertrand Russell");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriotism is the willingness to kill and be killed for trivial reasons.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles de Gaulle");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriotism is when love of your own people comes first; nationalism, when hate for people other than your own comes first.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "Patriotism is your conviction that this country is superior to all others because you were born in it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert Croly");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "The average American is nothing if not patriotic.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Lincoln");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I remember my mother's prayers and they have always followed me. They have clung to me all my life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Sandler");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I sing seriously to my mom on the phone. To put her to sleep, I have to sing \"Maria\" from West Side Story. When I hear her snoring, I hang up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Art Linkletter");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I stand fearlessly for small dogs, the American Flag, motherhood and the Bible. That's why people love me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arianna Huffington");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I think while all mothers deal with feelings of guilt, working mothers are plagued by guilt on steroids!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zhang Ziyi");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I wanted to escape so badly. But of course I knew I couldn't just give up and leave school. It was only when I heard my mom's voice that I came out of my hiding place.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sandra Bullock");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I was a brownie for a day. My mom made me stop. She didn't want me to conform.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martina Hingis");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I was always at peace because of the way my mom treated me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Sharpton");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I was raised by a single mother who made a way for me. She used to scrub floors as a domestic worker, put a cleaning rag in her pocketbook and ride the subways in Brooklyn so I would have food on the table. But she taught me as I walked her to the subway that life is about not where you start, but where you're going. That's family values.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carol Burnett");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I wish my mother had left me something about how she felt growing up. I wish my grandmother had done the same. I wanted my girls to know me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Toni Braxton");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I'd lose my mind if I heard my kid call the nanny Mommy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Susie Bright");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I'm a Mommy's Girl - the strongest influence in my young life was my mom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joely Fisher");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I'm sure that my mom would have been happy with any path I chose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elizabeth Taylor");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "I've been through it all, baby, I'm mother courage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Larisa Oleynik");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "If my mom reads that I'm grammatically incorrect I'll have hell to pay.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward W. Howe");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "If there were no schools to take the children away from home part of the time, the insane asylums would be filled with mothers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Pierce");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "If you ever become a mother, can I have one of the puppies?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Honore De Balzac");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "It is only in the act of nursing that a woman realizes her motherhood in visible and tangible fashion; it is a joy of every moment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alphonsus Liguori");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Just as a mother finds pleasure in taking her little child on her lap, there to feed and caress him, in like manner our loving God shows His fondness for His beloved souls who have given themselves entirely to Him and have placed all their hope in His goodness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Eliot");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Life began with waking up and loving my mother's face.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Men are what their mothers made them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jacques Yves Cousteau");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "I am not a scientist. I am, rather, an impresario of scientists.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Santayana");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "I believe in general in a dualism between facts and the ideas of those facts in human heads.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Wendell Holmes");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "I hate facts. I always say the chief end of man is to form general propositions - adding that no general proposition is worth a damn.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. B. White");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "I see nothing in space as promising as the view from a Ferris wheel.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "If a man's wit be wandering, let him study the mathematics.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur C. Clarke");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "If an elderly but distinguished scientist says that something is possible, he is almost certainly right; but if he says that it is impossible, he is very probably wrong.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Quillen");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "If we wish to make a new world we have the material ready. The first one, too, was made out of chaos.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hugh Walpole");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "In all science, error precedes the truth, and it is better it should go first than last.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Jay Gould");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "In science, \"fact\" can only mean \"confirmed to such a degree that it would be perverse to withhold provisional assent.\" I suppose that apples might start to rise tomorrow, but the possibility does not merit equal time in physics classrooms.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Russell Baker");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Inanimate objects can be classified scientifically into three major categories; those that don't work, those that break down and those that get lost.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Konrad Lorenz");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "It is a good morning exercise for a research scientist to discard a pet hypothesis every day before breakfast. It keeps him young.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wernher von Braun");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "It will free man from the remaining chains, the chains of gravity which still tie him to this planet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. Y. Harburg");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Leave the atom alone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Men love to wonder, and that is the seed of science.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "No amount of experimentation can ever prove me right; a single experiment can prove me wrong.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edmund Hillary");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Nobody climbs mountains for scientific reasons. Science is used to raise money for the expeditions, but you really climb for the hell of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Aurelius");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Nothing has such power to broaden the mind as the ability to investigate systematically and truly all that comes under thy observation in life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry B. Adams");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Nothing in education is so astonishing as the amount of ignorance it accumulates in the form of inert facts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Howard Nemerov");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Nothing in the universe can travel at the speed of light, they say, forgetful of the shadow's speed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Only two things are infinite, the universe and human stupidity, and I'm not sure about the former.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zach Braff");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "I'm by no means condemning prescription medicine for mental health. I've seen it save a lot of people's lives.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Groucho Marx");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "I'm not feeling very well - I need a doctor immediately. Ring the nearest golf course.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Schumer");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "I'm strongly for a patient Bill of Rights. Decisions ought to be made by doctors, not accountants.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Merkle");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "If you look at the human condition today, not everyone is well fed, has access to good medical care, or the physical basics that provide for a healthy and a happy life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcel Proust");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Illness is the doctor to whom we pay most heed; to kindness, to knowledge, we make promise only; pain we obey.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Everett Hale");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "In the name of Hypocrites, doctors have invented the most exquisite form of torture ever known to man: survival.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin H. Fischer");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "In the sick room, ten cents' worth of human understanding equals ten dollars' worth of medical science.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Cousins");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "It is reasonable to expect the doctor to recognize that science may not have all the answers to problems of health and healing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charlotte Ross");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Mammograms are really sort of a gift. You can either catch something early or count your lucky stars because nothing was discovered. Either way, you're ahead of the game.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Ensign");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Medical liability reform is not a Republican or Democrat issue or even a doctor versus lawyer issue. It is a patient issue.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anton Chekhov");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Medicine is my lawful wife and literature my mistress; when I get tired of one, I spend the night with the other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ovid");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Medicine sometimes snatches away health, sometimes gives it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ivan Illich");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Modern medicine is a negation of health. It isn't organized to serve human health, but only itself, as an institution. It makes more people sick than it heals.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walter Matthau");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "My doctor gave me six months to live, but when I couldn't pay the bill he gave me six months more.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Never go to a doctor whose office plants have died.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice James");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "One has a greater sense of degradation after an interview with a doctor than from any human experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Shaffer");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Passion, you see, can be destroyed by a doctor. It cannot be created.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martha Beck");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "People are so afraid of authority figures and doctors are authority figures.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cass Canfield");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Some people think that doctors and nurses can put scrambled eggs back in the shell.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Sydenham");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "The art of medicine was to be properly learned only from its practice and its exercise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dreams have only one owner at a time. That's why dreamers are lonely.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anais Nin");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dreams pass into the reality of action. From the actions stems the dream again; and this interdependence produces the highest form of living.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baltasar Gracian");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Dreams will get you nowhere, a good kick in the pants will take you a long way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Each man should frame life so that at some future hour fact and his dreaming meet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harriet Tubman");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Every great dream begins with a dreamer. Always remember, you have within you the strength, the patience, and the passion to reach for the stars to change the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Lithgow");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Everybody's a dreamer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elizabeth Barrett Browning");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "God's gifts put man's best dreams to shame.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abdul Kalam");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Great dreams of great dreamers are always transcended.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas Adams");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "He was a dreamer, a thinker, a speculative philosopher... or, as his wife would have it, an idiot.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eva Green");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I am a dreamer. Seriously, I'm living on another planet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rene Descartes");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I am accustomed to sleep and in my dreams to imagine the same things that lunatics imagine when awake.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brian Schweitzer");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I challenge you to be dreamers; I challenge you to be doers and let us make the greatest place in the world even better.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roy Orbison");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I close my eyes, then I drift away, into the magic night I softly say. A silent prayer, like dreamers do, then I fall asleep to dream my dreams of you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zhuangzi");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I do not know whether I was then a man dreaming I was a butterfly, or whether I am now a butterfly dreaming I am a man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "M. C. Escher");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I don't use drugs, my dreams are frightening enough.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andre Breton");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I have always been amazed at the way an ordinary observer lends so much more credence and attaches so much more importance to waking events than to those occurring in dreams... Man... is above all the plaything of his memory.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Madonna Ciccone");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I stand for freedom of expression, doing what you believe in, and going after your dreams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Debi Thomas");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I tell people I'm too stupid to know what's impossible. I have ridiculously large dreams, and half the time they come true.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Watterson");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I think we dream so we don't have to be apart so long. If we're in each other's dreams, we can play together all night.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charley Pride");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I was always a dreamer, in childhood especially. People thought I was a little strange.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Orben");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Life was a lot simpler when what we honored was father and mother rather than all major credit cards.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Vaughan");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Man is the animal that intends to shoot himself out into interplanetary space, after having given up on the problem of an efficient way to get himself five miles to work and back each day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack Kingston");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Marriage cannot be severed from its cultural, religious and natural roots without weakening the good influence of society.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Most of the change we think we see in life is due to truths being in and out of favor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "C. Wright Mills");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Neither the life of an individual nor the history of a society can be understood without understanding both.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Smith");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "No society can surely be flourishing and happy, of which the far greater part of the members are poor and miserable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kenneth L. Pike");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Normal social behavior requires that we be able to recognize identities in spite of change. Unless we can do so, there can be no human society as we know it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alvin Toffler");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "One of the definitions of sanity is the ability to tell real from unreal. Soon we'll need a new definition.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Horton Cooley");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Our individual lives cannot, generally, be works of art unless the social order is also.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Valerie Solanas");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Our society is not a community, but merely a collection of isolated family units.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Max Cannon");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "People are going to behave however the social norms permit, and beyond that.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Georg C. Lichtenberg");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Perhaps in time the so-called Dark Ages will be thought of as including our own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Szasz");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Punishment is now unfashionable... because it creates moral distinctions among men, which, to the democratic mind, are odious. We prefer a meaningless collective guilt to a meaningful individual responsibility.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Orwell");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Society has always seemed to demand a little more from human beings than it will get in practice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mignon McLaughlin");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Society honors its living conformists and its dead troublemakers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hugh Kingsmill");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Society is based on the assumption that everyone is alike and no one is alive.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Randolph Bourne");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Society is one vast conspiracy for carving one into the kind of statue likes, and then placing it in the most convenient niche it has.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles de Secondat");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Society is the union of men and not the men themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John W. Gardner");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Some people strengthen the society just by being the kind of people they are.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Vaughan");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Suburbia is where the developer bulldozes out the trees, then names the streets after them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith is spiritualized imagination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther King, Jr.");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith is taking the first step even when you don't see the whole staircase.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rabindranath Tagore");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith is the bird that feels the light when the dawn is still dark.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sarah Ban Breathnach");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith is the very first thing you should pack in a hope chest.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Saint Augustine");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith is to believe what you do not see; the reward of this faith is to see what you believe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William James");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith means belief in something concerning which doubt is theoretically possible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. M. Forster");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith, to my mind, is a stiffening process, a sort of mental starch.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Nietzsche");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Faith: not wanting to know what is true.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abdul Kalam");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "God, our Creator, has stored within our minds and personalities, great potential strength and ability. Prayer helps us tap and develop these powers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ayn Rand");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "God... a being whose only definition is that he is beyond man's power to conceive.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edwin Louis Cole");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Have faith in God; God has faith in you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "B. C. Forbes");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "He who has faith has... an inward reservoir of courage, hope, confidence, calmness, and assuring trust that all will come out well - even though to the world it may appear to come out most badly.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas More");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "I die the king's faithful servant, but God's first.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elie Wiesel");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "I have not lost faith in God. I have moments of anger and protest. Sometimes I've been closer to him for that reason.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pat Buckley");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "I hold that religion and faith are two different things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ted Lange");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "I seek a deeper truth, but I don't think I have to go to a building designated for worship to find it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Malone");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "I think the greatest taboos in America are faith and failure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "If patience is worth anything, it must endure to the end of time. And a living faith will last in the midst of the blackest storm.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Blaise Pascal");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "In faith there is enough light for those who want to believe and enough shadows to blind those who don't.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "In the affairs of this world, men are saved not by faith, but by the want of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Juliana Hatfield");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "If you want to achieve things in life, you've just got to do them, and if you're talented and smart, you'll succeed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anthony J. D'Angelo");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "In order to succeed you must fail, so that you know what not to do the next time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Cosby");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "In order to succeed, your desire for success should be greater than your fear of failure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "In this world it is not what we take up, but what we give up, that makes us rich.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Merrick");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "It's not enough that I should succeed - others should fail.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carrot Top");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "It's our nature: Human beings like success but they hate successful people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ross Perot");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Most people give up just when they're about to achieve success. They quit on the one yard line. They give up at the last minute of the game one foot from a winning touchdown.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harold MacMillan");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "No man succeeds without a good woman behind him. Wife or mother, if it is both, he is twice blessed indeed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walter Winchell");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Nothing recedes like success.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aeschylus");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Obedience is the mother of success and is wedded to safety.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "One secret of success in life is for a man to be ready for his opportunity when it comes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elbert Hubbard");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Pray that success will not come any faster than you are able to endure it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lily Tomlin");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Sometimes I worry about being a success in a mediocre world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Winston Churchill");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success consists of going from failure to failure without loss of enthusiasm.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sam Ewing");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success has a simple formula: do your best, and people may like it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sloan Wilson");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success in almost any field depends more on energy and drive than it does on intelligence. This explains why we have so many stupid leaders.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Gates");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is a lousy teacher. It seduces smart people into thinking they can't lose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is a science; if you have the conditions, you get the result.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tennessee Williams");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is blocked by concentrating on it and planning for it... Success is shy - it won't come out while you're watching.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sophocles");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is dependent on effort.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Randolph");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "I am an aristocrat. I love liberty; I hate equality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. C. Fields");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "I am free of all prejudices. I hate every one equally.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "I believe in equality for everyone, except reporters and photographers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther King, Jr.");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "I have a dream that my four little children will one day live in a nation where they will not be judged by the color of their skin, but by the content of their character.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther King, Jr.");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "I have a dream that one day on the red hills of Georgia, the sons of former slaves and the sons of former slave owners will be able to sit together at the table of brotherhood.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barack Obama");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "I know my country has not perfected itself. At times, we've struggled to keep the promise of liberty and equality for all of our people. We've made our share of mistakes, and there are times when our actions around the world have not lived up to our best intentions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brigid Brophy");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "I refuse to consign the whole male sex to the nursery. I insist on believing that some men are my equals.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Agnes Macphail");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "I want for myself what I want for other women, absolute equality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Patricia Ireland");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "I want to organize so that women see ourselves as people who are entitled to power, entitled to leadership.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Kennedy");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "If any man claims the Negro should be content... let him say he would willingly change the color of his skin and go to live in the Negro section of a large city. Then and only then has he a right to such a claim.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "If liberty and equality, as is thought by some, are chiefly to be found in democracy, they will be best attained when all persons alike share in government to the utmost.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Franz Boas");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "If we were to select the most intelligent, imaginative, energetic, and emotionally stable third of mankind, all races would be present.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bertrand Russell");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "In America everybody is of the opinion that he has no social superiors, since all men are equal, but he does not admit that he has no social inferiors, for, from the time of Jefferson onward, the doctrine that all men are equal applies only upwards, not downwards.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry A. Blackmun");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "In order to get beyond racism, we must first take account of race. There is no other way. And in order to treat some persons equally, we must treat them differently.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marlo Thomas");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "In the 1960s we were fighting to be recognized as equals in the marketplace, in marriage, in education and on the playing field. It was a very exciting, rebellious time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Johnson");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "It is better that some should be unhappy rather than that none should be happy, which would be the case in a general state of equality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kofi Annan");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "More countries have understood that women's equality is a prerequisite for development.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Warren Farrell");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Nobody really believes in equality anyway.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marlo Thomas");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "One of the things about equality is not just that you be treated equally to a man, but that you treat yourself equally to the way you treat a man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Linda Ellerbee");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "People are pretty much alike. It's only that our differences are more susceptible to definition than our similarities.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gustav Mahler");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "If a composer could say what he had to say in words he would not bother trying to say it in music.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "If music be the food of love, play on.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Carlyle");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "If you look deep enough you will see music; the heart of nature being everywhere music.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard M. Nixon");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "If you want to make beautiful music, you must play the black and the white notes together.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Nietzsche");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "In music the passions enjoy themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Britten");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "It is cruel, you know, that music should be so beautiful. It has the beauty of loneliness of pain: of strength and freedom. The beauty of disappointment and never-satisfied love. The cruel beauty of nature and everlasting beauty of monotony.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bruce Beresford");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "It is essential to do everything possible to attract young people to opera so they can see that it is not some antiquated art form but a repository of the most glorious music and drama that man has created.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stevie Nicks");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "It was my 16th birthday - my mom and dad gave me my Goya classical guitar that day. I sat down, wrote this song, and I just knew that that was the only thing I could ever really do - write songs and sing them to people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bono");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music can change the world because it can change people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jimi Hendrix");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music doesn't lie. If there is something to be changed in this world, then it can only happen through music.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music expresses that which cannot be said and on which it is impossible to be silent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbie Hancock");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music happens to be an art form that transcends language.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Congreve");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music has charms to sooth a savage breast, to soften rocks, or bend a knotted oak.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lao Tzu");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music in the soul can be heard by the universe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ludwig van Beethoven");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is a higher revelation than all wisdom and philosophy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plato");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is a moral law. It gives soul to the universe, wings to the mind, flight to the imagination, and charm and gaiety to life and to everything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Tippett");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is a performance and needs the audience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Billy Sheehan");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is always changing and the changes are unpredictable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Lennon");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is everybody's possession. It's only publishers who think that people own it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Simon");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is forever; music should grow and mature with you, following you right on up until you die.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I replaced the headlights in my car with strobe lights, so it looks like I'm the only one moving.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Best");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I spent a lot of money on booze, birds and fast cars. The rest I just squandered.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maurice Gibb");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I think we have to act like stars because it is expected of us. So we drive our big cars and live in our smart houses.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Valentino Rossi");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I would have probably stolen cars - it would have given me the same adrenaline rush as racing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don Van Vliet");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I would never kill a living thing, although I probably have inadvertently while driving automobiles.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Craig Johnston");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I've always had an inquisitive mind about everything from flowers to television sets to motor cars. Always pulled them apart - couldn't put 'em back, but always extremely interested in how things work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rip Torn");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "I've got two old Volvos, two old Subarus, and an old Ford Ranger. If you've got an old car, you've gotta have at least several old cars, 'cause one's always gonna be in the garage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doug Larson");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "If all the cars in the United States were placed end to end, it would probably be Labor Day Weekend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Gates");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "If GM had kept up with technology like the computer industry has, we would all be driving $25 cars that got 1000 MPG.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ingvar Kamprad");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Ikea people do not drive flashy cars or stay at luxury hotels.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Glenn Danzig");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "In Japan, they have TV sets in cars right now, where you can punch up traffic routes, weather, everything! You can get Internet access already in cars in Japan, so within the next 2 to 3 years it's gonna be so crazy!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Scott Adams");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "In less enlightened times, the best way to impress women was to own a hot car. But women wised up and realized it was better to buy their own hot cars so they wouldn't have to ride around with jerks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dale Earnhardt");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "It's a never ending battle of making your cars better and also trying to be better yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. L. Doctorow");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "It's like driving a car at night. You never see further than your headlights, but you can make the whole trip that way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brock Yates");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "More books, more racing and more foolishness with cars and motorcycles are in the works.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Julia Roberts");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "My boyfriend keeps telling me I've got to own things. So, first I bought this car. And then he told me I oughta get a house. 'Why a house?' 'Well, you gotta have a place to park the car.'");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tim Allen");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "My mom said the only reason men are alive is for lawn care and vehicle maintenance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Larry McMurtry");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "No illusion is more crucial than the illusion that great success and huge money buy you immunity from the common ills of mankind, such as cars that won't start.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Diane Johnson");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Not having to own a car has made me realize what a waste of time the automobile is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Cagney");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Perhaps people, and kids especially, are spoiled today, because all the kids today have cars, it seems. When I was young you were lucky to have a bike.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Mead");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "As long as any adult thinks that he, like the parents and teachers of old, can become introspective, invoking his own youth to understand the youth before him, he is lost.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marvin Davis");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "As men get older, the toys get more expensive.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frances Conroy");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "As you age naturally, your family shows more and more on your face. If you deny that, you deny your heritage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Bashfulness is an ornament to youth, but a reproach to old age.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Every man over forty is a scoundrel.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hosea Ballou");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Forty is the old age of youth, fifty is the youth of old age.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Forty is the old age of youth; fifty the youth of old age.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andre Maurois");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Growing old is no more than a bad habit which a busy person has no time to form.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gene Fowler");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "He has a profound respect for old age. Especially when it's bottled.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "C. S. Lewis");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "How incessant and great are the ills with which a prolonged old age is replete.");
		db.insert("citations", AUTHOR, cv);

		

		cv.put(AUTHOR, "Diane Lane");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I was raised by free-spirited people, though my father gave me a very strong work ethic.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sammy Davis, Jr.");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I wasn't anything special as a father. But I loved them and they knew it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Mapplethorpe");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I would never have done what I'd done if I'd considered my father as somebody I wanted to please.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dante Hall");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I would want my legacy to be that I was a great son, father and friend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gordon Brown");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I'm a father; that's what matters most. Nothing matters more.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Billie Joe Armstrong");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I'm a father. It isn't just my life any more. I don't want my kid finding bottles in the house or seeing his father completely smashed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Lithgow");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I'm a fun father, but not a good father. The hard decisions always went to my wife.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Malkovich");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I'm more comfortable with whatever's wrong with me than my father was whenever he felt he failed or didn't measure up to the standard he set.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ray Romano");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "If my father had hugged me even once, I'd be an accountant right now.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Sexton");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "It doesn't matter who my father was; it matters who I remember he was.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "It is a wise father that knows his own child.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Karen Horney");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life itself still remains a very effective therapist.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maya Angelou");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life loves the liver of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plato");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life must be lived as play.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leonardo da Vinci");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Life well spent is long.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mel Brooks");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Look, I don't want to wax philosophic, but I will say that if you're alive you've got to flap your arms and legs, you've got to jump around a lot, for life is the very opposite of death, and therefore you must at very least think noisy and colorfully, or you're not alive.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jonathan Swift");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "May you live all the days of your life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur Miller");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Maybe all one can do is hope to end up with the right regrets.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cary Grant");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "My formula for living is quite simple. I get up in the morning and I go to bed at night. In between, I occupy myself as best I can.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hugh Leonard");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "My life is every moment of my life. It is not a culmination of the past.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harvey Fierstein");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Never be bullied into silence. Never allow yourself to be made a victim. Accept no one's definition of your life; define yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brendan Gill");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Not a shred of evidence exists in favor of the idea that life is serious.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Socrates");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "Not life, but good life, is to be chiefly valued.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anais Nin");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "People living deeply have no fear of death.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Aurelius");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "The art of living is more like wrestling than dancing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. L. Mencken");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "The basic fact about human existence is not that it is a tragedy, but that it is a bore. It is not so much a war as an endless standing in line.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Lyon Phelps");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "The fear of life is the favorite disease of the 20th century.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William James");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "The great use of life is to spend it for something that will outlast it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. M. Forster");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "The main facts in human life are five: birth, food, sleep, love and death.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "The price of anything is the amount of life you exchange for it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Byrne");
		cv.put(TOPIC, "Life");
		cv.put(CITATION, "The purpose of life is a life of purpose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francois Truffaut");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "In love, women are professionals, men are amateurs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold Schwarzenegger");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "In our society, the women who break down barriers are those who ignore limits.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clare Boothe Luce");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "In politics women type the letters, lick the stamps, distribute the pamphlets and get out the vote. Men get elected.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hillary Clinton");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "In too many instances, the march to globalization has also meant the marginalization of women and girls. And that must change.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elizabeth Bowen");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Intimacies between women often go backwards, beginning in revelations and ending in small talk.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Amelia Barr");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "It is little men know of women; their smiles and their tears alike are seldom what they seem.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Clooney");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "It's incredibly unfair. You don't see a lot of 60-year-old women with 20-year-old men onscreen.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Groucho Marx");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Man does not control his own fate. The women in his life do that for him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Cosby");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Men and women belong to different species and communications between them is still in its infancy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erica Jong");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Men and women, women and men. It will never work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbra Streisand");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Men are allowed to have passion and commitment for their work... a woman is allowed that feeling for a man, but not her work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Men are April when they woo, December when they wed. Maids are May when they are maids, but the sky changes when they are wives.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur Schopenhauer");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Men are by nature merely indifferent to one another; but women are by nature enemies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Joyce");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Men are governed by lines of intellect - women: by curves of emotion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Josh Billings");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Men mourn for what they have lost; women for what they ain't got.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Giraudoux");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Men should only believe half of what women say. But which half?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Antonia Fraser");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "My advantage as a woman and a human being has been in having a mother who believed strongly in women's education. She was an early undergraduate at Oxford, and her own mother was a doctor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George William Curtis");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "Nature makes woman to be won and men to win.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. C. Fields");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "No doubt exists that all women are crazy; it's only a question of degree.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Julie Andrews");
		cv.put(TOPIC, "Women");
		cv.put(CITATION, "On the whole, I think women wear too much and are to fussy. You can't see the person for all the clutter.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rose Kennedy");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "I've had an exciting time; I married for love and got a little money along with it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ruth Rendell");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "I've had two proposals since I've been a widow. I am a wonderful catch, you know. I have a lot of money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Ade");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "If it were not for the presents, an elopement would be preferable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michel de Montaigne");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "If there is such a thing as a good marriage, it is because it resembles friendship rather than love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mignon McLaughlin");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "If you made a list of reasons why any couple got married, and another list of the reasons for their divorce, you'd have a hell of a lot of overlapping.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan King");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "If you want to read about love and marriage, you've got to buy two separate books.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Katharine Hepburn");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "If you want to sacrifice the admiration of many men for the criticism of one, go ahead, get married.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Anderson");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "In every marriage more than a week old, there are grounds for divorce. The trick is to find, and continue to find, grounds for marriage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Enid Bagnold");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "In marriage there are no manners to keep up, and beneath the wildest accusations no real criticism. Each is familiar with that ancient child in the other who may erupt again. We are not ridiculous to ourselves. We are ageless. That is the luxury of the wedding ring.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Rowland");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "In olden times sacrifices were made at the altar - a practice which is still continued.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lewis Grizzard");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Instead of getting married again, I'm going to find a woman I don't like and give her a house.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "It destroys one's nerves to be amiable every day to the same human being.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Rowland");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "It isn't tying himself to one woman that a man dreads when he thinks of marrying; it's separating himself from all the others.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Santayana");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "It takes patience to appreciate domestic bliss; volatile spirits prefer unhappiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Euripides");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "It's not beauty but fine qualities, my girl, that keep a husband.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rodney Dangerfield");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "It's tough to stay married. My wife kisses the dog on the lips, yet she won't drink from my glass.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Let the wife make the husband glad to come home, and let him make her sorry to see him leave.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alphonse Karr");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Love in marriage should be the accomplishment of a beautiful dream, and not, as it too often is, the end.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ellen Key");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Love is moral even without legal marriage, but marriage is immoral without love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Moliere");
		cv.put(TOPIC, "Marriage");
		cv.put(CITATION, "Love is often the fruit of marriage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Maynard Keynes");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The avoidance of taxes is the only intellectual pursuit that still carries any reward.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hjalmar Schacht");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The economy is a very sensitive organism.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Rivlin");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The job of the Central Bank is to worry.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Temple");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The only way for a rich man to be healthy is by exercise and abstinence, to live as if he were poor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Feldstein");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The only way that we can reduce our financial dependence on the inflow of funds from the rest of the world is to reduce our trade deficit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean-Paul Sartre");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The poor don't know that their function in life is to exercise our generosity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Marshall");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The price of every thing rises and falls from time to time and place to place; and with every such change the purchasing power of money changes so far as that thing goes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jacques Delors");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The problem of how we finance the welfare state should not obscure a separate issue: if each person thinks he has an inalienable right to welfare, no matter what happens to the world, that's not equity, it's just creating a society where you can't ask anything of people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Smith");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The propensity to truck, barter and exchange one thing for another is common to all men, and to be found in no other race of animals.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Irving Fisher");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The rate of interest acts as a link between income-value and capital-value.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Carnegie");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The way to become rich is to put all your eggs in one basket and then watch that basket.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John D. Rockefeller");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "The way to make money is to buy when blood is running in the streets.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Ricardo");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "There can be no rise in the value of labour without a fall of profits.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Carper");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Thirteen thousand dollars a year is not enough to raise a family. That's not enough to pay your bills and save for their future. That's barely enough to provide for even the most basic needs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Scott Cook");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Thirty to 40 years ago, most financial decisions were fairly simple.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Merton Miller");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "To beat the market you'll have to invest serious bucks to dig up information no one else has yet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Muhammad Yunus");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Today, if you look at financial systems around the globe, more than half the population of the world - out of six billion people, more than three billion - do not qualify to take out a loan from a bank. This is a shame.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Reisman");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Under capitalism each individual engages in economic planning.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Kenneth Galbraith");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "Under capitalism, man exploits man. Under communism, it's just the opposite.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lee Iacocca");
		cv.put(TOPIC, "Finance");
		cv.put(CITATION, "We at Chrysler borrow money the old-fashioned way. We pay it back.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas A. Edison");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "Hell, there are no rules here - we're trying to accomplish something.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Kettering");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "High achievement always takes place in the framework of high expectation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Winston Churchill");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "I am certainly not one of those who need to be prodded. In fact, if anything, I am the prod.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. Paul Getty");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "I buy when other people are selling.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Nader");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "I don't think meals have any business being deductible. I'm for separation of calories and corporations.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Cleese");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "I find it rather easy to portray a businessman. Being bland, rather cruel and incompetent comes naturally to me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tiger Woods");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "I get to play golf for a living. What more can you ask for - getting paid for doing what you love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Appleton");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "I rate enthusiasm even above professional skill.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "I think that there is nothing, not even crime, more opposed to poetry, to philosophy, ay, to life itself than this incessant business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steve Jobs");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "I want to put a ding in the universe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jennifer Aniston");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "I was told to avoid the business all together because of the rejection. People would say to me, 'Don't you want to have a normal job and a normal family?' I guess that would be good advice for some people, but I wanted to act.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lord Acton");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "I'm not a driven businessman, but a driven artist. I never think about money. Beautiful things make money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Debbi Fields");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "I've never felt like I was in the cookie business. I've always been in a feel good feeling business. My job is to sell joy. My job is to sell happiness. My job is to sell an experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "If all the economists were laid end to end, they'd never reach a conclusion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lucius Annaeus Seneca");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "If one does not know to which port one is sailing, no wind is favorable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jane Fonda");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "If the career you have chosen has some unexpected inconvenience, console yourself by reflecting that no career is without them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Lincoln");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "If there is anything that a man can do well, I say let him do it. Give him a chance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas J. Watson");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "If you aren't playing well, the game isn't as much fun. When that happens I tell myself just to go out and play as I did when I was a kid.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "If you can build a business up big enough, it's respectable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Business");
		cv.put(CITATION, "If you cannot work with love but only with distaste, it is better that you should leave your work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louis D. Brandeis");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Our government... teaches the whole people by its example. If the government becomes the lawbreaker, it breeds contempt for law; it invites every man to become a law unto himself; it invites anarchy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adrian Cronauer");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Our nation is built on the bedrock principle that governments derive their just powers from the consent of the governed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Half");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "People try to live within their income so they can afford to pay taxes to a government that can't live within its income.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Milton Friedman");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "So that the record of history is absolutely crystal clear. That there is no alternative way, so far discovered, of improving the lot of the ordinary people that can hold a candle to the productive activities that are unleashed by a free enterprise system.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cullen Hightower");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "Talk is cheap - except when Congress does it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "That government is best which governs least.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Winston Churchill");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The best argument against democracy is a five-minute conversation with the average voter.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ronald Reagan");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The best minds are not in government. If any were, business would steal them away.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lord Acton");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The danger is not that a particular class is unfit to govern: every class is unfit to govern.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kathleen Sebelius");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The essence of good government is trust.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wilhelm Reich");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The fact that political ideologies are tangible realities is not a proof of their vitally necessary character. The bubonic plague was an extraordinarily powerful social reality, but no one would have regarded it as vitally necessary.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gore Vidal");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The genius of our ruling class is that it has kept a majority of the people from ever questioning the inequity of a system where most people drudge along, paying heavy taxes for which they get nothing in return.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Milton Friedman");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The government solution to a problem is usually as bad as the problem.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woodrow Wilson");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The government, which was designed for the people, has got into the hands of the bosses and their employers, the special interests. An invisible empire has been set up above the forms of democracy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Paine");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The instant formal government is abolished, society begins to act. A general association takes place, and common interest produces common security.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cullen Hightower");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The mistakes made by Congress wouldn't be so bad if the next Congress didn't keep trying to correct them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Karl Marx");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The oppressed are allowed once every few years to decide which particular representatives of the oppressing class are to represent and repress them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Jackson");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The people are the government, administering it by their agents; they are the government, the sovereign power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lewis Mumford");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The way people in democracies think of the government as something different from themselves is a real handicap. And, of course, sometimes the government confirms their opinion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Government");
		cv.put(CITATION, "The worst thing in this world, next to anarchy, is government.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Calvin Trillin");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I never eat in a restaurant that's over a hundred feet off the ground and won't stand still.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Virgil Thomson");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I said to my friends that if I was going to starve, I might as well starve where the food is good.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Karrie Webb");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I think Australian food is probably some of the best in the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I want my food dead. Not sick, not dying, dead.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woody Allen");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I will not eat oysters. I want my food dead. Not sick. Not wounded. Dead.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kurt Cobain");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I won't eat anything green.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Yasmine Bleeth");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "I wouldn't hunt a person down for food. But if he were already dead.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas Horton");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "If food were free, why work?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Yan");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "If I could only have one type of food with me, I would bring soy sauce. The reason being that if I have soy sauce, I can flavor a lot of things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carol Alt");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "If I'm making a movie and get hungry, I call time-out and eat some crackers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leigh Hunt");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "If you are ever at a loss to support a flagging conversation, introduce the subject of eating.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cesar Chavez");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "If you really want to make a friend, go to someone's house and eat with him... the people who give you their food give you their heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. Somerset Maugham");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "If you want to eat well in England, eat three breakfasts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fran Lebowitz");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "If you're going to America, bring your own food.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mario Batali");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "In America, I would say New York and New Orleans are the two most interesting food towns. In New Orleans, they don't have a bad deli. There's no mediocrity accepted.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robin Leach");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "In Italy, they add work and life on to food and wine.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joan Rivers");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "Is Elizabeth Taylor fat? Her favorite food is seconds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ronald Reagan");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "It's difficult to believe that people are still starving in this country because food isn't available.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Dodd");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "It's easy for Americans to forget that the food they eat doesn't magically appear on a supermarket shelf.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kurt Cobain");
		cv.put(TOPIC, "Food");
		cv.put(CITATION, "It's okay to eat fish because they don't have any feelings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lawrence Durrell");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Like all young men I set out to be a genius, but mercifully laughter intervened.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. B. White");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Luck is not something you can mention in the presence of self-made men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charlie Chaplin");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Man as an individual is a genius. But men in the mass form the headless monster, a great, brutish idiot that goes where prodded.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Man becomes great exactly in the degree in which he works for the welfare of his fellow-men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Spurgeon");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Many men owe the grandeur of their lives to their tremendous difficulties.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men always want to be a woman's first love - women like to be a man's last romance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edwin Louis Cole");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men and women have strengths that complement each other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Weinberg");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men are actually the weaker sex.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men are born to succeed, not to fail.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zhang Ziyi");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men are different. When they are in love they may also have other girlfriends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Herbert Lawrence");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men are freest when they are most unconscious of freedom. The shout is a rattling of chains, always was.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Whately");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men are like sheep, of which a flock is more easily driven than a single one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Chuck Norris");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men are like steel. When they lose their temper, they lose their worth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men are more easily governed through their vices than through their virtues.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gene Fowler");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men are not against you; they are merely for themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kin Hubbard");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men are not punished for their sins, but by them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George William Norris");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men are often biased in their judgment on account of their sympathy and their interests.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Orwell");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men are only as good as their technical development allows them to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cher");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men aren't necessities. They're luxuries.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bette Davis");
		cv.put(TOPIC, "Men");
		cv.put(CITATION, "Men become much more attractive when they start looking older. But it doesn't do much for women, though we do have an advantage: make-up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abu Bakr");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "O man you are busy working for the world, and the world is busy trying to turn you out.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ann Landers");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Opportunities are usually disguised as hard work, so most people don't recognize them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas A. Edison");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Opportunity is missed by most people because it is dressed in overalls and looks like work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Drucker");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Plans are only good intentions unless they immediately degenerate into hard work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ann Richards");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "Teaching was the hardest work I had ever done, and it remains the hardest work I have done to date.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elbert Hubbard");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "The best preparation for good work tomorrow is to do good work today.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "The brain is a wonderful organ; it starts working the moment you get up in the morning and does not stop until you get into the office.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Goldwyn");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "The harder I work, the luckier I get.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Bach");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "The more I want to get something done, the less I call it work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vince Lombardi");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "The only place success comes before work is in the dictionary.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry Golden");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "The only thing that overcomes hard luck is hard work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold J. Toynbee");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "The supreme accomplishment is to blur the line between work and play.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ronald Reagan");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "The taxpayer - that's someone who works for the federal government but doesn't have to take the civil service examination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "J. Paul Getty");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "There are one hundred men seeking security to one able man who is willing to risk his fortune.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Nelson Bolles");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "There is a vast world of work out there in this country, where at least 111 million people are employed in this country alone - many of whom are bored out of their minds. All day long.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ford");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "There is joy in work. There is no happiness except in the realization that we have accomplished something.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas A. Edison");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "There is no substitute for hard work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "To be a poet is a condition, not a profession.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pearl S. Buck");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "To find joy in work is to discover the fountain of youth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Dewey");
		cv.put(TOPIC, "Work");
		cv.put(CITATION, "To find out what one is fitted to do, and to secure an opportunity to do it, is the key to happiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ed Smith");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "It seems every year, people make the resolution to exercise and lose weight and get in shape.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helena Rubinstein");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Leave the table while you still feel you could eat a little more.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orson Welles");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "My doctor told me to stop having intimate dinners for four. Unless there are three other people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lois Capps");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "My experience as a school nurse taught me that we need to make a concerted effort, all of us, to increase physical fitness activity among our children and to encourage all Americans to adopt a healthier diet that includes fruits and vegetables, but there is more.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maimonides");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "No disease that can be treated by diet should be treated with any other means.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tipper Gore");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Our society's strong emphasis on dieting and self-image can sometimes lead to eating disorders. We know that more than 5 million Americans suffer from eating disorders, most of them young women.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Amy Sedaris");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "People who shop in health food stores never look healthy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Ditka");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "So, when it comes to eating healthy, it's just doing the right thing. And it's not something you have to do 365 days a year, but I think it's something you have to do 25 days a month. Let's put it that way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Ditka");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Some people are willing to pay the price and it's the same with staying healthy or eating healthy. There's some discipline involved. There's some sacrifices.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gro Harlem Brundtland");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Such lifestyle factors such as cigarette smoking, excessive alcohol consumption, little physical activity and low dietary calcium intake are risk factors for osteoporosis as well as for many other non-communicable diseases.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Koch");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "The best way to lose weight is to close your mouth - something very difficult for a politician. Or watch your food - just watch it, don't eat it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lee Haney");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "The circuit training program along with a healthy clean diet is the way to excellent results.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack LaLanne");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "The only way you get that fat off is to eat less and exercise more.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jackie Gleason");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "The second day of a diet is always easier than the first. By the second day you're off it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kenneth H. Cooper");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "There are six components of wellness: proper weight and diet, proper exercise, breaking the smoking habit, control of alcohol, stress management and periodic exams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Davis");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "Vegetables are a must on a diet. I suggest carrot cake, zucchini bread, and pumpkin pie.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Keith Emerson");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "We can all put weight on or lose weight.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Pope");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "What some call health, if purchased by perpetual anxiety about diet, isn't much better than tedious disease.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hank Stram");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "You can't be fat and fast, too; so lift, run, diet and work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Ditka");
		cv.put(TOPIC, "Diet");
		cv.put(CITATION, "You see people who have been very heavy in their life who have taken that body, trimmed it down, firmed it up through discipline, exercise and being able to say no. Eating properly, that all comes into it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lord Chesterfield");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "In seeking wisdom thou art wise; in imagining that thou hast attained it - thou art a fool.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "It is a characteristic of wisdom not to do desperate things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Steinbeck");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "It is a common experience that a problem difficult at night is resolved in the morning after the committee of sleep has worked on it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Ralph Inge");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "It is astonishing with how little wisdom mankind can be governed, when that little wisdom is its own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francois de La Rochefoucauld");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "It is great folly to wish to be wise all alone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harold S. Geneen");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "It is much more difficult to measure nonperformance than performance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Epictetus");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "It is the nature of the wise to resist pleasures, but the foolish to be a slave to them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Whately");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "It is the neglect of timely repair that makes rebuilding necessary.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walter Lippmann");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "It requires wisdom to understand wisdom: the music is nothing if the audience is deaf.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "It's not what you look at that matters, it's what you see.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Lord Tennyson");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Knowledge comes, but wisdom lingers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Josh Billings");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Learning sleeps and snores in libraries, but wisdom is everywhere, wide awake, on tiptoe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Herbert Lawrence");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Life is a travelling to the edge of knowledge, then a leap taken.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aeschylus");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Memory is the mother of all wisdom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Nature and books belong to the eyes that see them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Juvenal");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Never does nature say one thing and wisdom another.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George S. Patton");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Never tell people how to do things. Tell them what to do and they will surprise you with their ingenuity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodore Roosevelt");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Nine-tenths of wisdom is being wise in time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lucius Annaeus Seneca");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "No man was ever wise by chance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Tullius Cicero");
		cv.put(TOPIC, "Wisdom");
		cv.put(CITATION, "Nobody can give you wiser advice than yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dixie Lee Ray");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Mankind is considered (by the radical environmentalists) the lowest and the meanest of all species and is blamed for everything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Mead");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Never doubt that a small group of thoughtful, committed citizens can change the world; indeed, it's the only thing that ever has.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Garrett Hardin");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "No one should be able to enter a wilderness by mechanical means.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roger Tory Peterson");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Not all is doom and gloom. We are beginning to understand the natural world and are gaining a reverence for life - all life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Burgess");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Nuclear power will help provide the electricity that our growing economy needs without increasing emissions. This is truly an environmentally responsible source of energy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Gore");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Our world faces a true planetary emergency. I know the phrase sounds shrill, and I know it's a challenge to the moral imagination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Collier");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "People blame their environment. There is only one person to blame - and only one - themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Pollan");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "People in Slow Food understand that food is an environmental issue.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Malthus");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Population, when unchecked, goes on doubling itself every 25 years or increases in a geometrical ratio.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Murkowski");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Pushing production out of America to nations without our environmental standards increases global environmental risks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Muir");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Take a course in good water and air; and in the eternal youth of Nature you may renew your own. Go quietly, alone; no harm will befall you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Thank God men cannot fly, and lay waste the sky as well as the earth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norm Dicks");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "The Endangered Species Act is the strongest and most effective tool we have to repair the environmental harm that is causing a species to decline.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "The environment is everything that isn't me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dixie Lee Ray");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "The government should set a goal for a clean environment but not mandate how that goal should be implemented.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barry Commoner");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "The modern assault on the environment began about 50 years ago, during and immediately after World War II.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Rogers");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "The only way forward, if we are going to improve the quality of the environment, is to get everybody involved.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David R. Brower");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "There is no place where we can safely store worn-out reactors or their garbage. No place!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sitting Bull");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "They claim this mother of ours, the Earth, for their own use, and fence their neighbors away from her, and deface her with their buildings and their refuse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Melissa Bean");
		cv.put(TOPIC, "Environmental");
		cv.put(CITATION, "Under the Environmental Protection Agency's Energy Star Program, homes are independently verified to be measurably more energy efficient than average houses.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Paul");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Our birthdays are feathers in the broad wing of time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gabriel Byrne");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Presents don't really mean much to me. I don't want to sound mawkish, but - it was the realization that I have a great many people in my life who really love me, and who I really love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. Joseph Cossman");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "The best way to remember your wife's birthday is to remember it once.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brian Tracy");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "The greatest gift that you can give to others is the gift of unconditional love and acceptance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Catherine Bell");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "The main prank that we play with props is for people's birthdays. The special effects people will put a little explosive in the cake so it blows up in their face - that's always fun to play on a guest star, or one of the trainees or someone who's new.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oprah Winfrey");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "The more you praise and celebrate your life, the more there is in life to celebrate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ella Fitzgerald");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "The only thing better than singing is more singing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Johnson");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "The return of my birthday, if I remember it, fills me with thoughts which it seems to be the general care of humanity to escape.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paris Hilton");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "The way I see it, you should live everyday like its your birthday.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lewis Carroll");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "There are three hundred and sixty-four days when you might get un-birthday presents, and only one for birthday presents, you know.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Glenn");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "There is still no cure for the common birthday.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "C. S. Lewis");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "Thirty was so strange for me. I've really had to come to terms with the fact that I am now a walking and talking adult.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Art Buchwald");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "This is a wonderful way to celebrate an 80th birthday... I wanted to be 65 again, but they wouldn't let me - Homeland Security.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Burroughs");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "To me - old age is always ten years older than I am.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lionel Blue");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "To my surprise, my 70s are nicer than my 60s and my 60s than my 50s, and I wouldn't wish my teens and 20s on my enemies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lauren Hutton");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "We have to be able to grow up. Our wrinkles are our medals of the passage of life. They are what we have been through and who we want to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Navjot Singh Sidhu");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "We'll take the cake with the red cherry on top.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Yoko Ono");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "When I turned 60, it didn't bother me at all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gloria Stuart");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "When I was little I thought, isn't it nice that everybody celebrates on my birthday? Because it's July 4th.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lisa Loeb");
		cv.put(TOPIC, "Birthday");
		cv.put(CITATION, "When someone asks if you'd like cake or pie, why not say you want cake and pie?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Tolstoy");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "The greater the state, the more wrong and cruel its patriotism, and the greater is the sum of suffering upon which its power is founded.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George McGovern");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "The highest patriotism is not a blind acceptance of official policy, but a love of one's country deep enough to call her to a higher plain.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pablo Casals");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "The love of one's country is a splendid thing. But why should love stop at the border?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Earl Warren");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "The most tragic paradox of our time is to be found in the failure of nation-states to recognize the imperatives of internationalism.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clara Barton");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "The patriot blood of my father was warm in my veins.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shenstone");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "The proper means of increasing the love we bear our native country is to reside some time in a foreign one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lyn Nofziger");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "The tree of liberty needs to be watered from time to time with the blood of patriots and tyrants.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Walpole");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "The very idea of true patriotism is lost, and the term has been prostituted to the very worst of purposes. A patriot, sir! Why, patriots spring up like mushrooms!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William J. Clinton");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "There is nothing wrong with America that cannot be cured with what is right in America.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry A. Wallace");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "They are patriotic in time of war because it is to their interest to be so, but in time of peace they follow power and the dollar wherever they may lead.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Santayana");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "To me, it seems a dreadful indignity to have a soul controlled by geography.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clarence Darrow");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "True patriotism hates injustice in its own land more than anywhere else.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Durrenmatt");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "We do not consider patriotism desirable if it contradicts civilized behavior.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dave Barry");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "We'll try to cooperate fully with the IRS, because, as citizens, we feel a strong patriotic duty not to go to jail.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Patriotism");
		cv.put(CITATION, "You'll never have a quiet world till you knock the patriotism out of the human race.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Loretta Lynn");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Mommy smoked but she didn't want us to. She saw smoke coming out of the barn one time, so we got whipped.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emma Goldman");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Morality and its victim, the mother - what a terrible picture! Is there indeed anything more terrible, more criminal, than our glorified sacred function of motherhood?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold Bennett");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Mother is far too clever to understand anything she does not like.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erich Fromm");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Mother's love is peace. It need not be acquired, it need not be deserved.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Meryl Streep");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Motherhood has a very humanizing effect. Everything gets reduced to essentials.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Harris");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Motherhood is at its best when the tender chords of sympathy have been touched.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Hunt Jackson");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Motherhood is priced Of God, at price no man may dare To lessen or misunderstand.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rebecca West");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Motherhood is the strangest thing, it can be like being one's own Trojan horse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gloria Estefan");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Motherhood is... difficult and... rewarding.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Browning");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Motherhood: All love begins and ends there.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Hoffman");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Mothers always find ways to fit in the work - but then when you're working, you feel that you should be spending time with your children and then when you're with your children, you're thinking about working.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Mothers are fonder than fathers of their children because they are more certain they are their own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Watterson");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Mothers are the necessity of invention.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Janice Dickinson");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "Mothers don't let your daughters grow up to be models unless you're present.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joel Madden");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "My mom always said that there would be haters. Not everyone can love ya.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alicia Keys");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "My mom is definitely my rock.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Toby Keith");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "My mom taught us the Serenity Prayer at a young age.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ben Affleck");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "My mother gets all mad at me if I stay in a hotel. I'm 31-years-old, and I don't want to sleep on a sleeping bag down in the basement. It's humiliating.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pablo Picasso");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "My mother said to me, 'If you are a soldier, you will become a general. If you are a monk, you will become the Pope.' Instead, I was a painter, and became Picasso.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carly Fiorina");
		cv.put(TOPIC, "Mom");
		cv.put(CITATION, "My mother taught me about the power of inspiration and courage, and she did it with a strength and a passion that I wish could be bottled.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther King, Jr.");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Our scientific power has outrun our spiritual power. We have guided missiles and misguided men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Kettering");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "People think of the inventor as a screwball, but no one ever asks the inventor what he thinks of other people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sam Ervin");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Polygraph tests are 20th-century witchcraft.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wernher von Braun");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Research is what I'm doing when I don't know what I'm doing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Pence");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Sadly, embryonic stem cell research is completely legal in this country and has been going on at universities and research facilities for years.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur M. Schlesinger");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Science and technology revolutionize our lives, but memory, tradition and myth frame our response.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Science does not know its debt to imagination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Rostand");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Science has made us gods even before we are worthy of being men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther King, Jr.");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Science investigates religion interprets. Science gives man knowledge which is power religion gives man wisdom which is control.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Wendell Holmes");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Science is a first-rate piece of furniture for a man's upper chamber, if he has common sense on the ground floor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Sagan");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Science is a way of thinking much more than it is a body of knowledge.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Huxley");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Science is organized common sense where many a beautiful theory was killed by an ugly fact.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Huxley");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Science is simply common sense at its best, that is, rigidly accurate in observation, and merciless to fallacy in logic.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Smith");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Science is the great antidote to the poison of enthusiasm and superstition.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erwin Chargaff");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Science is wonderfully equipped to answer the question \"How?\" but it gets terribly confused when you ask the question \"Why?\"");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Science never solves a problem without creating ten more.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wilhelm Reich");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Scientific theory is a contrived foothold in the chaos of living phenomena.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Donella Meadows");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Scientists worldwide agree that the reduction needed to stabilize the climate is actually more like 80 percent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Charles Polanyi");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Some dreamers demand that scientists only discover things that can be used for good.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James D. Watson");
		cv.put(TOPIC, "Science");
		cv.put(CITATION, "Take young researchers, put them together in virtual seclusion, give them an unprecedented degree of freedom and turn up the pressure by fostering competitiveness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Denis Diderot");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "The best doctor is the one you run to and can't find.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur Schopenhauer");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "The doctor sees all the weakness of mankind; the lawyer all the wickedness, the theologian all the stupidity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dizzy Dean");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "The doctors x-rayed my head and found nothing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lewis Thomas");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "The great secret of doctors, known only to their wives, but still hidden from the public, is that most things get better by themselves; most things, in fact, are better in the morning.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Hahnemann");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "The physician's highest calling, his only calling, is to make sick people healthy - to heal, as it is termed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plato");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "They certainly give very strange names to diseases.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcel Proust");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Three-quarters of the sicknesses of intelligent people come from their intelligence. They need at least a doctor who can understand this sickness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ovid");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Time is generally the best doctor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Enid Bagnold");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "When a man goes through six years training to be a doctor he will never be the same. He knows too much.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henny Youngman");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "When I told my doctor I couldn't afford an operation, he offered to touch-up my X-rays.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rodney Dangerfield");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "When I was born I was so ugly the doctor slapped my mother.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hippocrates");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "Whenever a doctor cannot do good, he must be kept from doing harm.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Landon");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "You can die of the cure before you die of the illness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Earl Wilson");
		cv.put(TOPIC, "Medical");
		cv.put(CITATION, "You may not be able to read a doctor's handwriting and prescription, but you'll notice his bills are neatly typewritten.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Tyson");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I'm a dreamer. I have to dream and reach for the stars, and if I miss a star then I grab a handful of clouds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Drew Barrymore");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "I've always said that one night, I'm going to find myself in some field somewhere, I'm standing on grass, and it's raining, and I'm with the person I love, and I know I'm at the very point I've been dreaming of getting to.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eugene Ionesco");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Ideologies separate us. Dreams and anguish bring us together.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "If one advances confidently in the direction of his dreams, and endeavors to live the life which he has imagined, he will meet with success unexpected in common hours.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Les Brown");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "If you take responsibility for yourself you will develop a hunger to accomplish your dreams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Butler Yeats");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "In dreams begins responsibility.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "It takes a lot of courage to show your dreams to someone else.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Judge of your natural character by what you do in your dreams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean-Paul Sartre");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Like all dreamers, I mistook disenchantment for truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Lindbergh");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Living in dreams of yesterday, we find ourselves still dreaming of impossible future conquests.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Akira Kurosawa");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Man is a genius when he is dreaming.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Donald G. Mitchell");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Married or unmarried, young or old, poet or worker, you are still a dreamer, and will one time know, and feel, that your life is but a dream.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Wollstonecraft Shelley");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "My dreams were all my own; I accounted for them to nobody; they were my refuge when annoyed - my dearest pleasure when free.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Campbell");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Myths are public dreams, dreams are private myths.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jesse Jackson");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "No one should negotiate their dreams. Dreams must be free to fly high. No government, no legislature, has a right to limit your dreams. You should never agree to surrender your dreams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Bakker");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Oh, I was never a businessman. I was a visionary, a dreamer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. V. Lucas");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "One of the most adventurous things left us is to go to bed. For no one can lay a hand on our dreams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Terry Pratchett");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Only in our dreams are we free. The rest of the time we need wages.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Fuller");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Only the dreamer shall understand realities, though in truth his dreaming must be not out of proportion to his waking.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert Kaufman");
		cv.put(TOPIC, "Dreams");
		cv.put(CITATION, "Only things the dreamers make live on. They are the eternal conquerors.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erich Fromm");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "The danger of the past was that men became slaves. The danger of the future is that man may become robots.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orson Welles");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "The enemy of society is middle class and the enemy of life is middle age.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adlai E. Stevenson");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "The first principle of a free society is an untrammeled flow of words in an open forum.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "A. J. Liebling");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "The function of the press in society is to inform, but its role in society is to make money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold J. Toynbee");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "The human race's prospects of survival were considerably better when we were defenceless against tigers than they are today when we have become defenceless against ourselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abbie Hoffman");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "The key to organizing an alternative society is to organize people around what they can do, and more importantly, what they want to do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James A. Baldwin");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "The most dangerous creation of any society is the man who has nothing to lose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John W. Gardner");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "The society which scorns excellence in plumbing as a humble activity and tolerates shoddiness in philosophy because it is an exalted activity will have neither good plumbing nor good philosophy: neither its pipes nor its theories will hold water.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lily Tomlin");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "The trouble with the rat race is that even if you win, you're still a rat.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean de la Bruyere");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "The wise person often shuns society for fear of being bored.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel Webster");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "The world is governed more by appearance than realities so that it is fully as necessary to seem to know something as to know it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "There is more to life than increasing its speed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Jordan");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Think what a better world it would be if we all, the whole world, had cookies and milk about three o'clock every afternoon and then lay down on our blankets for a nap.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baruch Spinoza");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "To give aid to every poor man is far beyond the reach and power of every man. Care of the poor is incumbent on society as a whole.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Medawar");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Today the world changes so quickly that in growing up we take leave not just of youth but of the world we were young in.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Russell Baker");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "Usually, terrible things that are done with the excuse that progress requires them are not really progress at all, but just terrible things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Milton Friedman");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "We have a system that increasingly taxes work and subsidizes nonwork.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hillary Clinton");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "We must stop thinking of the individual and start thinking about what is best for society.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Irv Kupcinet");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "What can you say about a society that says that God is dead and Elvis is alive?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Society");
		cv.put(CITATION, "What is the use of a house if you haven't got a tolerable planet to put it on?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miguel de Unamuno");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "It is truer to say that martyrs create faith more than faith creates martyrs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Owen D. Young");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "It takes vision and courage to create - it takes faith and courage to prove.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orson Pratt");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "It was seldom that I attended any religious meetings, as my parents had not much faith in and were never so unfortunate as to unite themselves with any of the religious sects.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paula Abdul");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Keep the faith, don't lose your perseverance and always trust your gut extinct.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Cromwell");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Keep your faith in God, but keep your powder dry.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Novalis");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Knowledge is only one half. Faith is the other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Browning Hamilton");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Love, hope, fear, faith - these make humanity; These are its sign and note and character.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Puff Daddy");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Man, I just feel blessed... I was in a situation where the only way I could come out of it was by putting my faith in God. No matter how good my lawyers were, no matter how much celebrity I had, everything was just stacked up against me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Cousins");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "My reason nourishes my faith and my faith my reason.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Desmond Morris");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "No matter how old we become, we can still call them 'Holy Mother' and 'Father' and put a child-like trust in them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Nor shall derision prove powerful against those who listen to humanity or those who follow in the footsteps of divinity, for they shall live forever. Forever.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Ley");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Only faith is sufficient.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Our faith comes in moments; our vice is habitual.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kirk Cameron");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Put your nose into the Bible everyday. It is your spiritual food. And then share it. Make a vow not to be a lukewarm Christian.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Donne");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Reason is our soul's left hand, faith her right.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther King, Jr.");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "Take the first step in faith. You don't have to see the whole staircase, just take the first step.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "That deep emotional conviction of the presence of a superior reasoning power, which is revealed in the incomprehensible universe, forms my idea of God.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "The faith that stands on authority is not faith.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Russell Lowell");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "The only faith that wears well and holds its color in all weathers is that which is woven of conviction and set with the sharp mordant of experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Augustus Hare");
		cv.put(TOPIC, "Faith");
		cv.put(CITATION, "The power of faith will often shine forth the most when the character is naturally weak.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jon Bon Jovi");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is falling nine times and getting up ten.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dale Carnegie");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is getting what you want. Happiness is wanting what you get.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George S. Patton");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is how high you bounce when you hit bottom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kevin Spacey");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is like death. The more successful you become, the higher the houses in the hills get and the higer the fences get.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Bernstein");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is often the result of taking a misstep in the right direction.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold H. Glasow");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is simple. Do what's right, the right way, at the right time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Earl Wilson");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is simply a matter of luck. Ask any failure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Luckman");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is that old ABC - ability, breaks, and courage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is the one unpardonable sin against our fellows.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul J. Meyer");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is the progressive realization of predetermined, worthwhile, personal goals.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Booker T. Washington");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success is to be measured not so much by the position that one has reached in life as by the obstacles which he has overcome.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold H. Glasow");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success isn't a result of spontaneous combustion. You must set yourself on fire.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Feather");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success seems to be largely a matter of hanging on after others have let go.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbra Streisand");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success to me is having ten honeydew melons and eating only the top half of each slice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joe Paterno");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "Success without honor is an unseasoned dish; it will satisfy your hunger, but it won't taste good.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. Somerset Maugham");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "The common idea that success spoils people by making them vain, egotistic and self-complacent is erroneous; on the contrary it makes them, for the most part, humble, tolerant and kind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ayn Rand");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "The ladder of success is best climbed by stepping on the rungs of opportunity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "B. C. Forbes");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "The man who has done his level best... is a success, even though the world may write him down a failure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Foster Dulles");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "The measure of success is not whether you have a tough problem to deal with, but whether it is the same problem you had last year.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodore Roosevelt");
		cv.put(TOPIC, "Success");
		cv.put(CITATION, "The most important single ingredient in the formula of success is knowing how to get along with people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Alito");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Private religious speech can't be discriminated against. It has to be treated equally with secular speech.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Douglas");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Real equality is immensely difficult to achieve, it needs continual revision and monitoring of distributions. And it does not provide buffers between members, so they are continually colliding or frustrating each other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Patricia Ireland");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Stewardesses are still paid so little that in many cases, new hires qualify for food stamps.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Thatcher");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "The battle for women's rights has been largely won.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Iris Murdoch");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "The cry of equality pulls everyone down.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Shirley Chisholm");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "The emotional, sexual, and psychological stereotyping of females begins when the doctor says: It's a girl.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Fourier");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "The extension of women's rights is the basic principle of all social progress.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther King, Jr.");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "The sweltering summer of the Negro's legitimate discontent will not pass until there is an invigorating autumn of freedom and equality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andrew Jackson");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "The wisdom of man never yet contrived a system of taxation that would operate with perfect equality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woodrow Wilson");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "There can be no equality or opportunity if men and women and children be not shielded in their lives from the consequences of great industrial and social processes which they cannot alter, control, or singly cope with.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Lincoln");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "These men ask for just the same thing, fairness, and fairness only. This, so far as in my power, they, and all others, shall have.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Golda Meir");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "To be successful, a woman has to be much better at her job than a man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Faulkner");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "To live anywhere in the world today and be against equality because of race or color is like living in Alaska and being against snow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Crystal Eastman");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Until women learn to want economic independence, and until they work out a way to get this independence without denying themselves the joys of love and motherhood, it seems to me feminism has no roots.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Wollstonecraft");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Virtue can only flourish among equals.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tarja Halonen");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "We are a model country where gender equality is concerned.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lionel Trilling");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "We who are liberal and progressive know that the poor are our equals in every sense except that of being equal to us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erica Jong");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Women are the only exploited group in history to have been idealized into powerlessness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charlotte Bunch");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Women have a lot to say about how to advance women's rights, and governments need to learn from that, listen to the movement and respond.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Toni Morrison");
		cv.put(TOPIC, "Equality");
		cv.put(CITATION, "Women's rights is not only an abstraction, a cause; it is also a personal affair. It is not only about us; it is also about me and you. Just the two of us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jacques Barzun");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is intended and designed for sentient beings that have hopes and purposes and emotions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sidney Lanier");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is love in search of a word.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Paul");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is moonlight in the gloomy night of life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jimi Hendrix");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is my religion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is nothing else but wild sounds civilized into time and tune.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aaron Carter");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is something that always lifts my spirits and makes me happy, and when I make music I always hope it will have the same effect on whoever listens to it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. H. Auden");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is the best means we have of digesting time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christian Nestell Bovee");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is the fourth great material want, first food, then clothes, then shelter, then music.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plato");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is the movement of sound to reach the soul for the education of its virtue.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Tolstoy");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is the shorthand of emotion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Max Heindel");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is the soul of language.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edgar Winter");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is very spiritual, it has the power to bring people together.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Carlyle");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music is well said to be the speech of angels.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Missy Elliot");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music should be your escape.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maya Angelou");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music was my refuge. I could crawl into the space between the notes and curl my back to loneliness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Berthold Auerbach");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music washes away from the soul the dust of everyday life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Ruskin");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music when healthy, is the teacher of perfect order, and when depraved, the teacher of perfect disorder.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Zappa");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music, in performance, is a type of sculpture. The air in the performance is sculpted into something.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charlie Byrd");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music's not like becoming a doctor, who can walk into a community and find people who need him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gary Wright");
		cv.put(TOPIC, "Music");
		cv.put(CITATION, "Music's staying power is a function of how timeless the lyrics, song and production are.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Norman Hall");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Remote villages and communities have lost their identity, and their peace and charm have been sacrificed to that worst of abominations, the automobile.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robbie Coltrane");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "See, what you're meant to do when you have a mid-life crisis is buy a fast car, aren't you? Well, I've always had fast cars. It's not that. It's the fear that you're past your best. It's the fear that the stuff you've done in the past is your best work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jeff Gordon");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "So much of my life is spent just focused on driving race cars.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marvin Minsky");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Societies need rules that make no sense for individuals. For example, it makes no difference whether a single car drives on the left or on the right. But it makes all the difference when there are many cars!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rudolf Diesel");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "The automobile engine will come, and then I will consider my life's work complete.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brock Yates");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "The automobile, both a cause and an effect of this decentralization, is ideally suited for our vast landscape and our generally confused and contrary commuting patterns.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexandra Paul");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "The cars we drive say a lot about us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gore Vidal");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "The greatest pleasure when I started making money was not buying cars or yachts but finding myself able to have as many freshly typed drafts as possible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Pryor");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "There was a time in my life when I thought I had everything - millions of dollars, mansions, cars, nice clothes, beautiful women, and every other materialistic thing you can imagine. Now I struggle for peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mario Lopez");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "There's three things men always talk about - women, sports, and cars.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Hillman");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "We approach people the same way we approach our cars. We take the poor kid to a doctor and ask, What's wrong with him, how much will it cost, and when can I pick him up?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Yuan T. Lee");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "We need to become good citizens in the global village, instead of competing. What are we competing for - to drive more cars, eat more steaks? That will destroy the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Searle");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "We often attribute 'understanding' and other cognitive predicates by metaphor and analogy to cars, adding machines, and other artifacts, but nothing is proved by such attributions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Catherine Bell");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "We're just into toys, whether it's motorcycles or race cars or computers. I've got the Palm Pilot right here with me, I've got the world's smallest phone. Maybe it's just because I'm still a big little kid and I just love toys, you know?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bud Abbott");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "Well, I always had a chauffer, because I have never driven a car in my life. I still can't drive.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Russell Baker");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "When it comes to cars, only two varieties of people are possible - cowards and fools.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ayrton Senna");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "When you are fitted in a racing car and you race to win, second or third place is not enough.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ed Markey");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "You have to wait six months to purchase a fuel efficient automobile made from overseas.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gordon Sinclair");
		cv.put(TOPIC, "Car");
		cv.put(CITATION, "You talk about German technocracy and you get automobiles.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Shelley Duvall");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I'm not afraid of aging.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henri Frederic Amiel");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I'm not interested in age. People who tell me their age are silly. You're as old as you feel.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Knut Hamsun");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "In old age we are like a batch of letters that someone has sent. We are no longer in the past, we have arrived.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pope Paul VI");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "In youth the days are short and the years are long. In old age the years are short and day's long.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Beverly Sills");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "In youth we run into difficulties. In old age difficulties run into us.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sam Ewing");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Inflation is when you pay fifteen dollars for the ten-dollar haircut you used to get for five dollars when you had hair.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brigitte Bardot");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "It is sad to grow old but nice to ripen.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pablo Picasso");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "It takes a long time to become young.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Cosby");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Like everyone else who makes the mistake of getting older, I begin each day with coffee and obituaries.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harold Coffin");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Middle age is the awkward period when Father Time starts catching up with Mother Nature.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don Marquis");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Middle age is the time when a man is always thinking that in a week or two he will feel as good as ever.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bob Hope");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Middle age is when your age starts to show around your middle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doris Day");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Middle age is youth without levity, and age without decay.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Holbrook Jackson");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "No man is ever old enough to know better.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Katharine Graham");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "No one can avoid aging, but aging productively is something else.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Ullman");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Nobody grows old merely by living a number of years. We grow old by deserting our ideals. Years may wrinkle the skin, but to give up enthusiasm wrinkles the soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "None are so old as those who have outlived enthusiasm.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ira Gershwin");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Old age adds to the respect due to virtue, but it takes nothing from the contempt inspired by vice; it whitens only the hair.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emily Dickinson");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Old age comes on suddenly, and not gradually as is thought.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eleanor Roosevelt");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "Old age has deformities enough of its own. It should never add to them the deformity of vice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Perlis");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "If your computer speaks English, it was probably made in Japan.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jef Raskin");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Imagine if every Thursday your shoes exploded if you tied them the usual way. This happens to us all the time with computers, and nobody thinks of complaining.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Perlis");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "In computing, turning the obvious into the useful is a living definition of the word \"frustration\".");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Niklaus Wirth");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "In the practical world of computing, it is rather uncommon that a program, once it performs correctly and satisfactorily, remains unchanged forever.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Buffy Sainte-Marie");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "It was a black and white only computer at the time, but it kept me fascinated.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Royko");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "It's been my policy to view the Internet not as an 'information highway,' but as an electronic asylum filled with babbling loonies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Craig Bruce");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "It's hardware that makes a machine fast. It's software that makes a fast machine slow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erykah Badu");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Man, I don't want to have nothing to do with computers. I don't want the government in my business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thabo Mbeki");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Many of our own people here in this country do not ask about computers, telephones and television sets. They ask - when will we get a road to our village.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alain Robert");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Modern people are only willing to believe in their computers, while I believe in myself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Merkle");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Nanotechnology will let us build computers that are incredibly powerful. We'll have more power in the volume of a sugar cube than exists in the entire world today.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steve Wozniak");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Never trust a computer you can't throw out a window.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Danielle Berry");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "No one ever said on their deathbed, 'Gee, I wish I had spent more time alone with my computer'.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Ralph Augustine");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "One of the most feared expressions in modern times is 'The computer is down.'");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isaac Asimov");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Part of the inhumanity of the computer is that, once it is competently programmed and working smoothly, it is completely honest.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Osborne");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "People think computers will keep them from making mistakes. They're wrong. With computers you make mistakes faster.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Dawkins");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Personally, I rather look forward to a computer program winning the world chess championship. Humanity needs a lesson in humility.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur C. Clarke");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Reading computer manuals without the hardware is as frustrating as reading manuals without the software.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jef Raskin");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Right now, computers, which are supposed to be our servant, are oppressing us.");
		db.insert("citations", AUTHOR, cv);
	}
	
	public void onCreate5(SQLiteDatabase db){
		ContentValues cv = new ContentValues();
		cv.put(AUTHOR, "T. S. Eliot");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I don't believe one grows older. I think that what happens early on in life is that at a certain age one stands still and stagnates.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bob Hope");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I don't feel old. I don't feel anything till noon. That's when it's time for my nap.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christine Lahti");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I don't want to fight aging; I want to take good care of myself, but plastic surgery and all that? I'm not interested.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Ernest Hocking");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I find that a man is as old as his work. If his work keeps him from moving forward, he will look forward with the work.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Willard Scott");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I get all fired up about aging in America.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Josh Billings");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I think when the full horror of being fifty hits you, you should stay home and have a good cry.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lauren Bacall");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I think your whole life shows in your face and you should be proud of that.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sting");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I want to get old gracefully. I want to have good posture, I want to be healthy and be an example to my children.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I will never be an old man. To me, old age is always 15 years older than I am.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Broughton");
		cv.put(TOPIC, "Age");
		cv.put(CITATION, "I'm happy to report that my inner child is still ageless.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doug Larson");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "Home computers are being called upon to perform many new functions, including the consumption of homework formerly eaten by the dog.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dave Barry");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I am not the only person who uses his computer mainly for the purpose of diddling with his computer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bruce Scheneier");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I am regularly asked what the average Internet user can do to ensure his security. My first answer is usually 'Nothing; you're screwed'.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Isaac Asimov");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I do not fear computers. I fear the lack of them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jeff Hawkins");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I do two things. I design mobile computers and I study brains.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eugene Jarvis");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I got interested in computers and how they could be enslaved to the megalomaniac impulses of a teenager.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Stephen");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I got up with my wife, I sat down at the computer when she went to work, and I didn't stop until she got home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Moog");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I happen to think that computers are the most important thing to happen to musicians since the invention of cat-gut which was a long time ago.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Griffith");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I just became one with my browser software.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Budge");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I started on an Apple II, which I had bought at the very end of 1978 for half of my annual income. I made $4,500 a year, and I spent half of it on the computer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Hawking");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I think computer viruses should count as life. I think it says something about human nature that the only form of life we have created so far is purely destructive. We've created life in our own image.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Gates");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I think it's fair to say that personal computers have become the most empowering tool we've ever created. They're tools of communication, they're tools of creativity, and they can be shaped by their user.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack Welch");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I was afraid of the internet... because I couldn't type.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marc Jacobs");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I wouldn't know how to find eBay on the computer if my life depended on it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Florence Henderson");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I'm a '70s mom, and my daughter is a '90s mom. I know a lot of women my age who are real computer freaks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Plummer");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I'm too old-fashioned to use a computer. I'm too old-fashioned to use a quill.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dee Brown");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "I've tried word processors, but I think I'm too old a dog to use one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Hilton");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "If net neutrality goes away, it will fundamentally change everything about the Internet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Niklas Zennstrom");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "If you could utilize the resources of the end users' computers, you could do things much more efficiently.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Tufte");
		cv.put(TOPIC, "Computers");
		cv.put(CITATION, "If you like overheads, you'll love PowerPoint.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paris Hilton");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I'd imagine my wedding as a fairy tale... huge, beautiful and white.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Piper Laurie");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "I'm one of those people who has always been a bridesmaid.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Groucho Marx");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "In Hollywood, brides keep the bouquets and throw away the groom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael J. Fox");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "In my 50s I'll be dancing at my children's weddings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Reddy");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "In the '50s, a lot of girls never saw beyond the wedding day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Trisha Goddard");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "It was only literally hours after the wedding when he felt he didn't have to keep up the facade.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "It's a funny thing that when a man hasn't anything on earth to worry about, he goes off and gets married.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Wendell Holmes");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "Love is the master key that opens the gates of happiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Socrates");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "My advice to you is get married: if you find a good wife you'll be happy; if not, you'll become a philosopher.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Hornung");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "Never get married in the morning - you never know who you might meet that night.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Beattie");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "No jealousy their dawn of love overcast, nor blasted were their wedded days with strife; each season looked delightful as it past, to the fond husband and the faithful wife.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helen Hunt Jackson");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "O month when they who love must love and wed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "King Edward VIII");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "Of course, I do have a slight advantage over the rest of you. It helps in a pinch to be able to remind your bride that you gave up a throne for her.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Pepys");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "Saw a wedding in the church. It was strange to see what delight we married people have to see these poor fools decoyed into our condition.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James M. Barrie");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "That is ever the way. 'Tis all jealousy to the bride and good wishes to the corpse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Heraclitus");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "The chain of wedlock is so heavy that it takes two to carry it - and sometimes three.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nicholas Sparks");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "The first thing I did when I sold my book was buy a new wedding ring for my wife and asked her to marry me all over again.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rob Mariano");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "The one thing that I'm in charge of in this wedding is the food.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara de Angelis");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "The real act of marriage takes place in the heart, not in the ballroom or church or synagogue. It's a choice you make - not just on your wedding day, but over and over again - and that choice is reflected in the way you treat your husband or wife.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Morley");
		cv.put(TOPIC, "Wedding");
		cv.put(CITATION, "The trouble with wedlock is that there's not enough wed and too much lock.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Heinrich Heine");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience is a good school. But the fees are high.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Legend");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience is a great teacher.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience is one thing you can't get for nothing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience is simply the name we give our mistakes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience is the child of thought, and thought is the child of action.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alphonse de Lamartine");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience is the only prophecy of wise men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Julius Caesar");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience is the teacher of all things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aldous Huxley");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experience teaches only the teachable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Karl Kraus");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Experiences are savings which a miser puts aside. Wisdom is an inheritance which a wastrel cannot exhaust.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Randolph Bourne");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Few people even scratch the surface, much less exhaust the contemplation of their own experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fred Brooks");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Good judgment comes from experience and experience comes from bad judgment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rita Mae Brown");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Good judgment comes from experience, and often experience comes from bad judgment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas Adams");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Human beings, who are almost unique in having the ability to learn from the experience of others, are also remarkable for their apparent disinclination to do so.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sanford I. Weill");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "I think we are a product of all our experiences.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abigail Van Buren");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "If we could sell our experiences for what they cost us, we'd all be millionaires.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clarence Day");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Information's pretty thin stuff unless mixed with experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Lincoln");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "It has been my experience that folks who have no vices have very few virtues.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walter Wriston");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Judgment comes from experience - and experience comes from bad judgment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Soren Kierkegaard");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Life can only be understood backwards; but it must be lived forwards.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Butler");
		cv.put(TOPIC, "Experience");
		cv.put(CITATION, "Life is like playing a violin solo in public and learning the instrument as one goes on.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Morrison");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Film spectators are quiet vampires.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Goldwyn");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Give me a couple of years, and I'll make that actress an overnight success.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jerry Orbach");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "'Home Alone' was a movie, not an alibi.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bette Davis");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "I don't take the movies seriously, and anyone who does is in for a headache.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean-Luc Godard");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "I don't think you should feel about a film. You should feel about a woman, not a movie. You can't kiss a movie.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alex Winter");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "I just like movies that somehow expose the world in a way that's different than you imagine it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Neil LaBute");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "I make movies I want to see.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marlene Dietrich");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "I never enjoyed working in a film.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Ford Coppola");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "I think cinema, movies, and magic have always been closely associated. The very earliest people who made film were magicians.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Hughes");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "I was obsessed with romance. When I was in high school, I saw 'Doctor Zhivago' every day from the day it opened until the day it left the theater.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oskar Werner");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "I'm married to the theater but my mistress is the films.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Estes");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "I've always been an animal lover. I've grown up with dogs my whole life. I think that is what helped me get the role on 'Lassie', I was comfortable around the dog, where many of the kids were afraid or intimidated by Lassie.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woody Allen");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "If my films don't show a profit, I know I'm doing something right.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dario Argento");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "If you don't like my movies, don't watch them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Leeson");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "It couldn't sound like a dog, because K9 isn't a dog, but I made it sound as mechanical as possible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Warhol");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "It's the movies that have really been running things in America ever since they were invented. They show you what to do, how to do it, when to do it, how to feel about it, and how to look how you feel about it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roger Ebert");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Most of us do not consciously look at movies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Yahoo Serious");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Movies are a complicated collision of literature, theatre, music and all the visual arts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard King");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Movies are an art form that is very available to the masses.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tim Burton");
		cv.put(TOPIC, "Movies");
		cv.put(CITATION, "Movies are like an expensive form of therapy for me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Jarmusch");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I didn't get my degree at NYU; I got it later, they gave me an honourary one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Keegan");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I don't look to find an educated person in the ranks of university graduates, necessarily. Some of the most educated people I know have never been near a university.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Green Somerville");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I dropped out of school for a semester, transferred to another college, switched to an art major, graduated, got married, and for a while worked as a graphic designer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carol P. Christ");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I first became interested in women and religion when I was one of the few women doing graduate work in Religious Studies at Yale University in the late 1960's.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fiona Apple");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I got all my work done to graduate in two months and then they were like, I'm sorry, you have to take driver's ed. I just kind of went, Oh, forget it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Scott Hamilton");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I graduated a the top of my class in the '84 Olympic Games; I won a gold medal.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Katherine Dunham");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I have actually five honorary degrees.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jim Clyburn");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I have no problems with private schools. I graduated from one and so did my mother. Private schools are useful and we often use public funds to pay for their infrastructures and other common needs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fred Allen");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I learned law so well, the day I graduated I sued the college, won the case, and got my tuition back.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frederick Reines");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I received my undergraduate degree in engineering in 1939 and a Master of Science degree in mathematical physics in 1941 at Steven Institute of Technology.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tobias Wolff");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I teach one semester a year, and this year I'm just teaching one course during that semester, a writing workshop for older students in their late 20s and early 30s, people in our graduate program who are already working on a manuscript and trying to bring it to completion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel J. Evans");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I think I finally chose the graduate degree in engineering primarily because it only took one year and law school took three years, and I felt the pressure of being a little behind - although I was just 22.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ken Buck");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I think women as well as men are concerned about jobs and the economy and spending and, and other issues. They're concerned that when their kids graduate from college they have an economy and they have a future in this country and they, they have the same opportunity that we've had and our grandparents have had.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Irvine Welsh");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I think young writers should get other degrees first, social sciences, arts degrees or even business degrees. What you learn is research skills, a necessity because a lot of writing is about trying to find information.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Claire Danes");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I took three years off. I differentiated myself from the industry. Found my identity - sort of... I haven't graduated yet. I'm not legitimately educated yet, but maybe one day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Gallagher");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I was about to get a degree in economics when I accepted that I'd be a lousy businessman, and if I didn't give acting a try I'd regret it for the rest of my life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Stewart");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I was going to be an architect. I graduated with a degree in architecture and I had a scholarship to go back to Princeton and get my Masters in architecture. I'd done theatricals in college, but I'd done them because it was fun.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bob Newhart");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I was never a Certified Public Accountant. I just had a degree in accounting. It would require passing a test, which I would not have been able to do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Shannon Lucid");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I was really desperate. I don't know if you can remember back that far, but when I went to graduate school they didn't want females in graduate school. They were very open about it. They didn't mince their words. But then I got in and I got my degree.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Kennedy");
		cv.put(TOPIC, "Graduation");
		cv.put(CITATION, "I was the first boy in the Kennedy family to graduate from college.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louis Sullivan");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "But the building's identity resided in the ornament.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel Libeskind");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Cities are the greatest creations of humanity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Antonio Gaudi");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Color in certain places has the great value of making the outlines and structural planes seem more energetic.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jimmy Breslin");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Designed by architects with honorable intentions but hands of palsy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Osborne");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Don't clap too hard - it's a very old building.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Enid Nemy");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Even at the United Nations, where legend has it that the building was designed so that there could be no corner offices, the expanse of glass in individual offices is said to be a dead giveaway as to rank. Five windows are excellent, one window not so great.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Helmut Jahn");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Every building is a prototype. No two are alike.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Lloyd Wright");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Every great architect is - necessarily - a great poet. He must be a great original interpreter of his time, his day, his age.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Rouse");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "For many years, I have lived uncomfortably with the belief that most planning and architectural design suffers for lack of real and basic purpose. The ultimate purpose, it seems to me, must be the improvement of mankind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louis Sullivan");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Form follows function.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Rogers");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Form follows profit is the aesthetic principle of our times.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur Erickson");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "Great buildings that move the spirit have always been rare. In every case they are unique, poetic, products of the heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Jackson Davis");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "I am but an architectural composer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tadao Ando");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "I believe that the way people live can be directed a little by architecture.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ayn Rand");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "I don't build in order to have clients. I have clients in order to build.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Luis Barragan");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "I don't divide architecture, landscape and gardening; to me they are one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Philip Johnson");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "I hate vacations. If you can build buildings, why sit on the beach?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Jackson Davis");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "I have designed the most buildings of any living American architect.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Donna Karan");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "I love building spaces: architecture, furniture, all of it, probably more than fashion. The development procedure is more tactile. It's about space and form and it's something you can share with other people.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maya Lin");
		cv.put(TOPIC, "Architecture");
		cv.put(CITATION, "I try to give people a different way of looking at their surroundings. That's art to me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Democritus");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Happiness resides not in possessions, and not in gold, happiness dwells in the soul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lao Tzu");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Health is the greatest possession. Contentment is the greatest treasure. Confidence is the greatest friend. Non-being is the greatest joy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Hope is a waking dream.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vincent McNabb");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Hope is some extraordinary spiritual grace that God gives us to control our fears, not to oust them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Muir");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "How glorious a greeting the sun gives the mountains!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pope John XXIII");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "I have looked into your eyes with my eyes. I have put my heart near your heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Junipero Serra");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "I pray God may preserve your health and life many years.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Maynard Keynes");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Ideas shape the course of history.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "If it were not for hopes, the heart would break.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "In a gentle way, you can shake the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry Emerson Fosdick");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "It is by acts and not by ideas that people live.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle Onassis");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "It is during our darkest moments that we must focus to see the light.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Wood Krutch");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "It is not ignorance but knowledge which is the mother of wonder.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Paul");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Joy descends gently upon us like the evening dew, and does not patter down like a hailstorm.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Louis Stevenson");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Judge each day not by the harvest you reap but by the seeds you plant.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Norris Russell");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Just what future the Designer of the universe has provided for the souls of men I do not know, I cannot prove. But I find that the whole order of Nature confirms my confidence that, if it is not like our noblest hopes and dreams, it will transcend them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristophanes");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Let each man exercise the art he knows.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Aurelius");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Look within. Within is the fountain of good, and it will ever bubble up, if thou wilt ever dig.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johann Wolfgang von Goethe");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Love and desire are the spirit's wings to great deeds.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bern Williams");
		cv.put(TOPIC, "Inspirational");
		cv.put(CITATION, "Man never made any material as resilient as the human spirit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Franklin P. Adams");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Health is the thing that makes you feel that now is the best time of the year.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Thomson");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Health is the vital principle of bliss, and exercise, of health.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Ditka");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "Here's what I tell anybody and this is what I believe. The greatest gift we have is the gift of life. We understand that. That comes from our Creator. We're given a body. Now you may not like it, but you can maximize that body the best it can be maximized.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tina Louise");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I concentrate on exercises from the waist down, since that is the laziest part of a woman's body.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martina Hingis");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I didn't have the same fitness or ability as the other girls, so I had to beat them with my mind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sarah Michelle Gellar");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I don't smoke, don't drink much, and go to the gym five times a week. I live a healthy lifestyle and feel great. I can run a marathon, you know.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sting");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I have been through various fitness regimes. I used to run about five miles a day and I did aerobics for a while.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erin Gray");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I like exercise. I like a healthy body.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jill Clayburgh");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I love to swim for miles; I could just go back and forth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peggy Fleming");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I think exercise tests us in so many ways, our skills, our hearts, our ability to bounce back after setbacks. This is the inner beauty of sports and competition, and it can serve us all well as adult athletes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stone Gossard");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I think if you exercise, your state of mind - my state of mind - is usually more at ease, ready for more mental challenges. Once I get the physical stuff out of the way it always seems like I have more calmness and better self-esteem.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rachel Blanchard");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I think it's more important to be fit so that you can be healthy and enjoy activities than it is to have a good body.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Haile Gebrselassie");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I will always listen to my coaches. But first I listen to my body. If what they tell me suits my body, great. If my body doesn't feel good with what they say, then always my body comes first.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kim Alexis");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I would rather exercise than read a newspaper.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eva Green");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I'm French, so I'm quite lazy about exercising, and I smoke. But I do love going for a run in the morning with my dog. That's all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Shilton");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "I'm still very professional about my fitness. I stay in trim as I always did.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vinoba Bhave");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "If a man achieves victory over this body, who in the world can exercise power over him? He who rules himself rules over the whole world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joey Adams");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "If it weren't for the fact that the TV set and the refrigerator are so far apart, some of us wouldn't get any exercise at all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hippocrates");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "If we could give every individual the right amount of nourishment and exercise, not too little and not too much, we would have found the safest way to health.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Grete Waitz");
		cv.put(TOPIC, "Fitness");
		cv.put(CITATION, "In terms of fitness and battling through cancer, exercise helps you stay strong physically and mentally.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walt Disney");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "I don't like formal gardens. I like wild nature. It's just the wilderness instinct in me, I guess.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Hobson");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "I grow plants for many reasons: to please my eye or to please my soul, to challenge the elements or to challenge my patience, for novelty or for nostalgia, but mostly for the joy in seeing them grow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Suzy Bogguss");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "I love decorating my home. I'm a gardener too, so that's usually something I have to play catch up with.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dorothy Malone");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "I loved to get all dusty and ride horses and plant potatoes and cotton.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Penelope Keith");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "I plant a lot of trees. I am a great believer in planting things for future generations. I loathe the now culture where you just live for today.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phyllis Theroux");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "I think this is what hooks one to gardening: it is the closest one can come to being present at creation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carolus Linnaeus");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "If a tree dies, plant another in its place.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Luther Burbank");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "If we had paid no more attention to our plants than we have to our children, we would now be living in a jungle of weed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Harrison");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "If you build up the soil with organic material, the plants will do just fine.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Tullius Cicero");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "If you have a garden and a library, you have everything you need.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Walker");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "In search of my mother's garden, I found my own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Louis Stevenson");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "It is a golden maxim to cultivate the garden for the nose, and the eyes will take care of themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orison Swett Marden");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "It is like the seed put in the soil - the more one sows, the greater the harvest.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "B. C. Forbes");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "It is only the farmer who faithfully plants seeds in the Spring, who reaps a harvest in the Autumn.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Eliot");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "It will never rain roses: when we want to have more roses we must plant more trees.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eric Morecambe");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "My neighbour asked if he could use my lawnmower and I told him of course he could, so long as he didn't take it out of my garden.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Cabot Lowell");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "One lifetime is never enough to accomplish one's horticultural goals. If a garden is a site for the imagination, how can we be very far from the beginning?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Moore");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Plants that wake when others sleep. Timid jasmine buds that keep their fragrance to themselves all day, but when the sunlight dies away let the delicious secret out to every breeze that roams about.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. Jackson Brown, Jr.");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Remember that children, marriages, and flower gardens reflect the kind of care they get.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Austin");
		cv.put(TOPIC, "Gardening");
		cv.put(CITATION, "Show me your garden and I shall tell you what you are.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mencius");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Friendship is one mind in two bodies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "C. S. Lewis");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Friendship is unnecessary, like philosophy, like art... It has no survival value; rather it is one of those things that give value to survival.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dag Hammarskjold");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Friendship needs no words - it is solitude delivered from the anguish of loneliness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Muhammad Ali");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Friendship... is not something you learn in school. But if you haven't learned the meaning of friendship, you really haven't learned anything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "He has no enemies, but is intensely disliked by his friends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Katherine Mansfield");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "I always felt that the great high privilege, relief and comfort of friendship was that one had to explain nothing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plutarch");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "I don't need a friend who changes when I change and who nods when I nod; my shadow does that much better.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas A. Edison");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "I have friends in overalls whose friendship I would not swap for the favor of the kings of the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Brault");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "I value the friend who for me finds time on his calendar, but I cherish the friend who for me does not consult his calendar.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George MacDonald");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "If instead of a gem, or even a flower, we should cast the gift of a loving thought into the heart of a friend, that would be giving as the angels give.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Duer Miller");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "If it's very painful for you to criticize your friends - you're safe in doing it. But if you take the slightest pleasure in it, that's the time to hold your tongue.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Schweitzer");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "In everyone's life, at some time, our inner fire goes out. It is then burst into flame by an encounter with another human being. We should all be thankful for those people who rekindle the inner spirit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mignon McLaughlin");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "It is important to our friends to believe that we are unreservedly frank with them, and important to friendship that we are not.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Epicurus");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "It is not so much our friends' help that helps us, as the confidence of their help.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "It is one of the blessings of old friends that you can afford to be stupid with them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Leonard");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "It takes a long time to grow an old friend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marlene Dietrich");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "It's the friends you can call up at 4 a.m. that matter.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcel Proust");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Let us be grateful to people who make us happy, they are the charming gardeners who make our souls blossom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oprah Winfrey");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Lots of people want to ride with you in the limo, but what you want is someone who will take the bus with you when the limo breaks down.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emil Ludwig");
		cv.put(TOPIC, "Friendship");
		cv.put(CITATION, "Many a person has held close, throughout their entire lives, two friends that always remained strange to one another, because one of them attracted by virtue of similarity, the other by difference.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel J. Boorstin");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "As you make your bed, so you must lie in it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William E. Simon");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Bad politicians are sent to Washington by good people who don't vote.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Spiro T. Agnew");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Confronted with the choice, the American people would choose the policeman's truncheon over the anarchist's bomb.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Conservative, n: A statesman who is enamored of existing evils, as distinguished from the Liberal who wishes to replace them with others.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Stuart Mill");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Conservatives are not necessarily stupid, but most stupid people are conservatives.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Will");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Conservatives define themselves in terms of what they oppose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Byrne");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Democracy is being allowed to vote for the candidate you dislike least.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "E. B. White");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Democracy is the recurrent suspicion that more than half of the people are right more than half of the time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Democracy is when the indigent, and not the men of property, are the rulers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Orben");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Do you ever get the feeling that the only reason we have elections is to find out if the polls were right?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "B. R. Ambedkar");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Every man who repeats the dogma of Mill that one country is no fit to rule another country must admit that one class is not fit to rule another class.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lady Bird Johnson");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Every politician should have been born an orphan and remain a bachelor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John W. Gardner");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "For every talent that poverty has stimulated it has blighted a hundred.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Kennedy");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Frankly, I don't mind not being President. I just mind that someone else is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William O. Douglas");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Free speech is not to be regulated like diseased cattle and impure butter. The audience that hissed yesterday may applaud today, even for the same performance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel J. Boorstin");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Freedom means the opportunity to be what we never thought we would be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "A. J. Liebling");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Freedom of the press is guaranteed only to those who own one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mayer Amschel Rothschild");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Give me control of a nation's money and I care not who makes her laws.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gore Vidal");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "Half of the American people have never read a newspaper. Half never voted for President. One hopes it is the same half.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry B. Adams");
		cv.put(TOPIC, "Politics");
		cv.put(CITATION, "He too serves a certain purpose who only stands and cheers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Susan B. Anthony");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Failure is impossible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louis D. Brandeis");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Fear of serious injury alone cannot justify oppression of free speech and assembly. Men feared witches and burnt women. It is the function of speech to free men from the bondage of irrational fears.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Columbus");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Following the light of the sun, we left the Old World.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francois Mitterrand");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "France is delighted at this new opportunity to show the world that when one has the will one can succeed in joining peoples who have been brought close by history.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Augustine Birrell");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Friendship is a word, the very sight of which in print makes the heart warm.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eddie Bernice Johnson");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Generally speaking, historically in this country, the care of a child has been thought of as female business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Glory is fleeting, but obscurity is forever.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Butler");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "God cannot alter the past, though historians can.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. L. Mencken");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "Historian: an unsuccessful novelist.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Percy Bysshe Shelley");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is a cyclic poem written by time upon the memories of man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexis de Tocqueville");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is a gallery of pictures in which there are few originals and many copies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Santayana");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is a pack of lies about events that never happened told by people who weren't there.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is a relentless master. It has no present, only the past rushing into the future. To try to hold fast is to be swept aside.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ted Koppel");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is a tool used by politicians to justify their intentions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Cousins");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is a vast early warning system.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold J. Toynbee");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is a vision of God's creation on the move.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is an account, mostly false, of events, mostly unimportant, which are brought about by rulers, mostly knaves, and soldiers, mostly fools.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Gibbon");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is indeed little more than the register of the crimes, follies, and misfortunes of mankind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ford");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is more or less bunk.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Konrad Adenauer");
		cv.put(TOPIC, "History");
		cv.put(CITATION, "History is the sum total of things that could have been avoided.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Hold yourself responsible for a higher standard than anybody expects of you. Never excuse yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Joseph Schwartz");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "How we think shows through in how we act. Attitudes are mirrors of the mind. They reflect thinking.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "R. Buckminster Fuller");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "I look for what needs to be done. After all, that's how the universe designs itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Peters");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "If a window of opportunity appears, don't pull down the shade.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William James");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "If you care enough for a result, you will most certainly attain it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "If you command wisely, you'll be obeyed cheerfully.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harvey S. Firestone");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "If you have ideas, you have the main asset you need, and there isn't any limit to what you can do with your business and your life. Ideas are any man's greatest asset.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ford");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "If you think you can do a thing or think you can't do a thing, you're right.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William James");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "If you want a quality, act as if you already had it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Fuller");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "In fair weather prepare for foul.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steve Jobs");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Innovation distinguishes between a leader and a follower.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Eisner");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "It is rare to find a business partner who is selfless. If you are lucky it happens once in a lifetime.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Ogilvy");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Leaders grasp nettles.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John C. Maxwell");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Leaders must be close enough to relate to others, but far enough ahead to motivate them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harold S. Geneen");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Leadership cannot really be taught. It can only be learned.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harold S. Geneen");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Leadership is practiced not so much in words as in attitude and in actions.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dwight D. Eisenhower");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Leadership is the art of getting someone else to do something you want done because he wants to do it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Warren G. Bennis");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Leadership is the capacity to translate vision into reality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ray Kroc");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Luck is a dividend of sweat. The more you sweat, the luckier you get.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Drucker");
		cv.put(TOPIC, "Leadership");
		cv.put(CITATION, "Making good decisions is a crucial skill at every level.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Love all, trust a few, do wrong to none.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Leacock");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Men are able to trust one another, knowing the exact degree of dishonesty they are entitled to expect.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herodotus");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Men trust their ears less than their eyes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Corrie Ten Boom");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Never be afraid to trust an unknown future to a known God.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lawrence Welk");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Never trust anyone completely but God. Love people, but put your full trust only in God.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eubie Blake");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Never trust anyone who wants what you've got. Friend or no, envy is an overwhelming emotion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ryan Stiles");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Never trust sheep.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roy Acuff");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Put your trust in the Lord and go ahead. Worry gets you no place.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eric Hoffer");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Someone who thinks the world is always cheating him is right. He is missing that wonderful feeling of trust in someone or something.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gisele Bundchen");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "The more you trust your intuition, the more empowered you become, the stronger you become, and the happier you become.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen King");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "The trust of the innocent is the liar's most useful tool.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Finley Peter Dunne");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust everybody, but cut the cards.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emmeline Pankhurst");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust in God - she will provide.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Natalie Goldberg");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust in what you love, continue to do it, and it will take you where you need to go.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Claudia Black");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust in yourself. Your perceptions are often far more accurate than you are willing to believe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tom Ridge");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust is a great force multiplier.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara Smith");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust is to human relationships what faith is to gospel living. It is the beginning place, the foundation upon which more can be built. Where trust is, love can flourish.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doris Lessing");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust no friend without faults, and love a woman, but no angel.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Virgil");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust not too much to appearances.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Baden-Powell");
		cv.put(TOPIC, "Trust");
		cv.put(CITATION, "Trust should be the basis for all our moral training.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Warhol");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "An artist is somebody who produces things that people don't need to have.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Valery");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "An artist never really finishes his work, he merely abandons it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andre Gide");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art begins with resistance - at the point where resistance is overcome. No human masterpiece has ever been created without great labor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gilbert K. Chesterton");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art consists of limitation. The most beautiful part of every picture is the frame.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roy Lichtenstein");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art doesn't transform. It just plain forms.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Merton");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art enables us to find ourselves and lose ourselves at the same time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Lloyd Wright");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art for art's sake is a philosophy of the well-fed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gwendolyn Brooks");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art hurts. Art urges voyages - and it is easier to stay at home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Delaunay");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art in Nature is rhythmic and has a horror of constraint.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andre Gide");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is a collaboration between God and the artist, and the less the artist does the better.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andre Malraux");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is a revolt against fate.All art is a revolt against man's fate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is a step from what is obvious and well-known toward what is arcane and concealed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Octavio Paz");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is an invention of aesthetics, which in turn is an invention of philosophers... What we call art is a game.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Gauguin");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is either plagiarism or revolution.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodor Adorno");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is magic delivered from the lie of being truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry A. Kissinger");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is man's expression of his joy in labor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Ruskin");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is not a study of positive reality, it is the seeking for ideal truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elbert Hubbard");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is not a thing; it is a way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry S. Truman");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is parasitic on life, just as criticism is parasitic on art.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wilson Mizner");
		cv.put(TOPIC, "Art");
		cv.put(CITATION, "Art is science made clear.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alicia Machado");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is only temporary, but your mind lasts you a lifetime.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Camille Paglia");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is our weapon against nature; by it we make objects, giving them limit, symmetry, proportion. Beauty halts and freezes the melting flux of nature.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Ray");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is power; a smile is its sword.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fay Weldon");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is the first present nature gives to women and the first it takes away.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edmund Burke");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is the promise of happiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Baudelaire");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is the sole ambition, the exclusive goal of Taste.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Camus");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is unbearable, drives us to despair, offering us for a minute the glimpse of an eternity that we should like to stretch out over the whole of time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas Horton");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is variable, ugliness is constant.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edna St. Vincent Millay");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is whatever gives joy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aldous Huxley");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty is worse than wine, it intoxicates both the holder and beholder.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty itself is but the sensible image of the Infinite.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Matthew Fox");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty saves. Beauty heals. Beauty motivates. Beauty unites. Beauty returns us to our origins, and here lies the ultimate act of saving, of healing, of overcoming dualism.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Beauty, n: the power by which a woman charms a lover and terrifies a husband.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eva Herzigova");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Because beauty isn't enough, there must be something more.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Dear God! how beauty varies in nature and art. In a woman the flesh must be like marble; in a statue the marble must be like flesh.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Baudelaire");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Even in the centuries which appear to us to be the most monstrous and foolish, the immortal appetite for beauty has always found satisfaction.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Cecil");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Every year of my life I grow more convinced that it is wisest and best to fix our attention on the beautiful and the good, and dwell as little as possible on the evil and the false.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Confucius");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Everything has beauty, but not everyone sees it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "Flowers... are a proud assertion that a ray of beauty outvalues all the utilities of the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gotthold Ephraim Lessing");
		cv.put(TOPIC, "Beauty");
		cv.put(CITATION, "For me the greatest beauty always lies in the greatest clarity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Olivia Newton-John");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "Family, nature and health all go together.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paula Abdul");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "For all of those willing to help me start a family, I am flattered. I will let you know when I need your help.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jessica Lange");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "For me, nothing has ever taken precedence over being a mother and having a family and a home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Knox");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "God did not intend the human family to be wafted to heaven on flowery beds of ease.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Dudley Warner");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "Happy is said to be the family which can eat onions together. They are, for the time being, separate, from the world, and have a harmony of aspiration.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Chanakya");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "He who is overly attached to his family members experiences fear and sorrow, for the root of all grief is attachment. Thus one should discard attachment to be happy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Janet Jackson");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I am the baby in the family, and I always will be. I am actually very happy to have that position. But I still get teased. I don't mind that.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Hardy");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I am the family face; flesh perishes, I live on.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jessica Lange");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I am tortured when I am away from my family, from my children. I am horribly guilt-ridden.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jet Li");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I believe the world is one big family, and we need to help each other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ednita Nazario");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I come from that society and there is a common thread, specifically family values - the idea that you do anything for your family, and the unconditional love for one's children.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexa Vega");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I consider my mom and all my sisters my friends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Penelope Cruz");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I don't know if I believe in marriage. I believe in family, love and children.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Furlong");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I don't think anyone has a normal family.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Amos Oz");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I find the family the most mysterious and fascinating institution in the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Heidi Klum");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I grew up in a big family with a lot of kids around, and I definitely want to have children as well.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Stiglitz");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I grew up in a family in which political issues were often discussed, and debated intensely.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Akhmad Kadyrov");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I grew up in a very religious family. I could read the Qu'ran easily at the age of five.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Patrick Warburton");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I have a family to support. And I'm not always going to be doing exactly what I want to do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Shirley Manson");
		cv.put(TOPIC, "Family");
		cv.put(CITATION, "I have a lot of very close girlfriends and sisters - I'm from an all female family. My father often quips that even the cat was neutered!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "I must have a prodigious quantity of mind; it takes me as much as a week sometimes to make it up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woodrow Wilson");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "I not only use all the brains that I have, but all that I can borrow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dolly Parton");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "I'm not offended by all the dumb blonde jokes because I know I'm not dumb... and I also know that I'm not blonde.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Franklin D. Roosevelt");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "I'm not the smartest fellow in the world, but I can sure pick smart colleagues.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Scott Adams");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "If there are no stupid questions, then what kind of questions do stupid people ask? Do they get smart just in time to ask questions?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Irene Peter");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Ignorance is no excuse, it's the real thing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Anderson");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Intellectuals are too sentimental for me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Carlucci");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Intelligence is not a science.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Susan Sontag");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Intelligence is really a kind of taste: taste in ideas.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Intelligence is the wife, imagination is the mistress, memory is the servant.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Salvador Dali");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Intelligence without ambition is a bird without wings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bryant H. McGill");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "It is better to have a fair intellect that is well used than a powerful one that is idle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "G. H. Hardy");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "It is not worth an intelligent man's time to be in the majority. By definition, there are already enough people to do that.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "It is the mark of a truly intelligent person to be moved by statistics.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lewis Carroll");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "It's a poor sort of memory that only works backwards.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "It's not that I'm so smart, it's just that I stay with problems longer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Mad, adj. Affected with a high degree of intellectual independence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henri Frederic Amiel");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Man becomes man only by his intelligence, but he is man only by his heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Diogenes");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Man is the most intelligent of the animals - and the most silly.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Jung");
		cv.put(TOPIC, "Intelligence");
		cv.put(CITATION, "Often the hands will solve a mystery that the intellect has struggled with in vain.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rupert Brooke");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Breathless, we flung us on a windy hill, Laughed in the sun, and kissed the lovely grass.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Merton");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "By reading the scriptures I am so renewed that all nature seems renewed around me and with me. The sky seems to be a pure, a cooler blue, the trees a deeper green. The whole world is charged with the glory of God and I feel fire and music under my feet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Wordsworth");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Come forth into the light of things, let nature be your teacher.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodore Roethke");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Deep in their roots, all flowers keep the light.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kin Hubbard");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Don't knock the weather; nine-tenths of the people couldn't start a conversation if it didn't change once in a while.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Satchel Paige");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Don't pray when it rains if you don't pray when the sun shines.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Conrad");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Each blade of grass has its spot on earth whence it draws its life, its strength; and so is man rooted to the land from which he draws his faith together with his life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Lubbock");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Earth and sky, woods and fields, lakes and rivers, the mountain and the sea, are excellent schoolmasters, and teach some of us more than we can ever learn from books.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Earth laughs in flowers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gerard De Nerval");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Every flower is a soul blossoming in nature.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rainer Maria Rilke");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Everything is blooming most recklessly; if it were voices instead of colors, there would be an unbelievable shrieking into the heart of the night.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Except during the nine months before he draws his first breath, no man manages his affairs as well as a tree does.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Letterman");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Fall is my favorite season in Los Angeles, watching the birds change color and fall from the trees.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Fishes live in the sea, as men do a-land; the great ones eat up the little ones.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Ward Beecher");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Flowers are the sweetest things God ever made and forgot to put a soul into.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Antonio Porchia");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Flowers are without hope. Because hope is tomorrow and flowers have no tomorrow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pam Brown");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "For every person who has ever lived there has come, at last, a spring he will never see. Glory then in the springs that are yours.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Luther");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "For in the true nature of things, if we rightly consider, every green tree is far more glorious than if it were made of gold and silver.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Abbey");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "For myself I hold no preferences among flowers, so long as they are wild, free, spontaneous. Bricks to all greenhouses! Black thumb and cutworm to the potted plant!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orison Swett Marden");
		cv.put(TOPIC, "Nature");
		cv.put(CITATION, "Forests, lakes, and rivers, clouds and winds, stars and flowers, stupendous glaciers and crystal snowflakes - every form of animate or inanimate existence, leaves its impress upon the soul of man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Reese Witherspoon");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I feel very blessed to have two wonderful, healthy children who keep me completely grounded, sane and throw up on my shoes just before I go to an awards show just so I know to keep it real.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Roseanne Barr");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I figure that if the children are alive when I get home, I've done my job.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norma Shearer");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I get whatever placidity I have from my father. But my mother taught me how to take it on the chin.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Susie Bright");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I had no idea that mothering my own child would be so healing to my own sadness from my childhood.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tina Turner");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I regret not having had more time with my kids when they were growing up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Orben");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I take my children everywhere, but they always find their way back home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brooke Shields");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I think I'm going to have to live vicariously through my daughter's rebellion because I certainly never did go through adolescence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rita Rudner");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I want to have children, but my friends scare me. One of my friends told me she was in labor for 36 hours. I don't even want to do anything that feels good for 36 hours.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jill Clayburgh");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I wasn't very good about juggling family and my career. I was interested in who was coming to the children's birthday party, what my son was writing. I was thinking about Legos.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "June Allyson");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I'd just as soon stay home and raise babies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Liza Minelli");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "I've said it before, but it's absolutely true: My mother gave me my drive, but my father gave me my dreams. Thanks to him, I could see a future.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bruce Jenner");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "If you're asking your kids to exercise, then you better do it, too. Practice what you preach.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abigail Van Buren");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "It is a sad commentary of our times when our young must seek advice and counsel from \"Dear Abby\" instead of going to Mom and Dad.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anne Sullivan");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "It's a great mistake, I think, to put children off with falsehoods and nonsense, when their growing powers of observation and discrimination excite in them a desire to know about things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cindy Crawford");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "It's a huge change for your body. You don't even want to look in the mirror after you've had a baby, because your stomach is just hanging there like a Shar-Pei.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Patricia Heaton");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "It's hard enough to work and raise a family when your kids are all healthy and relatively normal, but when you add on some kind of disability or disease, it can just be such a burden.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plato");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Let parents bequeath to their children not riches, but the spirit of reverence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Frost");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Love is staying up all night with a sick child - or a healthy adult.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Coretta Scott King");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Mama and Daddy King represent the best in manhood and womanhood, the best in a marriage, the kind of people we are trying to become.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lynn Johnston");
		cv.put(TOPIC, "Parenting");
		cv.put(CITATION, "Mom and Dad would stay in bed on Sunday morning, but the kids would have to go to church.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George S. Patton");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "If everyone is thinking alike, then somebody isn't thinking.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Orwell");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "If you want a vision of the future, imagine a boot stamping on a human face - forever.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Simone Weil");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Imagination and fiction make up more than three quarters of our real life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. Somerset Maugham");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Imagination grows by exercise, and contrary to common belief, is more powerful in the mature than in the young.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "L. Frank Baum");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Imagination has brought mankind through the dark ages to its present state of civilization. Imagination led Columbus to discover America. Imagination led Franklin to discover electricity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Miller");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Imagination is the voice of daring. If there is anything Godlike about God it is that. He dared to imagine everything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Imagination rules the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Sagan");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Imagination will often carry us to worlds that never were. But without it we go nowhere.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Epictetus");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Imagine for yourself a character, a model personality, whose example you determine to follow, in private as well as in public.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. L. Mencken");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "It is impossible to imagine the universe run by a wise, just and omnipotent God, but it is quite easy to imagine it run by a board of gods.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Gauguin");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "It is the eye of ignorance that assigns a fixed and unchangeable color to every object; beware of this stumbling block.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Covey");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Live out of your imagination, not your history.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francois de La Rochefoucauld");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "One is never fortunate or as unfortunate as one imagines.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Our truest life is when we are in dreams awake.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Butler Yeats");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "People who lean on logic and philosophy and rational exposition end by starving the best part of the mind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Leacock");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Personally, I would sooner have written Alice in Wonderland than the whole Encyclopedia Britannica.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elie Wiesel");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "Some stories are true that never happened.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wallace Stevens");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "The imagination is man's power over nature.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "The lunatic, the lover, and the poet, are of imagination all compact.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Muhammad Ali");
		cv.put(TOPIC, "Imagination");
		cv.put(CITATION, "The man who has no imagination has no wings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dale Earnhardt");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Growing up, I've enjoyed hunting with my father.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Laurence J. Peter");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Heredity is what sets the parents of a teenager wondering about each other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Diane Cilento");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I got through my teen years by being a bit of a clown.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Constance Baker Motley");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I grew up in a house where nobody had to tell me to go to school every day and do my homework.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Cormier");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I have always had a sense that we are all pretty much alone in life, particularly in adolescence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Winona Ryder");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I have this sense that I didn't really start growing up until my twenties.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rob Lowe");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I liked being a teenager, but I would not go back for all the tea in China.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Brittany Snow");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I mean, I'm pretty good in real life, but sometimes people seem surprised that I'm like a normal teenager and wear black nail polish and I'm just a little bit more edgy than the person I play on television.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Uma Thurman");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I spent the first fourteen years of my life convinced that my looks were hideous. Adolescence is painful for everyone, I know, but mine was plain weird.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Hoffman");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I think growing up is difficult and it's a process that I'm always interested in, with kids and adults, they are often on two different universes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Matthew Vaughn");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I think there's a time in your life where you don't feel like you fit in. I think everyone has that when you're a teenager, especially, and especially in the society we live in.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miranda Otto");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I think you go through a period as a teenager of being quite cool and unaffected by things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ethel Waters");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I wanted to be with the kind of people I'd grown up with, but you can't go back to them and be one of them again, no matter how hard you try.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Olivia Wilde");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I was a handful growing up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nigella Lawson");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I was a quiet teenager, introverted, full of angst.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adriana Lima");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "I'm a teenager, but I'm independent - I have my own apartment, I have my own life. And I think I have learned more than any of those teenagers have in school. I learned to be responsible, leaving my family and coming here alone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jamie Lee Curtis");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "If you just watch a teenager, you see a lot of uncertainty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "e. e. cummings");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "It takes courage to grow up and become who you really are.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jules Feiffer");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "Maturity is only a short break in adolescence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Foreman");
		cv.put(TOPIC, "Teen");
		cv.put(CITATION, "My kids idea of a hard life is to live in a house with only one phone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Herbert Simon");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Everyone designs who devises courses of action aimed at changing existing situations into preferred ones.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Gardiner");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Good buildings come from good people, ad all problems are solved by good design.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harry Seidler");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Good design doesn't date.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas J. Watson");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Good design is good business.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Randolph Adams");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Great designers seldom make great advertising men, because they get overcome by the beauty of the picture - and forget that merchandise must be sold.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Giorgio Armani");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "I design for real people. I think of our customers all the time. There is no virtue whatsoever in creating clothing or accessories that are not practical.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Diane von Furstenberg");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "I design for the woman who loves being a woman.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Lauren");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "I don't design clothes, I design dreams.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mike Davidson");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "I don't start with a design objective, I start with a communication objective. I feel my project is successful if it communicates what it is supposed to communicate.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gianni Versace");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "I like the body. I like to design everything to do with the body.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marc Newson");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "I think it's really important to design things with a kind of personality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel Libeskind");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "I think there is a new awareness in this 21st century that design is as important to where and how we live as it is for museums, concert halls and civic buildings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anna Quindlen");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "I would be most content if my children grew up to be the kind of people who think decorating consists mostly of building enough bookshelves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robin Day");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "I would think twice about designing stuff for which there was no need and which didn't endure.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Donald Norman");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "In my opinion, no single design is apt to be optimal for everyone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henri Matisse");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "It is only after years of preparation that the young artist should touch color - not color used descriptively, that is, but as a means of personal expression.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Branch Rickey");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Luck is the residue of design.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Johnson");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Many things difficult to design prove easy to performance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David McFadden");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Outside of the chair, the teapot is the most ubiquitous and important design element in the domestic environment and almost everyone who has tackled the world of design has ended up designing one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Terence");
		cv.put(TOPIC, "Design");
		cv.put(CITATION, "Perhaps believing in good design is like believing in God, it makes you an optimist.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Julie Walters");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "I don't know if you can change things, but it's a drop in the ocean.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Jung");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "If there is anything that we wish to change in the child, we should first examine it and see whether it is not something that could better be changed in ourselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frederick Douglass");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "If there is no struggle, there is no progress.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gail Sheehy");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "If we don't change, we don't grow. If we don't grow, we aren't really living.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ian Botham");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "If you can change three lives in 10, three lives in a hundred, that's got to be good, hasn't it?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maya Angelou");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "If you don't like something, change it. If you can't change it, change your attitude.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mary Douglas");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "If you want to change the culture, you will have to start by changing the organization.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John A. Simone, Sr.");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "If you're in a bad situation, don't worry it'll change. If you're in a good situation, don't worry it'll change.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "In a progressive country change is constant; change is inevitable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "C. S. Lewis");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "It may be hard for an egg to turn into a bird: it would be a jolly sight harder for it to learn to fly while remaining an egg. We are like eggs at present. And you cannot go on indefinitely being just an ordinary, decent egg. We must be hatched or go bad.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mignon McLaughlin");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "It's the most unhappy people who most fear change.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Irene Peter");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Just because everything is different doesn't mean anything has changed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hugh Prather");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Just when I think I have learned the way to live, life changes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johann Wolfgang von Goethe");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Life belongs to the living, and he who lives must be prepared for changes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Marshall");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Lord, where we are wrong, make us willing to change; where we are right, make us easy to live with.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Margaret Mead");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Never believe that a few caring people can't change the world. For, indeed, that's all who ever have.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Copeland");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Not only is women's work never done, the definition keeps changing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Confucius");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Only the wisest and stupidest of men never change.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Denis Waitley");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Relentless, repetitive self talk is what changes our self-image.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Keri Russell");
		cv.put(TOPIC, "Change");
		cv.put(CITATION, "Sometimes its the smallest decisions that can change your life forever.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Simon Raven");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Art for art's sake, money for God's sake.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan King");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Banks have a new image. Now you have 'a friend,' your friendly banker. If the banks are so friendly, how come they chain down the pens?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Earl Warren");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Ben Franklin may have discovered electricity- but it is the man who invented the meter who made the money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Delphine de Girardin");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Business is other people's money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred Marshall");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Civilized countries generally adopt gold or silver or both as money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marsha Sinetar");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Do what you love and the money will follow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samantha Bee");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Don't judge me. I made a lot of money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Burns");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Don't stay in bed, unless you can make money in bed.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gregg Easterbrook");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Everyone needs a certain amount of money. Beyond that, we pursue money because we know how to obtain it. We don't necessarily know how to obtain happiness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wilhelm Steinitz");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Fame, I have already. Now I need the money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "For I can raise no money by vile means.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Butler");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Friendship is like money, easier made than kept.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "He that is of the opinion money will do everything may well be suspected of doing everything for money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Twain");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "Honesty is the best policy - when there is money in it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Harvey");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I am fiercely loyal to those willing to put their money where my mouth is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kid Rock");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I am happy to make money. I want to make more money, make more music, eat Big Macs and drink Budweisers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marlon Brando");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I don't mind that I'm fat. You still get the same money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lauren Hutton");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I don't spend much money on clothes; I never did.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Neil Cavuto");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I don't think business news is just for old white men with money.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Miller");
		cv.put(TOPIC, "Money");
		cv.put(CITATION, "I have no money, no resources, no hopes. I am the happiest man alive.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Willard Gaylin");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Expressing anger is a form of public littering.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ovid");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Fair peace becomes men; ferocious anger belongs to beasts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "For every minute you remain angry, you give up sixty seconds of peace of mind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Colin Powell");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Get mad, then get over it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur Schopenhauer");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Hatred is an affair of the heart; contempt that of the head.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Ruskin");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "He that would be angry and sin not, must not be angry with anything but sin.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elizabeth Kenny");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "He who angers you conquers you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Congreve");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Heaven has no rage like love to hatred turned, nor hell a fury like a woman scorned.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Buddha");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Holding on to anger is like grasping a hot coal with the intent of throwing it at someone else; you are the one who gets burned.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Aurelius");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "How much more grievous are the consequences of anger than the causes of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Herbert");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "How often it is that the angry man rages denial of what his inner self is telling him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Moore Colby");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "I know of no more disagreeable situation than to be left feeling generally angry without anybody in particular to be angry at.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sydney J. Harris");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "If a small thing has the power to make you angry, does that not indicate something about your size?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wayne Dyer");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "It is impossible for you to be angry and laugh at the same time. Anger and laughter are mutually exclusive and you have the power to choose either.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel Webster");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Keep cool; anger is not an argument.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Thurber");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Let us not look back in anger, nor forward in fear, but around in awareness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Man should forget his anger before he lies down to sleep.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baltasar Gracian");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Never contend with a man who has nothing to lose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baltasar Gracian");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Never do anything when you are in a temper, for you will do everything wrong.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Congreve");
		cv.put(TOPIC, "Anger");
		cv.put(CITATION, "Never go to bed angry, stay up and fight.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jane Sherwood Ace");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Home wasn't built in a day.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Earl Wilson");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Home, nowadays, is a place where part of the family waits till the rest of the family brings the car back.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Cheever");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Homesickness is nothing. Fifty percent of the people in the world are homesick all the time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Evan Esar");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Housework is what a woman does that nobody notices unless she hasn't done it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Cosby");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Human beings are the only creatures on earth that allow their children to come back home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sarah Brightman");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "I am not quite sure where home is right now. I do have places in London and Milan, and a house in Spain. I guess I would say home is where my mother is, and she lives in Spain.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elie Wiesel");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "I do not recall a Jewish home without a book on the table.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sam Shepard");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "I feel like I've never had a home, you know? I feel related to the country, to this country, and yet I don't know exactly where I fit in... There's always this kind of nostalgia for a place, a place where you can reckon with yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Langella");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "I grew up in a household where everybody lived at the top of his lungs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pope John Paul II");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "I kiss the soil as if I placed a kiss on the hands of a mother, for the homeland is our earthly mother. I consider it my duty to be with my compatriots in this sublime and difficult moment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Yo-Yo Ma");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "I love grocery shopping when I'm home. That's what makes me feel totally normal. I love both the idea of home as in being with my family and friends, and also the idea of exploration. I think those two are probably my great interests.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jeff Foxworthy");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "I say, If everybody in this house lives where it's God first, friends and family second and you third, we won't ever have an argument.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Suzy Bogguss");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "I'm a real Suzy Homemaker.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ryan Stiles");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "I'm going to buy some green bananas because by the time I get home they'll be ripe.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michelle Branch");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "I'm lucky because I have a job I love. I really miss being away from home, being in my own bed, seeing my animals and siblings, having my moms cookies. I have a couple cats. I got a kitten about a year ago and now Im going on the road so I wont see him for a while. I feel bad.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Warhol");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "I'm the type who'd be happy not going anywhere as long as I was sure I knew exactly what was happening at the places I wasn't going to. I'm the type who'd like to sit home and watch every party that I'm invited to on a monitor in my bedroom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gaston Bachelard");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "If I were asked to name the chief benefit of the house, I should say: the house shelters day-dreaming, the house protects the dreamer, the house allows one to dream in peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Imran Khan");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "If your house is burning, wouldn't you try and put out the fire?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lyndon B. Johnson");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "In our home there was always prayer - aloud, proud and unapologetic.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kin Hubbard");
		cv.put(TOPIC, "Home");
		cv.put(CITATION, "Kindness goes a long ways lots of times when it ought to stay at home.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Washington Carver");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is the key to unlock the golden door of freedom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Durant");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is the transmission of civilization.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "B. F. Skinner");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education is what survives when what has been learned has been forgotten.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education, n.: That which discloses to the wise and disguises from the foolish their lack of understanding.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "G. M. Trevelyan");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education... has produced a vast population able to read but unable to distinguish what is worth reading.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Malcolm Forbes");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Education's purpose is to replace an empty mind with an open one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Martin Bormann");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Every educated person is a future enemy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gail Godwin");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "Good teaching is one-fourth preparation and three-fourths pure theatre.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "He who opens a school door, closes a prison.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leon Spinks");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "I know a lot of people think I'm dumb. Well, at least I ain't no educated fool.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Green");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "I learned more stuff in church than I did in the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michel de Montaigne");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "I prefer the company of peasants because they have not been educated sufficiently to reason incorrectly.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tallulah Bankhead");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "I read Shakespeare and the Bible, and I can shoot dice. That's what I call a liberal education.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al McGuire");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "I think everyone should go to college and get a degree and then spend six months as a bartender and six months as a cabdriver. Then they would really be educated.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Annie Dillard");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "I would like to learn, or remember, how to live.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walt Disney");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "I would rather entertain and hope that people learned something than educate people and hope they were entertained.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aeschylus");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "I'm not afraid of storms, for I'm learning to sail my ship.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abu Bakr");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "If an ignorant person is attracted by the things of the world, that is bad. But if a learned person is thus attracted, it is worse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cornelius Vanderbilt");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "If I had learned education I would not have had time to learn anything else.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Heinrich Heine");
		cv.put(TOPIC, "Education");
		cv.put(CITATION, "If the Romans had been obliged to learn Latin, they would never have found time to conquer the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dan Millman");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "I have an almost religious zeal... not for technology per se, but for the Internet which is for me, the nervous system of mother Earth, which I see as a living creature, linking up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "R. Buckminster Fuller");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "I just invent, then wait until man comes around to needing what I've invented.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jaron Lanier");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "I think complexity is mostly sort of crummy stuff that is there because it's too expensive to change the interface.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Perlis");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "I think it is inevitable that people program poorly. Training will not substantially help matters. We have to learn to live with it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael K. Powell");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "I think people have a vague sense that the television system is changing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bruce Sterling");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "I used to think that cyberspace was fifty years away. What I thought was fifty years away, was only ten years away. And what I thought was ten years away... it was already here. I just wasn't aware of it yet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Lloyd Wright");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "If it keeps up, man will atrophy all his limbs but the push-button finger.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Omar N. Bradley");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "If we continue to develop our technology without wisdom or prudence, our servant may prove to be our executioner.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alan Perlis");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "In software systems it is often the early bird that makes the worm.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Inventor: A person who makes an ingenious arrangement of wheels, levers and springs, and believes it civilization.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "It has become appallingly obvious that our technology has exceeded our humanity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Clive James");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "It is only when they go wrong that machines remind you how powerful they are.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Stuart Mill");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "It is questionable if all the mechanical inventions yet made have lightened the day's toil of any human being.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Esther Dyson");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "It may not always be profitable at first for businesses to be online, but it is certainly going to be unprofitable not to be online.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Gibson");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "It's impossible to move, to live, to operate at any level without leaving traces, bits, seemingly meaningless fragments of personal information.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Grove");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Just as we could have rode into the sunset, along came the Internet, and it tripled the significance of the PC.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Grove");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Leaders have to act more quickly today. The pressure comes much faster.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Andy Rooney");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Making duplicate copies and computer printouts of things no one wanted even one of in the first place is giving America a new sense of purpose.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Men have become the tools of their tools.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orrin Hatch");
		cv.put(TOPIC, "Technology");
		cv.put(CITATION, "Microsoft is engaging in unlawful predatory practices that go well beyond the scope of fair competition.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fyodor Dostoevsky");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness does not lie in happiness, but in the achievement of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dale Carnegie");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness doesn't depend on any external conditions, it is governed by our mental attitude.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas William Jerrold");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness grows at our own firesides, and is not to be picked in strangers' gardens.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ernest Hemingway");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness in intelligent people is the rarest thing I know.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold Bennett");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness includes chiefly the idea of satisfaction after full honest effort. No one can possibly be satisfied and no one can be happy who feels that in some paramount affairs he failed to take up the challenge of life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sam Levenson");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is a by-product. You cannot pursue it by itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Deepak Chopra");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is a continuation of happenings which are not resisted.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sydney J. Harris");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is a direction, not a place.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baruch Spinoza");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is a virtue, not its reward.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Euripides");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is brief. It will not stay. God batters at its sails.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alphonse Karr");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is composed of misfortunes avoided.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Hill");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is found in doing, not merely possessing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank McCourt");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is hard to recall. Its just a glow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ogden Nash");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is having a scratch for every itch.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marquis de Sade");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is ideal, it is the work of the imagination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Wood Krutch");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is itself a kind of gratitude.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sarah McLachlan");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is like a cloud, if you stare at it long enough, it evaporates.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bernard Meltzer");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is like a kiss. You must share it to enjoy it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexandre Dumas");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is like those palaces in fairy tales whose gates are guarded by dragons: we must fight in order to conquer it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Spock");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is mostly a by-product of doing what makes us feel fulfilled.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fyodor Dostoevsky");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness does not lie in happiness, but in the achievement of it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dale Carnegie");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness doesn't depend on any external conditions, it is governed by our mental attitude.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Douglas William Jerrold");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness grows at our own firesides, and is not to be picked in strangers' gardens.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ernest Hemingway");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness in intelligent people is the rarest thing I know.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arnold Bennett");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness includes chiefly the idea of satisfaction after full honest effort. No one can possibly be satisfied and no one can be happy who feels that in some paramount affairs he failed to take up the challenge of life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sam Levenson");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is a by-product. You cannot pursue it by itself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Deepak Chopra");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is a continuation of happenings which are not resisted.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sydney J. Harris");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is a direction, not a place.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baruch Spinoza");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is a virtue, not its reward.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Euripides");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is brief. It will not stay. God batters at its sails.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alphonse Karr");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is composed of misfortunes avoided.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Hill");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is found in doing, not merely possessing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank McCourt");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is hard to recall. Its just a glow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ogden Nash");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is having a scratch for every itch.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marquis de Sade");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is ideal, it is the work of the imagination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joseph Wood Krutch");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is itself a kind of gratitude.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sarah McLachlan");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is like a cloud, if you stare at it long enough, it evaporates.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bernard Meltzer");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is like a kiss. You must share it to enjoy it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexandre Dumas");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is like those palaces in fairy tales whose gates are guarded by dragons: we must fight in order to conquer it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Spock");
		cv.put(TOPIC, "Happiness");
		cv.put(CITATION, "Happiness is mostly a by-product of doing what makes us feel fulfilled.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erma Bombeck");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Guilt: the gift that keeps on giving.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Burns");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Happiness is having a large, loving, caring, close-knit family in another city.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marilyn vos Savant");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Have enough sense to know, ahead of time, when your skills will not extend to wallpapering.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zsa Zsa Gabor");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "He taught me housekeeping; when I divorce I keep the house.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phyllis Diller");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "Housework can't kill you, but why take a chance?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emo Philips");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "How many people here have telekenetic powers? Raise my hand.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lily Tomlin");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I always wanted to be somebody, but now I realize I should have been more specific.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woody Allen");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I am not afraid of death, I just don't want to be there when it happens.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen King");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I am the literary equivalent of a Big Mac and Fries.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Steven Wright");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I bought some batteries, but they weren't included.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Warren Buffett");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I buy expensive suits. They just look cheap on me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Letterman");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I cannot sing, dance or act; what else would I be but a talk show host.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. C. Fields");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I cook with wine, sometimes I even add it to the food.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Imelda Marcos");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I did not have three thousand pairs of shoes, I had one thousand and sixty.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joe E. Lewis");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I distrust camels, and anyone else who can go a week without a drink.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paula Poundstone");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I don't have a bank account because I don't know my mother's maiden name.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fred Allen");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I don't have to look up my family tree, because I know that I'm the sap.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Stephen Fry");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I don't need you to remind me of my age. I have a bladder to do that for me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Goldwyn");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I don't think anyone should write their autobiography until after they're dead.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mitch Hedberg");
		cv.put(TOPIC, "Funny");
		cv.put(CITATION, "I drank some boiling water because I wanted to whistle.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Sandburg");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time is the coin of your life. It is the only coin you have, and only you can determine how it will be spent. Be careful lest you let other people spend it for you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Giordano Bruno");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time is the father of truth, its mother is our mind.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tennessee Williams");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time is the longest distance between two places.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Delmore Schwartz");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time is the school in which we learn, time is the fire in which we burn.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pericles");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time is the wisest counselor of all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Penn");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time is what we want most, but what we use worst.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Daniel J. Boorstin");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time makes heroes but dissolves celebrities.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Gibson");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time moves in one direction, memory in another.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Huxley");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Time, whose tooth gnaws away everything else, is powerless against truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "M. Scott Peck");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Until you value yourself, you won't value your time. Until you value your time, you will not do anything with it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael LeBoeuf");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Waste your money and you're only out of money, but waste your time and you've lost a part of your life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "We are time's subjects, and time bids be gone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nelson Mandela");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "We must use time wisely and forever realize that the time is always ripe to do right.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Helprin");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Well-timed silence is the most commanding expression.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Saint Augustine");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "What then is time? If no one asks me, I know what it is. If I wish to explain it to him who asks, I do not know.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Zimmerman");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "When in doubt, take more time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas J. Watson");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Wisdom is the power to put our time and our knowledge to the proper use.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Hochschild");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "Work is hard. Distractions are plentiful. And time is short.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edmund Burke");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "You can never plan the future by the past.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "Time");
		cv.put(CITATION, "You may delay, but time will not.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Scott Caan");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "If you don't have any fight in you, you might as well be dead.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Epicurus");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "It is possible to provide security against other ills, but as far as death is concerned, we men live in a city without walls.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anna Akhmatova");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "It was a time when only the dead smiled, happy in their peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maureen O'Hara");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "John Candy knew he was going to die. He told me on his 40th birthday. He said, well, Maureen, I'm on borrowed time.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Quintus Ennius");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Let no one weep for me, or celebrate my funeral with mourning; for I still live, as I pass to and fro through the mouths of men.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Walker");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Life is better than death, I believe, if only because it is less boring, and because it has fresh peaches in it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Gerrold");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Life is hard. Then you die. Then they throw dirt in your face. Then the worms eat you. Be grateful it happens in that order.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "B. R. Hayden");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Love and death are the two great hinges on which all human sympathies turn.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Erich Fromm");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Man always dies before he is fully born.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Miguel de Unamuno");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Man dies of cold, not of darkness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Men fear death as children fear to go in the dark; and as that natural fear in children is increased by tales, so is the other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Susan Ertz");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Millions long for immortality who don't know what to do with themselves on a rainy Sunday afternoon.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Euripides");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "No one can confidently say that he will still be living tomorrow.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ernst Moritz Arndt");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Nothing that is really good and God-like dies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aeschylus");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Of all the gods only death does not desire gifts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Eliot");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Our dead are never dead to us, until we have forgotten them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Horace");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Pale Death beats equally at the poor man's gate and at the palaces of kings.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jean Cocteau");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Since the day of my birth, my death began its walk. It is walking toward me, without hurrying.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward W. Howe");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Some men are alive simply because it is against the law to kill them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry Van Dyke");
		cv.put(TOPIC, "Death");
		cv.put(CITATION, "Some people are so afraid do die that they never begin to live.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Albee");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "I have a fine sense of the ridiculous, but no sense of humor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Howard Clark");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "I think the next best thing to solving a problem is finding some humor in it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "If I had no sense of humor, I would long ago have committed suicide.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jennifer Jones");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "If you could choose one characteristic that would get you through life, choose a sense of humor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Imagination was given to man to compensate him for what he is not; a sense of humor to console him for what he is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Herbert");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "In conversation, humor is worth more than wit and easiness more than knowledge.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Langston Hughes");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Like a welcome summer rain, humor may suddenly cleanse and cool the earth, the air and you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emo Philips");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "My computer beat me at checkers, but I sure beat it at kickboxing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas W. Higginson");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Nothing is so galling to a people not broken in from the birth as a paternal, or in other words a meddling government, a government which tells them what to read and say and eat and drink and wear.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Larry Gelbart");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "One doesn't have a sense of humor. It has you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Guillermo Cabrera Infante");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Puns are a form of humor with words.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "W. C. Fields");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "Start every day off with a smile and get it over with.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jacob August Riis");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "The more I live, the more I think that humor is the saving sense.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter De Vries");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "The satirist shoots to kill while the humorist brings his prey back alive and eventually releases him again for another chance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "The secret to humor is surprise.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Cosby");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "There is hope for the future because God has a sense of humor and we are funny to God.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas W. Higginson");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "There is no defense against adverse fortune which is so effectual as an habitual sense of humor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Benchley");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "There seems to be no lengths to which humorless people will not go to analyze humor. It seems to worry them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lin Yutang");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "This I conceive to be the chemical function of humor: to change the character of our thought.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bern Williams");
		cv.put(TOPIC, "Humor");
		cv.put(CITATION, "What a strange world this would be if we all had the same sense of humor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Epictetus");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Only the educated are free.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kyle Chandler");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Opportunity does not knock, it presents itself when you beat down the door.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Walter Elliot");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Perseverance is not a long race; it is many short races one after the other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jimmy Dean");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Poverty was the greatest motivating factor in my life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert H. Schuller");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Problems are not stop signs, they are guidelines.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aristotle");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Quality is not an act, it is a habit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bo Jackson");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Set your goals high, and don't stop till you get there.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eileen Caddy");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Set your sights high, the higher the better. Expect the most wonderful things to happen, not in the future but right now. Realize that nothing is too good. Allow absolutely nothing to hamper you or hold you up in any way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tony Robbins");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Setting goals is the first step in turning the invisible into the visible.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Marshall");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "Small deeds done are better than great deeds planned.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Golda Meir");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "The dog that trots about finds a bone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Caine");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "The first step toward success is taken when you refuse to be a captive of the environment in which you first find yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Paine");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "The harder the conflict, the more glorious the triumph.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Epictetus");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "The key is to keep company only with people who uplift you, whose presence calls forth your best.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Confucius");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "The more man meditates upon good thoughts, the better will be his world and the world at large.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Muhammed Iqbal");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "The ultimate aim of the ego is not to see something, but to be something.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sylvia Browne");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "The weeds keep multiplying in our garden, which is our mind ruled by fear. Rip them out and call them by name.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Confucius");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "The will to win, the desire to succeed, the urge to reach your full potential... these are the keys that will unlock the door to personal excellence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Baltasar Gracian");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "The wise does at once what the fool does at last.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard Rorty");
		cv.put(TOPIC, "Motivational");
		cv.put(CITATION, "There is nothing deep down inside us except what we have put there ourselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Vivien Leigh");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I've always been mad about cats.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lefty Gomez");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "I've got a new invention. It's a revolving bowl for tired goldfish.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alfred North Whitehead");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "If a dog jumps into your lap, it is because he is fond of you; but if a cat does the same thing, it is because your lap is warmer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Woodrow Wilson");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "If a dog will not come to you after having looked you in the face, you should go home and examine your conscience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Doug Coupland");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "If cats were double the size they are now, they'd probably be illegal.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fran Lebowitz");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "If you are a dog and your owner suggests that you wear a sweater suggest that he wear a tail.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Agnes Repplier");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "It is impossible for a lover of cats to banish these alert, gentle, and discriminating friends, who give us just enough of their regard and complaisance to make us hunger for more.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mickey Rivers");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "It was so cold today that I saw a dog chasing a cat, and the dog was walking.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paula Cole");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Just watching my cats can make me happy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Pam Brown");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Kittens are wide-eyed, soft and sweet. With needles in their jaws and feet.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Gallico");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Kittens can happen to anyone.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William S. Burroughs");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Like all pure creatures, cats are practical.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kaspar Hauser");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Many cats are the death of the mouse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thornton Wilder");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Many who have spent a lifetime in it can tell us less of love than the child that lost a dog yesterday.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kinky Friedman");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Money can buy you a fine dog, but only love can make him wag his tail.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Peers");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Never stand between a dog and the hydrant.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "P. J. O'Rourke");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Never wear anything that panics the cat.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Abraham Lincoln");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "No matter how much cats fight, there always seem to be plenty of kittens.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kin Hubbard");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "No one can feel as helpless as the owner of a sick goldfish.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Earl Derr Biggers");
		cv.put(TOPIC, "Pet");
		cv.put(CITATION, "Only very brave mouse makes nest in cat's ear.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Though we travel the world over to find the beautiful, we must carry it with us or we find it not.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Muir");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "To the lover of wilderness, Alaska is one of the most wonderful countries in the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aldous Huxley");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "To travel is to discover that everyone is wrong about other countries.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Theroux");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Tourists don't know where they've been, travelers don't know where they're going.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Susan Sontag");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Travel becomes a strategy for accumulating photographs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lawrence Durrell");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Travel can be one of the most rewarding forms of introspection.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Theroux");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Travel is glamorous only in retrospect.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francis Bacon");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Travel, in the younger sort, is a part of education; in the elder, a part of experience.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jan Morris");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Travel, which was once either a necessity or an adventure, has become very largely a commodity, and from all sides we are persuaded into thinking that it is a social requirement, too.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mason Cooley");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Travelers never think that they are the foreigners.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Blake");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Travelers repose and dream among my leaves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Cynthia Ozick");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Traveling is seeing; it is the implicit that we travel by.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fanny Burney");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Traveling is the ruin of all happiness! There's no looking at a building after seeing Italy.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Italo Calvino");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Traveling, you realize that differences are lost: each city takes to resembling all cities, places exchange their form, order, distances, a shapeless dust cloud invades the continents.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hans Christian Andersen");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "Travelling expands the mind rarely.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anais Nin");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "We travel, some of us forever, to seek other states, other lives, other souls.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hilaire Belloc");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "We wander for distraction, but we travel for fulfillment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Carlin");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "What does it mean to pre-board? Do you get on before you get on?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Phil Collins");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "When I go on Japanese Airlines, I really love it because I like Japanese food.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Blunkett");
		cv.put(TOPIC, "Travel");
		cv.put(CITATION, "When I'm in London I do have the convenience of being close to St James Park which is also good for me because it gives me an excuse to get out and get some much needed exercise!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace is not only better than war, but infinitely more arduous.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Schiller");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace is rarely denied to the peaceful.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Keble");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace is the first thing the angels sang.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Maria Schell");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Peace is when time doesn't matter as it passes by.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Herbert Lawrence");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "People always make war when they say they love peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michael Franti");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Power to the peaceful!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tony Snow");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "The business of peace requires more than showing up with paint brushes, foodstuffs and an oil pipeline or two.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Montgomery");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "The Dove, on silver pinions, winged her peaceful way.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Arthur Henderson");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "The forces that are driving mankind toward unity and peace are deep-seated and powerful. They are material and natural, as well as moral and intellectual.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carlos Santana");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "The most valuable possession you can own is an open heart. The most powerful weapon you can be is an instrument of peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Zinedine Zidane");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "The only important thing I have to say is that my father never fought against his country.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dag Hammarskjold");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "The pursuit of peace and progress cannot end in a few years in either victory or defeat. The pursuit of peace and progress, with its trials and its errors, its successes and its setbacks, can never be relaxed and never abandoned.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Silvia Cartwright");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "The quest for peace begins in the home, in the school and in the workplace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ralph Waldo Emerson");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "The real and lasting victories are those of peace, and not of war.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peace Pilgrim");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "The simplification of life is one of the steps to inner peace. A persistent simplification will create an inner and outer well-being that places harmony in one's life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mark Kennedy");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "This enemy of peace in the world today is unlike any we have seen in the past, and our military is learning from, and building on, previous successes while carrying peace and freedom into the future.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Hazlitt");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Those who are at war with others are not at peace with themselves.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John F. Kennedy");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "Those who make peaceful revolution impossible will make violent revolution inevitable.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peace Pilgrim");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "To attain inner peace you must actually give your life, not just your possessions. When you at last give your life - bringing into alignment your beliefs and the way you live then, and only then, can you begin to find inner peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "H. L. Mencken");
		cv.put(TOPIC, "Peace");
		cv.put(CITATION, "War will never cease until babies begin to come into the world with larger cerebrums and smaller adrenal glands.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plutarch");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Painting is silent poetry, and poetry is painting that speaks.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Novalis");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry heals the wounds inflicted by reason.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is a deal of joy and pain and wonder, with a dash of the dictionary.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Percy Bysshe Shelley");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is a mirror which makes beautiful that which is distorted.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Hazlitt");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is all that is worth remembering in life.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Sandburg");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is an echo, asking a shadow to dance.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Simic");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is an orphan of silence. The words never quite equal the experience behind them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leonard Cohen");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is just the evidence of life. If your life is burning well, poetry is just the ash.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rita Dove");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is language at its most distilled and most powerful.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plato");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is nearer to vital truth than history.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "T. S. Eliot");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is not a turning loose of emotion, but an escape from emotion; it is not the expression of personality, but an escape from personality. But, of course, only those who have personality and emotions know what it means to want to escape from these things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Engle");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is ordinary language raised to the Nth power. Poetry is boned with ideas, nerved and blooded with emotions, all held together by the delicate, tough skin of words.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dennis Gabor");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is plucking at the heartstrings, and making music with them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marianne Moore");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is the art of creating imaginary gardens with real toads.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Samuel Johnson");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is the art of uniting pleasure with truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Sandburg");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is the opening and closing of a door, leaving those who look through to guess about what is seen during the moment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Salvatore Quasimodo");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is the revelation of a feeling that the poet believes to be interior and personal which the reader recognizes as his own.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edgar Allan Poe");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is the rhythmical creation of beauty in words.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carl Sandburg");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is the synthesis of hyacinths and biscuits.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Gray");
		cv.put(TOPIC, "Poetry");
		cv.put(CITATION, "Poetry is thoughts that breathe, and words that burn.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William E. Gladstone");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Justice delayed is justice denied.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Plato");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Justice in the life and conduct of the State is possible only as first it resides in the hearts and souls of the citizens.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Honore de Balzac");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Laws are spider webs through which the big flies pass and the little ones get caught.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles W. Pickering");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Lawsuit abuse is a major contributor to the increased costs of healthcare, goods and services to consumers.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ambrose Bierce");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Lawsuit: A machine which you go into as a pig and come out of as a sausage.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don Henley");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Lawsuits should not be used to destroy a viable and independent distribution system. The solution lies in the marketplace and not the courtroom.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aaron Allston");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Lawyers are the first refuge of the incompetent.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jeremy Bentham");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Lawyers are the only persons in whom ignorance of the law is not punished.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oliver Wendell Holmes, Jr.");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Lawyers spend a great deal of their time shoveling smoke.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Lamb");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Lawyers, I suppose, were children once.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Make crime pay. Become a lawyer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Francois Rabelais");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Misery is the company of lawsuits.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert E. Lee");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "Obedience to lawful authority is the foundation of manly character.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anita Hill");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "One of the things I was taught in law school is that I'd never be able to think the same again - that being a lawyer is something that's part of who I am as an individual now.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "People are getting smarter nowadays; they are letting lawyers, instead of their conscience, be their guide.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Norman Ralph Augustine");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "People do not win people fights. Lawyers do.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Darden");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "The events of the day inspired me to become a lawyer.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Janet Reno");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "The good lawyer is the great salesman.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christopher Darden");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "The law has no compassion. And justice is administered without compassion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marcus Tullius Cicero");
		cv.put(TOPIC, "Legal");
		cv.put(CITATION, "The more laws, the less justice.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tim Duncan");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Good, better, best. Never let it rest. Until your good is better and your better is best.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frank Gifford");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Gray skies are just clouds passing over.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Yogi Berra");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Half the lies they tell about me aren't true.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al Boliska");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Have you ever noticed what golf spells backwards?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack Nicklaus");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "He has the finest, fundamentally sound golf swing I've ever seen.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nick Faldo");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "He hits it long. His shoulders are impressively quick through the ball. That's where he's getting his power from. He's young and has great elasticity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gary McCord");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "He hits the ball a long way and he knows how to win.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jack Nicklaus");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "He's going to be around a long, long time, if his body holds up. That's always a concern with a lot of players because of how much they play. A lot of guys can't handle it. But it looks like he can.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gary McCord");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "He's got an overall flair for the game. It looks to me like he really loves what he does and he can't wait to get up in the morning, go hit some balls and go play.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Nick Price");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "He's got everything. He' not a great player yet because he hasn't won any major championships, but it's a matter of time. He's an outstanding talent. I didn't realize how tall he is.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Warren Spahn");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Hitting is timing. Pitching is upsetting timing.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tiger Woods");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "Hockey is a sport for white men. Basketball is a sport for black men. Golf is a sport for white men dressed like black pimps.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Earl Warren");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "I always turn to the sports pages first, which records people's accomplishments. The front page has nothing but man's failures.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mia Hamm");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "I am building a fire, and everyday I train, I add more fuel. At just the right moment, I light the match.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Al McGuire");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "I don't know why people question the academic training of an athlete. Fifty percent of the doctors in this country graduated in the bottom half of their classes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rogers Hornsby");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "I don't want to play golf. When I hit a ball, I want someone else to go chase it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Will Rogers");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "I guess there is nothing that will get your mind off everything like golf. I have never been depressed enough to take up the game, but they say you get so sore at yourself you forget to hate your enemies.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gerald R. Ford");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "I had pro offers from the Detroit Lions and Green Bay Packers, who were pretty hard up for linemen in those days. If I had gone into professional football the name Jerry Ford might have been a household word today.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gerald R. Ford");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "I know I am getting better at golf because I am hitting fewer spectators.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joe E. Lewis");
		cv.put(TOPIC, "Sports");
		cv.put(CITATION, "I play in the low 80s. If it's any hotter than that, I won't play.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Soren Kierkegaard");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Prayer does not change God, but it changes him who prays.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Philip James Bailey");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Prayer is the spirit speaking truth to Truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas Jefferson");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Question with boldness even the existence of a God; because, if there be one, he must more approve of the homage of reason, than that of blind-folded fear.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Tournier");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Recounting of a life story, a mind thinking aloud leads one inevitably to the consideration of problems which are no longer psychological but spiritual.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edmund Burke");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Religion is essentially the art and the theory of the remaking of man. Man is not a finished creation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frederick II");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Religion is the idol of the mob; it adores everything it does not understand.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Bonaparte");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Religion is what keeps the poor from murdering the rich.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Santayana");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "The Bible is literature, not dogma.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Maher");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "The Bible looks like it started out as a game of Mad Libs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Friedrich Nietzsche");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "The Christian resolution to find the world ugly and bad has made the world ugly and bad.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Clayton");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "The emphasis on the birth of Christ tends to polarize our pluralistic society and create legal and ethnic belligerence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Blake");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "The glory of Christianity is to conquer by forgiveness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orson Pratt");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "The Godhead consists of the Father, the Son, and the Holy Spirit. The Father is a material being.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Aldous Huxley");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "The more powerful and original a mind, the more it will incline towards the religion of solitude.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anita Diament");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "The Sabbath is a weekly cathedral raised up in my dining room, in my family, in my heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert A. Heinlein");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "Theology is never any help; it is searching in a dark cellar at midnight for a black cat that isn't there. Theologians can persuade themselves of anything.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dalai Lama");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "There is no need for temples, no need for complicated philosophies. My brain and my heart are my temples; my philosophy is kindness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Louis Kronenberger");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "There seems to be a terrible misunderstanding on the part of a great many people to the effect that when you cease to believe you may cease to behave.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Gary North");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "This is God's world, not Satan's. Christians are the lawful heirs, not non-Christians.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Dalai Lama");
		cv.put(TOPIC, "Religion");
		cv.put(CITATION, "This is my simple religion. There is no need for temples; no need for complicated philosophy. Our own brain, our own heart is our temple; the philosophy is kindness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lucius Annaeus Seneca");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "The wish for healing has always been half of health.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Henry David Thoreau");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "'Tis healthy to be sick sometimes.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Buddha");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "To keep the body in good health is a duty... otherwise we shall not be able to keep our mind strong and clear.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Penn");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "True silence is the rest of the mind, and is to the spirit what sleep is to the body, nourishment and refreshment.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Michelle Obama");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "We can make a commitment to promote vegetables and fruits and whole grains on every part of every menu. We can make portion sizes smaller and emphasize quality over quantity. And we can help create a culture - imagine this - where our kids ask for healthy options instead of resisting them.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Esther Williams");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "What the public expects and what is healthy for an individual are two very different things.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Annette Funicello");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "When you are young and healthy, it never occurs to you that in a single second your whole life could change.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Walker");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "You know, all that really matters is that the people you love are happy and healthy. Everything else is just sprinkles on the sundae.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Courtney Thorne Smith");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "You know, true love really matters, friends really matter, family really matters. Being responsible and disciplined and healthy really matters.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Naomi Judd");
		cv.put(TOPIC, "Health");
		cv.put(CITATION, "Your body hears everything your mind says.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Simmons");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Never go backward. Attempt, and do it with all your might. Determination is power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert A. Heinlein");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Never underestimate the power of human stupidity.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eric Sevareid");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Next to power without honor, the most dangerous thing in the world is power without humor.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Allen Klein");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "No matter what has happened, you too have the power to enjoy yourself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara de Angelis");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "No one is in control of your happiness but you; therefore, you have the power to change anything about yourself or your life that you want to change.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alice Walker");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Nobody is as powerful as we make them out to be.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eric Hoffer");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Our sense of power is more vivid when we break a man's spirit than when we win his heart.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don DeLillo");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "People who are in power make their arrangements in secret, largely as a way of maintaining and furthering that power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Don DeLillo");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "People who are powerless make an open theater of violence.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Harris");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Personality has power to uplift, power to depress, power to curse, and power to bless.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Wellstone");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Politics is not about power.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Denis Diderot");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Power acquired by violence is only a usurpation, and lasts only as long as the force of him who commands prevails over that of those who obey.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George A. Smith");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Power and position often make a man trifle with the truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Frederick Douglass");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Power concedes nothing without a demand. It never did and it never will.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Disraeli");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Power has only one duty - to secure the social welfare of the People.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Theodore White");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Power in America today is control of the means of communication.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Honore De Balzac");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Power is action; the electoral principle is discussion. No political action is possible when discussion is permanently established.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Edward Abbey");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Power is always dangerous. Power attracts the worst and corrupts the best.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Richard J. Daley");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Power is dangerous unless you have humility.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Fyodor Dostoevsky");
		cv.put(TOPIC, "Power");
		cv.put(CITATION, "Power is given only to those who dare to lower themselves and pick it up. Only one thing matters, one thing; to be able to dare!");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Shirley MacLaine");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "It's useless to hold a person to anything he says while he's in love, drunk, or running for office.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mother Teresa");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Let us always meet each other with smile, for the smile is the beginning of love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Victor Hugo");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Life is the flower for which love is the honey.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Khalil Gibran");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Life without love is like a tree without blossoms or fruit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "David Grayson");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Looking back, I have this to regret, that too often when I loved, I did not say so.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Javan");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love can sometimes be magic. But magic can sometimes... just be an illusion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Virgil");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love conquers all.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rainer Maria Rilke");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love consists in this, that two solitudes protect and touch and greet each other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James A. Baldwin");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love does not begin and end the way we seem to think it does. Love is a battle, love is a war; love is a growing up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Johann Wolfgang von Goethe");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love does not dominate; it cultivates.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Voltaire");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love is a canvas furnished by nature and embroidered by imagination.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Barbara de Angelis");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love is a force more formidable than any other. It is invisible - it cannot be seen or measured, yet it is powerful enough to transform you in a moment, and offer you more joy than any material possession could.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Eva Gabor");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love is a game that two can play and both win.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Bernard Shaw");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love is a gross exaggeration of the difference between one person and everybody else.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Shakespeare");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love is a smoke made with the fume of sighs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Euripides");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love is all we have, the only way that each can help the other.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Leo Buscaglia");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love is always bestowed as a gift - freely, willingly and without expectation. We don't love to be loved; we love to love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Peter Ustinov");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love is an act of endless forgiveness, a tender look which becomes a habit.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love is an irresistible desire to be irresistibly desired.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Paul Valery");
		cv.put(TOPIC, "Love");
		cv.put(CITATION, "Love is being stupid together.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ben Kingsley");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "If your best friend has stolen your girlfriend, it does become life and death.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Neil LaBute");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "In a relationship you have to open yourself up.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Soren Kierkegaard");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "It seems essential, in relationships and all tasks, that we concentrate only on what is most significant and important.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "James Levine");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "It was just that we had this phenomenal honeymoon relationship that just kept on going.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Luke Wilson");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "It's always been my personal feeling that unless you are married, there is something that is not very dignified about talking about who you are dating.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Carrot Top");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "It's weird, I never wish anything bad upon anybody, except two or three old girlfriends.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Minnie Driver");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Last year my boyfriend gave me a painting - a very personal one. I really prefer personal gifts or ones made by someone for me. Except diamonds. That's the exception to the rule.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marianne Faithfull");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Maybe the most that you can expect from a relationship that goes bad is to come out of it with a few good songs.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Rita Rudner");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "My boyfriend and I broke up. He wanted to get married and I didn't want him to.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alicia Silverstone");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "My boyfriend calls me 'princess', but I think of myself more along the lines of 'monkey' and 'retard'.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jay London");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "My girlfriend bought me a down jacket, she said it fit my personality.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Scott Adams");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Nothing defines humans better than their willingness to do irrational things in the pursuit of phenomenally unlikely payoffs. This is the principle behind lotteries, dating, and religion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hugh Mackay");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Nothing is perfect. Life is messy. Relationships are complex. Outcomes are uncertain. People are irrational.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Ellis");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "People could rationally decide that prolonged relationships take up too much time and effort and that they'd much rather do other kinds of things. But most people are afraid of rejection.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Orson Welles");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Personally, I don't like a girlfriend to have a husband. If she'll fool her husband, I figure she'll fool me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Kim Cattrall");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Practically all the relationships I know are based on a foundation of lies and mutually accepted delusion.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Wayne Dyer");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Real magic in relationships means an absence of judgment of others.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Neil LaBute");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Relationships in general make people a bit nervous. It's about trust. Do I trust you enough to go there?");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Christina Aguilera");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Right now I'm pretty single... My career is my boyfriend.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Ben Affleck");
		cv.put(TOPIC, "Dating");
		cv.put(CITATION, "Rumors about me? Calista Flockhart, Pam Anderson, and Matt Damon. That's who I'm dating.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bill Cosby");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "People can be more forgiving than you can imagine. But you have to forgive yourself. Let go of what's bitter and move on.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Garrison Keillor");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Thank you, God, for this good life and forgive us if we do not love it enough.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Elbert Hubbard");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "The ineffable joy of forgiving and being forgiven forms an ecstasy that might well arouse the envy of the gods.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Marianne Williamson");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "The practice of forgiveness is our most important contribution to the healing of the world.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Oscar Wilde");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "The public is wonderfully tolerant. It forgives everything except genius.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Mahatma Gandhi");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "The weak can never forgive. Forgiveness is the attribute of the strong.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bryant H. McGill");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "There is no love without forgiveness, and there is no forgiveness without love.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Josh Billings");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "There is no revenge so complete as forgiveness.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Robert Frost");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "To be social is to be forgiving.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Pope");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "To err is human; to forgive, divine.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lewis B. Smedes");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "To forgive is to set a prisoner free and discover that the prisoner was you.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Alexander Chase");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "To understand is to forgive, even oneself.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Emo Philips");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "When I was a kid I used to pray every night for a new bicycle. Then I realised that the Lord doesn't work that way so I stole one and asked Him to forgive me.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Honore de Balzac");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "When women love us, they forgive us everything, even our crimes; when they do not love us, they give us credit for nothing, not even our virtues.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bernard Meltzer");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "When you forgive, you in no way change the past - but you sure do change the future.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Desmond Tutu");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "Without forgiveness, there's no future.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Lewis B. Smedes");
		cv.put(TOPIC, "Forgiveness");
		cv.put(CITATION, "You will know that forgiveness has begun when you recall those who hurt you and feel the power to wish them well.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George McGovern");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "The Establishment center... has led us into the stupidest and cruelest war in all history. That war is a moral and political disaster - a terrible cancer eating away at the soul of our nation.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Hiram Johnson");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "The first casualty when war comes is truth.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Westmoreland");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "The military don't start wars. Politicians start wars.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George S. Patton");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "The object of war is not to die for your country but to make the other bastard die for his.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Charles Evans Hughes");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "The power to wage war is the power to wage war successfully.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Einstein");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "The release of atomic energy has not created a new problem. It has merely made more urgent the necessity of solving an existing one.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Tecumseh Sherman");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "The scenes on this field would have cured anybody of war.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Omar N. Bradley");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "The way to win an atomic war is to make certain it never starts.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Niccolo Machiavelli");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "There is no avoiding war; it can only be postponed to the advantage of others.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Sun Tzu");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "There is no instance of a nation benefitting from prolonged warfare.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Havelock Ellis");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "There is nothing that war has ever achieved that we could not better achieve without it.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Benjamin Franklin");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "There was never a good war, or a bad peace.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Thomas A. Edison");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "There will one day spring from the brain of science a machine or force so fearful in its potentialities, so absolutely terrifying, that even man, the fighter, who will dare torture and death in order to inflict torture and death, will be appalled, and so abandon war forever.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "George Orwell");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "To walk through the ruined cities of Germany is to feel an actual doubt about the continuity of civilization.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Bertrand Russell");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "War does not determine who is right - only who is left.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Napoleon Hill");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "War grows out of the desire of the individual to gain advantage at the expense of his fellow man.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Albert Pike");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "War is a series of catastrophes which result in victory.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "John Stuart Mill");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "War is an ugly thing, but not the ugliest of things. The decayed and degraded state of moral and patriotic feeling which thinks that nothing is worth war is much worse.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "William Tecumseh Sherman");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "War is hell.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Antoine de Saint-Exupery");
		cv.put(TOPIC, "War");
		cv.put(CITATION, "War is not an adventure. It is a disease. It is like typhus.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Liv Tyler");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I love my dad, although I'm definitely critical of him sometimes, like when his pants are too tight. But I love him so much and I try to be really supportive of him.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harrison Ford");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I love the comic opportunities that come up in the context of a father-son relationship.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Joel Osteen");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I made a decision when my father passed away that I was going to be who God made me to be and not try to preach like my father.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Adam Sandler");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I never had a speech from my father 'this is what you must do or shouldn't do' but I just learned to be led by example. My father wasn't perfect.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Chief Joseph");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I pressed my father's hand and told him I would protect his grave with my life. My father smiled and passed away to the spirit land.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Anais Nin");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I stopped loving my father a long time ago. What remained was the slavery to a pattern.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Harrison Ford");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I wanted to be a forest ranger or a coal man. At a very early age, I knew I didn't want to do what my dad did, which was work in an office.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Jay London");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I wanted to take up music, so my father bought me a blunt instrument. He told me to knock myself out.");
		db.insert("citations", AUTHOR, cv);

		cv.put(AUTHOR, "Tatum O'Neal");
		cv.put(TOPIC, "Dad");
		cv.put(CITATION, "I was punished for blowing the whistle on my father's lifestyle.");
		db.insert("citations", AUTHOR, cv);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		android.util.Log.w("Constants", "Upgrading database, which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS constants");
		onCreate(db);
	}
}