package cz.mendelu.pur.xkosteln;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class CitationActivity extends Activity {
	private Bundle citationValuesBundle;
	private TextView author; 
	private TextView citation;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		citationValuesBundle = this.getIntent().getExtras();
		this.setContentView(R.layout.citation);
		this.setGuiObjects();
		this.setCitationValues();
	}
	private void setGuiObjects() {
		this.author = (TextView) this.findViewById(R.id.authorTxt);
		this.citation = (TextView) this.findViewById(R.id.citationTxt);
	}
	
	private void setCitationValues(){
		String authorText = citationValuesBundle.getString("author") + " On " + 
						citationValuesBundle.getString("topic");
		author.setText(authorText);
		citation.setText(citationValuesBundle.getString("citation"));
		
	}

	
	
}
