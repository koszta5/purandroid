package cz.mendelu.pur.xkosteln;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class CathegoriesActitvity extends ListActivity {
	private String[] cathegories;
	private ArrayAdapter<String> adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cathegories = this.getIntent().getExtras().getStringArray("cathegories");
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, cathegories);
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		setListAdapter(adapter);
		selectGivenOption(this.getIntent().getExtras().getString("whereConstraintValue"));
	}
	
	public void selectGivenOption(String value) {
		if (value.isEmpty()) {
			value = "All";
		}
		List<String> catArrayList = Arrays.asList(cathegories);
		getListView().setItemChecked(catArrayList.indexOf(value), true);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Intent intent = getIntent();
		if (intent == null){
			intent = new Intent();
		}
		
		intent.putExtra("whereConstraintValue", cathegories[position]);
		intent.putExtra("whereConstraint", "topic");
		setResult(RESULT_OK, intent);
		finish();
	}
	
	
	
	
}
